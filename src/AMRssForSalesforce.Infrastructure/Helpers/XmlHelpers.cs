﻿using System.Xml.Linq;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public static class XmlHelpers
    {
        public static string TryGetElementValue(this XElement parentEl, string elementName, string defaultValue = null)
        {
            var foundEl = parentEl.Element(elementName);
            return foundEl != null ? foundEl.Value : defaultValue;
        }
    }
}
