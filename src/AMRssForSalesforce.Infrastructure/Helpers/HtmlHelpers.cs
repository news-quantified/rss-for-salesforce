﻿using System;
using System.Text;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Threading;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public static class HtmlHelpers
    {
        public static string GetNodeText(HtmlNode source)
        {
            return source.HasChildNodes ? source.ChildNodes[0].InnerText : source.InnerText;
        }


        public static string GetAllNodeText(HtmlNode source,  bool appendAllElement = false)
        {
            var result = new StringBuilder();
            try
            {
                
                if (source.HasChildNodes)
                {
                    foreach (var item in source.ChildNodes)
                    {
                        result.Append(item.InnerText);
                        if (!(String.IsNullOrEmpty(StringHelpers.CleanAndFormatText(item.InnerText))) && !appendAllElement) return result.ToString();
                    }
                 } else result.Append(source.InnerText); 
            }
            catch
            {
                return null;
            }
            return result.ToString();



        }

        public static ResponseResult GetResponse(string p)
        {
            return GetResponse(p,
                userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
        }

        public static ResponseResult GetResponse(string url, string userAgent)
        {
            ResponseResult res = new ResponseResult();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //if (p.Contains("www.accesswire.com")) p = p.Replace("http://", "http://proxy9747.my-addr.org/myaddrproxy.php/https/");

                var request = (HttpWebRequest)WebRequest.Create(url);

                request.Credentials = CredentialCache.DefaultCredentials;

                request.UserAgent = userAgent;
                // Get the response.
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                res.Redirect = response.ResponseUri.AbsoluteUri;

                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                res.Html = reader.ReadToEnd();
                // Display the content.

                // Clean up the streams and the response.
                reader.Close();
                response.Close();
            }
            catch (Exception Exception)
            {
                Logger.Logger.WriteInformation(Exception);
            }

            return res;
        }
    }
}
