﻿namespace AMRssForSalesforce.Infrastructure.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Constants;
    public static class ConstValue
    {
        public static List<String> TitlesList = Dictionaries.Titles;
        public static List<String> NoNameList = Dictionaries.MarcerNoName;
        
        
        static ConstValue()
        {

            TitlesList.Add("Marketing Manager");
            TitlesList.Add("Senior Vice President, Marketing & Business Development");
            TitlesList.Add("Senior Fellow");
            TitlesList.Add("Media Relations Specialist");
            TitlesList.Add("Head of Corporate Communications");
            TitlesList.Add("Director, Communications");
            TitlesList.Add("Investor Relations & Financial Media");
            TitlesList.Add("Senior director");
            TitlesList.Add("Global Marketing Director");
            TitlesList.Add("Chief Financial Officer");
            TitlesList.Add("Chief Executive Officer");
            TitlesList.Add("FischTank Marketing and PR");
            TitlesList.Add("Executive Chairman");
            TitlesList.Add("Financial Analyst");
            TitlesList.Add("Senior Communications Advisor");
            TitlesList.Add("President and CEO");
            TitlesList.Add("Vice President, Technical Services");
            TitlesList.Add("Technical Services");
            TitlesList.Add("Head of Corporate Communications");
            TitlesList.Add("Vice President");
            TitlesList.Add("Investor Relations Counsel");
            TitlesList.Add("Senior Advisor");
            TitlesList.Add(" CEO");
            TitlesList.Add(" PR");
            TitlesList.Add("Head of Business and Finance Communications");
            TitlesList.Add("President and Chief Executive Officer");
            TitlesList.Add(" MBA");
            TitlesList.Add("President");
            TitlesList.Add("Business Development Manager");
            TitlesList.Add("Manager");
            TitlesList = TitlesList.OrderByDescending(x => x.Length).ToList();


            Types.AddRange(Dictionaries.Types);
            TypesList = TypesList.OrderByDescending(x => x.Length).ToList();


            NoNameList.Add("Instacart");
            NoNameList.Add("Questions and requests for assistance concerning the Note Exchange Transaction");
            NoNameList.Add("Europe");
            NoNameList.Add("Maritimes Region");
            NoNameList.Add("Communications Branch");
            NoNameList.Add("Canada");
            NoNameList.Add("USA");
            NoNameList.Add("UK/EU");
            NoNameList.Add("Australia");
            NoNameList.Add("Corporate)");
            NoNameList.Add("Vancouver");
            NoNameList.Add("BC Canada V6C 2V6");
            NoNameList.Add("Canada");
            NoNameList.Add("or email");
            NoNameList.Add("For more information");
            NoNameList.Add("info");
            NoNameList.Add("For more information,");
            NoNameList.Add("US  INFO");
            NoNameList.Add("s");
            NoNameList.Add("For information,");
            
            NoNameList.Add("Australia");
           
            NoNameList = NoNameList.OrderByDescending(x => x.Length).ToList();




            RemoveWords = RemoveWords.OrderByDescending(x => x.Length).ToList();
            RemoveNode = RemoveNode.OrderByDescending(x => x.Length).ToList();
            PointerForNameInNextNode = PointerForNameInNextNode.OrderByDescending(x => x.Length).ToList();

            
        }

        public static List<String> TypesList = Dictionaries.Types;




        public static List<String> People = new List<string> {  
                "Esq. ", 
                "MS ", 
                "madam ", 
                "mayor ", 
                "Mr.",
                "Miss ",
                "Sir ",
                "Mrs ",
                "Dr.",
                "Lady ",
                "Lord ",
                "Mx ",
                "Prof ",
                "Ms."
        };


        public static List<String> SeparatorList = new List<string> {  
                " Or ", 
                "/", 
                
                                                               };


        public static List<String> RemoveWords = new List<string>
        {
            "&amp;",
            "SOURCE",
            "SOURCE:",
            "About",
            ":",
            ", Toronto",
            ", Montreal",
            "Contact",
            "Contact:",
            "Email",
            "Email:",
        };

        public static List<String> RemoveNode= new List<string> {  
                "For further information, please contact:",
                "For further information please contact:",
                "For media enquiries",
                "For investor relations",
                "For additional information, please contact:",
                "For additional information, please contact",
                "For further information on",
                "For additional information please contact",
                "please visit the auction website at",
                "please visit website at",
                "please visit website",
                "please contact",
                "contact us by e-mail",
                "Tel\\.",
                "-Canada",
                "- Canada",
                "USA",
                "e\\.",
                "t\\.",
                "For Company Contact:",
                "Contact Information",
                //"CONTACT:",
                "Email:",
                "Email contact",
                "Email contact:",
                "Contact Goodyear UK for more information",
                "For further information",
                "For further information:",
                "For media inquiries",
                "For media inquiries:",
                "For investor inquiries",
                "For investor inquiries:",
                "About",
                                                               };

        public static List<String> ShortAdressAbrList = new List<string>
        {
            " AL ",
            " AK ",
            " AS ",
            " AZ ",
            " AR ",
            " CA ",
            " CO ",
            " CT ",
            " DE ",
            " DC ",
            " FL ",
            " GA ",
            " GU ",
            " HI ",
            " ID ",
            " IL ",
            " IN ",
            " IA ",
            " KS ",
            " KY ",
            " LA ",
            " ME ",
            " MD ",
            " MH ",
            " MA ",
            " MI ",
            " FM ",
            " MN ",
            " MS ",
            " MO ",
            " MT ",
            " NE ",
            " NV ",
            " NH ",
            " NJ ",
            " NM ",
            " NY ",
            " NC ",
            " ND ",
            " MP ",
            " OH ",
            " OK ",
            " PW ",
            " PA ",
            " PR ",
            " RI ",
            " SC ",
            " SD ",
            " TN ",
            " TX ",
            " UT ",
            " VT ",
            " VA ",
            " VI ",
            " WA ",
            " WV ",
            " WI ",
            " WY "
        };


        public static List<String>  ShortAdressList = new List<string> {  
               " Ct.",
               " Court",
               " Ln.",
               " Lane",
               " St.",
               " Street",
               " Rd.",
               " Road",
               " Terr.",
               " Terrace",
               " Cir.",
               " Circle",
               " Hwy.",
               " Highway",
               " Pkwy.",
               " Parkway",
               " Ave.",
               " Avenue",
               " Way",
               " Rdg.",
               " Ridge",
               " Blvd.",
               " Boulevard",
               " Dr.",
               " Drive",
               " Box ",
               " suite",
               " FL.",
               " fl."

                
                                                               };

        public static List<String> PointerForNameInNextNode = new List<string> {  
                "contact:",
                "Contact:",
                "Contacs:",
                "please contact:",
                "Contact Information",
                "For further information on",
                "For further information ",
                "contact us by e-mail",
                "contact us by phone",
                "Contacts Information",
                "please visit the auction website at",
                "For further information, please contact:",
                "For further information please contact:",
                "For additional information, please contact:",
                "For additional information, please contact",
                "For further information on",
                "For additional information please contact",
                "For further information",
                "For further information:",
                "For media inquiries",
                "For media inquiries:",
                "For investor inquiries",
                "For investor inquiries:",
                "For Company Contact:",
                "For further information, please contact:",
                "For further information please contact:",
                "For Company Contact:",
                
                                                               };


        public static List<String> Types = new List<string> {  
                "Analysts & investors",  
                "Agency",
                "Media", 
                "PR",
                "Rosemont Media",
                "Agency Contact",
                "President and CEO",
                "CBL contact",
                "Associate Vice President",
                "Company",
                "Corporate Headquarters",
                "Corporate Communications and Media",
                "Corporate Communications",
                "Institutional Marketing Services (IMS)",
                "IMS",
                "Press Contact",
                "Press Contacts",
                "Media Contact",
                "Rosemont Media",
                "Media Contacts",
                "For Media",
                "Media Liaison",
                "Marketing and PR",
                "Public Relations",
                "Corporate Development",
                "President & CEO",
                "CEO",
                "President",
                "Media Relations",
                "Press inquiries",
                "Media inquiries",
                "PR Manager",
                "PR Contact", 
                "Investors & Financial Media",
                "Press & Media Relations",
                "Press Office",
                "Media Relations",
                "Investor Relations",
                "Analysts",
                "Shareholder Services",
                "Press Relations",
                "Investor and Financial Inquiries",
                "Media and Trade Inquiries",
                "Corporate Communications",
                "Communications Branch",
                "Public",
                "BD & Communication Department",
                "Customer Services",
                "Company Contact", 
                "Customer Services",
                "Contact", 
                "Investors",
                "Investors/Analysts",
                "Investor Relations Agency",
                "Investor Relations Contact",
                "Media Relations Contact",
                "Investor Contact", 
                "Investor Relations",
                "Investor",
                "Investor inquiries",
                "Media representatives",
                "Chief Financial Officer",
                "Investor Relations",
                "Wealth Management",
                "Chief Financial Officer",
                "Chairman of the Board of Directors",
                "Chief Executive Officer",
                "Manager, Investor Relations",
                "Chairman & Chief Executive Officer",
                "Shareholders",
                "Financial Advisors",
                "Shareholders/Financial Advisors",
         };

        public static List<String> Company = new List<string>
        {   
            " LP",
            " PLC",
            " Ltd",
            " Group",
            " LDC",
            " IBC",
            " SA",
            " S.A.",
            " A.G.",
            " SP",
            " GP",
            " L.C.",
            " L.C",
            " Joint Venture",
            " Communications",
            " Partners",
            " Group",
            "LEWIS PR",
            " SARL",
            " BV",
            " L.P",
            " LP",
            " LLC",
            "RNS",
            "Instacart",
            " LLLP",
            " Corporation",
            " Corporate",
            "CBX Software",
            " Software",
            " Co.",
            "10Fold",
            "Famous Smoke Shop",
            "FisherVista",
            "D.F. King & Co",
            " Corp.",
            " Corp",
            "IIROC Inquiries",
            "Brite Semiconductor",
            "IIROC",
            "Peregrine Exploration",
            "Evinrude engines",
            " Inc.",
            " PC ",
            " P.C",
            " N.A.",
            " N.A",
            " NT&SA",
            "CDPQ",
            " DBA",
            " & Со",
            " LLP",
            " NCUA",
            " Federal Credit Union",
            " Federal Savings Bank"
        };
    }
}
