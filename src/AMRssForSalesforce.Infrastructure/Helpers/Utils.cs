﻿using AMRssForSalesforce.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AMRssForSalesforce.Model;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public class Utils
    {
        private static Dictionary<string, string> TimeZoneOffsets = new Dictionary<string, string>() {
            {"ACDT", "+10:30"},
            {"ACST", "+09:30"},
            {"ADT", "-03:00"},
            {"AEDT", "+11:00"},
            {"AEST", "+10:00"},
            {"AHDT", "-09:00"},
            {"AHST", "-10:00"},
            {"AST", "-04:00"},
            {"AT", "-02:00"},
            {"AWDT", "+09:00"},
            {"AWST", "+08:00"},
            {"BAT", "+03:00"},
            {"BDST", "+02:00"},
            {"BET", "-11:00"},
            {"BST", "-03:00"},
            {"BT", "+03:00"},
            {"BZT2", "-03:00"},
            {"CADT", "+10:30"},
            {"CAST", "+09:30"},
            {"CAT", "-10:00"},
            {"CCT", "+08:00"},
            {"CDT", "-05:00"},
            {"CED", "+02:00"},
            {"CET", "+01:00"},
            {"CEST", "+02:00"},
            {"CST", "-06:00"},
            {"CT", "-06:00"},
            {"EAST", "+10:00"},
            {"EDT", "-04:00"},
            {"EED", "+03:00"},
            {"EET", "+02:00"},
            {"EEST", "+03:00"},
            {"EST", "-05:00"},
            {"ET", "-05:00"},
            {"FST", "+02:00"},
            {"FWT", "+01:00"},
            {"GMT", "+00:00"},
            {"GST", "+10:00"},
            {"HDT", "-09:00"},
            {"HST", "-10:00"},
            {"IDLE", "+12:00"},
            {"IDLW", "-12:00"},
            {"IST", "+05:30"},
            {"IT", "+03:30"},
            {"JST", "+09:00"},
            {"JT", "+07:00"},
            {"MDT", "-06:00"},
            {"MED", "+02:00"},
            {"MET", "+01:00"},
            {"MEST", "+02:00"},
            {"MEWT", "+01:00"},
            {"MST", "-07:00"},
            {"MT", "+08:00"},
            {"NDT", "-02:30"},
            {"NFT", "-03:30"},
            {"NT", "-11:00"},
            {"NST", "+06:30"},
            {"NZ", "+11:00"},
            {"NZST", "+12:00"},
            {"NZDT", "+13:00"},
            {"NZT", "+12:00"},
            {"PDT", "-07:00"},
            {"PST", "-08:00"},
            {"ROK", "+09:00"},
            {"SAD", "+10:00"},
            {"SAST", "+09:00"},
            {"SAT", "+09:00"},
            {"SDT", "+10:00"},
            {"SST", "+02:00"},
            {"SWT", "+01:00"},
            {"USZ3", "+04:00"},
            {"USZ4", "+05:00"},
            {"USZ5", "+06:00"},
            {"USZ6", "+07:00"},
            {"UT", "-00:00"},
            {"UTC", "-00:00"},
            {"UZ10", "+11:00"},
            {"WAT", "-01:00"},
            {"WET", "-00:00"},
            {"WST", "+08:00"},
            {"WT", "+08:00"},
            {"YDT", "-08:00"},
            {"YST", "-09:00"},
            {"ZP4", "+04:00"},
            {"ZP5", "+05:00"},
            {"ZP6", "+06:00"}
        };

        public static string FindType(string p)
        {
            string ret = null;
            if (string.IsNullOrEmpty(p))
                return string.Empty;

            if (p.Contains("Alex Press"))
                return null;

            if(p == "IRI")
                return string.Empty;

            var words3 = p.Split(new string[] { ", ", " or ", " / ", ";", ":", "|", " for ", "/", " at ", " " }, StringSplitOptions.RemoveEmptyEntries);
            if (string.IsNullOrEmpty(ret))
            {
                var toLover = p.ToLower().Replace("contact", "").Replace(":", "").Trim();
                
                foreach (var w in words3)
                {
                    foreach (var t in Dictionaries.AgencyType)
                    {
                        if (w.ToLower().Trim() == t.ToLower())
                        {
                            ret = t;
                            break;
                        }
                    }
                }
                if (string.IsNullOrEmpty(ret))
                {
                    foreach (var t in Dictionaries.AgencyType)
                    {
                        if (t.Length >= 5 && p.Contains(t))
                        {
                            ret = t;
                            break;
                        }
                    }
                }
            }

            //if (string.IsNullOrEmpty(ret))
            //{
            //    if (p.ToLower().Contains("for ") && !p.ToLower().Contains(" information"))
            //    {
            //        foreach (var t in Dictionaries.Types)
            //        {
            //            if (p.ToLower().Replace("for ", "").Trim() == t.ToLower())
            //                ret = t;
            //        }
            //        ret = "Agency";
            //    }
            //}
            if (string.IsNullOrEmpty(ret))
            {
                foreach (var t in Dictionaries.Types)
                {
                    if (p.ToLower().Replace("contact", "").Replace(":", "").Trim() == t.ToLower())
                    {
                        ret = t;
                        break;
                    }
                   
                }

                foreach (var w in words3)
                {
                    foreach (var t in Dictionaries.Types)
                    {
                        if (w.ToLower().Trim() == t.ToLower())
                        {
                            ret = t;
                            break;
                        }
                    }
                }
                if (string.IsNullOrEmpty(ret))
                {
                    foreach (var t in Dictionaries.Types)
                    {
                        if (t.Length >= 5 && p.Contains(t))
                        {
                            ret = t;
                            break;
                        }
                    }
                }

            }
            //else
            //{
            //    foreach (var t in Dictionaries.Types)
            //    {
            //        if (p.Contains(t))
            //        {
            //            if(t.Length > 2)
            //              return t;
            //        }
            //    }
            //    if(ret.Length > 2)
            //      return ret;
            //}
            if (string.IsNullOrEmpty(ret))
            {
                foreach (var t in Dictionaries.Types)
                {
                    if (p.ToLower().Replace("contact", "").Replace(":", "").Trim().Contains(t.ToLower()))
                    {
                        ret = t;
                        break;
                    }
                    else if (p.ToLower().Replace(" and ", "&").Trim().Contains(t.ToLower()))
                    {
                        ret = t;
                        break;
                    }
                }
            }
            if (string.IsNullOrEmpty(ret))
            {
                
                foreach (var w in words3)
                {
                    foreach (var t in Dictionaries.Types)
                    {
                        if (w.ToLower().Trim() == t.ToLower())
                        {
                            ret = t;
                            break;
                        }
                    }
                }
            }

            if (ret == null)
                return ret;

            if (ret.ToLower() == p.ToLower().ToLower().Replace("contact", "").Replace(":", "").Trim())
                return ret;

            if (p.ToLower().IndexOf(ret.ToLower()) > 0)
            {
                var indexOff = p.ToLower().IndexOf(ret.ToLower());
                if( indexOff>0 &&( p[indexOff-1] >='a' && p[indexOff-1] <='z' || p[indexOff-1] >='A' && p[indexOff-1] <='Z'))
                  return null;
            }

            var removed = p.ToLower().Replace(ret.ToLower(), "");

            if (removed[0] >= 'a' && removed[0] <= 'z' && p.ToLower().IndexOf(ret.ToLower()) == 0 )
                return null;
          
            return ret;
        }

        public static bool IsAgency(string type)
        {
            if (!string.IsNullOrEmpty(type))
            {
                foreach (var t in Dictionaries.AgencyType)
                {
                   if (type.ToLower() == t.ToLower())
                            return true;
                }
                
            }

            return false;
        }

        public static bool IsAgency(string companyName1, string companyName2)
        {
            if (string.IsNullOrEmpty(companyName1) || string.IsNullOrEmpty(companyName2))
                return false;
            return !companyName1.Contains(companyName2) && companyName2.Contains(companyName1);

        }

        public static bool IsAddress(string p)
        {
            var words = p.Split(' ');

            foreach (var w in words)
            {
                foreach (var a in Dictionaries.MarcerAdress)
                {
                    if (w == a)
                        return true;
                }
            }
            return false;
        }

        public static bool IsAgency(string type, string companyName1, string companyName2)
        {
            if (IsAgency(type))
                return true;

            if (string.IsNullOrEmpty(companyName1) || string.IsNullOrEmpty(companyName2))
                return false;

            var wordsCompany1 = companyName1.Split(' ');
            var wordsCompany2 = companyName2.Split(' ');
            if (companyName1[0] == companyName2[0])
                return false;
            return !companyName1.Contains(companyName2) && !companyName2.Contains(companyName1);

        }

        public static string FindTypeH(string p)
        {
            var words3 = p.Split(';');
            
            foreach (var w in words3)
            {
                foreach (var t in Dictionaries.Types)
                {
                    if (w.ToLower().Trim().Contains(t.ToLower()))
                        return t;
                }
            }
            return null;
        }

        public static string FindTypeH2(string p)
        {
            var words3 = p.Split(';');
            string type = string.Empty;
            foreach (var w in words3)
            {
                foreach (var t in Dictionaries.Types)
                {
                    if (w.ToLower().Trim().Contains(t.ToLower()))
                    {
                        type = t;
                        break;
                    }
                }
            }
            return type;
        }

        public static bool IsStartContactData(string innerText)
        {
            var text = NormalizationString(innerText).ToLower();

            foreach (var marcer in Dictionaries.MarkerStartContact)
            {
                if (text == marcer)
                    return true;
            }

            foreach (var marcer in Dictionaries.Types)
            {
                if (text == marcer.ToLower())
                    return true;
            }
            return false;
        }

        public static bool IsCompanyName(string val, string source)
        {        
            if (val.Length > 70)
                return false;

            if (val == "Chinese and English")
                return false;

            if (val == "Magazine")
                return false;

            if (val == "Investors")
                return false;

            if (val.Contains("Report data field structure is as follows"))
                return false;

            if (string.IsNullOrEmpty(val.Trim()))
                return false;

            if (val.Trim().Length < 3)
                return false;

            if (val.ToLower().Trim().Contains("tél. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("tel. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("for information"))
                return false;

            if (val.ToLower().Trim().Contains("for more information"))
                return false;

            if (val.ToLower().Trim().Contains("for additional information"))
                return false;

            if (val.ToLower().Trim().Contains("in addition to"))
                return false;
            if (val.ToLower().Trim() == "general information")
                return false;

            if (val.ToLower().Trim() == "contact")
                return false;

            if (val.ToLower().Trim() == "inquiries")
                return false;

            if (val.ToLower().Trim() == "invaluable")
                return false;

            if (val.ToLower().Trim() == "сompany")
                return false;

            if (val.ToLower().Trim() == "contact:")
                return false;

            if (val.ToLower().Trim() == "primary analyst")
                return false;

            if (val.ToLower().Trim() == "secondary analyst")
                return false;

            if (val.Trim() == "CHF Investor Relations")
                return false;

            if (val.ToLower().Trim() == "sector")
                return false;

            if (val.ToLower().Trim().IndexOf("at ") == 0)
                return false;

            if (val.ToLower().Trim().IndexOf("for ") == 0)
                return false;

            if (val.ToLower().Trim().Contains(" please "))
                return false;

            if (val.Contains(">") || val.Contains("<"))
                return false;

            if (val.Contains("(on-site)") )
                return false;
            //if (val.ToLower().Contains("for "))
            //    return false;

            if (!string.IsNullOrEmpty(source) &&( val.Contains(source) || source.Contains(val)))
                return true;

            if (val.Contains('®'))
                return true;

            var words = val.Split(' ');
            var i = 0;
            foreach (var w in words)
            {
                foreach (var t in Dictionaries.MarcerCompany)
                {
                    if (w.ToLower() == t.ToLower())
                    {
                        if ((w.ToLower() == "and" || w.ToLower() == "&") && (i == words.Count() - 1  || val[0]<'A' || val[0] >'Z'))
                            return false;
                       
                        return true;
                    }
                }
                i++;
            }

            if (val.Contains(":"))
                return false;

            foreach (var w in words)
            {
                foreach (var t in Dictionaries.MarcerNoCompany)
                {
                    if (w.ToLower() == t.ToLower())
                        return false;
                }
            }

            foreach (var t in Dictionaries.MarcerNoCompany)
            {
                if (val.ToLower().Contains(t.ToLower()))
                    return false;
            }
            if (val.ToLower() == val)
                return false;

            if (RegExp.HasNumber(val))
                return false;

            var phone = Regex.Replace(val, "[^0-9,]", "").Trim();
            if (phone.Length > 2)
                return false;

            if (val.Contains("of "))
                return false;

            var names = val.Split(',');
            foreach (var name in names)
            {
                if (RegExp.IsName(name) || RegExp.IsNumber(name) && RegExp.IsEmail(name) || Utils.HasTitle(name) || !string.IsNullOrEmpty(Utils.FindType(name)))
                    return false;
            }
            return true;
        }

        public static string NormalizationString3(string st)
        {
            if (string.IsNullOrEmpty(st))
                return st;

            st = st.Replace("&amp;", "&");

            st = st.Replace("&amp", "&");

            st = st.Replace("&nbsp;", " ");
            st = st.Replace("&#160;", " ");

            st = st.Replace("&#8211;", "–");

            st = st.Replace("&mdash;", " - ");
            return st;
        }

        public static string NormalizationString(string st)
        {
            if (string.IsNullOrEmpty(st))
                return st;

            st = System.Net.WebUtility.HtmlDecode(st);

            st = st.Replace("\n\t", "; ");

            st = st.Replace("&amp;", "&");

            st = st.Replace("&amp", "&");

            st = st.Replace("\""," ");

            st = st.Replace("&nbsp;"," ");
            st = st.Replace("&#160;", " ");
            st = st.Trim();
                     
            st = st.Replace("&#233;", "é");

            st = st.Replace("&#246;", "ö");

            st = st.Replace("&#248;", "ø");

            st = st.Replace("&#8226;", "|");

            st = st.Replace("(on-site)", "");

            st = st.Replace(" [at] ", "@").Replace(" (at) ", "@");

            st = st.Replace(" (dot) ", ".");

            st = st.Replace("[at]", "@").Replace("(at)", "@");


            st = st.Replace("(dot)", ".");

            st = st.Replace("<b>", " ").Replace("</b>", " ").Replace("&#160;", "").Replace("\r\n", " ").Replace("\n", " ").Replace("&amp;", "&").Replace("\t"," ");
            st = System.Text.RegularExpressions.Regex.Replace(st, " +", " ");

            while(st.Contains("&#"))
            {
                var indexOF1 = st.IndexOf("&#");
                
                var indexOF2 = st.IndexOf(";", indexOF1);
                if (indexOF2 >= 0)
                    st = st.Remove(indexOF1, indexOF2 - indexOF1 + 1);
                else if (indexOF1 + 5 < st.Length - 1)
                    st = st.Remove(indexOF1, 5);
                else
                    break;
            }

            st = st.Trim();

            if (!string.IsNullOrEmpty(st) && st.IndexOf(":") == st.Length - 1)
                st = st.Remove(st.Length - 1, 1).Trim();

            if (string.IsNullOrEmpty(st))
                return st;

            //remove no a leter in the begin an the end
            if (st[0] == ',' || st[0] == ':' || st[0] == '.' || st[0] == '-' || st[0] == '–')
            {
                st = st.Remove(0, 1);
            }

            if (string.IsNullOrEmpty(st))
                return st;

            st = st.Trim();

            if (st[st.Length - 1] == ',' || st[st.Length - 1] == ':' || st[st.Length - 1] == '|' || st[st.Length - 1] == '-' || st[st.Length - 1] == '–')
            {
                st = st.Remove(st.Length - 1, 1);
            }

            st = st.Replace("()", "");

            
            if (string.IsNullOrEmpty(st))
                return st;
            if(st.IndexOf(" for") > 0 && st.IndexOf(" for") == st.Length - " for".Length)
            {
                st = st.Remove(st.IndexOf(" for"));
            }
            return st.Trim();
        }
        public static string NormalizationName(string st)
        {
            st = st.Replace("*", "").Trim();

            foreach (var s in Dictionaries.removeFromName)
            {
                if (st.IndexOf(s) == st.Length - s.Length)
                    st = st.Remove(st.IndexOf(s));
            }

            if (st[0] == ',' || st[0] == ':' || st[0] == '.' || st[0] == '-' || st[0] == '–' || st[0] == '|')
            {
                st = st.Remove(0, 1);
            }
            return st.Trim();
        }
        public static string NormalizationString2(string st)
        {
            st = st.Trim();
            st = st.Replace("\r\n", ";").Replace("\n", ";").Replace("&amp;", "&").Replace("  ",";");
            return st;
        }

        public static string NormalizationType(string p, string source)
        {
          
           return p.Replace(":", "");
        }

        public static string ResolveTimeZoneAbbreviations(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                foreach (var tz in TimeZoneOffsets)
                {
                    value = value.Replace(tz.Key, tz.Value);
                }
            }

            return value;
        }



        public static string CutCpmpanyName(string val, string sourse)
        {        
            try
            {
            if (string.IsNullOrEmpty(val) || string.IsNullOrEmpty(sourse))
                return val;

            if (val.ToLower().Contains("for "))
                return val;


            var companyStrings = sourse.Split(' ');

                foreach (var s in companyStrings)
                {
                    var word = NormalizationStringTrim(s);
                    if (!string.IsNullOrEmpty(word))
                    {
                        if (val.IndexOf(word) == 0)
                        {
                            val = val.Replace(word, "").Trim();

                            if (!string.IsNullOrEmpty(val) && !Char.IsLetter(val[0]))
                                val = val.Remove(0, 1).Trim();
                        }
                        else
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return val;
        }

        private static string NormalizationStringTrim(string s)
        {
            s = s.Trim();
            if (!string.IsNullOrEmpty(s) &&!Char.IsLetter(s[0]))
                s = s.Remove(0, 1);

            if (!string.IsNullOrEmpty(s) && !Char.IsLetter(s[s.Length-1]))
                s = s.Remove(s.Length - 1, 1);
            return s.Trim();
        }



        public static string NormalizationStringTrim2(string s)
        {
            s = s.Trim();
            if (!string.IsNullOrEmpty(s) && !Char.IsLetterOrDigit(s[0]))
                s = s.Remove(0, 1);

            if (!string.IsNullOrEmpty(s) && !Char.IsLetterOrDigit(s[s.Length - 1]))
                s = s.Remove(s.Length - 1, 1);
            return s.Trim();
        }

        public static bool HasTitle(string p)
        {
            foreach (var t in Dictionaries.Titles)
            {
                if (p.ToLower().Contains(t.ToLower()))
                    return true;

            }
            return false;
        }

        public static string CutString(string p, string title)
        {
            p = NormalizationStringTrim(p.Replace(title, ""));
            if (!string.IsNullOrEmpty(p) &&!Char.IsLetterOrDigit(p[0]) && p[0] != '+' && p[0] != '(')
                p = p.Remove(0, 1);
            return p;
        }

        public static string GetTitle(string p)
        {
            foreach (var t in Dictionaries.Titles)
            {
                if (p.ToLower()== t.ToLower())
                    return t;
            }

            foreach(var t in Dictionaries.Titles)
            {
                if (p.ToLower().Contains(t.ToLower()))
                {
                    if(t.Length > 3)
                     return t;
                    else
                    {
                        var words = p.Split(' ');
                        foreach(var w in words)
                        {
                            if (w.ToLower() == t.ToLower())
                                return t;
                        }
                    }
                }
            }
            return null;
        }

        public static bool StringContensName(string s)
        {
            foreach (var t in Dictionaries.MarcerName)
            {
                if (s.Contains(t))
                    return true;
            }
            return false;
        }

        public static string SummContactData(List<Contact> savedContact)
        {
            StringBuilder contactInfo = new StringBuilder();

            foreach(var c in savedContact)
            {
                if (contactInfo.Length > 0)
                    contactInfo.Append("; ");

                if (!string.IsNullOrEmpty(c.Type))
                    contactInfo.Append(c.Type + ": ");

                if (!string.IsNullOrEmpty(c.Name))
                    contactInfo.Append(c.Name + " ");

                if (!string.IsNullOrEmpty(c.Title))
                    contactInfo.Append(c.Title + " ");

                if (!string.IsNullOrEmpty(c.Phone))
                    contactInfo.Append(c.Phone + " ");

                if (!string.IsNullOrEmpty(c.Email))
                    contactInfo.Append(c.Email + " ");

            }
            return contactInfo.ToString();
        }

        public static string NormPhone(string s)
        {
            var phones = RegExp.GetPnoneNumbers(s);

            if (!phones.Any())
            {
                if (s.Length > 100)
                    return null;
                if (ValidationHelpers.ContainsPhone(s))
                    return s.Trim();
                return null;
            }
            if (phones.Count() == 1 && (phones[0] == "2016-2026" || phones[0] == "2016-2021"))
                return null;
            int indexOffNumber = s.IndexOf(phones[0]);
            if (indexOffNumber > 0)
            {
                var indexOffPlus = s.IndexOf("+");
                if (indexOffPlus >= 0 && indexOffPlus < indexOffNumber)
                    indexOffNumber = indexOffPlus; 
                s = s.Remove(0, indexOffNumber);
            }

           int indexOffLastNumber =  s.IndexOf(phones[phones.Count() - 1]);
           if (s.Length - (indexOffLastNumber + phones[phones.Count() - 1].Length) > 15)
               s = s.Remove(indexOffLastNumber + phones[phones.Count() - 1].Length);
           if (string.IsNullOrEmpty(s))
               return s;

            s = s.Replace("</span>", "").Replace("<span>", "").Replace("or",",").Replace(" / ",", ").Replace("|",",").Replace("<p>","").Replace("</p>","").Trim();
            if (string.IsNullOrEmpty(s))
                return s;
            s = s.Trim();
            if (s[s.Length - 1] == ')' && !s.Contains('(') || s[s.Length - 1] == '(')
               s = s.Remove(s.Length - 1);

           s = s.Trim();
           if (s[s.Length - 1] == ',' || s[s.Length - 1] == '-' || s[s.Length - 1] == '.'  || s[s.Length - 1] == '–')
               s = s.Remove(s.Length - 1);

           if (s[0] == ',' || s[0] == ')' || s[0] == '.' || s[0] == '-' || s[0] == '–')
               s = s.Remove(0, 1);
           

           if (s.Length > 50)
               return string.Join(", ", phones);
           s = s.Replace("Toll Free Tel:", "").Replace("Phone:", "").Replace("Telephone:", "").Replace("Tel:.", "").Replace("Tel:", "").Replace("Tel", "").Replace("PH:", "").Replace("F:", "").Replace("PH.", "").Replace("Ph.", "").Replace("Email", "").Replace("P:", "").Replace("T:", "").Replace("T.", "").Replace("mobile", "").Replace("E-mail", "").Replace("E-Mail", "").Replace("Mail", "").Replace("Fax","");
           return s.Replace("()", "").Trim();
        }

        public static string CutCpmpanyName(string p)
        {
            var strCompanyName = NormalizationString(p);
            foreach (var n in Dictionaries.NotName)
            {
                int indexFor = strCompanyName.ToLower().IndexOf(n);
                if (indexFor >= 0)
                    strCompanyName=(strCompanyName.Remove(indexFor, n.Length).Trim());
            }
            return strCompanyName;
        }

        public static bool trueEmail(string email, string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;
            var indexAm = email.IndexOf("@");
            email = email.Remove(indexAm);

            string[] e = email.Split(new string[] { ".", "-", "_" }, StringSplitOptions.RemoveEmptyEntries);
            string[] n = name.Split(' ');

            foreach (var pe in e)
            {
                foreach (var pn in n)
                {
                    if (pn.ToLower().Contains(pe.ToLower()) || pe.ToLower().Contains(pn.ToLower()))
                        return true;
                }
            }


            return false;
        }

        public static string NormName(string s)
        {
            if (s == null)
                return s;
            var parts = s.Split(Dictionaries.Separate, StringSplitOptions.RemoveEmptyEntries);

            if (!parts.Any())
                return s;

            foreach(var p in parts)
            {
                if (!string.IsNullOrEmpty(p.Trim()))
                    return p.Trim();
            }

            return s;
        }



        public static string CutType(string text, string type)
        {

            try
            {
                var text1 = text.Replace(" " + type, "").Replace(" Contacts", "").Replace(" Contact", "").Replace(" contact", "").Trim();

                if (text1.IndexOf(":") == text1.Length - 1)
                    text1 = text1.Remove(text1.Length - 1).Trim();

                if (text1.IndexOf(",") == text1.Length - 1)
                    text1 = text1.Remove(text1.Length - 1).Trim();

                if (text1.IndexOf(",") == 0)
                    text1 = text1.Remove(0, 1).Trim();

                if (text1.ToLower().IndexOf("for ") >= 0)
                {
                    text1 = text1.Remove(text1.ToLower().IndexOf("for "), "for ".Length).Trim();
                }

                if (text1.Trim() == type)
                    return "";
                return text1;
            }
            catch (Exception ex)
            {
                return text;
            }

        }

        public static bool IsCompanyName(string val, List<string> companyNames)
        {
            if (val.Length > 70)
                return false;

            if (val == "Chinese and English")
                return false;

            if (val == "Magazine")
                return false;

            if (val == "Investors")
                return false;

            if (val.Contains("Report data field structure is as follows"))
                return false;

            if (string.IsNullOrEmpty(val.Trim()))
                return false;

            if (val.Trim().Length < 3)
                return false;

            if (val.ToLower().Trim().Contains("tél. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("tel. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("for information"))
                return false;

            if (val.ToLower().Trim().Contains("for more information"))
                return false;

            if (val.ToLower().Trim().Contains("for additional information"))
                return false;

            if (val.ToLower().Trim().Contains("in addition to"))
                return false;
            if (val.ToLower().Trim() == "general information")
                return false;

            if (val.ToLower().Trim() == "contact")
                return false;

            if (val.ToLower().Trim() == "inquiries")
                return false;

            if (val.ToLower().Trim() == "invaluable")
                return false;

            if (val.ToLower().Trim() == "сompany")
                return false;

            if (val.ToLower().Trim() == "contact:")
                return false;

            if (val.ToLower().Trim() == "primary analyst")
                return false;

            if (val.ToLower().Trim() == "secondary analyst")
                return false;

            if (val.Trim() == "CHF Investor Relations")
                return false;

            if (val.ToLower().Trim() == "sector")
                return false;

            if (val.ToLower().Trim().IndexOf("at ") == 0)
                return false;

            if (val.ToLower().Trim().IndexOf("for ") == 0)
                return false;

            if (val.ToLower().Trim().Contains(" please "))
                return false;

            if (val.Contains(">") || val.Contains("<"))
                return false;

            //if (val.ToLower().Contains("for "))
            //    return false;

            foreach (var source in companyNames)
            {
                if (!string.IsNullOrEmpty(source))
                {
                    if (!string.IsNullOrEmpty(source))
                    {
                        if (val.ToLower().Contains(source.ToLower()) || source.ToLower().Contains(val.ToLower()))
                            return true;

                        if (val.ToLower().Contains(source.ToLower().Replace("®",""))
                            || source.ToLower().Contains(val.ToLower().Replace("®", ""))
                            || val.ToLower().Contains(source.ToLower().Replace(" and ", "and"))
                            || source.ToLower().Contains(val.ToLower().Replace(" and ", "and"))
                            || val.ToLower().Contains(source.ToLower().Replace(" and ", " & "))
                            || source.ToLower().Contains(val.ToLower().Replace(" and ", " & ")))
                            return true;
                    }
                }
            }

            if (val.Contains('®'))
                return true;

           
            var words = val.Split(' ');
            var i = 0;
            foreach (var w in words)
            {
                foreach (var t in Dictionaries.MarcerCompany)
                {
                    if (w.ToLower() == t.ToLower())
                    {
                        if ((w.ToLower() == "and" || w.ToLower() == "&") && (i == words.Count() - 1 || val[0] < 'A' || val[0] > 'Z'))
                            return false;

                        return true;
                    }
                }
                i++;
            }

            if (val.Contains(":"))
                return false;

            foreach (var w in words)
            {
                foreach (var t in Dictionaries.MarcerNoCompany)
                {
                    if (w.ToLower() == t.ToLower())
                        return false;
                }
            }

            foreach (var t in Dictionaries.MarcerNoCompany)
            {
                if (val.ToLower().Contains(t.ToLower()))
                    return false;
            }
            if (val.ToLower() == val)
                return false;

            if (RegExp.HasNumber(val))
                return false;

            var phone = Regex.Replace(val, "[^0-9,]", "").Trim();
            if (phone.Length > 2)
                return false;

            if (val.Contains("of "))
                return false;


            if (companyNames.Any())
                return false;

            var names = val.Split(',');
            foreach (var name in names)
            {
                if (RegExp.IsName(name) || RegExp.IsNumber(name) && RegExp.IsEmail(name) || Utils.HasTitle(name) || !string.IsNullOrEmpty(Utils.FindType(name)))
                    return false;
            }
            return true;
        }

        public static string GetCompanyName(string word)
        {
           var words = word.Split(' ');

            if(words.Count() > 1)
            {
                if(words[0].ToLower() == "to")
                {
                    int i = 1;
                    while (words[i][0] < 'A' || words[i][0] > 'Z')
                        i++;

                    if(i < words.Count())
                    {
                        return NormalizationString(string.Join(" ",words.Skip(i)));

                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return word;
        }

        public static string GetName(string p)
        {
            var parts = p.Split(Dictionaries.Separate, StringSplitOptions.RemoveEmptyEntries);

            foreach(var n in parts)
            {
                if (RegExp.IsName(n))
                    return n;
            }

            return null;
        }

        internal static string NormPhone2(string p)
        {
            if (string.IsNullOrEmpty(p))
                return p;

            StringBuilder res = new StringBuilder();

            var parts = p.Split(',');
            foreach(var number in parts)
            {
                var correctNumber = number.Replace("(..)", "").Replace("(.)", "").Replace("'","").Trim();
                int value = 0;
                if(int.TryParse(correctNumber, out value))
                {
                    if (value >= 2000 || value <= 2030)
                    {
                        continue;
                    }
                }

                if (res.Length > 0)
                    res.Append(", ");

                res.Append(correctNumber);
            }

            if (res.Length == 0)
                return null;

            return res.ToString();
        }

        public static string[] months = new string[] { 
        "january","february","marc","march","april","apri","may","june","july","august","september","october","november","december"
        ,"jan","feb","mar","apr","may","jun","jul","aug","sep","sept","oct","nov","dec"
        ,"jan.","feb.","mar.","apr.","may.","jun.","jul.","aug.","sep.","sept.","oct.","nov.","dec."};

        public static string CatDataInDL(string dl)
        {
            var capWord = false;
            string res = null;
            if (string.IsNullOrEmpty(dl))
                return dl;

            StringBuilder dl2 = new StringBuilder();

            var words2 = dl.Split(' ');
            foreach(var w in words2)
            {
                DateTime date = DateTime.Now;
                if(DateTime.TryParse(w, out date))
                {
                    if (dl2.Length > 0)
                        return dl2.ToString();
                }
                else
                {
                    dl2.Append(w + " ");
                }
            }

            if (dl.Contains("EXCHANGE RELEASE"))
            {
                var exchIndex = dl.IndexOf("EXCHANGE RELEASE");
                if (exchIndex + "EXCHANGE RELEASE".Length < dl.Length)
                    return dl.Remove(exchIndex + "EXCHANGE RELEASE".Length).Trim();
                else
                    return dl;
            }
            if (dl.Contains("--(Marketwired"))
            {
                var exchIndex = dl.IndexOf("--(Marketwired");
                var result = dl.Remove(exchIndex).Trim();
                if (string.IsNullOrEmpty(result))
                {
                    return "Marketwired";
                }
                else
                    return result;
            }
            foreach(var m in months)
            {
                for (int i = 1; i <= 31; i++ )
                {
                    string data = string.Format(", {0} {1}", i, m);
                    {
                        if (dl.ToLower().Contains(data))
                        {
                            var index = dl.ToLower().IndexOf(data);
                            res = dl.Remove(index).Trim();
                            capWord = true;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(res))
                        break;
                }
                if (string.IsNullOrEmpty(res))
                {
                    if (dl.ToLower().Contains(", " + m + " "))
                    {
                        var index = dl.ToLower().IndexOf(", " + m + " ");
                        res = dl.Remove(index).Trim();
                        capWord = true;
                        break;
                    }
                    else if (dl.ToLower().Contains(" " + m + " "))
                    {
                        var index = dl.ToLower().IndexOf(" " + m + " ");
                        res = dl.Remove(index).Trim();
                        capWord = true;
                        break;
                    }
                    else
                        if (dl.ToLower().Contains("(" + m + " "))
                        {
                            var index = dl.ToLower().IndexOf("(" + m + " ");
                            res = dl.Remove(index).Trim();
                            capWord = true;
                            break;
                        }
                        else if (dl.ToLower().IndexOf(m + " ") == 0)
                        {
                            var year = dl.IndexOf(DateTime.Now.Year.ToString());
                            if (year > 0)
                            {
                                res = dl.Remove(0, year + DateTime.Now.Year.ToString().Length).Trim();
                                break;
                            }
                        }
                }
            }
            if(res == null)
            {
                for(int i = 1; i<=31; i++)
                {
                    var data = string.Format(", {0} de ", i);
                    if (dl.ToLower().Contains(data))
                    {
                        var index = dl.ToLower().IndexOf(data);
                        res = dl.Remove(index).Trim();
                        break;
                    }  
                }
            }
            if (res == null)
            {
                var year2 = dl.IndexOf(DateTime.Now.Year.ToString());
                if (year2 > 0)
                {
                    dl = dl.Remove(year2);

                    res = Utils.NormalizationString(dl);

                }

            }
            if (res == null)
            {
                var year3 = dl.IndexOf((DateTime.Now.Year - 1).ToString());
                if (year3 > 0)
                {
                    dl = dl.Remove(year3);

                    res = Utils.NormalizationString(dl);

                }
            }
            if (res == null)
                res = dl;

            
            if (res != null &&  res.Length > 2 && res.IndexOf("-") == res.Length - 1)
            {
                res = res.Remove(res.Length - 1).Trim();

            }

            //test
            var words = res.Split(new string[] { " ", ",", "(", ")", "/", "&", ",", "." }, StringSplitOptions.RemoveEmptyEntries);
            

            if (words.Length == 0)
                return "";

            var countWord = 0;
            foreach(var w in words)
            {
                countWord++;
                if (w.ToUpper() == w)
                {
                    capWord = true;
                }
            }

            if (res.Length > 1)
            {
                if (capWord  && res.Length < 80 || countWord <10)
                {
                    if (res[0] == ';')
                        res = res.Remove(0, 1).Trim();
                    if (res[res.Length - 1] == ';')
                        res = res.Remove(res.Length - 1).Trim();
                    return res.Trim();
                }
            }
            return "";
        }

        public static string[] noCompany = new string[] { "as well as", "follow us on", "media contact", "media contacts", "the tender agent and information agent" };

        public static string NormCompanyName(string company)
        {
            if (string.IsNullOrEmpty(company))
                return null;

            foreach(var n in noCompany)
            {
                if (company.ToLower().Contains(n))
                    return null;
            }

            if (!(company.ToLower()[company.Length - 1] >= 'a' && company.ToLower()[company.Length - 1] <= 'z') && company.IndexOf("Inc.") != company.Length - "Inc.".Length)
                company = company.Remove(company.Length - 1).Trim();

            if (company.ToLower().IndexOf("about the ") == 0)
            {
                company = company.Remove(0, "about the ".Length).Trim();
            }

            if (company.ToLower().IndexOf("about ") == 0)
            {
                company = company.Remove(0, "about ".Length).Trim();
            }

            return company.Replace("&amp;", "&");
        }

        internal static string NormPhoneMin(string s)
        {
            
            s = s.Trim();
            if (s[s.Length - 1] == ')' && !s.Contains('(') || s[s.Length - 1] == '(')
                s = s.Remove(s.Length - 1);

            s = s.Trim();
            if (s[s.Length - 1] == ',' || s[s.Length - 1] == '-' || s[s.Length - 1] == '.' || s[s.Length - 1] == '–')
                s = s.Remove(s.Length - 1);

            if (s[0] == ',' || s[0] == ')' || s[0] == '.' || s[0] == '-' || s[0] == '–')
                s = s.Remove(0, 1);


           return s.Replace("()", "").Trim();
        }

        public static bool IsCompanyNameNotStrong(string val)
        {
            val = System.Net.WebUtility.HtmlDecode(val);

            if (val.Contains(" has made ") || val.Contains(" team of ") || val.Contains(" will "))
                return false;

            if (val.Length > 70)
                return false;
            if (val == "More information about company")
                return false;

            if (val == "Malta")
                return false;

            if (val == "Mosta")
                return false;

            if (val.Contains("Street"))
                return false;

            if (val == "Medias")
                return false;

            if (val.Contains("Follow us on"))
                return false;

            if (val.Contains("additional information"))
                return false;

            if (val.Contains("and reporters "))
                return false;

            if (val.Contains("This is only a summary"))
                return false;

            if (val.Contains(" will publish "))
                return false;

            if (val.Contains(" if ") || val.Contains("If "))
                return false;

            if (val == "Chinese and English")
                return false;

            if (val == "Magazine")
                return false;

            if (val == "ON-")
                return false;

            if (val == "Marketwired - Mar")
                return false;

            if (val == " 44-FLUKE")
                return false;

            if (val == " Datashield / or")
                return false;

            if (val == "Investors")
                return false;

            if (val.Contains("Report data field structure is as follows"))
                return false;

            if (string.IsNullOrEmpty(val.Trim()))
                return false;

            if (val.Trim().Length < 3)
                return false;

            if (val.ToLower().Trim().Contains("tél. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("tel. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("for information"))
                return false;

            if (val.ToLower().Trim().Contains("for more information"))
                return false;

            if (val.ToLower().Trim().Contains("for additional information"))
                return false;

            if (val.ToLower().Trim().Contains("in addition to"))
                return false;
            if (val.ToLower().Trim() == "general information")
                return false;

            if (val.ToLower().Trim() == "contact")
                return false;

            if (val.ToLower().Trim() == "inquiries")
                return false;

            if (val.ToLower().Trim() == "invaluable")
                return false;

            if (val.ToLower().Trim() == "сompany")
                return false;

            if (val.ToLower().Trim() == "contact:")
                return false;

            if (val.ToLower().Trim() == "primary analyst")
                return false;

            if (val.ToLower().Trim() == "secondary analyst")
                return false;

            if (val.Trim() == "CHF Investor Relations")
                return false;

            if (val.ToLower().Trim() == "sector")
                return false;

            if (val.ToLower().Trim().IndexOf("at ") == 0)
                return false;

            if (val.ToLower().Trim().IndexOf("for ") == 0)
                return false;

            if (val.ToLower().Trim().Contains(" please "))
                return false;

            if (val.Contains(">") || val.Contains("<"))
                return false;

            if (val.Contains("%") || val.Contains("@"))
                return false;

            if (RegExp.IsNumber(val))
                return false;

            if (Utils.GetTitle(val) != null)
                return false;
            //if (val.ToLower().Contains("for "))
            //    return false;

            if (val[0] == val.ToLower()[0])
                return false;

            if (val.Contains('®'))
                return true;

            var words = val.Split(' ');
            var i = 0;
            var shortName = "";
            foreach (var w in words)
            {


                foreach (var t in Dictionaries.MarcerCompany)
                {
                    if (w.ToLower() == t.ToLower())
                    {
                        if ((t.ToLower() == "and" || t.ToLower() == "&") && (i == words.Count() - 1 || val[0] < 'A' || val[0] > 'Z'))
                            return false;

                        if (t == "The")
                        {
                            if (i > 0 || words.Count() < i + 1)
                                continue;

                            var nextWord = words[i + 1].Trim();

                            if (string.IsNullOrEmpty(nextWord) || nextWord[0] < 'A' || nextWord[0] > 'Z')
                                continue;
                        }
                        else
                        {
                            shortName += w[0];
                        }
                        if (val.ToLower() != t.ToLower())
                            return true;
                    }
                }
                i++;
            }

            if (val.Contains(":"))
                return false;

 
            if (Utils.FindType(val) != null)
                return false;

            if (Utils.GetTitle(val) != null)
                return false;

            return true;

        }

        public static bool IsCompanyName(string val, List<string> domains, List<string> companyNames, string Source, string Title)
        {
            val = System.Net.WebUtility.HtmlDecode(val);

            if (val.Contains("@"))
                return false;

            if (val.ToLower().Contains("More information".ToLower()))
                return false;

            if (val.ToLower() == "Media Inquiries".ToLower())
                return false;

            if (val.ToLower() == "or".ToLower())
                return false;

            if (val.ToLower().Contains(" or ") || val.ToLower().Contains(" or,"))
                return false;

            if (val.ToLower().IndexOf("or ") == 0)
                return false;

            if (val.ToLower().IndexOf(" or") == val.Length - " or".Length)
                return false;

            if (val.ToLower().Contains("email"))
                return false;

            if (RegExp.getUrls(val).Count() > 0)
                return false;

            if (val.Contains(" has made ") || val.Contains(" team of ") || val.Contains(" will "))
                return false;

            if (val.Contains("On site at "))
                return false;

            if (val.Length > 70)
                return false;
            if (val == "More information about company")
                return false;

            if (val == "Malta")
                return false;

            if (val == "Mosta")
                return false;

            if (val.Contains("Street"))
                return false;

            if (val == "Medias")
                return false;

            if (val.Contains("Follow us on"))
                return false;

            if (val.Contains("additional information"))
                return false;

            if (val.Contains("and reporters "))
                return false;

            if (val.Contains("This is only a summary"))
                return false;

            if (val.Contains(" will publish "))
                return false;

            if (val.Contains(" if ") || val.Contains("If "))
                return false;

            if (val == "Chinese and English")
                return false;

            if (val == "Magazine")
                return false;

            if (val == "ON-")
                return false;

            if (val == "Marketwired - Mar")
                return false;

            if (val == " 44-FLUKE")
                return false;

            if (val == " Datashield / or")
                return false;

            if (val == "Investors")
                return false;

            if (val.Contains("Report data field structure is as follows"))
                return false;

            if (string.IsNullOrEmpty(val.Trim()))
                return false;

            if (val.Trim().Length < 3)
                return false;

            if (val.ToLower().Trim().Contains("tél. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("tel. & fax"))
                return false;

            if (val.ToLower().Trim().Contains("for information"))
                return false;

            if (val.ToLower().Trim().Contains("for more information"))
                return false;

            if (val.ToLower().Trim().Contains("for additional information"))
                return false;

            if (val.ToLower().Trim().Contains("in addition to"))
                return false;
            if (val.ToLower().Trim() == "general information")
                return false;

            if (val.ToLower().Trim() == "contact")
                return false;

            if (val.ToLower().Trim() == "inquiries")
                return false;

            if (val.ToLower().Trim() == "invaluable")
                return false;

            if (val.ToLower().Trim() == "сompany")
                return false;

            if (val.ToLower().Trim() == "contact:")
                return false;

            if (val.ToLower().Trim() == "primary analyst")
                return false;

            if (val.ToLower().Trim() == "secondary analyst")
                return false;

            if (val.Trim() == "CHF Investor Relations")
                return false;

            if (val.ToLower().Trim() == "sector")
                return false;

            if (val.ToLower().Trim().IndexOf("at ") == 0)
                return false;

            if (val.ToLower().Trim().IndexOf("for ") == 0)
                return false;

            if (val.ToLower().Trim().Contains(" please "))
                return false;

            if (val.Contains(">") || val.Contains("<"))
                return false;

            if (val.Contains("%") || val.Contains("@"))
                return false;

            if (RegExp.IsNumber(val))
                return false;

            if (Utils.GetTitle(val) != null)
                return false;
            //if (val.ToLower().Contains("for "))
            //    return false;
            if (companyNames != null)
                foreach (var source in companyNames)
                {
                    if (!string.IsNullOrEmpty(source))
                    {
                        if (!string.IsNullOrEmpty(source))
                        {
                            if (val.ToLower().Contains(source.ToLower()) || source.ToLower().Contains(val.ToLower()))
                                return true;

                            if (val.ToLower().Contains(source.ToLower().Replace("®", ""))
                                || source.ToLower().Contains(val.ToLower().Replace("®", ""))
                                || val.ToLower().Contains(source.ToLower().Replace(" and ", "and"))
                                || source.ToLower().Contains(val.ToLower().Replace(" and ", "and"))
                                || val.ToLower().Contains(source.ToLower().Replace(" and ", " & "))
                                || source.ToLower().Contains(val.ToLower().Replace(" and ", " & ")))
                                return true;
                        }
                    }
                }
            if (domains != null)
                foreach (var domain in domains)
                {
                    if (Utils.NormalizationString(val).ToLower().Replace(" ", "").Replace("&", "and").IndexOf(domain) == 0)
                        return true;

                    if (domain.Contains(Utils.NormalizationString(val).ToLower().Replace(" ", "").Replace("&", "and")))
                        return true;
                }

            if (val[0] < 'A' || val[0] > 'Z')
                return false;

            if (val.Contains('®'))
                return true;


            var words = val.Split(' ');
            var i = 0;
            var shortName = "";
            foreach (var w in words)
            {


                foreach (var t in Dictionaries.MarcerCompany)
                {
                    if (w.ToLower() == t.ToLower())
                    {
                        if ((t.ToLower() == "and" || t.ToLower() == "&") && (i == words.Count() - 1 || val[0] < 'A' || val[0] > 'Z'))
                            return false;

                        if (t == "The")
                        {
                            if (i > 0 || words.Count() < i + 1)
                                continue;

                            var nextWord = words[i + 1].Trim();

                            if (string.IsNullOrEmpty(nextWord) || nextWord[0] < 'A' || nextWord[0] > 'Z')
                                continue;
                        }

                        if (val.ToLower() != t.ToLower())
                            return true;
                    }

                }
                if (!string.IsNullOrEmpty(w) && w != "The" && w[0] >= 'A' && w[0] <= 'Z')
                {
                    shortName += w.ToLower()[0];
                }
                foreach (var domain in domains)
                {
                    if (w.ToLower().IndexOf(domain.ToLower()) == 0)
                        return true;
                }

                i++;
            }

            if (domains != null && !string.IsNullOrEmpty(shortName) && shortName.Length > 2)
                foreach (var domain in domains)
                {
                    if (domain.ToLower() == shortName.ToLower())
                        return true;
                }

            if (val.Contains(":"))
                return false;

            if (!string.IsNullOrEmpty(Source))
            {
                if (val.Contains(Source.Replace("Ltd", "")))
                    return true;
            }

            if (RegExp.IsName(val))
                return false;


            if (Utils.FindType(val) != null)
                return false;

            if (Utils.GetTitle(val) != null)
                return false;

            if (!string.IsNullOrEmpty(Title) && Title.Contains(val))
                return true;

            //if (val != null && val.Length > 2 && val[0] >= 'A' && val[0] <= 'Z' && words.Count() < 3 && words.Count(c => c[0] >= 'A' && c[0] <= 'Z') == words.Count())
            //        return true;
            return false;
        }


        internal static string NormalizationEmail(Contact item)
        {
            if (string.IsNullOrEmpty(item.Email) || !item.Email.Contains(","))
                return item.Email;

            var Emails = item.Email.Split(',').Select(e => e.Trim()).ToList().Distinct();

            if(Emails == null || Emails.Count() < 2)
            {
                return string.Join(", ", Emails.ToArray());
            }

            if(string.IsNullOrEmpty(item.Name))
                return Emails.FirstOrDefault();

            foreach(var e in Emails)
            {
                if (trueEmail(e, item.Name))
                    return e;
            }

            return Emails.FirstOrDefault();
        }
    }
}
