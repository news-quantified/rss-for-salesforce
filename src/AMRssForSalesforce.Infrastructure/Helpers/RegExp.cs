﻿using AMRssForSalesforce.Infrastructure.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public class RegExp
    {
        private RegExp()
        {
        }

        public const string US_ZIP_OR_ZIP_AND_4 = @"^(?(^00000(|-0000))|(\d{5}(|-\d{4})))$";
        public const string EMAIL = @"^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$";
        public const string FULL_EMAIL = @"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$";
        public const string US_SSN = @"^(?!000)(?<SSN3>[0-6]\d{2}|7(?:[0-6]\d|7[012]))([- ]?)(?!00)(?<SSN2>\d\d)\1(?!0000)(?<SSN4>\d{4})$";
        public const string HOST_NAME = @"://(?<name>\w+)[^/:]+(?<port>:\d+)?/";
        public const string HOST_FULL_NAME = @"://(?<name>\w+)[^/:]+(?<port>:\d+)?/\w+/";
        public const string PHONE0 = @"([\+]+)*[0-9\x20\x28\x29\xA0\-\.]{8,25}";
        public const string PHONE = @"([\+]+)*[0-9\x20\x28\x29\xA0\-\.A-Z]{8,25}";
        //@"([+])?(\d)?(\(\d+\))?(\s)*\d+((\s)*(-)?(\s)*\d+)*";
        public const string POSTCODE = @"^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$";
        public const string ONLYLETERS = @"^[A-Z][a-z]*$";
        public const string MatchPhonePattern = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";
        public const string Brackets = @"\((.*)\)";
        public const string beforeBrackets = @"([^\(]+)";
        public const string URL = @"\b(?:https?://|www\.)[a-z0-9_\-\.]{2,}\.([a-z]{2,6})";
        public const string URL2 = @"[a-z0-9_\-\.]{2,}\.([a-z]{2,6})";
        public const string URL3 = @"[a-z0-9_\-\.]{2,}\.(com)";
        public const string host = @"://(?<host>([a-z\\d][-a-z\\d]*[a-z\\d]\\.)*[a-z][-a-z\\d]+[a-z])";
        public const string PHONE_A_Z = @"([A-Z]{3})\-([A-Z]{4})";
        public const string PHONE_2 = @"([0-9]{3})/([0-9]{3})\-([0-9]{4})"; //203/328-7310

        public const string PHONE_3 = @"\(([0-9]{3})\)\x20([0-9]{3})\-([A-Z]{4})"; //(775) 372-(VIRA)

        const string MatchEmailPattern =
           @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";

        public const string MatchPhonePattern2 = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d";

        public const string example = "Susan Jevens / Isabell Novakov, +44(0) 20 8943 4685 or +33(0) 497 218 300";

        public const string htmlTeg = @"<[^>]*>";

        private static string[] dns = new string[] { "aero", "asia", "biz", "cat", "com", "coop", "edu",  "gov", "info", "int", "jobs", "mil", "mobi", "museum", "name", "net", "org"
            , "pro", "rich", "tel", "travel"
            , "ac", "ad", "ae", "af", "ag", "ai", "al", "am", "an", "ao", "aq", "ar", "as", "at", "au", "aw", "ax", "az"
            , "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz"
            , "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cs", "cu", "cv", "cx", "cy"
            , "dd", "de", "dj", "dk", "dm", "do", "dz"
            , "ec", "ee", "eg", "er", "es", "et", "eu"
            , "fj", "fi", "fk", "fm", "fo", "fr"
            , "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn", "gp", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy"
            , "hk", "hm", "hn", "hr", "ht", "hu"
            , "id", "ie", "il", "im", "in", "io", "iq", "ir", "is", "it"
            , "je", "jm", "jo", "jp"
            , "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "krd", "kw", "ky", "kz"
            , "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly"
            , "ma", "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz"
            , "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz"
            , "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py"
            , "qa"
            , "re", "ro", "rs", "ru", "rw"
            , "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "st", "su", "sv", "sy", "sz"
            , "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tr", "tt", "tv", "tw", "tz"
            , "ua", "ug", "uk", "us", "uy", "uz"
            , "va", "vc", "ve", "vg", "vi", "vn", "vu"
            , "wf", "ws"
            , "ye", "yt", "za", "zm", "zw"
        };

        public static string[] getEmails(string p_sVaalue)
        {
            List<string> ret = new List<string>();

            if (string.IsNullOrEmpty(p_sVaalue))
                return ret.ToArray();

            p_sVaalue = System.Net.WebUtility.HtmlDecode(p_sVaalue);

            p_sVaalue = p_sVaalue.Replace("[at]", "@").Replace(" (at) ", "@").Replace(" (dot) ", ".").Replace("(at)", "@").Replace("(dot)", ".");
            Regex rx = new Regex(MatchEmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // Find matches.
            MatchCollection matches = rx.Matches(p_sVaalue);
            // Report the number of matches found.
            int noOfMatches = matches.Count;
            // Report on each match.
            foreach (Match match in matches)
            {
                ret.Add(match.Value.ToString());
            }

            return ret.Distinct().ToArray();
        }

        public static string[] getUrls(string p_sVaalue)
        {

            List<string> ret = new List<string>();

            var strings = p_sVaalue.ToLower().Split(';');
            foreach (var s in strings)
            {

                var reg = Regex.Matches(s.ToLower(), URL);

                var regCom = Regex.Matches(s.ToLower(), URL3);


                //if(reg.Count == 0)
                //{
                //    reg = Regex.Matches(s.ToLower(), URL2);

                //}
                foreach (var r in reg)
                {

                    var parts = r.ToString().Replace("http://", "").Replace("https://", "").Split('/');
                    if (!ret.Any(re => re == parts[0])
                        && !parts[0].Contains("facebook.com")
                        && !parts[0].Contains("twitter.com")
                        && !parts[0].Contains("linkedin.com")
                        && !parts[0].Contains("google.com")
                        && !parts[0].Contains("www.w3.org")
                        && !parts[0].Contains("developer.xfinity.com")
                        && !parts[0].Contains("cts.businesswire.com")
                        && !parts[0].Contains("schema.org"))
                    {
                        var parts2 = parts[0].Split('.');
                        if (parts2.Length >= 2 && dns.Any(d => d == parts2[parts2.Length - 1]))
                        {
                            ret.Add(parts[0]);
                        }
                    }
                }

                if (regCom.Count > 0)
                {

                    foreach (var r in regCom)
                    {
                        if (r.ToString()[0] != '.')
                            ret.Add(r.ToString());
                    }

                }
            }
            return ret.Distinct().ToArray();
        }

        public static string GetInBrackets(string p_sVaalue)
        {
            var reg = Regex.Matches(p_sVaalue, Brackets);

            if (reg.Count == 0)
                return string.Empty;

            return reg[0].Value.Replace("(", "").Replace(")", "");
        }

        public static string GetBiforeBrackets(string p_sVaalue)
        {
            var reg = Regex.Matches(p_sVaalue, beforeBrackets);

            if (reg.Count == 0)
                return string.Empty;

            return reg[0].Value;
        }

        public static bool IsEmail(string p_sValue)
        {
            p_sValue = p_sValue.Replace("[at]", "@").Replace("(at)", "@").Replace("(dot)", ".");
            return IsMatch(EMAIL, p_sValue.ToLower());
        }

        public static string GetEmail(string email)
        {
            email = email.Replace("[at]", "@").Replace("(at)", "@").Replace("(dot)", ".");

            var reg = Regex.Matches(email, EMAIL);
            foreach (var r in reg)
            {
                return r.ToString();
            }

            return "";
        }

        private static bool IsMatch(string p_sPattern, string p_sValue)
        {
            Regex regex = new Regex(p_sPattern);
            return regex.IsMatch(p_sValue);
        }

        public static string[] GetPnoneNumbers(string p_sVaalue)
        {
            p_sVaalue = p_sVaalue.Replace("&nbsp;", " ");

            List<string> ret = new List<string>();

            p_sVaalue = p_sVaalue.Replace("–", "-");

            int countNotYear = 0;

            var partsP = p_sVaalue.Split(new string[] { "\t\n", "\r\n", "\n", "; ", "Additional information:", "Distribution:" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var pP in partsP)
            {
                if (pP.Contains("serie B"))
                    continue;

                if (pP.Contains(" till "))
                    continue;

                if (pP.Contains(" serie "))
                    continue;

                if (pP.Contains("fördelat på "))
                    continue;

                if (pP.Contains(".pdf"))
                    continue;

                if (pP.Contains(string.Format(".{0}", DateTime.Now.Year)) || pP.Contains(string.Format(".{0}", DateTime.Now.Year - 1))
                    || pP.Contains(string.Format(" {0}", DateTime.Now.Year)) || pP.Contains(string.Format(" {0}", DateTime.Now.Year - 1)))
                    continue;

                if (pP.Contains("2011-2013") || pP.Contains("2014-2016"))
                    continue;

                if (pP.Contains("$"))
                    continue;

                if (pP.Contains("USD"))
                    continue;

                if (pP.Contains("usd "))
                    continue;

                if (pP.Contains(" usd"))
                    continue;

                if (pP.Contains("EUR"))
                    continue;

                if (pP.Contains("eur "))
                    continue;

                if (pP.Contains(" eur"))
                    continue;

                if (pP.Contains(".html"))
                    continue;

                if (pP.Contains("%"))
                    continue;


                var parts = pP.Split(new string[] { ";", ", ", "at ", "al ", " o", " hotline " }, StringSplitOptions.RemoveEmptyEntries);

                if (parts.Count() == 0)
                    return new List<string>().ToArray();

                foreach (var ppp in parts)
                {

                    var p = ppp.Trim();
                    var reg = Regex.Matches(p, PHONE0);
                    var inBusket = ppp.Split(new string[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries);

                    if (inBusket.Length > 1 && inBusket.Min(i => i.Length) > 4)
                    {
                        foreach (var n in inBusket)
                        {
                            p = n;

                            reg = Regex.Matches(p, PHONE0);

                            if (reg.Count == 0)
                                reg = Regex.Matches(p, PHONE_2);

                            if (reg.Count == 0)
                                reg = Regex.Matches(p, PHONE);
                            else
                            {

                            }

                            countNotYear = 0;

                            foreach (var r in reg)
                            {
                                var substring = r.ToString().Split('-');
                                foreach (var s in substring)
                                {
                                    int value = 0;
                                    if (int.TryParse(s.Replace("'", "").Trim(), out value))
                                    {
                                        if (value < 2000 || value > 2030)
                                            countNotYear++;
                                    }
                                    else
                                        countNotYear++;
                                }
                            }

                            if (countNotYear == 0)
                                continue;


                            foreach (var r in reg)
                            {
                                var number = r.ToString().Trim();
                                var phone = Regex.Replace(number, "[^0-9,]", "").Trim();
                                //if (phone.Length >= 8 || Regex.Matches(number, PHONE_A_Z).Count == 1 && phone.Length >= 3 || r.ToString().Length == 16 && phone.Length == 6
                                //    || r.ToString().Length == 14 && phone.Length == 7)
                                if (phone.Length >= 8 || (Regex.Matches(number, PHONE_A_Z).Count == 1 || Regex.Matches(number, @"([A-Z]{7})").Count == 1) && phone.Length >= 3 || r.ToString().Length == 16 && phone.Length == 6
                                    || r.ToString().Length == 14 && phone.Length == 7)
                                {
                                    if (number[0] == ')' || number[0] == '.')
                                        number = number.Remove(0, 1);

                                    var indexOfnumber = p.IndexOf(number.Trim());
                                    if (indexOfnumber >= 0)
                                    {
                                        if (indexOfnumber != 0)
                                        {
                                            if (p[indexOfnumber - 1] != ' ' && p[indexOfnumber - 1] != '+' && p[indexOfnumber - 1] != '-' && p[indexOfnumber - 1] != '(')
                                                continue;
                                        }
                                    }
                                    if (number.Contains(string.Format("{0}-{1}-", DateTime.Now.Year, DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString())))
                                        continue;
                                    if (NotExterSpase(number))
                                        ret.Add(Utils.NormPhoneMin(number.Trim()));
                                }
                            }
                        }
                        continue;
                    }


                    //digets only
                    reg = Regex.Matches(p, PHONE0);

                    if (reg.Count == 0)
                        reg = Regex.Matches(p, PHONE_2);

                    if (reg.Count == 0)
                        reg = Regex.Matches(p, PHONE);
                    else
                    {

                    }

                    countNotYear = 0;

                    foreach (var r in reg)
                    {
                        var substring = r.ToString().Split('-');
                        foreach (var s in substring)
                        {
                            int value = 0;
                            if (int.TryParse(s.Replace("'", "").Trim(), out value))
                            {
                                if (value < 2000 || value > 2030)
                                    countNotYear++;
                            }
                            else
                                countNotYear++;
                        }
                    }

                    if (countNotYear == 0)
                        continue;


                    foreach (var r in reg)
                    {
                        var number = r.ToString().Trim();
                        var phone = Regex.Replace(number, "[^0-9,]", "").Trim();
                        if (phone.Length >= 8 || Regex.Matches(number, PHONE_A_Z).Count == 1 && phone.Length >= 3 || r.ToString().Length == 16 && phone.Length == 6
                            || r.ToString().Length == 14 && phone.Length == 7)
                        {
                            if (number[0] == ')' || number[0] == '.')
                                number = number.Remove(0, 1);

                            var indexOfnumber = p.IndexOf(number.Trim());
                            if (indexOfnumber >= 0)
                            {
                                if (indexOfnumber != 0)
                                {
                                    if (p[indexOfnumber - 1] != ' ' && p[indexOfnumber - 1] != '+' && p[indexOfnumber - 1] != '-' && p[indexOfnumber - 1] != '(')
                                        continue;
                                }
                            }
                            if (number.Contains(string.Format("{0}-{1}-", DateTime.Now.Year, DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString())))
                                continue;
                            if (NotExterSpase(number))
                                ret.Add(Utils.NormPhoneMin(number.Trim()));
                        }
                    }
                }
            }
            return ret.Distinct().ToArray();
        }

        private static bool NotExterSpase(string number)
        {
            number = number.Trim();

            if (number.Contains(".") && number.Contains(" ") && !number.Contains("+"))
                return false;
            var str = Regex.Replace(number, "\\s+", " ");
            return number == str;
        }

        public static string[] GetPnoneNumbers2(string p_sVaalue)
        {

            List<string> ret = new List<string>();

            p_sVaalue = p_sVaalue.Replace("\n", ",");
            p_sVaalue = p_sVaalue.Replace("–", "-");
            var reg = Regex.Matches(p_sVaalue, PHONE);

            foreach (var r in reg)
            {
                var number = r.ToString().Trim();
                var phone = Regex.Replace(number, "[^0-9,]", "").Trim();
                if (phone.Length >= 10)
                {
                    if (number[0] == ')' || number[0] == '.')
                        number = number.Remove(0, 1);

                    if (number[number.Length - 1] < '0' || number[number.Length - 1] > '9')
                        number = number.Remove(number.Length - 1, 1);

                    var indexOfnumber = p_sVaalue.IndexOf(number.Trim());
                    if (indexOfnumber >= 0)
                    {
                        if (indexOfnumber != 0)
                        {
                            if (p_sVaalue[indexOfnumber - 1] != ' ' && p_sVaalue[indexOfnumber - 1] != '+' && p_sVaalue[indexOfnumber - 1] != '-' && p_sVaalue[indexOfnumber - 1] != '(')
                                continue;
                        }
                    }
                    ret.Add(number.Trim());
                }
            }

            return ret.ToArray();
        }

        public static bool IsNumber(string phone)
        {
            return GetPnoneNumbers(phone).Count() > 0;

        }

        public static bool IsName(string p_sValue)
        {
            if (p_sValue == "VÄNLIGEN KONTAKTA")
                return false;

            if (p_sValue == "Precise Biometrics")
                return false;

            if (p_sValue == "Skandinaviska Enskilda")
                return false;

            if (p_sValue == "Smile Henkilöstöpalvelut")
                return false;

            if (p_sValue.Contains("Nasdaq"))
                return false;

            if (p_sValue.Contains("Mid Cap"))
                return false;

            if (p_sValue == "Alex Press")
                return true;

            if (p_sValue.Contains("   "))
                return false;

            if (p_sValue.Contains(" AB"))
                return false;

            if (p_sValue.Contains("Konserthus"))
                return false;

            if (p_sValue.Contains("(Bill)"))
                return true;

            if (p_sValue.Length < 4)
                return false;

            if (p_sValue.Contains("(") || p_sValue.Contains(")"))
                return false;

            if (!Char.IsLetterOrDigit(p_sValue[0]))
            {
                //– Edward Sagebiel 
                p_sValue = p_sValue.Remove(0, 1).Trim();
            }

            if (p_sValue.Length < 4)
                return false;

            if (!Char.IsLetterOrDigit(p_sValue[p_sValue.Length - 1]))
            {
                //– Edward Sagebiel 
                p_sValue = p_sValue.Remove(p_sValue.Length - 1, 1).Trim();
            }

            if (p_sValue.Length < 4)
                return false;

            char[] s = new char[] { ' ', ' ' };
            var parts = p_sValue.Split(s, StringSplitOptions.RemoveEmptyEntries);

            foreach (var p in parts)
            {
                if (p.ToLower() == "note")
                    return false;

                if (p.ToLower() == "to")
                    return false;


                foreach (var t in Dictionaries.MarcerNoName)
                {
                    if (t.Length > 4)
                    {
                        if (p.ToLower() == t.ToLower())
                            return false;
                    }
                    else
                    {
                        if (p == t)
                            return false;

                        if (p == t.ToUpper())
                            return false;
                    }
                }
                if (p.ToLower() == p && p.ToLower() != "dela" && p.ToLower() != "de" && p != "van" && p != "der")
                    return false;

                foreach (var t in Dictionaries.NotNames2)
                {
                    if (t.Length > 2)
                    {
                        if (p.ToLower() == t.ToLower())
                            return false;
                    }
                    else
                    {
                        if (p == t)
                            return false;
                    }
                }

                if (p.Length > 3)
                {
                    if (p.ToLower() == p)
                        return false;
                }
            }

            if (parts.Count() < 2)
                return false;
            if (parts.Count() > 4)
                return false;
            var countNotLeter = 0;
            foreach (var c in p_sValue)
            {
                if (!Char.IsLetter(c) && c != ' ' && c != ' ')
                {
                    countNotLeter++;
                    if (countNotLeter > 2)
                        return false;
                }
                if (!Char.IsLetter(c) && c != ' ' && c != ' ' && c != '.' && c != '-' && c != '\'' && c != 8217)
                    return false;
            }
            if (p_sValue == "Fitch Ratings")
                return false;

            return true;
        }



        internal static string GetHost(string _url)
        {
            if (!_url.Contains("://"))
                _url = "http://" + _url;

            return new Uri(_url).Host;
        }

        /* 
** Method 1 (using the build-in Uri-object)
*/
        public static string ExtractDomainNameFromURL_Method1(string Url)
        {
            if (!Url.Contains("://"))
                Url = "http://" + Url;

            return new Uri(Url).Host;
        }

        /*
        ** Method 2 (using string modifiers)
        */
        public static string ExtractDomainNameFromURL_Method2(string Url)
        {
            if (Url.Contains(@"://"))
                Url = Url.Split(new string[] { "://" }, 2, StringSplitOptions.None)[1];

            return Url.Split('/')[0];
        }

        /*
        ** Method 3 (using regular expressions -> slowest) 
        */
        public static string ExtractDomainNameFromURL_Method3(string Url)
        {
            return System.Text.RegularExpressions.Regex.Replace(
                Url,
                @"^([a-zA-Z]+:\/\/)?([^\/]+)\/.*?$",
                "$2"
            );
        }

        internal static bool HasNumber(string val)
        {
            var reg = Regex.Matches(val, PHONE);

            if (reg == null || reg.Count == 0)
            {
                reg = Regex.Matches(val, PHONE_2);
                if (reg == null || reg.Count == 0)
                    return false;
            }

            return true;
        }


        public static string[] getUrls2(string p_sVaalue)
        {
            List<string> ret = new List<string>();

            var strings = p_sVaalue.Split(';');
            foreach (var s in strings)
            {
                var reg = Regex.Matches(s.ToLower(), URL2);

                foreach (var r in reg)
                {

                    var parts = r.ToString().Replace("http://", "").Replace("https://", "").Split('/');

                    var parts2 = parts[0].Split('.');
                    if (parts2.Length >= 2 && dns.Any(d => d == parts2[parts2.Length - 1]))
                    {
                        ret.Add(parts[0]);
                    }

                }
            }
            return ret.ToArray();
        }


        public static string GetInnerText(string contact)
        {
            if (string.IsNullOrEmpty(contact))
                return contact;

            contact = contact.Replace("<br />", "; ");
            contact = contact.Replace("<br>", "; ");

            var ret = Regex.Replace(contact, htmlTeg, " ");
            ret = System.Net.WebUtility.HtmlDecode(ret);
            return ret.Trim();
        }

        public static string GetInnerText2(string contact)
        {
            if (string.IsNullOrEmpty(contact))
                return contact;

            contact = contact.Replace("<br />", "; ");
            contact = contact.Replace("<br>", "; ");

            var ret = Regex.Replace(contact, htmlTeg, "; ");
            ret = System.Net.WebUtility.HtmlDecode(ret).Replace("/r/n", "; ").Replace("/n", "; ").Replace("/n/t", "; ");
            return ret.Trim();
        }
    }
}
