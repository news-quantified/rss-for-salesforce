﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public static class StringHelpers
    {
        public static string CleanAndFormatText(string source)
        {
            var text = source.Replace("\t", " ").Replace("\r", " ").Replace("\n", " ").Replace("amp;", "").TrimEnd().TrimStart();
            text = ReplaceMultipleSpacesWithOne(text);
            return text;
        }

        public static string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }

        public static string ReplaceMultipleSpacesWithOne(string source)
        {
            var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
            return regex.Replace(source, @" ");
        }
    }
}
