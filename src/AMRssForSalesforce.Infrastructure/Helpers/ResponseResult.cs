﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public class ResponseResult
    {
        public string Html { get; set; }
        public string Redirect { get; set; }
    }
}
