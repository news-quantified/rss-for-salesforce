﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace AMRssForSalesforce.Infrastructure.Helpers
{
    public static class ValidationHelpers
    {
        public const string MatchPhonePattern = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";
        public const string MatchUrlPattern = @"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$";


        public static bool PhoneIsValid(string phone)
        {
            if (String.IsNullOrEmpty(phone)) return false;
            phone = Regex.Replace(phone, "[a-zA-Z:@,]", "").Trim();
            var rx = new Regex(MatchPhonePattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var isValidate = rx.IsMatch(phone);
            if (!isValidate && phone.Length > 12) isValidate = true;
            if (isValidate && phone.Length <=  6) isValidate = false;
            return isValidate;
        }

        public static bool EmailIsValid(string emailaddress)
        {
            if (String.IsNullOrEmpty(emailaddress)) return false;
            try
            {
                var m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException ex)
            {
                return false;
            }
        }

        public static bool IsValidStreetAddress(string address)
        {
            if ((from item in ConstValue.ShortAdressList where address.IndexOf(item, StringComparison.CurrentCultureIgnoreCase) >= 0 select address).Any()) return true;
            return (from item in ConstValue.ShortAdressAbrList where address.IndexOf(item, StringComparison.CurrentCultureIgnoreCase) >= 0 select address).Any() && Regex.Match(address, @"\d+").Value.Length >= 4;
        }
        
        public static bool UrlIsValid(string url)
        {
            
            if (String.IsNullOrEmpty(url)) return false;
            if (url.Length <= 7) return  false;
            try
            {
                const string exp = @"\b(?:https?://|www\.)\S+\b";
              
              return Regex.IsMatch(url, exp);

            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool TypeIsValid(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            if (value.Contains("Alex Press"))
                return false;
            try
            {
                var val = value.Replace(":", "");
                if (ConstValue.Types.Any(item => String.Equals(item, val, StringComparison.CurrentCultureIgnoreCase)) || value[value.Length - 1] == ':') return true;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            return false;
        }

        public static Tuple<bool, string> TitleIsValid(string value)
        {
            if (String.IsNullOrEmpty(value)) return new Tuple<bool, string>(false, String.Empty);
            try
            {
                var val = value.Replace(":", "");
                foreach (var item in ConstValue.TitlesList)
                {
                    if (value.ToUpper().Contains(item.ToUpper()))
                    {
                        if (item.Length > 3)
                            return new Tuple<bool, string>(true, item);
                        else
                        {
                            var words = value.Split(' ');
                            foreach(var w in words)
                            {
                                if(w.ToUpper() == item.ToUpper())
                                    return new Tuple<bool, string>(true, item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.WriteError(ex);
                return new Tuple<bool, string>(false, String.Empty);
            }
            return new Tuple<bool, string>(false, String.Empty);
        }

        public static bool ContainsPhone(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;

            var phone = Regex.Replace(value, "[a-zA-Z:@,]", "").Trim();

            return phone.Length >= 8;
        }

        public static bool CompanyIsValid(string company)
        {
            if (String.IsNullOrEmpty(company)) return false;
            try
            {
                if ((from item in ConstValue.Company where company.IndexOf(item, StringComparison.CurrentCultureIgnoreCase) >= 0 select company).Any()) return true;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            return false;
        }

        public static bool ContactIsValid(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            try
            {
                if ((from item in ConstValue.People where value.IndexOf(item, StringComparison.CurrentCultureIgnoreCase) >= 0 select value).Any()) return true;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            return false;
        }
    }
}
