﻿namespace AMRssForSalesforce.Infrastructure.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Linq.Expressions;
    using Model;
    using Dapper;
    using AMRssForSalesforce.Infrastructure.Helpers;
    using System.Threading;
    using System.Text;
    using System.Data;

    public sealed class RssFeedRepository : IRepository<RssFeed>
    {
        public static object Obforlock = new object();

        public static int CountCount = 0;
        public static int InsertCount = 0;
        public void Add(RssFeed item)
        {
            InsertCount++;

            if (item == null)
            {
                Logger.Logger.WriteWarning("RssFeedRepository - item Is null (void Add)");
                return ;
            }
            if (item.DateTime.Year < 2000)
            {
                item.DateTime = DateTime.Now;
            }
            item.HeadLine = Utils.NormalizationString(item.HeadLine);
            item.Dateline = Utils.CatDataInDL(item.Dateline);

            string err = CheackRssFeed(item);

            if (!string.IsNullOrEmpty(err))
                Logger.Logger.WriteWarning(err);

            const string requestInsert = @" if exists (select TOP(1) LINK from [dbo].[RssFeed] WITH(NOLOCK) where  Link = @Link
)
update [dbo].[RssFeed] set [Headline] = @Headline, Dateline = @Dateline, ContactInfo = @ContactInfo, English = @English, Language = @Language, DateTime= @DateTime, Redirect = @Redirect,
MultiMediaCount = @MultiMediaCount,
StaticMediaCount = @StaticMediaCount,
                                   [Link] = @Link, 
                                   StoryCount = @StoryCount
                                   where Link = @Link

else
INSERT INTO [dbo].[RssFeed]([Source],[ID],[Provider],[Link],[Headline],[DateTime],[Dateline], [ContactInfo], [English],[StoryCount],[Language],[Redirect],[MultiMediaCount],[StaticMediaCount])
                                            VALUES (@Source, @Id, @Provider, @Link, @HeadLine,  @DateTime, @Dateline, @ContactInfo, @English, @StoryCount, @Language, @Redirect,@MultiMediaCount,@StaticMediaCount
)";


            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    if (item.Dateline != null && item.Dateline.Length > 255)
                        item.Dateline = item.Dateline.Remove(254);


                    db.Execute(requestInsert, new
                    {
                        item.Source,
                        item.Id,
                        item.Provider,
                        item.Link
                        ,
                        item.HeadLine,
                        item.DateTime,
                        item.Dateline,
                        item.ContactInfo,
                        item.English,
                        item.StoryCount,
                        item.Language,
                        item.Redirect,
                        item.MultiMediaCount,
                        item.StaticMediaCount
                    });
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteError(ex);
                }
            }
            

        }

        public void Add_New(RssFeed item)
        {
            try
            {
                InsertCount++;

                if (item == null)
                {
                    Logger.Logger.WriteWarning("RssFeedRepository - item Is null (void Add)");
                    return;
                }
                if (item.DateTime.Year < 2000)
                {
                    item.DateTime = DateTime.Now;
                }
                item.HeadLine = Utils.NormalizationString(item.HeadLine);
                item.Dateline = Utils.CatDataInDL(item.Dateline);

                string err = CheackRssFeed(item);

                if (!string.IsNullOrEmpty(err))
                    Logger.Logger.WriteWarning(err);


                using (var db = new NQDBEntities())
                {
                    var rss = db.RssFeeds.FirstOrDefault(r => r.Link == item.Link);
                    if (rss != null)
                    {
                        rss.Headline = item.HeadLine;
                        rss.Dateline = item.Dateline;
                        rss.ContactInfo = item.ContactInfo;
                        rss.English = item.English;
                        rss.Language = item.Language;
                        rss.DateTime = item.DateTime;
                        rss.Redirect = item.Redirect;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.RssFeeds.Add(new Infrastructure.RssFeed
                        {
                            Source = item.Source,
                            ID = item.Id,
                            Provider = item.Provider,
                            Link = item.Link,
                            Headline = item.HeadLine,
                            DateTime = item.DateTime,
                            Dateline = item.Dateline,
                            ContactInfo = item.ContactInfo,
                            English = item.English,
                            StoryCount = item.StoryCount,
                            Language = item.Language,
                            Redirect = item.Redirect,
                            MultiMediaCount = item.MultiMediaCount,
                            StaticMediaCount = item.StaticMediaCount,
                        });
                        db.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public void AddNotUpdate(RssFeed item)
        {
            if (item == null)
            {
                Logger.Logger.WriteWarning("RssFeedRepository - item Is null (void Add)");
                return;
            }
            if (item.DateTime.Year < 2000)
            {
                item.DateTime = DateTime.Now;
            }
            item.Dateline = Utils.CatDataInDL(item.Dateline);

            string err = CheackRssFeed(item);

            if (!string.IsNullOrEmpty(err))
                Logger.Logger.WriteWarning(err);

            const string requestInsert = @"  
if not exists (select TOP(1) LINK from [dbo].[RssFeed] WITH(NOLOCK) where  Link = @Link
   
    )
    
 INSERT INTO [dbo].[RssFeed]([Source],[ID],[Provider],[Link],[Headline],[DateTime],[Dateline], [ContactInfo], [English],[StoryCount],[Language],[MultiMediaCount],[StaticMediaCount])
                                            VALUES (@Source, @Id, @Provider, @Link, @HeadLine,  @DateTime, @Dateline, @ContactInfo, @English, @StoryCount, @Language, @MultiMediaCount,@StaticMediaCount)";


            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    if (item.Dateline != null && item.Dateline.Length > 255)
                        item.Dateline = item.Dateline.Remove(254);


                    db.Execute(requestInsert, new
                    {
                        item.Source,
                        item.Id,
                        item.Provider,
                        item.Link
                        ,
                        item.HeadLine,
                        item.DateTime,
                        item.Dateline,
                        item.ContactInfo,
                        item.English,
                        item.StoryCount,
                        item.Language,
                        item.MultiMediaCount,
                        item.StaticMediaCount
                    });
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteError(ex);
                }
            }


        }

        private string CheackRssFeed(RssFeed item)
        {
            List<string> errors = new List<string>();
            if (item.Source != null && item.Source.Length > 255)
            {
                errors.Add("Source: "+ item.Source);
                item.Source = item.Source.Remove(255);
            }

            if (item.Link != null && item.Link.Length > 900)
            {
                errors.Add("Link:" + item.Link);
                item.Link = item.Link.Remove(900);
            }

            if (item.Redirect != null && item.Link.Length > 500)
            {
                
                item.Redirect = item.Link.Remove(500);
            }

            if (errors.Any())
                return string.Format("Provider: {0} News: {1} {2}", item.Provider, item.Link, string.Join(" ", errors.ToArray()));
            
            return null;
        }

        public void Remove(RssFeed item)
        {
            throw new NotImplementedException();
        }

        public void Update(RssFeed item)
        {
            throw new NotImplementedException();
        }

        public RssFeed FindById(int id)
        {
            throw new NotImplementedException();
        }

        public int UpdateStoryCount(int count, bool? isEnglish, string language, string url)
        {
            const string requestUpdate = @" UPDATE [dbo].[RssFeed] SET [StoryCount] = @StoryCount, [English] = @English, Language = @Language  WHERE [Link] = @Link";
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                return db.Execute(requestUpdate, new { StoryCount = count, Link = url, Language = language, English = isEnglish });
            }
        }

        public bool AnyById(string id)
        {
            CountCount++;
            var asm = ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString;
            //using (var db = new NQDBEntities())
            //{
            //    try
            //    {
            //        var found = db.RssFeeds.Any(r => r.Link == id);
            //        return found;
            //    }
            //    catch(Exception ex)
            //    {
            //        Logger.Logger.WriteFatal(ex);
            //        return false;
            //    }
            //}
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    var found = db.ExecuteScalar<int?>(@"SELECT top 1 1 from  [dbo].[RssFeed] WITH(NOLOCK)  where [Link] = @link ", new { link = id }) > 0;
                    return found;
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return false;
                }
            }

        }

        public bool AnyById_New(string id)
        {
            CountCount++;

            //using (var db = new NQDBEntities())
            //{
            //    return db.RssFeeds.Any(r => r.Link == id);
            //}

            //return count > 0;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    var found = db.ExecuteScalar<int?>(@"SELECT top 1 1 from  [dbo].[RssFeed] WITH(NOLOCK)  where [Link] = @link ", new { link = id }) > 0;
                    return found;
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return false;
                }
            }

        }


        /// <summary>
        /// Get Index news by Provider
        /// </summary>
        /// <param name="startIndex">Start index position</param>
        /// <param name="endIndex">End index position</param>
        /// <param name="provider">News provider</param>
        /// <returns></returns>
        public int GetMaxNewsNumberById(int startIndex, int endIndex, string provider)
        {

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    var found = db.ExecuteScalar<int>(@"SELECT MAX(SUBSTRING( Link ,@StartIndex , LEN(Link) - @EndIndex)) 
                                                        FROM [dbo].[RssFeed]
                                                        WHERE Provider = @Provider ", new { StartIndex = startIndex, EndIndex = endIndex, Provider = provider });
                    return found;
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return 0;
                }
            }

        }

        public IEnumerable<RssFeed> Find(Expression<Func<RssFeed, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RssFeed> FindNotContact(string provider)
        {
            List<RssFeed> ret;

          // return new List<RssFeed>();

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    var str = string.Format(@" SELECT  r.*
  FROM [dbo].[RssFeed] as r
  inner join ParserNewswires n on n.NewsID = r.Link
  where [CompanyName]= 'or' and  r.Provider = '{0}'
 -- and r.DateTime >= '2016-05-25'                                   
 ", provider);

//                    var str = string.Format(@" select  * from RssFeed
//where
//Provider = '{0}' and
// cast(datetime as time) > '00:00:00.0000000'
//and datetime > '2016-03-17'
//                                           
// ", provider);

                    var str2 = string.Format(@" SELECT distinct r.* 
from [dbo].[RssFeed] r WITH(NOLOCK)
left join ParserNewswires n WITH(NOLOCK) on n.NewsID = r.Link 
    where r.Provider = '{0}'
and (n.CompanyName = '' or n.CompanyName is null )                                         
     and datetime >= CAST('2016-03-25 00:00:00.000' AS DateTime)
                                           
 ", provider);



//                    var str = string.Format(@" SELECT distinct r.* 
//from [dbo].[RssFeed] r WITH(NOLOCK)
//
//    where r.Provider = '{0}' and (StoryCount is null or StoryCount = 0)
//                                               
//     and datetime >= CAST('2016-03-18 00:00:00.000' AS DateTime)
//                                           
// ", provider);

                    ret = db.Query<RssFeed>(str).ToList();

                    //var ret2 = db.Query<RssFeed>(str2).ToList();
                    //if(ret2.Any())
                    //{
                    //    ret = ret.Union(ret2).ToList();
                    //}
                    /*var str = string.Format(@" SELECT * from [dbo].[RssFeed] r WITH(NOLOCK) 
                                               where [Provider] = '{0}' 
                                               and Link not like '%subscriber.newsquantified.com%'
                                               and (r.Dateline is null or r.Dateline ='')", provider);*/
                    //ret = db.Query<RssFeed>(str).ToList();
                    
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return null;
                }
            }
            return ret;
        }

        public IEnumerable<RssFeed> FindAll()
        {
            List<RssFeed> ret;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    ret = db.Query<RssFeed>(@"SELECT * FROM [dbo].[RssFeed] WITH(NOLOCK) ").ToList();

//                    ret = db.Query<RssFeed>(@" select *  from RssFeed with(nolock)
// where
//Dateline = '.'
//and English = 1
//and datetime >= CAST('2016-03-01 00:00:00.000' AS DateTime)  ").ToList();
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return null;
                }
            }
            return ret;
        }

        public RssFeed CreateRssByFileName(string p)
        {
            RssFeed rssFeed = null;

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                var strReq = string.Format(@"  SELECT  n.NewsKey,  n.[ResourceID], n.[Provider], n.ID, n.[HeadLine],  n.[PubTime] 
                                                                 ,m.[URL], s.Story
                                                                 from [dbo].[News] as n
                                                                 left join [dbo].[Moreover] as m on n.NewsKey = m.NewsKey
                                                                 LEFT JOIN Story s (NOLOCK) ON n.NewsKey = s.NewsKey
                                                               where 
                                                                 n.[ResourceID] = '{0}'", p);
             List<News> news = db.Query<News>(strReq).ToList();
                 News news1 = news.FirstOrDefault();
                 if(news1 != null)
                 {
                     rssFeed = new RssFeed();
                     if(news.Any( n => n.URL != null))
                     {
                         news1 = news.FirstOrDefault(n => n.URL != null);
                     }

                     rssFeed.Date = null;
                     if (news1 != null)
                     {
                         rssFeed.DateTime = news1.PubTime;
                         rssFeed.HeadLine = news1.HeadLine;
                         rssFeed.Id = news1.Id;
                         rssFeed.Link = news1.URL ?? string.Format("http://subscriber.newsquantified.com/NewsRelease/{0}/All", news1.NewsKey);
                         rssFeed.Provider = news1.Provider;
                     }
                     rssFeed.Source = "xml";
                     rssFeed.Time = null;
                 }
             }
            return rssFeed;
        }

        public IEnumerable<RssFeed> FindErrContact(string provider)
        {
            List<RssFeed> ret;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    
                    var str = string.Format(@" SELECT *  from [dbo].[RssFeed] WITH(NOLOCK) 
 where datetime >= CAST('2016-03-09 00:00:00.000' AS DateTime) 
and [Provider] = '{0}'
                                               ", provider);
                    ret = db.Query<RssFeed>(str).ToList();
                   

                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return null;
                }
            }
            return ret;
        }

        public IEnumerable<RssFeed> FindErrContact(string[] links)
        {
            List<RssFeed> ret;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {

                    var str = string.Format(@" SELECT *  from [dbo].[RssFeed] WITH(NOLOCK) 
 where Link in ({0})
                                               ", string.Join(", ", links));
                    ret = db.Query<RssFeed>(str).ToList();


                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return null;
                }
            }
            return ret;
        }

        public void FixDateLine(string start, string end)
        {
            List<RssFeed> ret;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {

                    var str = string.Format(@"SELECT *  from [dbo].[RssFeed] WITH(NOLOCK) 
 WHERE datetime >= CAST('{0}' AS DateTime)
and datetime < CAST('{1}' AS DateTime) and Provider <> 'Business Wire'
and (dateline LIKE '%January%' OR
dateline LIKE '%Jan.%' OR
dateline LIKE '%Feb.%' OR
dateline LIKE '%March%' OR
dateline LIKE '%Mar.%' OR
dateline LIKE '%April%' OR
dateline LIKE '%Apr.%' )
                                               ", start, end);

//                    var str = string.Format(@"SELECT *  from [dbo].[RssFeed] WITH(NOLOCK) 
// WHERE datetime >= CAST('{0}' AS DateTime)
//and datetime < CAST('{1}' AS DateTime) and Provider <> 'Business Wire'
//and (and (dateline is null or dateline = '' or dateline = '.')
//                                               ", start, end);

                    ret = db.Query<RssFeed>(str).ToList();

                    foreach(var r in ret)
                    {
                        if(string.IsNullOrEmpty(r.Dateline) || r.Dateline == ".")
                        {
                            if(r.Provider == "PR Web" )
                            {

                            }
                        }
                        else
                        {
                            r.Dateline = Utils.CatDataInDL(r.Dateline);
                            const string requestUpdate = @"update [dbo].[RssFeed] set Dateline = @Dateline, ContactInfo = @ContactInfo  where [Link] = @Link ";
                            // , new { link = item.Link, Dateline = item.Dateline }).FirstOrDefault() > 0;
                            db.Execute(requestUpdate, new
                            {
                                r.Link,
                                r.Dateline,
                                r.ContactInfo

                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                  
                }
            }
            
        }


        public IEnumerable<RssFeed> GetErrData(string start, string end, string provider)
        {
            List<RssFeed> ret;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {

                    var str = string.Format(@"SELECT Distinct *  from [dbo].[RssFeed] r WITH(NOLOCK) 
inner join ParserNewswires n on n.NewsID = r.Link
 WHERE r.datetime >= CAST('{0}' AS DateTime)
and r.datetime < CAST('{1}' AS DateTime) and Provider = '{2}'
and (r.dateline is null 
or r.dateline LIKE '<!%' OR
r.dateline LIKE 'Announces 2016 Outlook%' OR
r.dateline LIKE 'Agenda for the 2016 Event%' OR
r.dateline LIKE '.%' 
 OR
r.dateline LIKE ',%' 
or n.CompanyName like '%media contact%' or n.CompanyName like '%Follow us%')

                                               ", start, end, provider);

                    ret = db.Query<RssFeed>(str).ToList();

                    
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return null;
                }
            }
            return ret;
        }

        public bool AnyFeedItemById(string id)
        {
            CountCount++;
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    var found = db.ExecuteScalar<int>(@"SELECT count(*) from  [dbo].[RssFeed] WITH(NOLOCK)  where [Link] = @link ", new { link = id });
                    return found > 0;
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteFatal(ex);
                    return false;
                }
            }

        }
        public int UpdateDateTime(DateTime datetime, string url)
        {
            const string requestUpdate = @" UPDATE [dbo].[RssFeed] SET [DateTime] = @Datetime WHERE [Link] = @Link";
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                return db.Execute(requestUpdate, new { Datetime = datetime, Link = url });
            }
        }

    }
}
