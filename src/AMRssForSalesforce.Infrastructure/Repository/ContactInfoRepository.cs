﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using AMRssForSalesforce.Model;
using Dapper;
using System.Configuration;
using System.Globalization;
using System.Threading;
using AMRssForSalesforce.Infrastructure.Helpers;

namespace AMRssForSalesforce.Infrastructure.Repository
{
    public sealed class ContactInfoRepository : IRepository<Contact>, IDisposable
    {
        public static object Obforlock = new object();

        public void Delete(string newsID)
        {
            string quryDel = string.Format(@"DELETE from [dbo].[ParserNewswires] where [NewsID] = @NewsID ");
            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    db.Execute(quryDel, new
                    {

                        NewsID = newsID

                    });
                }
                catch (Exception ex)
                {
                  //  Logger.Logger.WriteFatal(ex);
                }
            }
        }

      
        public void Add(Contact item)
        {
            if (item == null)
            {
                Logger.Logger.WriteWarning("ContactInfoRepository - item Is null (void Add)");
                return;
            }
            item.CompanyName = Utils.NormCompanyName(item.CompanyName);
            item.Email = Utils.NormalizationEmail(item);

            string err = CheackRssFeed(item);

            if (!string.IsNullOrEmpty(err))
                Logger.Logger.WriteError(err);

            const string requestInsert = @" INSERT INTO [dbo].[ParserNewswires] ([Name],[Phone],[Email],[Url],[NewsID],[NewsReleaseDate],
                                                                                 [Source],[Type],[UncnounPart],[Error],[CompanyName],[Title],[LastUpdated],[RelateLinks],[Agency])
                                            VALUES (@Name, @Phone, @Email, @Url, @NewsID, @NewsReleaseDate, @Source, @Type, @UncnounPart, @Error, @CompanyName, @Title, @LastUpdated, @RelateLinks, @Agency)";


            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    string qury = string.Format(@"SELECT count(*) from [dbo].[ParserNewswires]  WITH(NOLOCK)  where [NewsID] = @NewsID and {0} and {1} and {2}",
                            item.Name == null ? "Name is null" : "[Name] = @Name",
                            item.Phone == null ? "Phone is null" : "[Phone] =@Phone",
                            item.Email == null ? "Email is null" : "[Email]=@Email");
                    var found = db.ExecuteScalar<int>(
                          qury
                        , new { NewsID = item.NewsID, Name = item.Name, Phone = item.Phone, Email = item.Email }, null, 30000000) > 0;

                    if (item.CompanyName != null && item.CompanyName.ToLower() == "the")
                        item.CompanyName = null;

                    if (string.IsNullOrEmpty(item.CompanyName) && !string.IsNullOrEmpty(item.Source))
                        item.CompanyName = item.Source;

                    DateTime LastUpdate = DateTime.Now;

                    if (!DateTime.TryParse(item.LastUpdated, out LastUpdate) || LastUpdate.Year < 2000)
                        item.LastUpdated = DateTime.Now.ToString();

                    if (!found)
                    {
                        db.Execute(requestInsert, new
                        {
                            Name = item.Name,
                            Phone = Utils.NormPhone2(item.Phone),
                            Email = item.Email,
                            Url = item.Url,
                            NewsID = item.NewsID,
                            NewsReleaseDate = item.NewsReleaseDate,
                            Source = string.IsNullOrEmpty(item.Source) ? item.CompanyName : item.Source,
                            Type = item.Type,
                            UncnounPart = item.UncnounPart,
                            Error = item.Error,
                            CompanyName = (string.IsNullOrEmpty(item.Source) || string.IsNullOrEmpty(item.CompanyName) ? item.CompanyName : (item.CompanyName.Contains(item.Source) ? item.Source : item.CompanyName)),
                            Title = item.Title,
                            LastUpdated = item.LastUpdated,
                            RelateLinks = item.RelateLinks,
                            Agency = Utils.IsAgency(item.Type, item.Source, item.CompanyName) ? "Yes" : "No"

                        });
                        Console.WriteLine((item.Email ?? item.Name ?? item.Phone) + " - Added");
                    }
                    else
                    {
                        Console.WriteLine((item.Email ?? item.Name ?? item.Phone) + " - NOT Added");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Logger.WriteError(ex);
                }
            }
          
        }


        public void Add_New(Contact item)
        {
            try
            {
                if (item == null)
                {
                    Logger.Logger.WriteWarning("ContactInfoRepository - item Is null (void Add)");
                    return;
                }
                item.CompanyName = Utils.NormCompanyName(item.CompanyName);
                item.Email = Utils.NormalizationEmail(item);

                string err = CheackRssFeed(item);

                if (!string.IsNullOrEmpty(err))
                    Logger.Logger.WriteError(err);

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                using (var db = new NQDBEntities())
                {
                    var found = db.ParserNewswires.Where(p => p.NewsID == item.NewsID);
                    if (string.IsNullOrEmpty(item.Name))
                    {
                        found = found.Where(p => string.IsNullOrEmpty(p.Name));
                    }
                    else
                    {
                        found = found.Where(p => p.Name == item.Name);
                    }
                    if (string.IsNullOrEmpty(item.Phone))
                    {
                        found = found.Where(p => string.IsNullOrEmpty(p.Phone));
                    }
                    else
                    {
                        found = found.Where(p => p.Phone == item.Phone);
                    }
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        found = found.Where(p => string.IsNullOrEmpty(p.Email));
                    }
                    else
                    {
                        found = found.Where(p => p.Email == item.Email);
                    }

                    if (item.CompanyName != null && item.CompanyName.ToLower() == "the")
                        item.CompanyName = null;

                    if (string.IsNullOrEmpty(item.CompanyName) && !string.IsNullOrEmpty(item.Source))
                        item.CompanyName = item.Source;

                    DateTime LastUpdate = DateTime.Now;

                    if (!DateTime.TryParse(item.LastUpdated, out LastUpdate) || LastUpdate.Year < 2000)
                    {
                        item.LastUpdated = DateTime.Now.ToString();
                        LastUpdate = DateTime.Now;
                    }
                        

                    DateTime NewsReleaseDate = DateTime.Now;
                    if (!DateTime.TryParse(item.NewsReleaseDate, out NewsReleaseDate) || NewsReleaseDate.Year < 2000)
                        item.NewsReleaseDate = DateTime.Now.ToString();


                    if (!found.Any())
                    {
                        db.ParserNewswires.Add(new ParserNewswire
                        {
                            Name = item.Name,
                            Phone = Utils.NormPhone2(item.Phone),
                            Email = item.Email,
                            Url = item.Url,
                            NewsID = item.NewsID,
                            NewsReleaseDate = NewsReleaseDate,
                            Source = string.IsNullOrEmpty(item.Source) ? item.CompanyName : item.Source,
                            Type = item.Type,
                            UncnounPart = item.UncnounPart,
                            Error = item.Error,
                            CompanyName = (string.IsNullOrEmpty(item.Source) || string.IsNullOrEmpty(item.CompanyName) ? item.CompanyName : (item.CompanyName.Contains(item.Source) ? item.Source : item.CompanyName)),
                            Title = item.Title,
                            LastUpdated = LastUpdate,
                            RelateLinks = item.RelateLinks,
                            Agency = Utils.IsAgency(item.Type, item.Source, item.CompanyName) ? "Yes" : "No",
                        });
                        db.SaveChanges();
                        Console.WriteLine(item.Email + " - Added");
                    }
                    else
                    {
                        Console.WriteLine(item.Email + " - NOT Added");
                    }

                }
            }
            catch(Exception ex)
            {
                Logger.Logger.WriteError(ex);
            }
        }

        public void Remove(Contact item)
        {
            throw new NotImplementedException();
        }

        public void Update(Contact item)
        {
            throw new NotImplementedException();
        }

        public Contact FindById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Contact> Find(Expression<Func<Contact, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Contact> FindAll()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private string CheackRssFeed(Contact item)
        {
            List<string> errors = new List<string>();
            if (item.Name != null && item.Name.Length > 250)
            {
                errors.Add("Name: "+ item.Name);
                item.Name = item.Name.Remove(250);
            }

            if (item.Phone != null && item.Phone.Length > 250)
            {
                errors.Add("Phone:" + item.Phone);
                item.Phone = item.Phone.Remove(250);
            }

            if (item.Email != null && item.Email.Length > 250)
            {
                errors.Add("Email:" + item.Email);
                item.Email = item.Email.Remove(250);
            }


            if (item.Url != null && item.Url.Length > 500)
            {
                errors.Add("Url:" + item.Url);
                item.Url = item.Url.Remove(500);
            }

            if (item.NewsID != null && item.NewsID.Length > 900)
            {
                errors.Add("NewsID:" + item.NewsID);
                item.NewsID = item.NewsID.Remove(900);
            }

            if (item.Source != null && item.Source.Length > 500)
            {
                errors.Add("Source:" + item.Source);
                item.Source = item.Source.Remove(500);
            }

            if (item.Type != null && item.Type.Length > 500)
            {
                errors.Add("Type:" + item.Type);
                item.Type = item.Type.Remove(500);
            }

            if (item.CompanyName != null && item.CompanyName.Length > 500)
            {
                errors.Add("CompanyName:" + item.CompanyName);
                item.CompanyName = item.CompanyName.Remove(500);
            }

            if (item.Title != null && item.Title.Length > 500)
            {
                errors.Add("Title:" + item.Title);
                item.Title = item.Title.Remove(500);
            }

            if (item.RelateLinks != null && item.RelateLinks.Length > 500)
            {
                errors.Add("RelateLinks:" + item.RelateLinks);
                item.RelateLinks = item.RelateLinks.Remove(500);
            }

          
            if (errors.Any())
                return string.Format("News: {0} {1}", item.NewsID, string.Join(" ", errors.ToArray()));
            
            return null;
        }

        public IEnumerable<RssProvidersAndContactCount> GetProvidersToday(DateTime date)
        {
            const string request = @"select [Provider]
, sum(case when [DateTime] >=  @param1 then 1 else 0 end) [CountRecords]
, sum(case when p.Phone is not null and [DateTime] >= @param1 then 1 else 0 end) PhoneCount
, sum(case when p.Email is not null and [DateTime] >= @param1 then 1 else 0 end) EmailCount
, sum(case when p.Name is not null and [DateTime] >=  @param1 then 1 else 0 end) NameCount
FROM [NQDB].[dbo].[RssFeed] r
left join [dbo].[ParserNewswires] p on p.NewsID = r.[Link]
group by [Provider]";

                

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["MainDataBase"].ConnectionString))
            {
                try
                {
                    return db.Query<RssProvidersAndContactCount>(request, new { param1 = date }).ToList();

  
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
            
       

        }
    }
}
