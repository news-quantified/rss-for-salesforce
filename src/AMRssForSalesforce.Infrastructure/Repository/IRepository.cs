﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Infrastructure.Repository
{
    public interface IRepository<T> where T : class
    {
        void Add(T item);
        void Remove(T item);
        void Update(T item);
        T FindById(int id);
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        IEnumerable<T> FindAll();
    }
}
