﻿using System;
using Serilog;

namespace AMRssForSalesforce.Infrastructure.Logger
{
    public  class Logger
    {
        static int countlog = 0;
        const int maxLog = 100;

        private static ILogger _logger = new LoggerConfiguration().WriteTo.ColoredConsole().WriteTo.File(@"C:\@Logs@\ScheduleApiLog.txt", fileSizeLimitBytes: null).CreateLogger();

        public static void WriteVerbose(Object message)
        {
            if (++countlog < maxLog)
                _logger.Verbose("Verbose message: {message}", message);
        }

        public static void WriteDebug(Object message)
        {
            if (++countlog < maxLog)
               _logger.Debug("Debug message: {message}", message);
        }

        public static void WriteInformation(Object message)
        {
            if (++countlog < maxLog)
               _logger.Information("Debug message: {message}", message);
        }

        public static void WriteWarning(Object message)
        {
            if (++countlog < maxLog)
               _logger.Warning("Warning message: {message}", message);
        }

        public static void WriteError(Object message)
        {
            if (++countlog < maxLog)
              _logger.Error("Error message: {message}", message);
        }

        public static void WriteFatal(Object message)
        {
            if (++countlog < maxLog)
              _logger.Fatal("Fatal  message: {message}", message);
        }
    }
}
