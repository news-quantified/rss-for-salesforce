﻿namespace AMRssForSalesforce.Infrastructure.Constants
{
    public class DictionariValue
    {
       public int ID { get; set;}
       public string Value { get; set; }
       public int Order { get; set; }
    }
}
