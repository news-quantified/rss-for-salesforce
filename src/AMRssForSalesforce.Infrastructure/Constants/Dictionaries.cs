﻿namespace AMRssForSalesforce.Infrastructure.Constants
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Dapper;
    using System;
    public class Dictionaries
    {
        public static string[] MarcerNoCompany = new string[] { "contact", "phone", "email", "website", "fax", "attorneys", "for quotes", "int'l", "toll free", "investor relations counsel", "in china", "in u.s.", "in europe", "tel", "please"
            , "Interviews", "can be obtained from", "e-mail","source", "unit","no", "no."  };

        public static string[] NotName = new string[] { "contact:", "phone:", "email:", "website:", "fax:", "contact for", "toll free" };

        public static string[] NotNames2 = new string[] {
            "toll free",
            "of",
"Contact",
"After",
"Building",
"Computershare",
"DAMPSKIBSSELSKABET",
"Holding",
"Entré",
"Great Dane",
"ISS",
"Haldor Topsoe",
"Nasdaq",
"SimCorp",
"SSBV-Rovsing",
"Ste",
"SUBTOTAL",
"Sydinvest",
"Administration",
"Topdanmark",
"Ste.",
"Class",
"Entrance",
"NY",
"Aravind",
"For",
"Institutional",
"Sales",
"Agence",
"Consultante",
"Diretor",
"Diretora",
"BASF",
"SAP",
"PR",
"ATI",
"CAS",
"ITS-Vahvistus",
"Labels",
"MaaS",
"Nasdaq",
"Savon",
"Solidium",
"Translink",
"Veljekset",
"Toronto",
"DIRECTLY",
"NY",
"PC",
"PE",
"PH",
"MU",
"Abp",
"ADC",
"ext.",
"Gelsenkirchener",
"BC",
"Suite",
"(s)",
"(A)",
"AB)",
"AB.",
"ASA",
"ZTE",
"Fax",
"ESO",
"Eng",
"Go",
"iOS",
"Apple",
"INN",
"IBM",	
"Skype.",
"Skype",
"BRIGHT",
"PLC",
"PMP",
"PMR",
"QNB",
"RMB",
"RTi",
"SSM",
"SP)",
"SKF",
"TMC",
"TFO",
"TBD",
"von",
"DHL",
"Express",
"VCG",
"OFFICIAL",
"FILM",
"SITE",
"site",
"only",
"Ont.",
"or",
"INFO",
"info",
"EEP",
"HSBC",
"Contact:",
"For",
"multi-modal",
"OR",
"Unternehmenskommunikation",
"AB",
"Infrastructure",
"Accountants",
"Performance",
"management",
"and",
"INFORMATIONM",
"UK", 
"Local",
"&",
"Rating",
"Agencies"
        };

        public static string[] removeFromName = new string[] { " (", " .", " #", " &", " @", " |", " *", " -", " —", ")", "&" };


        public static string[] NotEnglishCh = new string[] { "&#252;", "&#228;", "&#205;", "&#221;", "&#250;", "&#382;", "&#225;", "&#233;", "&#237;", "&#243;", "&#353;", "&#269;", "&#246;", "&#229;"
            , " een ", " aan ", " le ", " e " 
            , "à", "é", "è", "À", "Û", "û", "ü", "Ü" ,"Ö", "ö","Â", "â","Ã","ã","Ä", "ä","Å", "å","Ā", "ā","Ă","ă", "ä"
            , "ž", "ū", "ī", "Š", "ų", "ą", "į" , "ė", "š"};

        public static string[] NotEnglish = new string[] { "portavoz de prensa", "teléfono", "página web", "tél.", "informácie", "inform&#225;cie" };

        public static string[] MarcerName = new string[] { "Mr.", "Mrs." };

        public static string[] MarkerContact = new string[] { "contact", "refer to", "please call", "get in touch:", "or call", "media inquiries", "@", "web:", "email:", "website:", "e-mail:"
            , "please email", "Further information", "simply call", "llame al", "llamar al", "para ver el formulario", "para una notificación", "for more information", "hotline", "contact person:" };

        private static string[] _marcerCompany = new string[] { "PLLC","Inc.", "co.", "LLC", "LCD", "LLP", "Corp.", "d/b/a", "N.A.", "LTD", "Corporation", "LP", "Limited", "Company", "LTD", "LTD.", "Consulting", "&", "L.L.P.", "SportsNet", "s.a.", "Group", "GK", "Biomedical" };

        private static string[] _markerSource = new string[] { "Source:", "SOURCE" };

        private static string[] _separate = new string[] { ", ", " or ", " / ", ";", ":", "|", " for ", "/"," at " };

        public static string[] MarkerStartContact = new string[] { "contact", "get in touch", "media inquiries", "further information", "contacts", "for more information" };

        private static string[] _agencyType = new string[]{
            "EM", "Enterprise Marketing",
            "PR Bureau",
            "PR", "Public Relations",
            "AR", "Analyst Relations",
            "IR", "Investor Relations",
            "CM", "Communications Manager",
            "Agency",
            "Strategy Group",
            "Business Communications",
            "Marketing",
            "Communications",
            "Communications Group",
            "Advertising",
            "Media Relations",
            "Media",
            "Media Agency",
            "Creative Agency",
            "Investor",
            "Group Communications",
            "Media relations agency",
            "Investor Relations Contact"
        };

        public static string[] MarcerAdress = new string[] { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
            "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV",
            "WI", "WY", "Street", "STE", "SUITE", "Box", "Apt", "Apartment", "Rd", "Road", "Way", "AVE", "avenue", "BLVD", "BOULEVARD", "CIR", "CIRCLE", "CRK", "CREEK", "DR", "DRIVE",
            "FL", "FLOOR", "IS", "ISLAND", "LN", "LANE", "PKWY", "PARKWAY", "PL", "PLACE", "TRL", "TRAIL", "VLY", "VALLEY", "VW", "VLG", "VIEW", "VILLAGE", "WAY" };

        public static string[] AgencyType
        {
            get
            {
               return _agencyType.OrderByDescending(a => a.Length).ToArray();
            }
        }

        private static Object thisLock = new Object();

        public static string[] Separate
        {
            get
            {
                return _separate;
            }
        }

        private static List<string> marcerNoType = null; 
        public static List<string> MarcerNoType
        {
            get
            {
                if (marcerNoType != null)
                {
                  return marcerNoType;
                }
                Init();
                return marcerNoType;
            }
        }

        public static string[] MrkerSource
        {
            get
            {
                return _markerSource;
            }
        }

        public static string[] MarcerCompany
        {
            get
            {
                return _marcerCompany;
            }
        }

        private static List<string> titles = null; 
        public static List<string> Titles
        {
            
            get
            {
             if(titles != null) return titles;
             Init();
             return titles;
          }
        }
        private static List<string> marcerNonName = null;

        public static List<string> MarcerNoName
        {
            get
            {
                if(marcerNonName != null) return marcerNonName;
                Init();
                return marcerNonName;
            }
        }

        private static List<string> types = null;
        public static List<string> Types
        {
            get
            {
                if (types != null)
                {
                    return types;
                }
                //TODO Why Exception?
                Init();
                return types;
               
            }
        }


        //TODO Why Init, why not default static constructor
        public static bool Init()
        {
            try
            {
                //Todo please fix me(change to config file)
                using (var db = new SqlConnection("Data Source=nq-data.com; Initial Catalog=ParserProject; User ID=NQ_Azure;Password=Yu89-nu23_2324-tta*PP"))
                {
                    var list = db.Query<DictionariValue>(@"SELECT * FROM [dbo].[ParserTyps]  WITH(NOLOCK)").OrderByDescending(d => d.Value.Length).ToList();
                    types = list.Select(l => l.Value).ToList();
                    var listNn = db.Query<DictionariValue>(@"SELECT *  FROM [dbo].[ParserNotNames]  WITH(NOLOCK)").ToList();

                    marcerNonName = listNn.Select(l => l.Value).ToList();

                    var listTit = db.Query<DictionariValue>(@"SELECT * FROM [dbo].[ParserTitles] WITH(NOLOCK) order by [Order] desc").OrderByDescending(d => d.Value.Length).ToList();

                    titles = listTit.Select(l => l.Value).ToList();

                    var listNoType = db.Query<DictionariValue>(@"SELECT * FROM [dbo].[ParserNotTypes]  WITH(NOLOCK)").ToList();

                    marcerNoType = listNoType.Select(l => l.Value).ToList();
                }
                return true;
            }
            catch(Exception ex)
            {
                Logger.Logger.WriteFatal("Dictionaries wasn't init");
                return false;
            }

        }
        
    }
}
