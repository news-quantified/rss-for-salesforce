﻿using AMRssForSalesforce.RssReader.Creator;
using AMRssForSalesforce.RssReader.Reloader;

namespace AMRssForSalesforce.CheckMissingRecord
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using NTextCat;
    using AMRssForSalesforce.Infrastructure.Repository;
    using AMRssForSalesforce.Infrastructure.Logger;
    class Program
    {
        private static RankedLanguageIdentifier _indicator;
        static void Main(string[] args)
        {
            if (
                System.Diagnostics.Process.GetProcessesByName(
                    System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location))
                    .ToList()
                    .Count() > 1) return;



            /*#if (!DEBUG)
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            #endif*/

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Infrastructure.Constants.Dictionaries.Init();
            var factory = new RankedLanguageIdentifierFactory();
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\Wiki82.profile.xml";
            _indicator = factory.Load(path);
            try
            {
                var rssTask = new List<Task>()
                {
                    Task.Factory.StartNew(StartAccessWireRssParser),
                    Task.Factory.StartNew(StartMarketwiredRssParser),
                };
                Task.WaitAll(rssTask.ToArray());
            }

                      
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
            #if (DEBUG)
                Console.ReadKey();
            #endif
            Logger.WriteInformation(string.Format("Check mising record Count: {0}", RssFeedRepository.CountCount));
            Logger.WriteInformation(string.Format("Check mising record Insert: {0}", RssFeedRepository.InsertCount));
        }

        private static void StartAccessWireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var checkAccessWirePages = new CheckAccessWirePages(_indicator);
            checkAccessWirePages.ChekCurrentPages();
        }
        private static void StartMarketwiredRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var checkMarketWiredPages = new CheckMarketWiredPages(_indicator);
            checkMarketWiredPages.ChekCurrentPages();
        }
    }
}
