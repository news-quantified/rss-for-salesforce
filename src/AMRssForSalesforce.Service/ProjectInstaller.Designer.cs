﻿namespace AMRssForSalesforce.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rssParserServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.RssParserServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // rssParserServiceProcessInstaller
            // 
            this.rssParserServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.rssParserServiceProcessInstaller.Password = null;
            this.rssParserServiceProcessInstaller.Username = null;
            // 
            // RssParserServiceInstaller
            // 
            this.RssParserServiceInstaller.Description = "Parse RssFeed";
            this.RssParserServiceInstaller.DisplayName = "RssParserService";
            this.RssParserServiceInstaller.ServiceName = "RssParserService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.rssParserServiceProcessInstaller,
            this.RssParserServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller rssParserServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller RssParserServiceInstaller;
    }
}