﻿using NTextCat;

namespace AMRssForSalesforce.Service
{
    using System;
    using System.Configuration;
    using System.ServiceProcess;
    using System.Threading;
    using System.Threading.Tasks;
    using RssReader.Creator;
    using System.Globalization;

    public partial class RssParserService : ServiceBase
    {
        private Timer _schedular;
        private RankedLanguageIdentifier _indicator;
        public RssParserService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ScheduleService();
        }

        protected override void OnStop()
        {
            _schedular.Dispose();
        }

        public void ScheduleService()
        {
            _schedular = new Timer(SchedularCallback);
            var mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
            var scheduledTime = DateTime.MinValue;
            if (mode == "DAILY")
            {
                scheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["ScheduledTime"]);
                if (DateTime.Now > scheduledTime)
                {
                    scheduledTime = scheduledTime.AddDays(1);
                }
            }
            else if (mode.ToUpper() == "INTERVAL")
            {

                var intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                if (DateTime.Now > scheduledTime)
                {
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                }
            }

            var timeSpan = scheduledTime.Subtract(DateTime.Now);
            var dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
            _schedular.Change(dueTime, Timeout.Infinite);
        }
        private void SchedularCallback(object e)
        {
            var factory = new RankedLanguageIdentifierFactory();
            _indicator = factory.Load(System.IO.Directory.GetCurrentDirectory() + "\\Core14.profile.xml");
            try
            {
                Infrastructure.Constants.Dictionaries.Init();
                Task taskStartCanadaNewswire = Task.Factory.StartNew(StartCanadaNewswireRssParser);
                Task taskAccessWireRssParser = Task.Factory.StartNew(StartAccessWireRssParser);
                Task taskGlobeNewswireRssParsercs = Task.Factory.StartNew(StartGlobeNewswireRssParsercs);
                Task taskBusinessWireRssParsercs = Task.Factory.StartNew(StartBusinessWireRssParsercs);
                Task taskMarketwiredRssParser = Task.Factory.StartNew(StartMarketwiredRssParser);
                Task taskPRnewswireNewsRssParser = Task.Factory.StartNew(StartPRnewswireNewsRssParser);
                Task taskPrWebRssParser = Task.Factory.StartNew(StartPrWebRssParser);
            }
            finally
            {
                ScheduleService();
            }
        }

        private void StartPrWebRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var pRWebRssParser = new PRWebRssParser(_indicator);
            pRWebRssParser.Start();

        }

        private void StartCanadaNewswireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var canadaNewswireRssParser = new CanadaNewswireRssParser(_indicator);
            canadaNewswireRssParser.Start();
        }

        private void StartAccessWireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var accessWireRssParser = new AccessWireRssParser(_indicator);
            accessWireRssParser.Start();
        }

        private void StartGlobeNewswireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var globeNewswireRssParsercs = new GlobeNewswireRssParsercs(_indicator);
            globeNewswireRssParsercs.Start();
        }


        private void StartBusinessWireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var businessWireRssParsercs = new BusinessWireRssParsercs(_indicator);
            businessWireRssParsercs.Start();

        }

        private void StartMarketwiredRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var marketwiredRssParser = new MarketwiredRssParser(_indicator);
            marketwiredRssParser.Start();
        }


        private void StartPRnewswireNewsRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var prNewswireNewsRssParser = new PRnewswireNewsRssParser(_indicator);
            prNewswireNewsRssParser.Start();
        }
    }
}
