﻿namespace TestApplication
{
    using AMRssForSalesforce.Infrastructure.Logger;
    using AMRssForSalesforce.Infrastructure.Repository;
    using AMRssForSalesforce.RssReader.Creator;
    using AMRssForSalesforce.RssReader.Reloader;
    using NTextCat;
    using System;
    using System.Globalization;
    using System.Threading;

    class Program
    {
        static bool  started = false;
        private static RankedLanguageIdentifier _indicator;


        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
           
            
            //AMRssForSalesforce.Infrastructure.Constants.Dictionaries.Init();
            
            
            var factory =  new RankedLanguageIdentifierFactory();
            //_indicator = factory.Load(@"C:\Users\sergey\@Repository@\@RssParser@\TestApplication\bin\Release\Core14.profile.xml");
            _indicator = factory.Load(@"C:\@RssScheduleApplication@\Release\Core14.profile.xml");
            //CheckAccessWirePages checkAccessWirePages = new CheckAccessWirePages(_indicator);
            //checkAccessWirePages.ChekCurrentPages();

            //CheckMarketWiredDateTime checkMarketWiredDateTime = new CheckMarketWiredDateTime();
            //checkMarketWiredDateTime.ChekCurrentPages();

           StartGlobeNewswireRssParsercs();

           // StartMarketwiredRssParser();

            //StartPRnewswireNewsRssParser();

           // StartAccessWireRssParser();


            //StartCanadaNewswireRssParser();
            //StartPrWebRssParser();

            //StartBusinessWireRssParsercs();

            Logger.WriteInformation(string.Format("Count: {0}",RssFeedRepository.CountCount));
            Logger.WriteInformation(string.Format("Insert: {0}",RssFeedRepository.InsertCount));
            Console.ReadKey();
        }

        private static void StartCanadaNewswireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var canadaNewswireRssParser = new CanadaNewswireRssParser(_indicator);
            canadaNewswireRssParser.Start();
        }

        private static void StartAccessWireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var accessWireRssParser = new AccessWireRssParser(_indicator);
            accessWireRssParser.Start();
        }

        private static void StartGlobeNewswireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var globeNewswireRssParsercs = new GlobeNewswireRssParsercs(_indicator);
            globeNewswireRssParsercs.Start();
        }


        private static void StartBusinessWireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var businessWireRssParsercs = new BusinessWireRssParsercs(_indicator);
            businessWireRssParsercs.Start();
        }

        private static void StartPrWebRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var pRWebRssParser = new PRWebRssParser(_indicator);
            pRWebRssParser.Start();
        }

        private static void StartMarketwiredRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var marketwiredRssParser = new MarketwiredRssParser(_indicator);
            marketwiredRssParser.Start();
        }


        private static void StartPRnewswireNewsRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var prNewswireNewsRssParser = new PRnewswireNewsRssParser(_indicator);
            prNewswireNewsRssParser.Start();
        }
    }
}
