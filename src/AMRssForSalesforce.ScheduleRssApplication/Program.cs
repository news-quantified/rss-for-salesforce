﻿using AMRssForSalesforce.Infrastructure.Logger;

namespace AMRssForSalesforce.ScheduleRssApplication
{
    using System.Globalization;
    using System.Threading;
    using RssReader.Creator;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Infrastructure.Helpers;
    using System.Configuration;
    using System;
    using NTextCat;
    using AMRssForSalesforce.Infrastructure.Repository;
    class Program
    {

        /*[DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);*/

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        private static RankedLanguageIdentifier _indicator;

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Input argument not found");
                return;
            }
            int i = 0;

            //----------------------is it? 
            //bool existed;

            //string guid = args[0];
           
            //Console.WriteLine(guid);

            //Mutex mutexObj = new Mutex(true, guid, out existed);

            //if (!existed)
            //{
            //    Console.WriteLine("Parser is started");
            //    return;
            //}
            //----------------------------------
            while(! Infrastructure.Constants.Dictionaries.Init() && i<10)
            {
                i++;
                Thread.Sleep(200);
            }
            var factory = new RankedLanguageIdentifierFactory();
            try
            {
               

                //var parserValueState = AppSettings.Get<bool>(args[0]);
                //if (parserValueState)
                //{
                //    Console.WriteLine("Parser is started");
                //    return;
                //}
                var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) +
                           @"\RankedLanguage";
                Console.WriteLine(path);
                Console.WriteLine(String.Format("Start Parameter - {0}", args[0]));
                var rssTask = new List<Task>();
                switch (args[0])
                {

                    case "CanadaNewswireRssParserState":
                        _indicator = factory.Load(String.Format(@"{0}\CanadaNewswireRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartCanadaNewswireRssParser));
                        Console.WriteLine("Started CanadaNewswireRssParser process");
                        break;
                    case "AccessWireRssParserState":
                        _indicator = factory.Load(String.Format(@"{0}\AccessWireRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartAccessWireRssParser));
                        Console.WriteLine("Started AccessWireRssParser process");
                        break;
                    case "GlobeNewswireRssParsercsState":
                        _indicator = factory.Load(String.Format(@"{0}\GlobeNewswireRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartGlobeNewswireRssParsercs));
                        Console.WriteLine("Started GlobeNewswireRssParsercs process");
                        break;
                    case "BusinessWireRssParsercsState":
                        _indicator = factory.Load(String.Format(@"{0}\BusinessWireRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartBusinessWireRssParsercs));
                        Console.WriteLine("Started BusinessWireRssParsercs process");
                        break;
                    case "PRWebRssParserState":
                        _indicator = factory.Load(String.Format(@"{0}\PrWebRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartPrWebRssParser));
                        Console.WriteLine("Started PrWebRssParser process");
                        break;
                    case "MarketwiredRssParserState":
                        _indicator = factory.Load(String.Format(@"{0}\MarketwiredRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartMarketwiredRssParser));
                        Console.WriteLine("Started MarketwiredRssParser process");
                        break;
                    case "PRnewswireNewsRssParserState":
                        _indicator = factory.Load(String.Format(@"{0}\PRnewswireNewsRssParser\Wiki82.profile.xml", path));
                        rssTask.Add(Task.Factory.StartNew(StartPRnewswireNewsRssParser));
                        Console.WriteLine("Started PRnewswireNewsRssParser process");
                        break;
                }
                //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //config.AppSettings.Settings[args[0]].Value = true.ToString();
                //config.Save(ConfigurationSaveMode.Modified);
                //ConfigurationManager.RefreshSection("appSettings");

                Task.WaitAll(rssTask.ToArray());

                //config.AppSettings.Settings[args[0]].Value = false.ToString();
                //config.Save(ConfigurationSaveMode.Modified);
                //ConfigurationManager.RefreshSection("appSettings");

            }
            catch (Exception exc)
            {
                //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //config.AppSettings.Settings[args[0]].Value = false.ToString();
                //config.Save(ConfigurationSaveMode.Modified);
                //ConfigurationManager.RefreshSection("appSettings");
                //Console.WriteLine(exc);
                Logger.WriteError(exc);
            }
            finally
            {
               // Logger.WriteInformation(String.Format("End Parser - {0}: {1}", args[0], DateTime.Now));
                Logger.WriteInformation(string.Format("Count: {0}", RssFeedRepository.CountCount));
                Logger.WriteInformation(string.Format("Insert: {0}", RssFeedRepository.InsertCount));
            }
            #if (DEBUG)
                //Console.ReadKey();
            #endif
        }
        private static void StartCanadaNewswireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var canadaNewswireRssParser = new CanadaNewswireRssParser(_indicator);
            canadaNewswireRssParser.Start();
        }

        private static void StartAccessWireRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var accessWireRssParser = new AccessWireRssParser(_indicator);
            accessWireRssParser.Start();
        }

        private static void StartGlobeNewswireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var globeNewswireRssParsercs = new GlobeNewswireRssParsercs(_indicator);
            globeNewswireRssParsercs.Start();
        }


        private static void StartBusinessWireRssParsercs()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var businessWireRssParsercs = new BusinessWireRssParsercs(_indicator);
            businessWireRssParsercs.Start();

        }

        private static void StartPrWebRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var pRWebRssParser = new PRWebRssParser(_indicator);
            pRWebRssParser.Start();

        }

        private static void StartMarketwiredRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var marketwiredRssParser = new MarketwiredRssParser(_indicator);
            marketwiredRssParser.Start();
        }


        private static void StartPRnewswireNewsRssParser()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var prNewswireNewsRssParser = new PRnewswireNewsRssParser(_indicator);
            prNewswireNewsRssParser.Start();
        }
        
    }
}
