﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using AMRssForSalesforce.Infrastructure.Repository;
using System.Net;
using System.Net.Mail;

namespace CheckRssData
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder errors = new StringBuilder();
            var appSettings = ConfigurationManager.AppSettings;
            var excludeProviders = appSettings["excludeProviders"].Split(';');
            var excludeEmails = appSettings["excludeEmails"].Split(';');

            var contactRep = new ContactInfoRepository();
            try
            {
                var date = DateTime.Now.Hour > 16 ? DateTime.Now.Date : DateTime.Now.AddDays(-1).Date;
                var providers = contactRep.GetProvidersToday(date);
                if(providers == null || !providers.Any())
                {
                    SendLogingInfo("RSS feeds parser for Business Wire error!", "Didn't finde any providers!");
                }

                foreach(var p in providers)
                {
                    if(excludeProviders.Contains(p.Provider))
                        continue;
                    if(p.CountRecords == 0)
                    {
                        errors.Append(string.Format("{0} doesn't  have any contacts during {1}. <br/>", p.Provider, date.ToShortDateString()));
                    }
                    else
                    {
                        if(p.EmailCount == 0 && !excludeEmails.Contains(p.Provider))
                            errors.Append(string.Format("{0} doesn't  have any contacts with E-mail during {1}. <br/>", p.Provider, date.ToShortDateString()));

                        if (p.PhoneCount == 0 )
                            errors.Append(string.Format("{0} doesn't  have any contacts with Phone number during {1}. <br/>", p.Provider, date.ToShortDateString()));

                        if (p.NameCount == 0)
                            errors.Append(string.Format("{0} doesn't  have any contacts with Name during {1}. <br/>", p.Provider, date.ToShortDateString()));
                    }
                }
                if(errors.Length > 0)
                    SendLogingInfo("RSS feeds parser for Business Wire error!", errors.ToString());
                else
                    SendLogingInfo("RSS feeds parser is OK", DateTime.Now.ToString(), true);

            }
            catch(Exception ex)
            {
                SendLogingInfo("RSS feeds parser for Business Wire error!", ex.ToString());
            }
            
        }


        private static void SendLogingInfo(string subject, string body, bool info=false)
        {
            try
            {
                var loginInfo = new NetworkCredential("system@newsquantified.com", "lpwd1986");
                var msg = new MailMessage();
                var smtpClient = new SmtpClient("smtp.gmail.com", 587);
                msg.From = new MailAddress("system@newsquantified.com");

                if (!info)
                {
                    msg.To.Add(new MailAddress("jay@newsquantified.com"));
                    msg.To.Add(new MailAddress("dan@newsquantified.com"));
                    //msg.To.Add(new MailAddress("anna@newsquantified.com"));
                    msg.To.Add(new MailAddress("oliver@newsquantified.com"));
                }
                msg.To.Add(new MailAddress("anna.amac@newsquantified.com"));
                msg.To.Add(new MailAddress("anastasia@newsquantified.com"));
                msg.To.Add(new MailAddress("sergei.arienchuk@newsquantified.com"));
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;

                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = loginInfo;
                smtpClient.Send(msg);



            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }
    }
}
