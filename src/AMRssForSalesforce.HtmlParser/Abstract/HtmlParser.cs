﻿using AMRssForSalesforce.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Threading;
using HtmlAgilityPack;
using AMRssForSalesforce.Infrastructure.Helpers;
using AMRssForSalesforce.Infrastructure.Constants;
using AMRssForSalesforce.Infrastructure.Logger;
using System.Text.RegularExpressions;

namespace AMRssForSalesforce.HtmlParser.Abstract
{
    public abstract class HtmlParser
    {
        protected string urlHtml;
        protected string htmlContent;
        protected string Source;
        protected List<string> companyNames;
        protected List<string> domains;
        protected List<string> domainscomplit;
        protected string Title;
        public bool hasContent { get { return !string.IsNullOrEmpty(htmlContent); } }


        public HtmlParser(string _url, Func<string, ResponseResult> GetResponseFunc)
        {
            urlHtml = _url;
            var res = GetResponseFunc(urlHtml);
            if (res != null)
            {
                htmlContent = res.Html;
                this.responsUri = res.Redirect;
            }

            if (hasContent)
            {
                try
                {
                    GetAllDomains();
                    companyNames = GetCompanyNames();
                    Title = GetTitle();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public HtmlParser(string _url): this(_url, x => HtmlHelpers.GetResponse(x)) { }

        private string GetTitle()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var title = doc.DocumentNode.SelectSingleNode("//title");
            if (title != null)
                return Utils.NormalizationString(title.InnerText);

            return null;
        }

  

        public HtmlParser(string _fileName, bool fromFile)
        {
            urlHtml = _fileName;
            htmlContent = File.ReadAllText(_fileName);

            if (hasContent)
            {
                companyNames = GetCompanyNames();
                GetAllDomains();
            }
        }

        private void GetAllDomains()
        {
            domains = new List<string>();
            domainscomplit = new List<string>();

            var emails = RegExp.getEmails(htmlContent);

            foreach(var email in emails)
            {
                var parts = email.Split('@');
                if(parts.Count() == 2)
                {
                    var parts2 = parts[1].Split('.');
                    if(parts2.Count() >= 2)
                    {
                        if (parts2[0].Length >= 2 && !domains.Any(d => d == parts2[0]) )
                            domains.Add(parts2[0]);
                    }
                    domainscomplit.Add(parts[1]);
                }
            }
            var urls = RegExp.getUrls(RegExp.GetInnerText2(htmlContent));

            foreach(var url in urls)
            {
                try
                {
                    var _url = url.Contains("http://") || url.Contains("https://") ? url : "http://" + url;

                    Uri myUri = new Uri(_url);
                    string host = myUri.Host;

                    var parts = host.Split('.');
                    domainscomplit.Add(host);
                    int i = 0;
                    foreach (var p in parts)
                    {
                        if ((p.Length >= 4 || i<2 && p.ToLower()!="www") && !domains.Any(d => d == p) && p != "fonts")
                            domains.Add(p);
                        i++;
                    }
                }
                catch(Exception ex)
                {

                }
            }
           
        }

        protected bool IsCompanyName(string val)
        {
                          //IsCompanyName(string val, List<string> domains, List<string> companyNames, string Source, string Title)
            return Utils.IsCompanyName(val, this.domains, this.companyNames, this.Source, this.Title);
        }

        public abstract Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed);

        public abstract List<HtmlNode> GetContactContent();

        public bool NodeContentEmailOrPhone(HtmlNode node)
        {
            var ass = node.SelectNodes(".//a");

            if (ass != null)
            {
                foreach (var a in ass)
                {
                    if (a.Attributes["href"] != null && a.Attributes["href"].Value.Contains("mailto:"))
                        return true;
                }

                if (node.InnerText.ToLower().Contains("web:") || node.InnerText.ToLower().Contains("website:") || node.InnerText.ToLower().Contains("site:"))
                    return true;
            }

            if (RegExp.GetPnoneNumbers(RegExp.GetInnerText(node.OuterHtml)).Any())
                return true;

            if (RegExp.getEmails(node.InnerText).Any())
                return true;

            if (node.InnerText.ToLower().Contains("name:") )
                return true;

            if (node.InnerText.ToLower().Contains("tel:"))
                return true;

            return false;
        }

        public HtmlNode GetFirstEmailNode(HtmlNode node)
        {
            var ass = node.SelectNodes(".//a");

            if (ass == null)
                return null;
            foreach (var a in ass)
            {
                if (a.Attributes["href"] != null && a.Attributes["href"].Value.Contains("mailto:"))
                    return a;
            }

            return null;
        }

        public abstract List<Contact> GetContacts(HtmlNode nodeContact);

        public virtual string GetRelatedLink(Contact contact)
        {
            List<string> urls = new List<string>();
            if (!string.IsNullOrEmpty(contact.Email))
            {
                string[] partsEmail = contact.Email.Split(',');
                foreach (var email in partsEmail)
                {
                    string[] parts = email.Split('@');
                    var url = parts[1];
                    if (!urls.Any(u => u == url))
                        urls.Add(url);


                }
                if (urls.Any())
                    return string.Join(", ", urls.ToArray());
            }
            string company = string.IsNullOrEmpty(contact.CompanyName) ? this.Source : contact.CompanyName;

            if (!string.IsNullOrEmpty(company) && this.domainscomplit.Any())
            {
                string url = null;

                foreach (var d in domainscomplit)
                {
                    var partOfDoains = d.Split('.');
                    for(var i = 0; i< partOfDoains.Count()-1; i++)
                    {
                        if (partOfDoains[i].ToLower() == "www")
                            continue;
                        if (partOfDoains[i].ToLower().Contains(company.ToLower()) || company.ToLower().Contains(partOfDoains[i].ToLower()))
                            return d;
                    }
                }
            }

            return null;
        }

        public abstract string GetRelatedLink();

        public abstract string GetSource();

        public virtual bool NoEnglish()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var countP = 0;
            var countPnoEnglish = 0;
            var allNoEnglish = 0;

            var ps = doc.DocumentNode.SelectNodes("//p");
            if (ps == null)
                return true;
            var pres = doc.DocumentNode.SelectNodes("//pre");

            if(pres != null)
            {
                foreach(var pre in pres)
                {
                    ps.Add(pre);
                }
            }

            foreach (var p in ps)
            {
              countP++;
                int countNoEnglish = 0;

                foreach (var s in Dictionaries.NotEnglishCh)
                {
                    int n = p.InnerText.IndexOf(s);
                    while (n != -1)
                    {
                        countNoEnglish++;
                        n = p.InnerText.IndexOf(s, n + s.Length);
                    }
                }
                if (countNoEnglish > 0)
                {
                    countPnoEnglish++;
                    allNoEnglish += countNoEnglish;
                }

                foreach (var ch in p.InnerText)
                {
                    if (ch >= (char)12000)
                        countNoEnglish++;

                    if (countNoEnglish > 5)
                         return true;
                }
            }

            if (countPnoEnglish * 100 / countP >= 50 || allNoEnglish >= 10)
                return true;

            return false;
        }

        protected List<string> GetCompanyNames()
        {
            List<string> _companyNames = new List<string>();
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.ChildNodes != null && p.ChildNodes.Any())
                {
                    foreach (var ch in p.ChildNodes)
                    {
                        var text = Utils.NormalizationString(ch.InnerText);
                        if (text.IndexOf("About ") == 0 && text.Length < 150)
                        {
                            var cmpanyName1 = Utils.NormalizationString(text.Remove(0, "About ".Length));
                            if (cmpanyName1.Length >= 2)
                                _companyNames.Add(cmpanyName1);
                        }
                        else if(ch.Name == "b" && !string.IsNullOrEmpty(text))
                        {
                            if(IsCompanyName(text))
                            {
                                _companyNames.Add(text);
                            }
                        }
                    }
                }
                else
                {
                    var text = Utils.NormalizationString(p.InnerText);
                    if (text.IndexOf("About ") == 0 && text.Length < 150)
                    {
                        var cmpanyName1 = Utils.NormalizationString(text.Remove(0, "About ".Length));
                        if (cmpanyName1.Length >= 2)
                            _companyNames.Add(cmpanyName1);
                    }
                }
            }

            return _companyNames.Distinct().ToList();
        }

        protected void GetContactNameTypeTitleAthterPhone(Contact contact, string p)
        {
            if (string.IsNullOrEmpty(p))
                return;

            var phone = Utils.NormPhone(p);

            if (string.IsNullOrEmpty(phone))
                return;

            if (!p.Contains(phone))
                return;

            var afterPthone = Utils.NormalizationString(p.Replace(phone, " ").Trim());

            if (string.IsNullOrEmpty(afterPthone))
                return;

            afterPthone = afterPthone.Replace("Toll Free Tel:", "").Replace("Phone:", "").Replace("Telephone:", "").Replace("Tel:.", "").Replace("Tel:", "").Replace("Tel", "").Replace("PH:", "").Replace("F:", "").Replace("PH.", "").Replace("Ph.", "").Replace("Email", "").Replace("P:", "").Replace("T:", "").Replace("T.", "").Replace("mobile", "");


            if (afterPthone.Length < 3)
                return;

            var emails = RegExp.getEmails(afterPthone);
            if (emails.Any())
            {
                contact.Email = string.Join(", ", emails);
                foreach (var email in emails)
                {
                    afterPthone = afterPthone.Replace(email, " ");
                }
            }

            var subStrings = afterPthone.Split(new string[] { "<br />", "(", ")", ":" }, StringSplitOptions.RemoveEmptyEntries);

            
            foreach(var s in subStrings)
            {
                var title = Utils.GetTitle(s);
                if(!string.IsNullOrEmpty(title))
                {
                    contact.Title = title;
                    continue;
                }

                var type = Utils.FindType(s);
                if (!string.IsNullOrEmpty(type))
                {
                    contact.Type = type;
                    continue;
                }

                if(IsCompanyName(s))
                {
                    contact.CompanyName = s;
                    continue;
                }

                if(RegExp.IsName(s))
                {
                 contact.Name = s;
                }
            }
            
        }
        protected void GetContactNameTypeTitleAthterPhone(List<string> names, List<string> types, List<string> titles, string p)
        {
            if (string.IsNullOrEmpty(p))
                return;

            var phone = Utils.NormPhone(p);

            if (string.IsNullOrEmpty(phone))
                return;

            if (!p.Contains(phone))
                return;

            var afterPthone = Utils.NormalizationString(p.Replace(phone, " ").Trim());

            if (string.IsNullOrEmpty(afterPthone))
                return;

            afterPthone = afterPthone.Replace("Toll Free Tel:", "").Replace("Phone:", "").Replace("Telephone:", "").Replace("Tel:.", "").Replace("Tel:", "").Replace("Tel", "").Replace("PH:", "").Replace("F:", "").Replace("PH.", "").Replace("Ph.", "").Replace("Email", "").Replace("P:", "").Replace("T:", "").Replace("T.", "").Replace("mobile", "");


            if (afterPthone.Length < 3)
                return;

            var subStrings = afterPthone.Split(new string[] { "<br />", "(", ")", ":" }, StringSplitOptions.RemoveEmptyEntries);


            foreach (var s in subStrings)
            {
                var title = Utils.GetTitle(s);
                if (!string.IsNullOrEmpty(title))
                {
                    titles.Add( title);
                    continue;
                }

                var type = Utils.FindType(s);
                if (!string.IsNullOrEmpty(type))
                {
                    types.Add(type);
                    continue;
                }

                if (IsCompanyName(s))
                {
                   
                    continue;
                }

                if (RegExp.IsName(s))
                {
                    names.Add(s);
                }
            }
        }

        public void FixHedlineContact(RssFeed rssFeed)
        {
            try
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var h1s = doc.DocumentNode.SelectNodes("//h1");

                foreach(var h1 in h1s)
                {
                    var HeadLine = Utils.NormalizationString(h1.InnerText);
                    if (HeadLine.ToLower() != "menu" && !string.IsNullOrEmpty(HeadLine))
                    {
                        rssFeed.HeadLine = HeadLine;
                        return;
                    }
                }

                rssFeed.HeadLine = "";
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(rssFeed.Link + " " + ex);
            }
        }

        public string responsUri { get; set; }
    }
    
}
