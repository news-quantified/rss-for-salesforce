﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Linq;
    using Infrastructure.Helpers;
    using Model;
    using HtmlAgilityPack;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    public class AccessWireHtmlParser
    {
        public readonly string UrlAdress;
        private readonly DateTime _newsDateTime;
        public AccessWireHtmlParser(string urlAdress, DateTime newsDateTime)
        {
            UrlAdress = urlAdress;
            _newsDateTime = newsDateTime;

        }

        public string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }


        private void GetLoadDataFromNode(HtmlNode node, List<TypeRepresent> list)
        {
            var valueMain = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));
            if (!String.IsNullOrEmpty(valueMain) && valueMain != "," && valueMain != ":" && valueMain != ".") list.Add(new TypeRepresent() { Value = valueMain });

            foreach (var item in node.ChildNodes)
            {
                if (item.InnerHtml.Contains("<br />"))
                {
                    var tempList = item.InnerHtml.Split(new string[] {"<br />"}, StringSplitOptions.None).ToList();
                    for (var i = 0; i < tempList.Count; i++)
                    {
                        if (!tempList[i].Contains("href=")) continue;
                        var regex = new Regex("<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")", RegexOptions.IgnoreCase);
                        var tmp = regex.Matches(tempList[i]).OfType<Match>().Select(m => m.Groups["href"].Value).FirstOrDefault();
                        tempList[i] = tmp;
                    }
                   
                    list.AddRange(from tempItem in tempList 
                                  select StringHelpers.ScrubHtml(tempItem) into value 
                                  where !String.IsNullOrEmpty(value) && value != "," && value != ":" && value != "." 
                                  select new TypeRepresent() {Value = value});
                }
                else
                {
                    var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(item));
                    if (String.IsNullOrEmpty(value) || value == "," || value == ":" || value == ".") continue;
                    list.Add(new TypeRepresent() { Value = value });
                    if (item.HasChildNodes)
                    {
                        GetLoadDataFromNode(item, list);
                    }
                }
            }
        }

        private void GetLoadDataFromNodetemp(HtmlNode node, List<TypeRepresent> list)
        {
            var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));
            if (!String.IsNullOrEmpty(value) && value != "," && value != ":" && value != ".") list.Add(new TypeRepresent() { Value = value });

            foreach (var item in node.ChildNodes)
            {
                value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(item));
                if (String.IsNullOrEmpty(value) || value == "," || value == ":" || value == ".") continue;
                list.Add(new TypeRepresent() { Value = value });
                if (item.HasChildNodes)
                {
                    GetLoadDataFromNode(item, list);
                }
            }
        }

        public void StartParse()
        {
            const string classMainToFind = "articlepreview";
            var web = new HtmlWeb();
            var doc = web.Load(UrlAdress);
            var typeRepresentList = new List<TypeRepresent>();
            doc.OptionWriteEmptyNodes = true;


            var searchNode = (doc.DocumentNode.SelectNodes(String.Format(@"//div[@class='{0}']", classMainToFind)));

            if (searchNode == null) return;
            foreach (var node in searchNode)
            {
                GetLoadDataFromNode(node, typeRepresentList);
            }
            typeRepresentList = typeRepresentList
                                                 .GroupBy(s => s.Value)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .ToList();

            typeRepresentList.Reverse();
            var validateList = new List<TypeRepresent>();
            var isContact = false;


            foreach (var item in typeRepresentList)
            {
                if (item.Value.Contains("Contact:"))
                {
                    isContact = true;
                    break;
                }
                validateList.Add(item);
            }

            if (!isContact) return;
            TextRecognising(validateList);
            CreateandAndSaveContact(validateList);

        }



        private void CreateandAndSaveContact(List<TypeRepresent> clearList)
        {
            var notRecognized = clearList.Where(c => c.Type == TypeEnum.String).ToList();

            //1 Contact
            if (notRecognized.Count <= 2)
            {
                ContactValidate contact = new ContactValidate();

                var type = clearList.FirstOrDefault(c => c.Type == TypeEnum.Type);
                if (type != null) contact.Type = type.Value;

                var email = clearList.FirstOrDefault(c => c.Type == TypeEnum.Email);
                if (email != null) contact.Email = email.Value;

                var pnone = clearList.FirstOrDefault(c => c.Type == TypeEnum.Pnone);
                if (pnone != null) contact.Phone = pnone.Value;

                var company = clearList.FirstOrDefault(c => c.Type == TypeEnum.Company);
                if (company != null) contact.CompanyName = company.Value;

                var url = clearList.FirstOrDefault(c => c.Type == TypeEnum.Url);
                contact.RelateLinks = url != null ? url.Value : GetUrl(contact.Email);

                if (notRecognized.Count > 0) contact.Name = notRecognized[0].Value;

                var title = clearList.FirstOrDefault(c => c.Type == TypeEnum.Title);
                if (title != null) contact.Title = title.Value;

                contact.LastUpdated = DateTime.Now.ToString();

                contact.NewsReleaseDate = _newsDateTime.ToString();

                var repo = new ContactInfoRepository();
                contact.Url = UrlAdress;
                repo.Add(contact);
            }

            else if (notRecognized.Count > 2)
            {
                ContactValidate contact = new ContactValidate();
                var type = clearList.FirstOrDefault(c => c.Type == TypeEnum.Type);
                if (type != null) contact.Type = type.Value;

                var email = clearList.FirstOrDefault(c => c.Type == TypeEnum.Email);
                if (email != null) contact.Email = email.Value;

                var pnone = clearList.FirstOrDefault(c => c.Type == TypeEnum.Pnone);
                if (pnone != null) contact.Phone = pnone.Value;

                var company = clearList.FirstOrDefault(c => c.Type == TypeEnum.Company);
                if (company != null) contact.CompanyName = company.Value;

                var title = clearList.FirstOrDefault(c => c.Type == TypeEnum.Title);
                if (title != null) contact.Title = title.Value;

                var url = clearList.FirstOrDefault(c => c.Type == TypeEnum.Url);
                contact.RelateLinks = url != null ? url.Value : GetUrl(contact.Email);

                contact.Name = notRecognized[0].Value;

                contact.LastUpdated = DateTime.Now.ToString();

                contact.NewsReleaseDate = _newsDateTime.ToString();
                contact.Url = UrlAdress;

                var repo = new ContactInfoRepository();
                repo.Add(contact);
            }

        }

        private string GetUrl(string value)
        {
            try
            {
                var addr = new MailAddress(value);
                return String.Format("www.{0}", addr.Host);
            }
            catch
            {
                return String.Empty;
            }
        }

        private void TextRecognising(List<TypeRepresent> typeRepresentList)
        {
            if (typeRepresentList == null)
            {
                Logger.WriteError(new ArgumentNullException("typeRepresentList"));
                return;
            }



            List<TypeRepresent> typeRepresentTempList = new List<TypeRepresent>();
            foreach (var item in typeRepresentList.Where(item => !String.IsNullOrEmpty(item.Value)))
            {
                if (item.Type != TypeEnum.String) continue;

                var isTitle = ValidationHelpers.TitleIsValid(item.Value);
                if (isTitle.Item1)
                {
                    if (String.Equals(item.Value, isTitle.Item2, StringComparison.CurrentCultureIgnoreCase))
                    {
                        item.Type = TypeEnum.Title;
                    }
                    else
                    {
                        typeRepresentTempList.Add(new TypeRepresent() { Type = TypeEnum.Title, Value = isTitle.Item2 });
                        item.Value = item.Value.Replace(isTitle.Item2, "");
                    }
                }
                else if (ValidationHelpers.EmailIsValid(item.Value))
                {
                    item.Type = TypeEnum.Email;
                }
                else
                    if (ValidationHelpers.UrlIsValid(item.Value))
                    {
                        item.Type = TypeEnum.Url;
                    }
                    else
                        if (ValidationHelpers.UrlIsValid(item.Value))
                        {
                            item.Type = TypeEnum.Url;
                        }
                        else
                            if (ValidationHelpers.TypeIsValid(item.Value))
                            {
                                item.Type = TypeEnum.Type;
                            }
                            else

                                if (ValidationHelpers.CompanyIsValid(item.Value))
                                {
                                    item.Type = TypeEnum.Company;
                                }
                                else
                                    if (ValidationHelpers.PhoneIsValid(item.Value))
                                    {
                                        item.Type = TypeEnum.Pnone;

                                    }
            }
            typeRepresentList.AddRange(typeRepresentTempList);
        }
    }
}
