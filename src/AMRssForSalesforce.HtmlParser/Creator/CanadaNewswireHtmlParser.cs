﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Linq;
    using Infrastructure.Helpers;
    using Model;
    using HtmlAgilityPack;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using Infrastructure.Constants;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    using System.Text;
    using System.Net;
    using System.IO;

    public class CanadaNewswireHtmlParser
    {
        public readonly string UrlAdress;
        private readonly DateTime _newsDateTime;

        private List<string> urlFromText = new List<string>();
        private string[] urlFromTextAll = null;
        private RssFeed rssFeed;

        List<Contact> savedContact = new List<Contact>();

        private string emailsCurrent = null;
        private string currentType;

        public CanadaNewswireHtmlParser(RssFeed _rssFeed)
        {
            UrlAdress = _rssFeed.Link;//@"http://www.newswire.ca/news-releases/notice-from-the-office-of-the-secretary---ontario-securities-commission-561303931.html"; 
            _newsDateTime = _rssFeed.DateTime;
            this.rssFeed = _rssFeed;
        }

        public string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }

        public void FixHedlineContact(RssFeed rssFeed)
        {
            try
            {
                var res = HtmlHelpers.GetResponse(UrlAdress);
                if (res == null)
                    return ;

                var htmlContent = res.Html;
                if (htmlContent == null)
                    return;

                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var h1 = doc.DocumentNode.SelectSingleNode("//h1");

                rssFeed.HeadLine = h1.InnerText;
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(rssFeed.Link + " " + ex);
            }
        }
        private string GetLoadDataFromNode(HtmlNode node)
        {
            var result = String.Empty;

            var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));

            if (!String.IsNullOrEmpty(value) && !result.Contains(value)) result = ", " + result + value; ;
            foreach (var item in node.ChildNodes)
            {
                var valueitem = GetLoadDataFromNode(item);
                if (!String.IsNullOrEmpty(value) && !result.Contains(valueitem)) result = ", " + result + valueitem; ;
            }

            if(string.IsNullOrEmpty(result))
            {
                foreach (var item in node.ChildNodes)
                {
                    var valueitem = item.InnerText;
                    if (!String.IsNullOrEmpty(value) && !result.Contains(valueitem)) result = ", " + result + valueitem; ;
                }
            }

            var emails = RegExp.getEmails(node.InnerHtml);
            if(emails.Any())
            {
                foreach(var e in emails)
                {
                    if(!result.Contains(e))
                        result = ", " + result + e;
                }
            }
            return result;

        }


        public bool NotEnglish(HtmlDocument doc)
        {
            
            var countP = 0;
            var countPnoEnglish = 0;
            var allNoEnglish = 0;

            var ps = doc.DocumentNode.SelectNodes("//body");
            if (ps == null) return true;

            foreach (var p in ps)
            {
                countP++;
                var countNoEnglish = 0;

                foreach (var s in Dictionaries.NotEnglishCh)
                {
                    var n = p.InnerText.IndexOf(s, StringComparison.Ordinal);
                    while (n != -1)
                    {
                        countNoEnglish++;
                        n = p.InnerText.IndexOf(s, n + s.Length, StringComparison.Ordinal);
                    }
                }
                if (countNoEnglish > 0)
                {
                    countPnoEnglish++;
                    allNoEnglish += countNoEnglish;
                }

                foreach (var ch in p.InnerText)
                {
                    if (ch >= (char)12000) countNoEnglish++;

                    if (countNoEnglish > 5) return true;
                }
            }

            return  allNoEnglish > 20;
        }

        public Tuple<string, HtmlDocument> StartParse()
        {
            try { 
            var web = new HtmlWeb();
            var doc = new HtmlDocument();

            var res = HtmlHelpers.GetResponse(UrlAdress);
            if (res == null || res.Html == null)
            {
                
                var contactsNf = new List<Contact>();
                contactsNf.Add(new Contact() { NewsID = UrlAdress });
                web = new HtmlWeb();
                doc = web.Load(UrlAdress);
                return new Tuple<string, HtmlDocument>("", doc);
            }
               

            var htmlContent = res.Html;
            rssFeed.Redirect = res.Redirect;
            
            doc.LoadHtml(htmlContent);
            

            doc.OptionWriteEmptyNodes = true;
            #region language
            rssFeed.English = true;
            #endregion

            urlFromTextAll = RegExp.getUrls(doc.DocumentNode.InnerHtml);
            #region Get Source Level 
            string sourceValue = null;
            var sourseNode = doc.DocumentNode.SelectNodes(String.Format(@"//*[text()[contains(., 'SOURCE ')]]"));
            if (sourseNode != null)
            {

                sourceValue = GetPageSource(doc.DocumentNode.SelectNodes(String.Format(@"//*[text()[contains(., 'SOURCE ')]]")).LastOrDefault());
                sourceValue = Regex.Replace(sourceValue, "SOURCE", "", RegexOptions.IgnoreCase).Trim();
            }
            else
            {
                sourceValue = "";
            }

            #endregion
            string Dateline = GetDateline(doc.DocumentNode);

            GetUrls(doc.DocumentNode);
            #region RELATED LINKS
            var relationLink = String.Empty;
            var relatedLinkNode = doc.DocumentNode.SelectNodes(String.Format(@"//*[text()[contains(., 'RELATED LINKS')]]"));
            if (relatedLinkNode != null)
            {
                var lastOrDefaultNode = relatedLinkNode.LastOrDefault();
                if (lastOrDefaultNode != null)
                {
                    var relatedLinkNodeValue= lastOrDefaultNode.ChildNodes.ToList();
                    for (var i = relatedLinkNodeValue.Count - 4; i > 1; i--)
                    {
                        var nodetext = relatedLinkNodeValue[i].InnerText;
                        if (nodetext.Contains("RELATED LINKS")) relationLink = relatedLinkNodeValue[i + 3].InnerText;	
                    }
                }
            }
            #endregion

            #region Get All Node
            var allContact = GetAllContactNode(doc);
            if (String.IsNullOrEmpty(allContact))
            {

                return new Tuple<string, HtmlDocument>(Dateline, doc);
            }
             

            allContact = Regex.Replace(allContact, "For further information:", "", RegexOptions.IgnoreCase).Trim();

            allContact = Regex.Replace(allContact, "http://www.", "www.", RegexOptions.IgnoreCase).Trim();
            allContact = Regex.Replace(allContact, "http://", "www.", RegexOptions.IgnoreCase).Trim();
            var contacts = //SplitContact(allContact);
                allContact.Split(new string[] {  "; " }, StringSplitOptions.RemoveEmptyEntries);
            

            if (contacts.Length == 0)
            {
                return new Tuple<string, HtmlDocument>(Dateline, doc);
            }
                
            foreach (var contact in contacts)
            {
                var textContact = RegExp.GetInnerText(contact);
                var emails = RegExp.getEmails(contact);
                var numbers = RegExp.GetPnoneNumbers(textContact);
                var titles = Utils.GetTitle(textContact);


                if (!emails.Any() && !numbers.Any() && (string.IsNullOrEmpty(titles) || textContact.Contains("analyst should contact")))
                {
                    var urls = RegExp.getUrls(contact);
                    if (urls.Any())
                    {
                        foreach (var u in urls)
                        {
                            if (!urlFromText.Any(uu => uu == u))
                                urlFromText.Add(u);
                        }
                    }

                    var type = Utils.FindType(contact);
                    if (!string.IsNullOrEmpty(type))
                    {
                        currentType = type;
                    }
                    continue;
                }

                if (emails.Any())
                {
                    emailsCurrent = string.Join(", ", emails);
                }
                else
                {
                    emails = RegExp.getEmails(doc.DocumentNode.InnerHtml);
                    if (emails.Any())
                    {
                        emailsCurrent = string.Join(", ", emails);
                    }
                }

                var clearList = new List<TypeRepresent>();

                var contactdata = SplitContact( contact);
                if (contactdata.Length == 0) 
                    contactdata = Regex.Split(allContact, "or", RegexOptions.IgnoreCase);

                foreach (var contactInfo in contactdata)
                {
                    if (String.IsNullOrEmpty(contactInfo.Trim())) continue;
                    if (!clearList.Any(itemClearList => String.Equals(itemClearList.Value, contactInfo.Trim(), StringComparison.CurrentCultureIgnoreCase)))
                        clearList.Add(new TypeRepresent() { Type = TypeEnum.String, Value = contactInfo.Trim() });
                }


                clearList = clearList.Where(c => !ConstValue.RemoveNode.Any(d => c.Value.IndexOf(d, StringComparison.InvariantCultureIgnoreCase) >= 0)).ToList();

                if(!clearList.Any())
                {
                    if(emails.Any())
                    {
                        foreach(var e in emails)
                        {
                            clearList.Add(new TypeRepresent() { Type = TypeEnum.Email, Value = e });
                        }
                    }

                    if(numbers.Any())
                    {
                        foreach (var n in numbers)
                        {
                            clearList.Add(new TypeRepresent() { Type = TypeEnum.Pnone, Value = n });
                        }
                    }
                }
                if (!String.IsNullOrEmpty(sourceValue))
                {
                    foreach (var item in clearList.Where(item => item.Type == TypeEnum.String && IsCompanyName(item.Value)))
                    {
                        item.Value = Utils.NormalizationString(Regex.Replace(item.Value, "Contact", "", RegexOptions.IgnoreCase).Trim());//продумать список
                        item.Type = TypeEnum.Company;
                    }    
                }

                clearList = clearList.Where(c => !ConstValue.ShortAdressList.Any(d => c.Value.IndexOf(d, StringComparison.InvariantCultureIgnoreCase) >= 0)).ToList();

                foreach (var item in clearList.Where(item => item.Type == TypeEnum.String))
                {
                    if (ConstValue.TypesList.Any(c => item.Value.IndexOf(c, c.Length <= 3 ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase) >= 0))
                    {
                        item.Type = TypeEnum.Type;
                    }
                    if (ConstValue.TitlesList.Any(c => item.Value.IndexOf(c, c.Length <= 3 ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase) >= 0))
                    {
                        item.Type = TypeEnum.Title;
                    }
                }


                foreach (var item in clearList.Where(item => item.Type == TypeEnum.String))
                {
                    var itemLocal = item;
                    foreach (var removeWordItem in ConstValue.RemoveWords.Where(removeWordItem => itemLocal.Value.IndexOf(removeWordItem, StringComparison.InvariantCultureIgnoreCase) >= 0 && itemLocal.Value.IndexOf(removeWordItem, StringComparison.InvariantCultureIgnoreCase) < 0))
                    {
                        item.Value = Regex.Replace(item.Value, removeWordItem, "", RegexOptions.IgnoreCase).Trim();
                    }
                }




                if (!String.IsNullOrEmpty(sourceValue)) clearList.Add(new TypeRepresent() { Type = TypeEnum.Source, Value = sourceValue });
                if (!String.IsNullOrEmpty(relationLink)) clearList.Add(new TypeRepresent() { Type = TypeEnum.RelationLink, Value = relationLink });
                clearList = clearList.Where(c => c.Value.Length > 1).ToList();



                TextRecognising(clearList);
                CreateandAndSaveContact(clearList);
            }

            if (!savedContact.Any())
            {
                //todo need add
                //  savedContact = getContacts(doc, sourceValue);

                Contact contact = new Contact();
                contact.Email = emailsCurrent;
                if (string.IsNullOrEmpty(contact.Email))
                {
                    var emails = RegExp.getEmails(doc.DocumentNode.InnerText);
                    if (emails.Any())
                    {
                        contact.Email = string.Join(", ", emails);
                    }
                }
                contact.Type = currentType;
                contact.Source = sourceValue;
                contact.Url = FindeUrlByCompanyName(sourceValue, sourceValue);
                contact.NewsID = UrlAdress;
               

                contact.NewsReleaseDate = _newsDateTime.ToString(CultureInfo.InvariantCulture);
                contact.LastUpdated = DateTime.Now.ToString(CultureInfo.InvariantCulture);

                ContactInfoRepository repo = new ContactInfoRepository();
                repo.Add(contact);
                savedContact.Add(contact);
            }

            rssFeed.ContactInfo = Utils.SummContactData(savedContact);

            return new Tuple<string, HtmlDocument>(Dateline, doc);

            #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = UrlAdress });
                var web = new HtmlWeb();
                var doc = web.Load(UrlAdress);
                return new Tuple<string, HtmlDocument>("", doc);
            }
        }

        private List<Contact> getContacts(HtmlDocument doc, string sourceValue)
        {
            List<Contact> contacts = new List<Contact>();
            var contactNodes = GetContactContent(doc);

            foreach(var cnode in contactNodes)
            {
                List<Contact> contacts1 = GetContactsFromNode(cnode);
                foreach(var c in contacts1)
                {
                    contacts.Add(c);
                }
            }
            return contacts;
        }

        private List<Contact> GetContactsFromNode(HtmlNode cnode)
        {
            throw new NotImplementedException();
        }

        private List<HtmlNode> GetContactContent(HtmlDocument doc)
        {
            List<HtmlNode> contacts = new List<HtmlNode>();

            var ps = doc.DocumentNode.SelectNodes("//p");

            var hasExternContacts = false;
            foreach (var p in ps)
            {
              
            }
            return contacts;
        }

        private string[] SplitContact(string allContact)
        {
            var contacts = allContact.Split(new string[] { "please contact ", " at ", " of ", " or ", ": ", ", ", "; " }, StringSplitOptions.RemoveEmptyEntries);

           List<string> contactsResult = new List<string>();

            foreach(var s in contacts)
            {
                if (string.IsNullOrEmpty(s))
                    continue;

                if (contactsResult.Any(c => c == s))
                    continue;

                contactsResult.Add(s);
            }

           return contactsResult.ToArray();
        }

        private string GetDateline(HtmlNode htmlNode)
        {
            var metaDate = htmlNode.SelectSingleNode("//meta[@name='description']");

            if (metaDate != null && metaDate.Attributes["content"] != null)
            {
                var strMetaDate = metaDate.Attributes["content"].Value;

                if (!string.IsNullOrEmpty(strMetaDate))
                {
                    var dl = Utils.CatDataInDL(strMetaDate);
                    if (!string.IsNullOrEmpty(dl))
                        return dl;
                }

            }

            var spanLocation = htmlNode.SelectSingleNode("//span[@class='xn-location']");
            if (spanLocation != null)
            {
                if (!string.IsNullOrEmpty(spanLocation.InnerText))
                {
                    var dl = Utils.CatDataInDL(spanLocation.InnerText);
                    if (!string.IsNullOrEmpty(dl))
                        return dl;
                }

            }

            var ps = htmlNode.SelectNodes("//p");

            if (ps == null)
                ps = htmlNode.SelectNodes("//div[@itemprop='articleBody']");

            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                    if ((p.Contains(DateTime.Now.Year.ToString()) || p.Contains((DateTime.Now.Year - 1).ToString())) || (p.Contains("CNW") || p.Contains("CNW")))
                    {
                        if (p.Contains("--"))
                        {
                            var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains("/ -"))
                        {
                            var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains("/ –"))
                        {
                            var strings = p.Split(new string[] { " –" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains((DateTime.Now.Year - 1).ToString()))
                        {
                            var strings = p.Split(new string[] { (DateTime.Now.Year - 1).ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }

                        }
                        else if (p.Contains(DateTime.Now.Year.ToString()))
                        {
                            var strings = p.Split(new string[] { DateTime.Now.Year.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        // / –
                    }
                }

                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                    if (p.Contains("--"))
                    {
                        var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                        if (strings.Count() == 0)
                        {

                        }
                        else
                        {
                            return Utils.NormalizationString(strings[0]);
                        }
                    }
                    else if (p.Contains("/ -"))
                    {
                        var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                        if (strings.Count() == 0)
                        {

                        }
                        else
                        {
                            return Utils.NormalizationString(strings[0]);
                        }
                    }
                }

            }

            ps = htmlNode.SelectNodes("//p");
            if (ps != null)
            {
                foreach (var pp in ps)
                {

                    var p = Utils.NormalizationString3(pp.InnerText);
                    if ((p.Contains(DateTime.Now.Year.ToString()) || p.Contains((DateTime.Now.Year - 1).ToString())) && (p.Contains("CNW") ))
                    {
                        var strong = pp.SelectSingleNode(".//strong");
                        if (strong != null)
                        {
                            return Utils.NormalizationString(strong.InnerText);
                        }
                        else if (p.Contains("/ -"))
                        {
                            var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                            return Utils.NormalizationString(strings[0]);

                        }

                    }

                }
            }


            var div = htmlNode.SelectSingleNode("//div[@class='dateline']");
            if (div != null)
                return Utils.NormalizationString(div.InnerText);

            div = htmlNode.SelectSingleNode("//span[@class='dateline']");
            if (div != null)
                return Utils.NormalizationString(div.InnerText);

            var strong2 = htmlNode.SelectSingleNode("//strong");
            if (strong2 != null && strong2.InnerText.Contains(DateTime.Now.Year.ToString()))
            {

                return Utils.NormalizationString(strong2.InnerText);
            }

            return "";
        }

        private string GetAllContactNode(HtmlDocument doc)
        {
            var result = String.Empty;
            var contactInfo = doc.DocumentNode.SelectNodes(String.Format(@"//*[text()[contains(., 'For further information:')]]"));

            if (contactInfo == null)
                contactInfo = GetContactP(doc);

            if (contactInfo == null) 
                return String.Empty;

            
            var contactNode = contactInfo.LastOrDefault();

            if (Utils.NormalizationString(contactNode.InnerText) == "For further information")
                contactNode = contactNode.NextSibling;

            bool finde = false;

            if(contactNode == null)
            {
                return String.Empty;
            }

            var numbers = RegExp.GetPnoneNumbers(RegExp.GetInnerText(contactNode.OuterHtml));
            var emails = RegExp.GetEmail(contactNode.OuterHtml);
            if (numbers.Any() || emails.Any())
                finde = true;
           

           // return contactNode == null ? String.Empty : contactNode.ChildNodes.Select(GetLoadDataFromNode).Where(value => !String.IsNullOrEmpty(value)).Aggregate(result, (current, value) => ", " + current + value);

            return contactNode == null ? String.Empty : GetStringContact(contactNode);
        }

        private string GetStringContact(HtmlNode contactNode)
        {
            StringBuilder ret = new StringBuilder();

            foreach(var c in contactNode.ChildNodes)
            {
                var emails = RegExp.getEmails(c.InnerHtml);
                if (emails.Any())
                    ret.Append(string.Join(", ", emails));

                if (!string.IsNullOrEmpty(c.InnerText))
                    ret.Append(", " + c.InnerText);
            }

            return ret.ToString();
        }

        private HtmlNodeCollection GetContactP(HtmlDocument doc)
        {
            HtmlNodeCollection nodes = null;
            var ps = doc.DocumentNode.SelectNodes("//p");
            
            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                var emails = RegExp.getEmails(p.InnerHtml);
                var phones = RegExp.GetPnoneNumbers(RegExp.GetInnerText(p.InnerHtml));
                if ((emails.Any() || phones.Any()) && (p.InnerHtml.Contains("<br>") || p.InnerHtml.Contains("<br />")))
                {
                    if(nodes == null)
                    {
                        nodes = new HtmlNodeCollection(p.ParentNode);
                        
                    }
                    nodes.Add(p);
                }
            }
            return nodes;
        }

        private string GetPageSource(HtmlNode sourceNode)
        {
            if (sourceNode == null) return null;
            return sourceNode.ChildNodes.Aggregate(String.Empty, (current, node) => current + StringHelpers.CleanAndFormatText(node.InnerText)); 
        }

        private void FindMoreContactByTelephone(List<TypeRepresent> clearList)
        {
            var phoneList = clearList.Where(c => c.Type == TypeEnum.Pnone).ToList();
            var listPosition = new List<int>{0};


            foreach (var item in phoneList)
            {
                var startPosition = listPosition.Last();
                var index = clearList.FindIndex(startPosition, c => c.Type == item.Type && c.Value == item.Value);

                var testList = clearList.Skip(startPosition).Take(index - startPosition).ToList();
                if (testList.Any(s => s.Type == TypeEnum.Contact)) listPosition.Add(index + 1);
                else 


                {
                    var tempitem = testList.FirstOrDefault(c => c.Type == TypeEnum.String);
                    var tempindex = clearList.FindIndex(startPosition, c => tempitem != null && (c.Type == tempitem.Type && c.Value == tempitem.Value));
                    if (tempindex >= 0) clearList[tempindex].Type = TypeEnum.Contact;
                    listPosition.Add(index + 1);
                }
            }

        }

        private void FindMoreContactByEmail(List<TypeRepresent> clearList)
        {
            var emailList = clearList.Where(c => c.Type == TypeEnum.Email).ToList();
            if (emailList.Any())
            {
                foreach (var item in emailList)
                {
                    var addr = new MailAddress(item.Value);
                    var username = addr.User;
                    var splitArray = username.Split('.');
                    if (splitArray.Length == 0) splitArray = username.Split('_');
                    foreach (string t in splitArray)
                    {
                        foreach (var clearListItem in clearList)
                        {
                            if (clearListItem.Type != TypeEnum.String) continue;

                            var tempValue = clearListItem.Value.Replace(" ", "").Trim();
                            if ((clearListItem.Value.IndexOf(t, StringComparison.OrdinalIgnoreCase) >= 0) || (tempValue.IndexOf(t, StringComparison.OrdinalIgnoreCase) >= 0))
                            {
                                clearListItem.Value = ConstValue.RemoveWords.Aggregate(clearListItem.Value, (current, itemreg) => Regex.Replace(current, itemreg, "", RegexOptions.IgnoreCase));
                                clearListItem.Type = TypeEnum.Contact;

                            }
                        }
                    }
                }
            }




        }

        public ContactValidate GetValidContact(ContactValidate contact)
        {
            var newContact = (contact.Clone() as ContactValidate);
            if (newContact == null) return null;
            var isValidateContact = true;


            #region CheckName
            if (!String.IsNullOrEmpty(newContact.Name))
            {
                var isTitle = ValidationHelpers.TitleIsValid(newContact.Name);
                if (isTitle.Item1)
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Title)) newContact.Title = newContact.Name;
                    newContact.Name = null;
                }
                else
                    if (ValidationHelpers.UrlIsValid(newContact.Name))
                    {
                        isValidateContact = false;
                        if (String.IsNullOrEmpty(newContact.Url)) newContact.Url = newContact.Name;
                        newContact.Name = null;
                    }
                    else
                        if (ValidationHelpers.EmailIsValid(newContact.Name))
                        {
                            isValidateContact = false;
                            if (String.IsNullOrEmpty(newContact.Email)) newContact.Email = newContact.Name;
                            newContact.Name = null;
                        }
                        else
                            if (ValidationHelpers.PhoneIsValid(newContact.Name))
                            {
                                isValidateContact = false;
                                if (String.IsNullOrEmpty(newContact.Phone)) newContact.Phone = newContact.Name;
                                newContact.Name = null;
                            }
                            else
                                if (ValidationHelpers.TypeIsValid(newContact.Name))
                                {
                                    isValidateContact = false;
                                    if (String.IsNullOrEmpty(newContact.Type)) newContact.Type = newContact.Name;
                                    newContact.Name = null;
                                }
                                else
                                    if (IsCompanyName(newContact.Name))
                                    {
                                        isValidateContact = false;
                                        if (String.IsNullOrEmpty(newContact.CompanyName)) 
                                            newContact.CompanyName = newContact.Name;
                                        newContact.Name = null;
                                    }
                if (!String.IsNullOrEmpty(newContact.Name)) newContact.Name = newContact.Name.Trim();


                foreach (var item in ConstValue.NoNameList)
                {
                    if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                    newContact.Name = null;
                    break;
                }

                foreach (var item in ConstValue.RemoveWords)
                {
                    if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                    newContact.Name = null;
                    break;
                }

                if (newContact.Name != null)
                {
                    foreach (var item in ConstValue.RemoveNode)
                    {
                        if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                        newContact.Name = null;
                        break;
                    }
                }
                if (newContact.Name != null)
                {



                    newContact.Name = newContact.Name.Replace(", CPA", " ");
                    newContact.Name = newContact.Name.Replace(" CPA", " ");
                    newContact.Name = newContact.Name.Replace(", CA", " ");
                    newContact.Name = newContact.Name.Replace(",CA", " ");
                    newContact.Name = newContact.Name.Replace(" CA", " ");
                    newContact.Name = newContact.Name.Replace("(USA)", " ");
                    newContact.Name = newContact.Name.Replace("(U.S.", " ");
                    newContact.Name = newContact.Name.Replace("USA", " ");

                    if (newContact.Name.Length <= 7) newContact.Name = null;
                }



            }
            #endregion

            /* #region CompanyName
            if (!String.IsNullOrEmpty(newContact.CompanyName))
            {
                var isTitle = ValidationHelpers.TitleIsValid(newContact.CompanyName);
                if (isTitle.Item1)
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Title)) newContact.Title = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.UrlIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Url)) newContact.Url = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.EmailIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Email)) newContact.Email = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.PhoneIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Phone)) newContact.Phone = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.TypeIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Type)) newContact.Type = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.ContactIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Name)) newContact.Name = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                if (!String.IsNullOrEmpty(newContact.CompanyName)) newContact.CompanyName = newContact.CompanyName.Trim();
            }
            #endregion*/
            #region CheckPhone

            if (!String.IsNullOrEmpty(newContact.Phone))
            {
                newContact.Phone = newContact.Phone.TrimStart('.', ',', '-', '/', ' ');
                newContact.Phone = newContact.Phone.TrimEnd('.', ',', '-', '/', ' ');
                newContact.Phone = newContact.Phone.Replace("()", " ");
                newContact.Phone = newContact.Phone.Replace("(..  )", " ");
            }
            #endregion




            if (!isValidateContact) newContact = GetValidContact(newContact);
            return newContact;
        }

        protected void CreateandAndSaveContact(List<TypeRepresent> clearList)
        {
            var contact = new ContactValidate
            {
                NewsID = UrlAdress,
                LastUpdated = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                NewsReleaseDate = _newsDateTime.ToString(CultureInfo.InvariantCulture),
                
            };




            var nameValue = clearList.FirstOrDefault(c => c.Type == TypeEnum.Contact);
            if (nameValue != null)
            {
                contact.Name = nameValue.Value;
            }
            else
            {
                nameValue = clearList.FirstOrDefault(c => c.Type == TypeEnum.String && RegExp.IsName(c.Value));
                if (nameValue != null)
                {
                    contact.Name = nameValue.Value;
                }
            }

            


             var type = clearList.FirstOrDefault(c => c.Type == TypeEnum.Type);
             if (type != null) contact.Type = type.Value;
             
             var title = clearList.FirstOrDefault(c => c.Type == TypeEnum.Title);
             if (title != null) contact.Title = title.Value;
             
             var email = clearList.FirstOrDefault(c => c.Type == TypeEnum.Email);
             if (email != null) contact.Email = email.Value;

             foreach (var item in clearList.Where(c => c.Type == TypeEnum.Pnone).ToList())
             {
                 contact.Phone += String.Format("{0}, ", item.Value);
             }
             if (!String.IsNullOrEmpty(contact.Phone)) contact.Phone = contact.Phone.Remove(contact.Phone.Length - 1, 1);
             
             var company = clearList.FirstOrDefault(c => c.Type == TypeEnum.Company);
             if (company != null) contact.CompanyName = company.Value;
             
             var source = clearList.FirstOrDefault(c => c.Type == TypeEnum.Source);
             if (source != null) contact.Source = source.Value;
             
             var url = clearList.FirstOrDefault(c => c.Type == TypeEnum.Url);

             contact.Url = url != null ? url.Value : GetUrl(contact.Email);
                     
                     
             if (String.IsNullOrEmpty(contact.CompanyName))
             {
                 contact.CompanyName = contact.Source;
             }

             var repo = new ContactInfoRepository();
             var isParse = false;

             if (contact.Name != null && contact.Name.Length > 21) contact.Name = null;

             if (contact.Name != null && ConstValue.NoNameList.Any(c => String.Equals(c, contact.Name, StringComparison.CurrentCultureIgnoreCase))) contact.Name = null;

             if (contact.Name != null)
             {
                 foreach (var itemSeparator in ConstValue.SeparatorList)
                 {
                     if (contact.Name.IndexOf(itemSeparator, StringComparison.InvariantCultureIgnoreCase) < 0)
                         continue;
                     var nameList = Regex.Split(contact.Name, itemSeparator, RegexOptions.IgnoreCase);
                     foreach (var t in nameList)
                     {
                         isParse = true;
                         var newContact = (contact.Clone() as ContactValidate);
                         if (newContact == null) continue;
                         newContact.Name = t;
                         newContact = GetValidContact(newContact);
                         repo.Add(newContact);
                     }
                 }
             }
             if (!isParse)
             {
                 contact = GetValidContact(contact);

                if (string.IsNullOrEmpty(contact.Url))
                    contact.Url = FindeUrlByCompanyName(contact.CompanyName, contact.Source);
                else
                {
                    var urls = RegExp.getUrls(contact.Url);
                    contact.Url = string.Join(", ", urls);
                }

                if (string.IsNullOrEmpty(contact.Email) && !string.IsNullOrEmpty(emailsCurrent))
                {
                    contact.Email = emailsCurrent;
                    if (!string.IsNullOrEmpty(contact.Name) && Utils.trueEmail(contact.Email, contact.Name))
                        emailsCurrent = null;
                }

                if (string.IsNullOrEmpty(contact.Type) && !string.IsNullOrEmpty(currentType))
                {
                    contact.Type = currentType;
                }

                if (string.IsNullOrEmpty(contact.Url) || string.IsNullOrEmpty(contact.Name) || string.IsNullOrEmpty(contact.Email) || string.IsNullOrEmpty(contact.Phone))
                {

                }

                if (contact.Type != null)
                {
                    contact.Type = Utils.FindType(contact.Type);
                }
                else
                {

                }

                if (contact.Title != null)
                {
                    contact.Title = Utils.GetTitle(contact.Title);
                }

                //temp
                if (contact.Name != null && contact.Phone != null || !savedContact.Any())
                {
                    repo.Add(contact);

                    savedContact.Add(contact);
                }
                else
                {

                }
                if (clearList.Count(c => c.Type == TypeEnum.Contact) > 1)
                {
                    var names = clearList.Where(c => c.Type == TypeEnum.Contact).Skip(1).ToList();

                    int i = 1;
                    foreach (var n in names)
                    {
                        ContactValidate contactAdd = contact.Clone() as ContactValidate;
                        contactAdd.Name = n.Value;

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Email))
                        {
                            contactAdd.Email = clearList.Where(c => c.Type == TypeEnum.Email).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Type))
                        {
                            contactAdd.Type = clearList.Where(c => c.Type == TypeEnum.Type).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Title))
                        {
                            contactAdd.Title = clearList.Where(c => c.Type == TypeEnum.Title).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Pnone))
                        {
                            contactAdd.Phone = clearList.Where(c => c.Type == TypeEnum.Pnone).ToArray()[i].Value;
                        }
                        repo.Add(contactAdd);
                        savedContact.Add(contact);
                        i++;
                    }
                }

                
            }
        }

        private string GetUrl(string value)
        {
            try
            {
                var addr = new MailAddress(value);
                return String.Format("www.{0}", addr.Host);
            }
            catch
            {
                return String.Empty;
            }
        }

        private void GetUrls(HtmlNode htmlNode)
        {
            var ps = htmlNode.SelectNodes("//p");
            string url = null;

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("replay at") || p.InnerText.ToLower().Contains("web address")
                        || p.InnerText.ToLower().Contains("visited at") || p.InnerText.ToLower().Contains("information, visit")
                        || p.InnerText.ToLower().Contains("available at")
                         || p.InnerText.ToLower().Contains("the company website at")
                        || p.InnerText.ToLower().Contains(" website at")
                         || p.InnerText.ToLower().Contains("website:")
                        || p.InnerText.ToLower().Contains("our website")
                         || p.InnerText.ToLower().Contains("visit website")
                         || p.InnerText.ToLower().Contains("please visit")
                        || p.InnerText.ToLower().Contains(" information ")
                        || p.InnerText.ToLower().Contains(" information,")
                        || p.InnerText.ToLower().Contains("more at")
                        || p.InnerText.ToLower().Contains("visit")
                         || p.InnerText.ToLower().Contains("inc. (")
                        || p.InnerText.ToLower().Contains("available on"))
                    {

                        var urls = RegExp.getUrls(p.InnerHtml);

                        if (urls.Any())
                        {
                            foreach (var u in urls)
                            {
                                if (!urlFromText.Any(uu => uu == u) && !u.Contains("2fwww.") && u != "newswire.ca" && u != "account.newswire.ca" && u != "schema.org")
                                    urlFromText.Add(u);
                            }
                        }
                        else
                        {

                        }
                    }
                }
            }
        }

        private string FindeUrlByCompanyName(string companyName, string Sourse)
        {
            if (!urlFromText.Any())
            {
                if (emailsCurrent != null && RegExp.getUrls2(emailsCurrent).Any())
                    return "www." + RegExp.getUrls2(emailsCurrent)[0];

                if (!string.IsNullOrEmpty(companyName))
                {
                    var partOfNameCompany = Regex.Replace(companyName.ToLower(), "[^a-z0-9,]", "").Trim();
                    if (partOfNameCompany.Length > 5)
                        partOfNameCompany = partOfNameCompany.Remove(5);


                    foreach (var domen in urlFromTextAll)
                    {
                        if (domen.Contains(partOfNameCompany))
                            return domen;
                    }
                }
                else if (!string.IsNullOrEmpty(Sourse))
                {
                    var partOfNameCompany = Regex.Replace(Sourse.ToLower(), "[^a-z0-9,]", "").Trim();
                    if (partOfNameCompany.Length > 5)
                        partOfNameCompany = partOfNameCompany.Remove(5);


                    foreach (var domen in urlFromTextAll)
                    {
                        if (domen.Contains(partOfNameCompany))
                            return domen;
                    }
                }
                return null;
            }
            if (!string.IsNullOrEmpty(companyName))
            {
                var partcompanyNames = companyName.ToLower().Split(new string[] { " ", "(", ")", ":", ".", ",", "@" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var url in urlFromText)
                {
                    foreach (var word in partcompanyNames)
                    {
                        var partWord = word.Length > 5 ? word.Remove(5) : word;
                        if (url.ToLower().Contains(partWord))
                            return url;
                    }
                }

            }

            if (!string.IsNullOrEmpty(Sourse))
            {
                var partcompanyNames = Sourse.ToLower().Split(new string[] { " ", "(", ")", ":", ".", ",", "@" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var url in urlFromText)
                {
                    foreach (var word in partcompanyNames)
                    {
                        var partWord = word.Length > 5 ? word.Remove(5) : word;
                        if (url.ToLower().Contains(partWord))
                            return url;
                    }
                }

            }
            return string.Join(", ", urlFromText.ToArray());
        }

        private void TextRecognising(IList<TypeRepresent> typeRepresentList)
        {
            if (typeRepresentList == null)
            {
                Logger.WriteError(new ArgumentNullException("typeRepresentList"));
                return;
            }

            var typeRepresentTempList = new List<Tuple<int, TypeRepresent>>();
            foreach (var item in typeRepresentList.Where(item => !String.IsNullOrEmpty(item.Value)))
            {
                if (item.Type == TypeEnum.Company || item.Type == TypeEnum.Source || item.Type == TypeEnum.RelationLink) continue;

                var isTitle = ValidationHelpers.TitleIsValid(item.Value);
                if (isTitle.Item1)
                {
                    if (String.Equals(item.Value, isTitle.Item2, isTitle.Item2.Length <= 3 ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase))
                    {
                        item.Type = TypeEnum.Title;
                    }
                    else
                    {
                        typeRepresentTempList.Add(new Tuple<int, TypeRepresent>(typeRepresentList.IndexOf(item), new TypeRepresent() { Type = TypeEnum.Title, Value = isTitle.Item2 }));
                        item.Value = item.Value.Replace(isTitle.Item2, "");
                    }
                }
                else if (ValidationHelpers.EmailIsValid(item.Value))
                {
                    item.Type = TypeEnum.Email;
                }
                else if (ValidationHelpers.PhoneIsValid(item.Value))
                {
                    item.Type = TypeEnum.Pnone;

                }
                else if (ValidationHelpers.UrlIsValid(item.Value.ToLower()))
                {
                    item.Type = TypeEnum.Url;
                }
                else if (ValidationHelpers.TypeIsValid(item.Value))
                {
                    item.Type = TypeEnum.Type;
                }
                else if (IsCompanyName(item.Value) )
                {
                    item.Type = TypeEnum.Company;
                }
                else if (ValidationHelpers.ContactIsValid(item.Value))
                {
                    item.Type = TypeEnum.Contact;

                }
                else if (ValidationHelpers.IsValidStreetAddress(item.Value))
                {
                    item.Type = TypeEnum.Adress;
                }

            }

            foreach (var item in typeRepresentTempList)
            {
                typeRepresentList.Insert(item.Item1, item.Item2);
            }
            //  typeRepresentList.AddRange(typeRepresentTempList);
        }

        private List<string> FindeDomansFromContact(IList<TypeRepresent> clearList)
        {
            List<string> domains = new List<string>();

            foreach (var item in clearList)
            {
                var emails = RegExp.getEmails(item.Value);
                if (emails.Any())
                {
                    foreach (var e in emails)
                    {
                        var parts = e.Split('@');
                        if (parts.Count() >= 2)
                        {
                            domains.Add(parts[1]);
                        }
                    }
                }
                else
                {
                    var urls = RegExp.getUrls(item.Value);
                    if (urls.Any())
                    {
                        foreach (var u in urls)
                        {
                            domains.Add(u);
                        }
                    }
                }
            }
            return domains.Distinct().ToList();
        }

        private bool IsItCompanyName(string value, IList<TypeRepresent> clearList)
        {
            List<string> contactDomains = FindeDomansFromContact(clearList);
            if (RegExp.getEmails(value).Any() ||
               RegExp.getUrls(value).Any())
                return false;

            var partOfNameCompany = Regex.Replace(value.ToLower(), "[^a-z0-9,]", "").Trim();
            if (partOfNameCompany.Length > 5)
                partOfNameCompany = partOfNameCompany.Remove(5);


            foreach (var domen in contactDomains)
            {
                if (domen.Contains(partOfNameCompany))
                    return true;
            }

            foreach (var domen in urlFromTextAll)
            {
                if (domen.Contains(partOfNameCompany))
                    return true;
            }
            if (value.Contains("MDS") || value.Contains("SRL"))
                return true;

            return false;
        }

        protected bool IsCompanyName(string val)
        {
            return Utils.IsCompanyName(val, this.urlFromText, null, null, null);


        }

       
    }
}
