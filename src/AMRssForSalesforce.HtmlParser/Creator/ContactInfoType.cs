﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    enum ContactInfoType
    {
        Empty,
        Name,
        Url,
        Email,
        Phone,
        Title,
        Type,
        CopanyName
    }
}
