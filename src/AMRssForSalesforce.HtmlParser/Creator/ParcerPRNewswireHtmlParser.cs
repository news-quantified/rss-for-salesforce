﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Abstract;
    using Model;
    using HtmlAgilityPack;
    using Infrastructure.Helpers;
    using Infrastructure.Constants;
    public class ParcerPRNewswireHtmlParser : HtmlParser
    {
        private const int maxLengContactInfo = 50;
        public ParcerPRNewswireHtmlParser(string _url) : base(_url) { }

        public ParcerPRNewswireHtmlParser(string _fileName, bool fromFile) : base(_fileName, fromFile) { }

        public override Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed)
        {
            try
            { 
           //if (!IsEngish())
           //     return null;
            rssFeed.English = true;
            Source = GetSource();

         
            rssFeed.Redirect = responsUri;
            if (rssFeed.Redirect.Contains("http://www.multivu.com/"))
            {
                rssFeed.Dateline = GetMultivuDeteLine();
            }
            else
            {
                rssFeed.Dateline = GetDeteLine();
            }
          //  companyNames = GetCompanyNames();

           if (!string.IsNullOrEmpty(Source) && !companyNames.Any(cn => cn == Source))
            {
                companyNames.Add(Source);
            }

            var contactNodes = GetContactContent();

            if(contactNodes == null || !contactNodes.Any())
            {
                contactNodes = GetContactContent2();
            }

            var contacts = new List<Contact>();

            bool haseThrueContact = false;

            if (contactNodes != null && contactNodes.Any())
                foreach (var contactNode in contactNodes)
                {
                    if (Utils.NormalizationString(contactNode.InnerText).ToLower() == "contacts:")
                        continue;
                    var contacts1 = GetContacts(contactNode);
                    foreach (var c in contacts1)
                    {
                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                        {
                            var brs = contactNode.SelectNodes(".//br");
                            if (brs == null && (contactNode.Name == "p" || contactNode.Name == "pre")
                                && !(contactNode.ChildNodes.Count == 1 && contactNode.ChildNodes[0].Name == "span")
                                && !(contactNode.InnerText.ToLower().Contains("contact") && contactNode.InnerText.Contains(":") || (c.Type != null || c.Title != null) && c.Name!= null)
                                && Utils.NormalizationString(contactNode.InnerText).Length > 1500)
                            {
                                
                                    c.Name = null;
                                    c.CompanyName = null;
                                    c.Type = null;
                                    c.Title = null;
                               
                                if (!string.IsNullOrEmpty(c.Phone))
                                    c.Phone = string.Join(", ", RegExp.GetPnoneNumbers(c.Phone));

                                if (!string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email))
                                    contacts.Add(c);
                                if(string.IsNullOrEmpty(c.Phone))
                                {
                                    c.Phone = string.Join(", ", RegExp.GetPnoneNumbers2(contactNode.InnerText));
                                }
                            }
                            else
                            {
                                contacts.Add(c);
                                if (c.Name != null || c.Email != null)
                                    haseThrueContact = true;
                            }
                        }
                        else
                        {
                            var emails = RegExp.getEmails(contactNode.InnerHtml);
                            if(emails.Any())
                            {
                                if(contacts.Any())
                                {
                                    if (contacts.Last().Email == null)
                                        contacts.Last().Email = string.Join(", ", emails);
                    }
                                else
                                {
                                    contacts.Add(new Contact() { Email = string.Join(", ", emails) });
                                }
                            }
                        }
                }
                }

            if (!haseThrueContact)
            {
                var contactsTable = GetDataFromDivOverflow();

                if (contactsTable != null && contactsTable.Any())
                {
                    if (contactsTable.Any(c => !string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Email)))
                        contacts = contactsTable;
                    else
                    {

                    }
                }
            }

            //else
            //{
              //  contacts = GetDataFromDivOverflow();
            //}

            string relatedLink = GetRelatedLink();

            if (!contacts.Any())
            {
                contacts = TryFindePhone();
            }

            if (!contacts.Any())
            {
                contacts.Add(new Contact());
            }

            List<Contact> forDelete = new List<Contact>();

            foreach (var contact in contacts)
            {
                if( string.IsNullOrEmpty(contact.Email) && string.IsNullOrEmpty(contact.Name))
                {
                    if (contacts.Any(c => !string.IsNullOrEmpty(c.Email) || !string.IsNullOrEmpty(c.Name)))
                    {
                        forDelete.Add(contact);
                        continue;
                    }
                }
                if(string.IsNullOrEmpty(contact.Phone))
                {
                    if (contacts.Any(c => c.Email == contact.Email && c.Name == contact.Name && !string.IsNullOrEmpty(c.Phone)))
                    {
                        forDelete.Add(contact);
                        continue;
                    }
                }
            }

            while(forDelete.Any())
            {
                contacts.Remove(forDelete.First());
                forDelete.Remove(forDelete.First());
            }

            foreach (var c in contacts)
            {
                c.LastUpdated = DateTime.Now.ToString();
                c.NewsID = urlHtml;
                if (string.IsNullOrEmpty(c.Url))
                {
                    if (string.IsNullOrEmpty(relatedLink))
                    {
                        if (string.IsNullOrEmpty(c.Email))
                        {
                            c.Url = TryToFindeUrlItText();
                        }
                        else
                        {
                            c.Url = GetRelatedLink(c);
                        }
                    }
                    else
                        c.Url = relatedLink;
                }
                c.Source = Source;
                if (string.IsNullOrEmpty(c.CompanyName))
                    c.CompanyName = Source;

                if (!string.IsNullOrEmpty(relatedLink))
                    c.RelateLinks = relatedLink;
            }


            List<Contact> forAdd = new List<Contact>();
            foreach (var c in contacts)
            {
                if(c.Name!= null && c.Name.Contains("/"))
                {
                    var names = c.Name.Split('/');
                    c.Name = Utils.NormName( names[0]);
                    if (names.Count() > 1)
                    {
                        for (int i = 1; i < names.Count(); i++)
                        {
                            if (RegExp.IsName(names[i]))
                            {
                                forAdd.Add(new Contact());
                                forAdd.Last().Name = Utils.NormName(names[i].Trim());
                                forAdd.Last().NewsID = c.NewsID;
                                forAdd.Last().CompanyName = c.CompanyName;
                                forAdd.Last().Email = c.Email;
                                forAdd.Last().Phone = c.Phone;
                                forAdd.Last().Url = c.Url;
                                forAdd.Last().Type = c.Type;
                                forAdd.Last().Title = c.Title;
                            }
                        }
                    }
                }
                else if (c.Name != null && c.Name.Contains(","))
                {
                    var names = c.Name.Split(',');
                    c.Name = Utils.NormName(names[0]);
                    if (names.Count() > 1)
                    {
                        if(Utils.HasTitle(names[1]))
                        {
                            c.Title = names[1].Trim();
                        }
                        c.Name = names[0];
                    }
                }

            }
            if(forAdd.Any())
            {
                foreach(var c in forAdd)
                {
                    contacts.Add(c);
                }
            }

            foreach(var c in contacts)
            {
                if(contacts.Any(c1 => c1.Name == c.Name && (c1.Email != c.Email && string.IsNullOrEmpty(c.Email)) ))
                {
                    var yesEmail = contacts.First(c1 => c1.Name == c.Name && c1.Email != c.Email);
                    forDelete.Add(c);
                    yesEmail.Phone = string.IsNullOrEmpty(yesEmail.Phone) ? c.Phone : yesEmail.Phone;
                    yesEmail.Title = string.IsNullOrEmpty( yesEmail.Title) ? c.Title : yesEmail.Title;
                    yesEmail.Type = string.IsNullOrEmpty(yesEmail.Type) ? c.Type : yesEmail.Type;
                }
            }
            
            while (forDelete.Any())
            {
                contacts.Remove(forDelete.First());
                forDelete.Remove(forDelete.First());
            }

            var notName = contacts.Where(c => !string.IsNullOrEmpty(c.Email) && string.IsNullOrEmpty(c.Name));

            var notEmail = contacts.Where(c => string.IsNullOrEmpty(c.Email) && !string.IsNullOrEmpty(c.Name));

            if(notEmail.Any() && notEmail.Any())
            {
                foreach(var n in notEmail)
                {
                    foreach(var e in notName)
                    {
                        if (Utils.trueEmail(e.Email, n.Name))
                        {
                            n.Email = e.Email;
                            forDelete.Add(e);
                        }
                    }
                }
            }

            while (forDelete.Any())
            {
                contacts.Remove(forDelete.First());
                forDelete.Remove(forDelete.First());
            }
            rssFeed.ContactInfo = Utils.SummContactData(contacts);
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = this.urlHtml });
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
        }

        private List<Contact> TryFindePhone()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var contacts = new List<Contact>();
            var ps = doc.DocumentNode.SelectNodes("//p[@itemprop='articleBody']");

            if (ps == null)
                ps = doc.DocumentNode.SelectNodes("//p");
            if (ps == null)
                return null;

            List<string> phones = new List<string>();
            foreach (var p in ps)
            {
                if (p.InnerText.Contains("888-776-0942"))
                    continue;
             

                var inerText = Utils.NormalizationString(RegExp.GetInnerText(p.InnerHtml));

                if (string.IsNullOrEmpty(inerText))
                    continue;

                var partStrings = inerText.Split(new string[] { ":", " or ", ",", "contact", "call"
                    , ". Follow us", " follow us","call: ","For more information", "For more information," }, StringSplitOptions.RemoveEmptyEntries);

                if(partStrings.Any())
                {
                    foreach(var s in partStrings)
                    {
                       
                        var pones1 = RegExp.GetPnoneNumbers(s);
                        if(pones1.Any())
                        {
                            foreach(var t in pones1)
                            {
                                phones.Add(t);
                            }
                        }
                    }
                }
            }
            if(phones.Any())
            {
                contacts.Add(new Contact() { Phone = string.Join(", ", phones.Distinct().ToArray()) });
            }
            return contacts;
        }

        private string GetMultivuDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var strongs = doc.DocumentNode.SelectNodes("//strong");

            if (strongs == null)
                strongs = doc.DocumentNode.SelectNodes("//p");


            if (strongs != null)
            {
                foreach (var pp in strongs)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                    if (p.Contains(DateTime.Now.Year.ToString()) || p.Contains((DateTime.Now.Year - 1).ToString()) || p.Contains("PR Newswire"))
                    {
                        return p;
                    }
                }
            }
            return ".";
        }

        private List<HtmlNode> GetContactContent2()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");
            
            if (ps == null)
                return null;

            foreach (var p in ps)
            {
                if (p.InnerText.Length < 8)
                    continue;
                if (p.InnerText.Contains("Logo - "))
                    continue;

                if (p.InnerText.Contains("Follow FCA US news and video on:"))
                    continue;

                if (p.InnerText.Contains("+1-888-776-0942"))
                    continue;
                if (p.Attributes["class"] != null && p.Attributes["class"].Value == "prnews_p")
                    continue;


                var notContactNode = false;

                if (NodeContentEmailOrPhone(p) && p.SelectNodes(".//br") != null)
                {

                    contacts.Add(p);

                }

            }

               

            return contacts;
        }

        private string GetDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var spanLocation = doc.DocumentNode.SelectSingleNode("//span[@itemprop='addressLocality']");
            if(spanLocation != null)
            {
                return spanLocation.InnerText;
            }

            var ps = doc.DocumentNode.SelectNodes("//p");

            if(ps == null)
                ps = doc.DocumentNode.SelectNodes("//div[@itemprop='articleBody']");

           
            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var spanLocation2 = doc.DocumentNode.SelectSingleNode("//span[@class='xn-location']");
                    if (spanLocation2 != null)
                    {
                        return spanLocation2.InnerText;
                    }

                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                    if ((p.Contains(DateTime.Now.Year.ToString()) || p.Contains((DateTime.Now.Year - 1).ToString())) && (p.Contains("PR Newswire") || p.Contains("PRNewswire")))
                    {
                        if (p.Contains("--"))
                        {
                            var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains("/ -"))
                        {
                            var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains("/ –"))
                        {
                            var strings = p.Split(new string[] { " –" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if( p.Contains((DateTime.Now.Year - 1).ToString()))
                        {
                            var strings = p.Split(new string[] { (DateTime.Now.Year - 1).ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                var dl = Utils.CatDataInDL(strings[0]);
                                if(!string.IsNullOrEmpty(dl))
                                    return dl;
                            }

                        }
                        else if( p.Contains(DateTime.Now.Year.ToString()))
                        {
                            var strings = p.Split(new string[] { DateTime.Now.Year.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                var dl = Utils.CatDataInDL(strings[0]);
                                if (!string.IsNullOrEmpty(dl))
                                    return dl;
                            }
                        }
                        // / –
                    }
                }

                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                        if (p.Contains("--"))
                        {
                            var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        else if (p.Contains("/ -"))
                        {
                            var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                    }
                
            }
           
                ps = doc.DocumentNode.SelectNodes("//p");
                if(ps != null)
                {
                    foreach (var pp in ps)
                    {
                        
                        var p = Utils.NormalizationString3(pp.InnerText);
                        if ((p.Contains(DateTime.Now.Year.ToString()) || p.Contains((DateTime.Now.Year - 1).ToString())) && (p.Contains("PR Newswire") || p.Contains("PRNewswire")))
                        {
                            var strong = pp.SelectSingleNode(".//strong");
                            if(strong != null)
                            {
                                return Utils.NormalizationString(strong.InnerText);
                            }
                            else if (p.Contains("/ -"))
                            {
                                var strings = p.Split(new string[] { " -" }, StringSplitOptions.RemoveEmptyEntries);
                                return Utils.NormalizationString(strings[0]);

                            }
                        
                        }
                     
                    }
                }
                
            
            var div = doc.DocumentNode.SelectSingleNode("//div[@class='dateline']");
            if(div!=null)
                return Utils.NormalizationString(div.InnerText);

            div = doc.DocumentNode.SelectSingleNode("//span[@class='dateline']");
            if (div != null)
                return Utils.NormalizationString(div.InnerText);

            var strong2  = doc.DocumentNode.SelectSingleNode("//strong");
            if (strong2 != null && strong2.InnerText.Contains(DateTime.Now.Year.ToString()))
            {

                return Utils.NormalizationString(strong2.InnerText);
            }

            return ".";
        }

      
        private string TryToFindeUrlItText()
        {
            string urlFromLink = null;
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p[@itemprop='articleBody']");

            if (ps == null)
                ps = doc.DocumentNode.SelectNodes("//p");
            if (ps == null)
                return null;

            foreach (var p in ps)
            {
                if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("replay at") || p.InnerText.ToLower().Contains("web address")
                    || p.InnerText.ToLower().Contains("visited at") || p.InnerText.ToLower().Contains("information, visit")
                    || p.InnerText.ToLower().Contains("available at")
                     || p.InnerText.ToLower().Contains("the company website at")
                    || p.InnerText.ToLower().Contains(" website at")
                     || p.InnerText.ToLower().Contains("website:")
                     || p.InnerText.ToLower().Contains(" website ")
                    || p.InnerText.ToLower().Contains("our website")
                     || p.InnerText.ToLower().Contains("visit website")
                     || p.InnerText.ToLower().Contains("please visit")
                    || p.InnerText.ToLower().Contains(" information ")
                    || p.InnerText.ToLower().Contains("more at")
                    || p.InnerText.ToLower().Contains(" visit ")
                     || p.InnerText.ToLower().Contains("inc. (")
                    || p.InnerText.ToLower().Contains("available on"))
                {

                    var urls = RegExp.getUrls(p.InnerText);

                    if (urls.Any())
                        return string.Join(", ", urls);

                    var aas = p.SelectNodes(".//a");
                    if (aas != null)
                    {
                        foreach (var a in aas)
                        {
                            if (a.Attributes["href"] != null)
                            {
                                var u = a.Attributes["href"].Value;
                                
                                if (!u.Contains("facebook") && !u.Contains("twitter") && !u.Contains("google") && !u.Contains("linkedin")
                                             && !u.Contains("schema.org"))
                                            urlFromLink = u;
                                    
                            }
                        }

                    }
                }
            }

            return urlFromLink;
        }

        private bool IsEngish()
        {
            if (this.urlHtml.Contains(".XML") || htmlContent.Contains("lang=\"en\""))
            {
                return !NoEnglish();
            }
            return false;
        }

        private List<Contact> GetDataFromDivOverflow()
        {
            List<Contact> contacts = new List<Contact>();
            Contact contact = null;

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var pss = doc.DocumentNode.SelectNodes("//div[@class='divOverflow']");
            if(pss!= null)
            foreach (var ps in pss)
            {
                if (ps.InnerText.Contains("$"))
                    continue;

                var emailsAll = RegExp.getEmails(ps.InnerText);
                var numbersAll = RegExp.GetPnoneNumbers( RegExp.GetInnerText(ps.InnerHtml));
                if (!emailsAll.Any() && !numbersAll.Any() )
                    continue;


                if (ps != null)
                {
                    var table = ps.SelectSingleNode(".//table");
                    if (table != null)
                    {
                        if (!emailsAll.Any() && table.InnerText.Length > 600 || table.InnerText.ToLower().Contains("when:"))
                            continue;

                        List<List<HtmlNode>> contactsNodes = GetContactsNodesFromTable(table);

                        if (contactsNodes == null || !contactsNodes.Any())
                            return contacts;

                        foreach (var col in contactsNodes)
                        {
                            if (contact == null || !string.IsNullOrEmpty(contact.Name) || !string.IsNullOrEmpty(contact.Email) || !string.IsNullOrEmpty(contact.CompanyName))
                            {
                                contact = new Contact();
                                contacts.Add(contact);
                            }

                            string curentCompanyName = "";
                            int indexName = 0;
                            for (int i = 0; i < col.Count; i++)
                            {
                                var pppps = col[i].SelectNodes(".//p");
                                if (pppps != null && col.Count < 3 && pppps.Count > 2)
                                {
                                    List<Contact> contactsP = TryParcePCntacts(pppps);
                                    if (contactsP != null)
                                    {
                                        foreach (var cp in contactsP)
                                        {
                                            if (!string.IsNullOrEmpty(cp.Name) || !string.IsNullOrEmpty(cp.Email) || !string.IsNullOrEmpty(cp.Phone))
                                               contacts.Add(cp);
                                        }
                                    }
                                    continue;
                                }

                                var spanBr = col[i].SelectNodes(".//span/br");
                                if (spanBr != null)
                                {
                                    var parts = col[i].SelectSingleNode(".//span").ChildNodes;

                                    if (parts.Count > 0)
                                    {

                                        List<Contact> contactsP = TryParcePCntacts(parts);
                                        if (contactsP != null)
                                        {
                                            foreach (var cp in contactsP)
                                            {
                                                if (!string.IsNullOrEmpty(cp.Name) || !string.IsNullOrEmpty(cp.Email) || !string.IsNullOrEmpty(cp.Phone))
                                                    contacts.Add(cp);
                                            }
                                        }
                                        continue;
                                    }
                                }

                                var contactString = Utils.NormalizationString(col[i].InnerText);
                                if (contactString.ToLower() == "contact:" || contactString.ToLower() == "contacts:" || contactString.ToLower() == "contact"
                                    || contactString.ToLower() == "contacts")
                                    continue;

                                if (i == 0)//company name
                                {
                                    //try tifinde company name
                                    var companyNameNode = col[i].SelectSingleNode(".//b");
                                    if (companyNameNode != null)
                                    {
                                        var cEmails = RegExp.getEmails(companyNameNode.InnerText);
                                        if(cEmails.Any())
                                        {
                                            contact.Email = string.Join(", ", cEmails);
                                            continue;
                                        }
                                        var cPhones = RegExp.GetPnoneNumbers(companyNameNode.InnerText);
                                        if (cPhones.Any() || RegExp.IsNumber(companyNameNode.InnerText))
                                        {
                                            contact.Phone = Utils.NormPhone(companyNameNode.InnerText);
                                            GetContactNameTypeTitleAthterPhone(contact, companyNameNode.InnerText);
                                            continue;
                                        }
                                        if (IsCompanyName(companyNameNode.InnerText))
                                        {
                                            curentCompanyName = Utils.CutCpmpanyName(companyNameNode.InnerText, Source);
                                            contact.CompanyName = curentCompanyName;
                                            continue;
                                        }
                                    }
                                }

                                //email
                                var emails = RegExp.getEmails(contactString);

                                if (emails.Any())
                                {
                                    contact.Email = string.Join(", ", emails);
                                    continue;
                                }
                                //phone
                                var numbers = RegExp.GetPnoneNumbers(contactString);

                                if (numbers.Any() || RegExp.IsNumber(contactString))
                                {
                                    contact.Phone = Utils.NormPhone(contactString); 
                                    GetContactNameTypeTitleAthterPhone(contact, contactString);
                                    continue;
                                }
                                //url
                                var urls = RegExp.getUrls(contactString);
                                if (urls.Any())
                                {
                                    contact.Url = string.Join(", ", urls);
                                    continue;
                                }

                                var type = Utils.FindType(contactString);

                                if(!string.IsNullOrEmpty(type))
                                {
                                    contact.Type = type;
                                    continue;
                                }

                                if (!contactString.Contains(","))
                                {
                                    var title = Utils.GetTitle(contactString);
                                    if (!string.IsNullOrEmpty(title))
                                    {
                                        contact.Title = title;
                                        continue;
                                    }
                                }

                                if (IsCompanyName(contactString))
                                {
                                    contact.CompanyName = contactString;
                                    continue;
                                }

                                if (i == 0 || i == 1)//name and title or filial
                                {
                                    indexName = i;
                                    var nameAndTitle = contactString.Split(',');

                                    if (RegExp.IsName(nameAndTitle[0]))
                                    {
                                        contact.Name = Utils.NormName(Utils.NormalizationString(nameAndTitle[0]));
                                        if (nameAndTitle.Count() > 1 && Utils.HasTitle(nameAndTitle[1]))
                                            contact.Title = nameAndTitle[1].Trim();
                                        continue;
                                    }
                                }

                                if (i - indexName == 1)//title
                                {
                                    if (Utils.HasTitle(contactString))
                                    {
                                        contact.Title = contactString;
                                        continue;
                                    }
                                }

                                //new contact
                                var names = contactString.Split(',');
                                if ((!string.IsNullOrEmpty(contact.Phone) || !string.IsNullOrEmpty(contact.Email) || !string.IsNullOrEmpty(contact.Url))
                                    && RegExp.IsName(names[0]))
                                {
                                    contact = new Contact();
                                    contacts.Add(contact);

                                    contact.Name = Utils.NormName( Utils.NormalizationString(names[0]));
                                    contact.CompanyName = curentCompanyName;
                                    indexName = i;
                                }
                                else
                                {
                                    if(RegExp.IsName(names[0]))
                                    {
                                        indexName = i;
                                        contact.Name = Utils.NormName(Utils.NormalizationString(names[0]));
                                    }
                                }
                            }
                        }
                    }

                }
            }
            return contacts;
        }

        private List<Contact> TryParcePCntacts(HtmlNodeCollection pppps)
        {
            List<string> names = new List<string>();
            List<string> emails = new List<string>();
            List<string> titles = new List<string>();
            List<string> companies = new List<string>();
            List<string> phones = new List<string>();
            List<string> types = new List<string>();

            foreach (var p in pppps)
            {
                var _emails = RegExp.getEmails(p.InnerText);
                if (_emails.Any())
                {
                    foreach (var e in _emails)
                        emails.Add(e);
                    continue;
                }

                if (Utils.IsAddress(p.InnerText))
                    continue;
                // var _phones = RegExp.GetPnoneNumbers(p.InnerText);
                if (RegExp.IsNumber(p.InnerText))
                {
                    phones.Add(Utils.NormPhone(p.InnerText));
                    GetContactNameTypeTitleAthterPhone(names,types,titles, p.InnerText);
                    continue;
                }                
                
                if (Utils.HasTitle(p.InnerText))
                {
                    titles.Add(Utils.NormalizationString(p.InnerText));
                    continue;
                }

                if(IsCompanyName(p.InnerText))
                {
                    companies.Add(Utils.NormalizationString(p.InnerText));
                    continue;
                }


                var _type = Utils.FindType(p.InnerText);
                if(!string.IsNullOrEmpty(_type))
                {
                    types.Add(_type);
                    continue;
                }
                var _names = p.InnerText.Split(',');

                foreach(var n in _names)
                {
                    if (RegExp.IsName(Utils.NormalizationString(n)))
                        names.Add(Utils.NormalizationString(n));
                }
            }

            List<Contact> contacts = new List<Contact>();
            if (names.Any())
            {
                int i = 0;
                foreach (var n in names)
                {
                    contacts.Add(new Contact() { Name = n });

                    if(emails.Any())
                    {
                        var emailTrue = emails.FirstOrDefault(e => Utils.trueEmail(e, n));
                        if(emailTrue != null)
                        {
                            contacts.Last().Email = emailTrue;
                            emails.Remove(emailTrue);
                        }
                    }

                    if(phones.Any())
                    {
                        if (phones.Count == names.Count)
                            contacts.Last().Phone = phones[i];
                        else
                            contacts.Last().Phone = string.Join(", ", phones.ToArray());
                    }

                    if (titles.Any())
                    {
                        if (titles.Count == names.Count)
                            contacts.Last().Title = titles[i];
                       
                    }

                    if (types.Any())
                    {
                        if (types.Count == names.Count)
                            contacts.Last().Type = types[i];
                        else
                            contacts.Last().Type = types[0];
                    }
                    if (companies.Any())
                    {
                        if (companies.Count == names.Count)
                            contacts.Last().CompanyName = companies[i];
                        else
                            contacts.Last().CompanyName = companies[0];
                    }
                    i++;
                }

                if(emails.Any() && contacts.Any(c => string.IsNullOrEmpty(c.Email)))
                {
                    foreach(var cont in contacts.Where(c => string.IsNullOrEmpty(c.Email)))
                    {
                        cont.Email = string.Join(", ", emails.ToArray());
                    }
                }
                return contacts;
            }
            else
            {
                if(emails.Any())
                {
                    int i=0;
                    foreach (var e in emails)
                    {
                        contacts.Add(new Contact() { Email = e });   
                        if (phones.Any())
                    {
                        if (phones.Count == emails.Count)
                            contacts.Last().Phone = phones[i];
                        else
                            contacts.Last().Phone = string.Join(", ", phones.ToArray());
                    }

                    if (titles.Any())
                    {
                        if (titles.Count == emails.Count)
                            contacts.Last().Title = titles[i];

                    }

                    if (types.Any())
                    {
                        if (types.Count == emails.Count)
                            contacts.Last().Type = types[i];
                        else
                            contacts.Last().Type = types[0];
                    }

                    if (companies.Any())
                    {
                        if (companies.Count == emails.Count)
                            contacts.Last().CompanyName = companies[i];
                        else
                            contacts.Last().CompanyName = companies[0];
                    }
                    i++;
                    }
                 
                    return contacts;
                }

                if (phones.Any())
                {
                    int i = 0;
                    foreach (var p in phones)
                    {
                        contacts.Add(new Contact() { Phone = p });

                        if (titles.Any())
                        {
                            if (titles.Count == phones.Count)
                                contacts.Last().Title = titles[i];

                        }

                        if (types.Any())
                        {
                            if (types.Count == phones.Count)
                                contacts.Last().Type = types[i];
                            else
                                contacts.Last().Type = types[0];
                        }

                        if (companies.Any())
                        {
                            if (companies.Count == phones.Count)
                                contacts.Last().CompanyName = companies[i];
                            else
                                contacts.Last().CompanyName = companies[0];
                        }
                        i++;
                    }

                    return contacts;
                }
            }
            return null;
        }

       

        private List<List<HtmlNode>> GetContactsNodesFromTable(HtmlNode table)
        {
            List<List<HtmlNode>> listNodes = new List<List<HtmlNode>>();

            var trs = table.SelectNodes("./tr");
            if (trs == null)
                trs = table.SelectNodes(".//tr");

            if (trs == null)
                return null;
            int countTD = 0;

            if (trs[0].SelectNodes("./td") == null)
                return null;
            foreach (var td in trs[0].SelectNodes("./td"))
            {
                if (td.Attributes["colspan"] != null)
                {
                    var colspan = 0;
                    if (int.TryParse(td.Attributes["colspan"].Value, out colspan))
                    {
                        countTD += colspan;
                    }
                    else
                        countTD++;
                }
                else
                    countTD++;
            }
            //trs[0].SelectNodes("./td").Count;

            if (countTD == 0)
                return null;

            for (int i = 0; i < countTD; i++)
            {
                listNodes.Add(new List<HtmlNode>());
            }

            foreach (var tr in trs)
            {
                
                var tds = tr.SelectNodes("./td");
                if (tds.Count > listNodes.Count)
                    //it isn't contact table
                    return null;
                for (int i = 0; i < tds.Count; i++)
                {
                    string contactString = Utils.NormalizationString(tds[i].InnerText);
                    if (!string.IsNullOrEmpty(contactString) 
                        && contactString.ToLower() != "contact:" 
                        && contactString.ToLower() != "contacts:"
                         && contactString.ToLower() != "contact"
                        && contactString.ToLower() != "telephone"
                        && contactString.ToLower() != "email"
                        && contactString.ToLower() != "website")
                        listNodes[i].Add(tds[i]);

                }
            }

            while (listNodes.Any(l => !l.Any()))
            {
                listNodes.Remove(listNodes.First(l => !l.Any()));
            }


            return listNodes;
        }

       
        public override string GetRelatedLink()
        {
            StringBuilder sb = new StringBuilder();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//a");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.Attributes["title"] != null && p.Attributes["title"].Value.Contains("Link to") && p.Attributes["href"] != null)
                    {
                        var link = p.InnerText;
                        var urls = RegExp.getUrls(link);
                        if(urls.Any())
                        {
                            if (sb.Length > 0)
                                     sb.Append(", ");
                            sb.Append(string.Join(", ", urls));
                        }
                       else
                        {
                            if(p.Attributes["href"] != null)
                            {
                                var link2 = p.Attributes["href"].Value;
                                var urls2 = RegExp.getUrls(link2);
                                if (urls2.Any())
                                {
                                    if (sb.Length > 0)
                                        sb.Append(", ");
                                    sb.Append(string.Join(", ", urls2));
                                }
                            }
                        }
                    }
                }
            }
           
            return sb.ToString();
        }

        public override string GetSource()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                   
                        int SourseIndex = p.InnerText.Trim().IndexOf("SOURCE");
                        if (SourseIndex >= 0 && Utils.NormalizationString(p.InnerText).Length < 200)
                        {
                            return Utils.NormalizationString(p.InnerText.Replace("SOURCE", "").Trim());
                        }
                    
                }
            }
            return "";
        }

        public override List<HtmlNode> GetContactContent()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);


            var bs = doc.DocumentNode.SelectNodes("//b");
            if (bs != null)
            {
                foreach (var b in bs)
                {
                    foreach (var c in Dictionaries.MarkerContact)
                    {
                        if (b.InnerText.ToLower().Contains(c) && b.SelectNodes(".//br") != null && (RegExp.getEmails(b.InnerText).Any() || RegExp.GetPnoneNumbers(b.InnerText).Any()))
                        {
                            contacts.Add(b);
                            return contacts;
                        }
                    }
                }
            }

            var ps = doc.DocumentNode.SelectNodes("//p");


            if (ps == null)
                return null;
            var countContact = 0;
            HtmlNode p_contact_m = null;
            int countAfterWordContact = 0;

            bool findeBrContact = false;

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (countContact > 0)
                {
                    if (p.InnerText.Contains("Related Images"))
                    {
                        countContact = 0;
                        break;
                    }
                    else
                    {
                        var text = RegExp.GetInnerText(p.InnerHtml);
                        if (text.Length > 5 && text.Length < 100)
                        {
                            if (!text.Contains("please contact") && !text.Contains("Location:") && (text.Contains(",") || text.ToLower().Contains("contact person:")))
                            {
                                contacts.Add(p);
                                continue;
                            }
                        }
                    }
                }
                if (p.InnerText.Length < 8)
                    continue;
                if (p.InnerText.Contains("Logo - "))
                    continue;

                if (p.InnerText.Contains("Follow FCA US news and video on:"))
                    continue;

                if (p.InnerText.Contains("+1-888-776-0942"))
                    continue;
                if (p.Attributes["class"] != null && p.Attributes["class"].Value == "prnews_p")
                    continue;
                if (p.InnerText.Contains("Contacts:") || p.InnerText.Contains("Contact:") || p.InnerText.Contains("Media Contact"))
                {
                    if (countContact == 0 && contacts.Any())
                        contacts.Clear();
                    countContact++;

                }

                var notContactNode = false;
                if (p.ChildNodes != null)
                {
                    foreach (var node in p.ChildNodes)
                    {
                        if (p.Name == "#text" && p.InnerText.Length > maxLengContactInfo)
                        {
                            notContactNode = true;
                            break;
                        }

                    }
                    if (notContactNode)
                        continue;
                }

                if (p_contact_m != null)
                {
                    if (NodeContentEmailOrPhone(p) && !contacts.Any(cs => cs.InnerText == p.InnerText))
                    {
                        if (p.SelectNodes(".//br") != null && !findeBrContact)
                        {
                            findeBrContact = true;
                            // contacts.Clear();
                        }
                        contacts.Add(p);
                        countAfterWordContact = 0;
                    }
                    countAfterWordContact++;

                }
                if (p.InnerHtml.Contains("<br") && (NodeContentEmailOrPhone(p) || p.InnerHtml.Contains("class=\"xn-location\"")))
                {
                    contacts.Add(p);
                    continue;
                }
                foreach (var c in Dictionaries.MarkerContact)
                {
                    if (Utils.NormalizationString(p.InnerText).ToLower().Contains(c))
                    {
                        p_contact_m = p;
                        countAfterWordContact = 0;
                        if (NodeContentEmailOrPhone(p) && !contacts.Any(cs => cs.InnerText == p.InnerText))
                        {
                            if (p.SelectNodes(".//br") != null && !findeBrContact)
                            {
                                findeBrContact = true;
                                //  contacts.Clear();
                            }
                            contacts.Add(p);
                            break;
                        }
                    }
                }
            }

            if (contacts.Count() > 1)
            {
                if (contacts.Any(c => c.InnerText.Length > 500) && contacts.Any(c => c.InnerText.Length < 500))
                    return contacts.Where(c => c.InnerText.Length < 500).ToList();
            }

            return contacts;
        }


        private List<HtmlNode> TryFindeContactContentInul()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var article = doc.DocumentNode.SelectSingleNode("//article");
            if (article == null)
                return contacts;

            var bs = article.SelectNodes("//ul");
            if (bs != null)
            {
                foreach (var b in bs)
                {
                    if (b.InnerText.Contains("Contact Us") || b.InnerText.Contains("+1-888-776-0942"))
                        continue;

                    if (NodeContentEmailOrPhone(b))
                        contacts.Add(b);
                    else
                    {
                        foreach (var c in Dictionaries.MarkerContact)
                        {
                            if (b.InnerText.ToLower().Contains(c))
                            {
                                contacts.Add(b);
                                break;
                            }
                        }
                    }
                }
            }
            return contacts;
        }

        public override List<Contact> GetContacts(HtmlNode nodeContact)
        {
            var nodeContactInnerText = RegExp.GetInnerText(nodeContact.InnerHtml);

            List<Contact> contacts = new List<Contact>();

            Contact currentContact = new Contact();
            contacts.Add(currentContact);

            currentContact.CompanyName = TryToGetCompanyName(nodeContact);

            string spanName = "";

            string type = Utils.FindType(nodeContact.InnerText);

            if (!string.IsNullOrEmpty(type))
                currentContact.Type = type;

            string contactText = "";
            //    nodeContact.InnerHtml.Replace("<br>", "\n");//RemoveWordContact(nodeContact.InnerHtml).Replace("<br>","\n");
            //try to finde person name
            //<span class="xn-person" itemscope="" itemtype="http://schema.org/Person"><span itemprop="name">Aline Henry</span></span>
            var names = nodeContact.SelectNodes(".//span[@itemprop='name']");

            var cEmails1 = RegExp.getEmails(nodeContactInnerText);
            if (cEmails1.Count() > 1 && nodeContact.ChildNodes != null)
            {
                contacts = TryParcePCntacts(nodeContact.ChildNodes);
                return contacts;
            }
            if ( names != null && names.Any()  )
            {
                if (names.Count > 1)
                {
                    contacts = TryParceMoreCntacts(nodeContact);
                    return contacts;
                }
                currentContact.Name = Utils.NormName( Utils.NormalizationString(names.Last().InnerText.Trim()));
                //spanName = nodeContact.SelectSingleNode(".//span[@class='xn-person']").OuterHtml;
            }
            else if (nodeContact.SelectSingleNode(".//span[@class='xn-person']") != null)
            {
                currentContact.Name = Utils.NormName(Utils.NormalizationString(nodeContact.SelectSingleNode(".//span[@class='xn-person']").InnerText.Trim()));
                //spanName = nodeContact.SelectSingleNode(".//span[@class='xn-person']").OuterHtml;
            }

            //if (string.IsNullOrEmpty(currentContact.Name))
            //{
            //    contactText = contactText.Replace(currentContact.Name, "");
            //}

            //finde and remuve email
            HtmlNode email = GetFirstEmailNode(nodeContact);

            if (email != null)
            {
                var emailTxt = RegExp.getEmails(email.InnerText);
                if (emailTxt.Any())
                    currentContact.Email = emailTxt[0];

                //contactText = contactText.Replace(currentContact.Email, "");
            }
            else
            {
                var emails = RegExp.getEmails(nodeContact.InnerText);
                if(emails.Any())
                   currentContact.Email = string.Join(" ,", emails);
            }

            bool nextName = false;
            string prev = "";
            string mr = "";

            if (nodeContact.ChildNodes != null)
            {
                foreach (var ch in nodeContact.ChildNodes)
                {
                    if (ch.InnerText.Contains("Address") || ch.InnerText.Contains("Date & time") ||
                        ch.InnerText.Contains("Date &amp; time:"))
                        continue;


                    var urls = RegExp.getUrls(ch.InnerText);
                    if(urls.Any())
                        currentContact.Url = string.Join(", ", urls);
                    
                    if (ch.Name == "a" && !ch.InnerText.Contains("@") && !ch.InnerText.Contains("photos.prnewswire") && RegExp.getUrls(ch.InnerText).Count() > 0)
                    {
                        currentContact.Url = ch.InnerText;
                    }
                    if (ch.Name == "b" && ch.InnerText.ToLower() != "contact" && !string.IsNullOrEmpty(Utils.NormalizationString(ch.InnerText)))
                    {
                        if (nextName)
                        {
                            currentContact.Name = Utils.NormName( string.Format("{0} {1}", mr, Utils.NormalizationString(ch.InnerText)));
                            nextName = false;
                            continue;
                        }
                        if (Utils.StringContensName(ch.InnerText))
                        {
                            foreach (var t in Dictionaries.MarcerName)
                            {
                                if (ch.InnerText.Contains(t))
                                    mr = t;
                            }
                            if (ch.InnerText == mr)
                            {
                                nextName = true;
                            }
                            else
                            {
                                currentContact.Name = Utils.NormName( Utils.NormalizationString(ch.InnerText));
                            }
                            continue;
                        }
                        var cEmails = RegExp.getEmails(ch.InnerText);
                        if (cEmails.Any())
                        {
                            currentContact.Email = string.Join(", ", cEmails);
                            continue;
                        }
                        var cPhones = RegExp.GetPnoneNumbers(ch.InnerText);
                        if (cPhones.Any() || RegExp.IsNumber(ch.InnerText))
                        {
                            currentContact.Phone = Utils.NormPhone(ch.InnerText);
                            GetContactNameTypeTitleAthterPhone(currentContact, ch.InnerText);
                            continue;
                        }
                        var type2 = Utils.FindType(ch.InnerText);
                        if(!string.IsNullOrEmpty(type2))
                        {
                            if (currentContact.Type != null)
                            {
                                contacts = TryParceMoreCntacts(nodeContact);
                                return contacts;
                            }
                            else
                            {
                                currentContact.Type = type2;
                            }
                            continue;
                        }
                        if (IsCompanyName(ch.InnerText))
                        {
                            var curentCompanyName = Utils.CutCpmpanyName(ch.InnerText);
                            currentContact.CompanyName = curentCompanyName;
                            continue;
                        }
                       // currentContact.CompanyName = ch.InnerText;
                    }

                    if (ch.Name != "#text" || ch.InnerText.ToLower().Contains(" please "))
                    {
                        if (ch.Name == "span" && !string.IsNullOrEmpty(prev) && prev[prev.Length - 1] == '&')
                        {
                            if(Utils.NormalizationString(ch.InnerText).Length < 150 )
                                 contactText = contactText + "\n" + ch.InnerText;
                        }
                        else
                        {
                            if (currentContact.Name != Utils.NormalizationString(ch.InnerText) && currentContact.Email != Utils.NormalizationString(ch.InnerText)
                                && Utils.NormalizationString(ch.InnerText).ToLower() != "contact" && Utils.NormalizationString(ch.InnerText).ToLower() != "contacts")
                            {
                                if (Utils.NormalizationString(ch.InnerText).Length < 150)
                                    contactText = contactText + "\n" + ch.InnerText;

                            }
                        }
                    }
                    else
                    {
                        if (Utils.NormalizationString(ch.InnerText).Length < 150)
                           contactText = contactText + "\n" + ch.InnerText;
                        prev = Utils.NormalizationString(ch.InnerText);
                    }
                }
                // nodeContact.RemoveAllChildren();
            }

            //var indexAddress = contactText.IndexOf("Address:");
            //if (indexAddress > 0)
            //    contactText = contactText.Remove(indexAddress);

            if (string.IsNullOrEmpty(contactText) && currentContact.Name == null && currentContact.Email == null && currentContact.Phone == null)
            {
                contacts = GetContactFromText(nodeContact);
            }
            else
            {
                TryParseLinesInformation(contacts, contactText);
                if(!contacts.Any(c => c.Email != null || c.Phone != null || c.Name != null))
                {
                    contacts = GetContactFromText(nodeContact);
                }
            }

            if (contacts.Any(c => string.IsNullOrEmpty(c.Name) && (!string.IsNullOrEmpty(c.Email) || !string.IsNullOrEmpty(c.Phone))))
            {
                contacts.First(c => string.IsNullOrEmpty(c.Name) && (!string.IsNullOrEmpty(c.Email) || !string.IsNullOrEmpty(c.Phone))).Name = TryGetName(nodeContactInnerText);
            }

            return contacts;
        }

        private string[] SplitContact(string allContact)
        {
            var contacts = allContact.Split(new string[] { " from ", "please contact ", " at ", " of ", " or ", ": ", ", ", "; ", " al ", " o " }, StringSplitOptions.RemoveEmptyEntries);

            List<string> contactsResult = new List<string>();

            foreach (var s in contacts)
            {
                if (string.IsNullOrEmpty(s))
                    continue;

                if (contactsResult.Any(c => c == s))
                    continue;

                contactsResult.Add(s);
            }

            return contactsResult.ToArray();
        }
        private List<Contact> GetContactFromText(HtmlNode nodeContact)
        {
            List<Contact> contacts = new List<Contact>();
            var currentContact = new Contact();
            contacts.Add(currentContact);
            var text = RegExp.GetInnerText2(nodeContact.InnerHtml);
            var parts = SplitContact(text);

            foreach (var p in parts)
            {
                var word = p.Trim();
                var emails2 = RegExp.getEmails(p);
                if (emails2.Any())
                {
                    currentContact.Email = string.Join(", ", emails2);
                    continue;
                }

                if (Utils.HasTitle(word))
                {
                    
                    currentContact.Title = Utils.GetTitle(word);
                    var type2 = Utils.FindType(word);
                    if (type2 != null)
                        currentContact.Type = type2;
                    if (currentContact.Title != null && word != currentContact.Title && word.Length - currentContact.Title.Length <= 2)
                    {
                        foreach (var c in contacts)
                        {
                            if (string.IsNullOrEmpty(c.Title))
                                c.Title = currentContact.Title;
                        }
                    }
                    if (word.Contains(","))
                    {
                        var nameTitles = word.Split(',');
                        if (nameTitles.Count() > 1 && Utils.HasTitle(nameTitles[1]))
                        {
                            word = nameTitles[0];
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(word) && currentContact.Title != null)
                        {
                            word = Utils.NormalizationString(word.Replace(currentContact.Title, " "));
                            if (word.Length < 5)
                                continue;
                        }
                        else
                            continue;
                    }
                }

                var numbers = RegExp.GetPnoneNumbers(word);

                if (numbers.Any() || RegExp.IsNumber(word))
                {
                   
                   currentContact.Phone = string.Join(", ", numbers );
                   
                    continue;
                }

                var type = Utils.FindType(word);
                if (!string.IsNullOrEmpty(type) && !IsCompanyName(word))
                {
                  
                    currentContact.Type = type;
                    var text2 = Utils.CutType(word, type);

                    if (Utils.HasTitle(text2))
                    {
                        currentContact.Title = text2;
                        continue;
                    }
                    if (IsCompanyName(text2))
                        currentContact.CompanyName = text2;

                    continue;
                }


                if (IsCompanyName(word))
                {
                    if (string.IsNullOrEmpty(currentContact.CompanyName))
                        currentContact.CompanyName = Utils.GetCompanyName(word);

                }
                else
                {
                    var urls = RegExp.getUrls(word);
                    if(urls.Any())
                    {
                        currentContact.Url = string.Join(", ", urls);
                    }
                    if (RegExp.IsName(word))
                    {
                       
                        currentContact.Name = Utils.NormName(word);
                       
                    }
                    else
                    {
                        if (word.Contains(","))
                        {
                            var partWords = word.Split(',');
                            foreach (var ww in partWords)
                            {
                                var titleP = Utils.GetTitle(ww);
                                if (titleP != null)
                                {
                                    currentContact.Title = titleP;
                                }
                                else if (RegExp.IsName(ww.Trim()))
                                {
                                    currentContact.Name = ww.Trim();
                                }
                            }
                        }
                        else
                        {
                            if (word.Contains(" contact ") && word.Contains(" at"))
                            {
                                var indexOffContact = word.IndexOf(" contact ");
                                var indexOffAt = word.IndexOf(" at");
                                if (indexOffContact > 0 && indexOffAt > 0 && indexOffAt > indexOffContact)
                                {
                                    var name = word.Substring(indexOffContact + " contact ".Length, indexOffAt - (indexOffContact + " contact ".Length)).Trim();
                                    if (RegExp.IsName(name))
                                        currentContact.Name = name;
                                }
                            }
                        }
                    }
                }
            }
            return contacts;
        }

        private string TryGetName(string innerText)
        {
            innerText = innerText.Replace("&#160;"," ");
            if(innerText.Contains(" at ") && innerText.ToLower().Contains("contact") )
            {
                var indexOfContact = innerText.ToLower().IndexOf("contact");
                var indexOffAt = innerText.ToLower().IndexOf(" at ",indexOfContact);
                if(indexOffAt > 0)
                {
                    var name = Utils.NormalizationString(innerText.Substring(indexOfContact + "contact".Length, indexOffAt-(indexOfContact + "contact".Length)));
                    if (RegExp.IsName(name))
                        return name;
                }
            }

            return null;
        }

        private List<Contact> TryParceMoreCntacts(HtmlNode nodeContact)
        {
            List<Contact> contacts = new List<Contact>();
            Contact currentContact = new Contact();

            List<Contact> contactsList = new List<Contact>();

            string curentCompanyName = "";

            contacts.Add(currentContact);

            bool startedrealData = false;

            if (nodeContact.ChildNodes != null)
            {
                startedrealData = nodeContact.ChildNodes.Count < 50 && Utils.NormalizationString(nodeContact.InnerText).Length < 500;

                var childrens = nodeContact.ChildNodes;

                foreach (var ch in nodeContact.ChildNodes)
                {
                    if(ch.Name  == "b" && ch.SelectNodes("br")!= null && (RegExp.getEmails(ch.InnerText).Any() || RegExp.GetPnoneNumbers(ch.InnerText).Any()))
                    {
                        childrens = ch.ChildNodes;
                    }

                }
                foreach (var ch in childrens)
                {
                    if (!startedrealData)
                    {
                        startedrealData = Utils.IsStartContactData(ch.InnerText);
                        continue;
                    }

                    if (ch.Name == "a" && RegExp.getEmails( ch.InnerText).Count() > 0)
                    {
                        var emails = RegExp.getEmails(ch.InnerText);
                        if (contactsList.Any())
                        {
                            if(contactsList.Count() == emails.Count())
                            {
                                for(int i = 0; i < contactsList.Count(); i ++)
                                {
                                    contactsList[i].Email = emails[i];
                                }
                            }
                            else
                            {
                                foreach(var e in emails)
                                {
                                    bool finde = false;
                                    foreach(var c in contactsList)
                                    {
                                        if(!string.IsNullOrEmpty(c.Name) && Utils.trueEmail(e, c.Name))
                                        {
                                            c.Email = e;
                                            finde = true;
                                            break;
                                        }
                                    }
                                    if(!finde)
                                    {
                                        foreach (var c in contactsList)
                                        {
                                            if (string.IsNullOrEmpty(c.Email) )
                                            {
                                                c.Email = e;
                                                //break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            currentContact.Email = string.Join(", ", emails);
                        }
                    }
                    if (ch.Name == "a" && !ch.InnerText.Contains("@") && !ch.InnerText.Contains("photos.prnewswire") && RegExp.getUrls(ch.InnerText).Count() > 0)
                    {
                        currentContact.Url = ch.InnerText;
                    }
                    if (ch.Name == "b" )
                    {
                        var type = Utils.FindType(ch.InnerText);

                        if(!string.IsNullOrEmpty(type))
                        {
                            var nnn = Utils.NormalizationString(ch.InnerText.Replace(type, "").Replace(type.ToUpper(),"").Replace("Contact","").Replace("CONTACT",""));
                            if(!string.IsNullOrEmpty(nnn) && RegExp.IsName(nnn))
                            {
                                if(currentContact.Name != null)
                                {
                                    currentContact = new Contact();
                                    contacts.Add(currentContact);
                                    currentContact.Name = nnn;
                                }
                                else
                                {
                                    currentContact.Name = nnn;
                                }
                            }
                            currentContact.Type = type;
                            
                            continue;
                        }

                        var title = Utils.GetTitle(ch.InnerText);
                        if (!string.IsNullOrEmpty(title))
                        {
                            currentContact.Title = title;
                            continue;
                        }


                        if (ch.InnerText.ToLower().Contains("contact"))
                        {

                        }
                        else
                        {
                            if (IsCompanyName(Utils.NormalizationString(ch.InnerText)))
                              curentCompanyName = Utils.NormalizationString(ch.InnerText);
                        }

                        if(!string.IsNullOrEmpty(currentContact.Name))
                        {
                            //next contact
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            currentContact.CompanyName = curentCompanyName;

                            contactsList = new List<Contact>();
                        }
                    }
                    if (ch.Name == "span" && (ch.Attributes["class"] != null && ch.Attributes["class"].Value == "xn-person"
                        || ch.Attributes["itemprop"] != null && ch.Attributes["itemprop"].Value == "name"))
                    {
                        if (string.IsNullOrEmpty(currentContact.Name))
                        {
                            currentContact.Name = Utils.NormalizationString(ch.InnerText);
                            currentContact.CompanyName = curentCompanyName;
                        }
                        else
                        {
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            currentContact.Name = Utils.NormalizationString(ch.InnerText);

                            contactsList = new List<Contact>();
                        }
                    }

                    if (ch.Name == "#text")
                    {
                        var s = Utils.NormalizationString(ch.InnerText);

                        if (s == "/" || (ch.InnerText[0] == ','  && !Utils.HasTitle(ch.InnerText)))
                        {
                            contactsList.Add(currentContact);

                            currentContact = new Contact();
                            contacts.Add(currentContact);

                            contactsList.Add(currentContact);
                            
                            currentContact.CompanyName = curentCompanyName;
                            if (s == "/")
                               continue;

                        }
                        if (s.ToLower().Contains("contact"))
                        {
                            //next contact
                            if (!string.IsNullOrEmpty(currentContact.Name))
                            {
                                currentContact = new Contact();
                                contacts.Add(currentContact);
                                currentContact.CompanyName = curentCompanyName;

                                contactsList = new List<Contact>();
                            }
                            continue;
                        }
                        if (s.Length < 5)
                            continue;
                        if (s.ToLower().Contains("e-mail") || s.ToLower().Contains("email"))
                            continue;
                        var numbers = RegExp.GetPnoneNumbers(s);
                        if (numbers.Any() || RegExp.IsNumber(s))
                        {
                            if (s.Contains("/"))
                            {
                               var numbersList = s.Split('/');
                                if(numbersList.Count() == contactsList.Count())
                                {
                                    for(int i = 0; i <numbersList.Count(); i++)
                                    {
                                        contactsList[i].Phone = Utils.NormPhone(numbersList[i]);
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < numbersList.Count(); i++)
                                    {
                                        
                                        if (!numbersList[i].ToLower().Contains("fax:") && !numbersList[i].ToLower().Contains("f:") && !numbersList[i].ToLower().Contains("fax.:"))
                                        {
                                            if (!string.IsNullOrEmpty(Utils.NormPhone(s)) && (string.IsNullOrEmpty(currentContact.Phone) || !currentContact.Phone.Contains(Utils.NormPhone(s))))
                                            {
                                                if (!string.IsNullOrEmpty(currentContact.Phone))
                                                    currentContact.Phone = currentContact.Phone + ", ";
                                                currentContact.Phone = currentContact.Phone + Utils.NormPhone(numbersList[i]);
                                            }
                                          
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if (!s.ToLower().Contains("fax:") && !s.ToLower().Contains("f:") && !s.ToLower().Contains("fax.:"))
                                {
                                    if (string.IsNullOrEmpty(currentContact.Phone) || !currentContact.Phone.Contains(Utils.NormPhone(s)))
                                    {
                                        if (!string.IsNullOrEmpty(currentContact.Phone))
                                            currentContact.Phone = currentContact.Phone + ", ";
                                        var phone = Utils.NormPhone(s);
                                        if (string.IsNullOrEmpty(phone))
                                            phone = string.Join(", ", numbers);
                                        currentContact.Phone = currentContact.Phone + phone;
                                    }
                                    if(contactsList.Any())
                                    {
                                        foreach(var c in contactsList)
                                        {
                                            if (string.IsNullOrEmpty(c.Phone) || !c.Phone.Contains(Utils.NormPhone(s)))
                                            {
                                                if (!string.IsNullOrEmpty(c.Phone))
                                                    c.Phone = c.Phone + ", ";
                                                c.Phone = c.Phone + Utils.NormPhone(s);
                                            }
                                        }
                                    }
                                }
                            }
                            GetContactNameTypeTitleAthterPhone(currentContact, s);
                            continue;
                        }
                        if (string.IsNullOrEmpty(currentContact.Name) && Utils.StringContensName(s))
                        {
                            currentContact.Name = Utils.NormName(s);
                            continue;
                        }
                        if (string.IsNullOrEmpty(currentContact.CompanyName) && IsCompanyName(s))
                        {
                            currentContact.CompanyName = s;
                            continue;
                        }
                        var title = Utils.GetTitle(s);
                        if (!string.IsNullOrEmpty(title))
                        {
                            currentContact.Title = title;
                            continue;
                        }
                        var type = Utils.FindType(s);
                        if (!string.IsNullOrEmpty(type))
                        {
                            currentContact.Type = type;
                            continue;
                        }
                        int position = s.IndexOf(":");

                        if (position == s.Length - 1)
                        {
                            currentContact.Type = s.Remove(position);
                            continue;
                        }

                        if (RegExp.IsName(s) && string.IsNullOrEmpty(currentContact.Name))
                            currentContact.Name = Utils.NormName(s); 

                    }
                }

            }
            for (int i = 0; i < contacts.Count; i++)
            {
                var cont = contacts[i];
                if (string.IsNullOrEmpty(cont.Phone) && string.IsNullOrEmpty(cont.Email))
                {
                    var j = i + 1;
                    while (j < contacts.Count)
                    {
                        if (!string.IsNullOrEmpty(contacts[j].Phone) || !string.IsNullOrEmpty(contacts[j].Email))
                        {
                            cont.Email = contacts[j].Email;
                            cont.Phone = contacts[j].Phone;
                            break;
                        }
                        j++;
                    }
                }
            }
            return contacts;
        }

        private string TryToGetCompanyName(HtmlNode nodeContact)
        {
            var previos = nodeContact.PreviousSibling;
            while (previos != null)
            {
                var b = previos.SelectSingleNode("./b");
                if (b != null)
                {
                    if (b.InnerText.IndexOf("About") == 0 && b.InnerText.Length < maxLengContactInfo)
                    {
                        var companyName = Utils.NormalizationString(b.InnerText.Replace("About", "").Trim());
                        if (companyName.Length > 2)
                            return companyName.Replace("&#160;", " ");
                        else
                            return null;

                    }
                }
                previos = previos.PreviousSibling;
            }
            return null;
        }

        private bool TryParseLinesInformation(List<Contact> contacts, string contactText)
        {
            string[] stringData = contactText.Split(new string[] { "\n", "|" }, StringSplitOptions.RemoveEmptyEntries);

            if (contacts == null || !contacts.Any())
                            {
                                if (contacts == null)
                                    contacts = new List<Contact>();
                                contacts.Add(new Contact());
                            }

            foreach (var st in stringData)
            {
                if (Utils.IsAddress(st))
                    continue;

                var parts = Utils.NormalizationString(st).Split(Dictionaries.Separate, StringSplitOptions.RemoveEmptyEntries);

                foreach (var p in parts)
                {
                    var s = Utils.NormalizationString(p);
                    if (s.Length < 5)
                        continue;
                    if (s.ToLower().Contains("e-mail") || s.ToLower().Contains("email"))
                        continue;
                    var numbers = RegExp.GetPnoneNumbers(s);

                    var numberStr = Utils.NormPhone(s);
                    
                    if (numbers.Any() || RegExp.IsNumber(s))
                    {
                        if (!s.ToLower().Contains("fax:") && !s.ToLower().Contains("f:") && !s.ToLower().Contains("fax.:"))
                        {
                            if (string.IsNullOrEmpty(numberStr))
                            {

                            }
                            else
                            {
                                if (string.IsNullOrEmpty(contacts.First().Phone) || !contacts.First().Phone.Contains(Utils.NormPhone(s)))
                                {
                                    if (!string.IsNullOrEmpty(contacts.First().Phone))
                                        contacts.First().Phone = contacts.First().Phone + ", ";
                                    contacts.First().Phone = contacts.First().Phone + Utils.NormPhone(s);
                                }
                            }
                        }
                        GetContactNameTypeTitleAthterPhone(contacts.First(), s);
                        continue;
                    }

                    var words = s.Split(new string[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var word in words)
                    {
                        if (string.IsNullOrEmpty(contacts.First().Name) && Utils.StringContensName(word))
                        {
                            contacts.First().Name = Utils.NormName(word);
                            continue;
                        }
                        if (string.IsNullOrEmpty(contacts.First().CompanyName) && IsCompanyName(word))
                        {
                            contacts.First().CompanyName = word;
                            continue;
                        }
                        var title = Utils.GetTitle(word);
                        if (!string.IsNullOrEmpty(title))
                        {
                            contacts.First().Title = title;
                            continue;
                        }
                        var type = Utils.FindType(word);
                        if (!string.IsNullOrEmpty(type))
                        {
                            contacts.First().Type = type;
                            continue;
                        }
                        int position = word.IndexOf(":");

                        if (position == word.Length - 1)
                        {
                            contacts.First().Type = word.Remove(position);
                            continue;
                        }

                        if (RegExp.IsName(word) && string.IsNullOrEmpty(contacts.First().Name))
                            contacts.First().Name = Utils.NormName(word);

                        //if (string.IsNullOrEmpty(contacts.First().CompanyName))
                        //{
                        //    contacts.First().CompanyName = word;
                        //    continue;
                        //}
                        var notFaund = word;
                    }
                }
            }
            return true;
        }

        public string RemoveWordContact(string p)
        {
            foreach (var c in Dictionaries.MarkerContact)
            {
                int indexOffContact = p.ToLower().IndexOf(c);
                if (indexOffContact > 0)
                {
                    int indexOffBR = p.IndexOf("<br>", indexOffContact);
                    if (indexOffBR > 0)
                        return p.Remove(0, indexOffBR + "<br>".Length);
                    else
                        return p.Remove(0, indexOffContact + c.Length);
                }
            }

            return p;
        }

     
    }
}
