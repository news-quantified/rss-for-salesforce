﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using HtmlAgilityPack;
    using Infrastructure.Helpers;
    using Infrastructure.Constants;
    public class BusinessWireHtmlParser : Abstract.HtmlParser
    {
        private const int maxLengContactInfo = 50;
        public BusinessWireHtmlParser(string _url) : base(_url) { }

        public BusinessWireHtmlParser(string _fileName, bool fromFile) : base(_fileName, fromFile) { }

        public override Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed)
        {
            try
            {

                rssFeed.English = true;
                rssFeed.Redirect = responsUri;
                this.domains = this.domains.Where(d => d != "google-analytics" && d != "com" && d != "businesswire" && d != "tradeshownews" && d != "pymnts").ToList();
                Source = GetSource();

                rssFeed.Dateline = GetDeteLine();
                companyNames = GetCompanyNames();

                if (string.IsNullOrEmpty(Source) && companyNames != null && companyNames.Any())
                {
                    Source = companyNames.First();
                }
                if(string.IsNullOrEmpty(Source))
                {
                    Source = GetSourceFromDomenAndTitle();
                }

                // var companyRightName = TryToGetCompanyName();

                if (!string.IsNullOrEmpty(Source) && !companyNames.Any(cn => cn == Source))
                {
                    companyNames.Add(Source);
                }

                var contactNodes = GetContactContent();

                var contacts = new List<Contact>();

                if (contactNodes != null && contactNodes.Any())
                    foreach (var contactNode in contactNodes)
                    {
                        if (Utils.NormalizationString(contactNode.InnerText).ToLower() == "contacts:")
                            continue;
                        var contacts1 = GetContacts(contactNode);

                        foreach(var c in contacts1)
                        {
                            contacts.Add(c);
                        }
                    }

                string url = TryToFindeUrlItText();

                foreach (var c in contacts)
                {
                    if (!string.IsNullOrEmpty(Source))
                        c.Source = Source;
                    c.NewsID = this.urlHtml;

                    if (string.IsNullOrEmpty(c.Url))
                    {
                        c.Url = string.IsNullOrEmpty(url) ? GetRelatedLink(c) : url;
                    }

                    if (!string.IsNullOrEmpty(c.Source) && string.IsNullOrEmpty(c.CompanyName))
                    {
                        c.CompanyName = c.Source;
                    }

                    if (string.IsNullOrEmpty(c.CompanyName))
                    {
                        c.CompanyName = null;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(c.Source) && c.CompanyName.ToLower().Contains(c.Source.ToLower()))
                            c.CompanyName = c.Source;
                    }
                }
                rssFeed.ContactInfo = Utils.SummContactData(contacts);

                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                if(string.IsNullOrEmpty(rssFeed.Dateline))
                {
                    rssFeed.Dateline = "(BUSINESS WIRE)";
                }
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = this.urlHtml });
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
        }

        private string GetSourceFromDomenAndTitle()
        {
            if(this.domains != null && this.domains.Count > 0)
            foreach(var d in this.domains)
            {
                if (this.Title.Contains(d))
                    return d;
            }

            return null;
        }

        private string TryToGetCompanyName()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            //itemprop="sourceOrganization"
            var h3_company = doc.DocumentNode.SelectSingleNode("//h3[@itemprop='sourceOrganization']");

            if (h3_company != null)
            {
                return Utils.NormalizationString(h3_company.InnerText);
            }

            return null;
        }

        private string GetDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='bw-release-story']");

            HtmlNodeCollection ps = null;
            ps = div != null ? div.SelectNodes(".//p") : doc.DocumentNode.SelectNodes(".//p");


            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("--"))
                    {
                        var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                        if (strings.Any())
                            return Utils.NormalizationString(strings[0]);
                    }
                }
            }

            div = doc.DocumentNode.SelectSingleNode("//div[@class='article-text']");
            if (div == null)
            {
                return null;
            }

            ps = div.SelectNodes(".//p");

            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("(BUSINESS WIRE"))
                    {
                        int PRWEB_Index = p.IndexOf(")");
                        return p.Remove(PRWEB_Index + 1).Replace("\r\n                                ", " ").Replace("\r\n", " ").Trim();
                    }
                }
            }

            return "(BUSINESS WIRE)";
        }
        private string TryToFindeUrlItText()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("replay at") || p.InnerText.ToLower().Contains("web address")
                    || p.InnerText.ToLower().Contains("visited at") || p.InnerText.ToLower().Contains("information, visit")
                    || p.InnerText.ToLower().Contains("available at")
                     || p.InnerText.ToLower().Contains("the company website at")
                    || p.InnerText.ToLower().Contains(" website at")
                     || p.InnerText.ToLower().Contains("website:")
                    || p.InnerText.ToLower().Contains("our website")
                     || p.InnerText.ToLower().Contains("visit website")
                     || p.InnerText.ToLower().Contains("please visit")
                    || p.InnerText.ToLower().Contains(" information ")
                    || p.InnerText.ToLower().Contains(" information,")
                    || p.InnerText.ToLower().Contains("more at")
                    || p.InnerText.ToLower().Contains(" visit ")
                     || p.InnerText.ToLower().Contains("inc. (")
                    || p.InnerText.ToLower().Contains("available on"))
                {

                    var urls = RegExp.getUrls(p.InnerText);

                    if (urls.Any())
                        return string.Join(", ", urls);

                    urls = RegExp.getUrls(p.InnerHtml);
                    if (urls.Any())
                        return urls[0];
                }
            }



            var div = doc.DocumentNode.SelectNodes("//div[@class='bw-release-sidebars']");

            if (div == null)
                return null;


            var divLogo = div[0].SelectSingleNode(".//div[@class='bw-release-logos']");

            if (divLogo == null)
                return null;

            var a = divLogo.SelectSingleNode(".//a");

            if (a != null && a.Attributes["href"] != null)
                return a.Attributes["href"].Value.Replace("http://", "");

            return null;
        }

        private bool IsEngish()
        {
            //if (htmlContent.Contains("lang=\"en\""))
            //{
            //    return !NoEnglish();
            //}
            //return false;
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var head = doc.DocumentNode.SelectSingleNode("/html/head");
            if (head != null)
            {
                var culture = head.Attributes.FirstOrDefault(s => s.Name == "culture");
                if (culture != null)
                {
                    var langValue = culture.Value;
                    if (langValue != "en-US")
                        return false;

                }
            }
            if (NoEnglish())
                return false;

            return true;
        }



        public override string GetRelatedLink()
        {
            StringBuilder sb = new StringBuilder();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//a");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.Attributes["title"] != null && p.Attributes["title"].Value.Contains("Link to") && p.Attributes["href"] != null)
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(p.Attributes["href"].Value);
                    }
                }
            }
            return sb.ToString();
        }

        public override string GetSource()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectNodes("//div[@class='bw-release-sidebars']");

            if (div != null)
            {

                var headCompany = div[0].SelectSingleNode(".//div[@class='bw-release-companyinfo']");

                if (headCompany != null)
                {
                    var h3 = headCompany.SelectSingleNode(".//h3");
                    if (h3 != null)
                    {
                        string companyName = Utils.NormalizationString(h3.InnerText);
                        if (!string.IsNullOrEmpty(companyName))
                            Source = companyName;
                        return Source;
                    }
                }

            }
            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps != null)
            {
                foreach (var p in ps)
                {

                    var innerText = Utils.NormalizationString(p.InnerText);
                    int sourseIndex = innerText.Trim().IndexOf("Source:");
                    if (sourseIndex == 0)
                    {
                        return innerText.Replace("Source:", "").Trim();
                    }

                }
            }

            var h33 = doc.DocumentNode.SelectSingleNode("//h3[@itemprop='sourceOrganization']");
            if (h33 != null)
                return Utils.NormalizationString(h33.InnerText);
            return "";
        }

        public override List<HtmlNode> GetContactContent()
        {
            HtmlNode contact = null;
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectNodes("//div[@class='bw-release-contact']");

            var ps = doc.DocumentNode.SelectNodes(".//p");

            if (div != null)
            {
                ps = div[0].SelectNodes(".//p");
            }

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.InnerText.Length < 8)
                    continue;

                var emails = RegExp.getEmails(p.InnerText);

                if (emails.Any() || RegExp.IsNumber(p.InnerText))
                {
                    contact = p;
                }
            }
            if (contact != null)
            {
                contacts.Add(contact);
                return contacts;
            }
            return null;
        }

        public override List<Contact> GetContacts(HtmlNode nodeContact)
        {
            List<Contact> contacts = new List<Contact>();

            Contact currentContact = new Contact();
            contacts.Add(currentContact);

            currentContact.CompanyName = TryToGetCompanyName(nodeContact);
            bool notNext = false;
            bool nextCompanyName = false;
            bool lastAddress = false;
            List<HtmlNode> nodesContact = new List<HtmlNode>();
            bool findeNathing = true;

            if (nodeContact.ChildNodes != null)
            {
                foreach (var ch in nodeContact.ChildNodes)
                {


                    if (ch.Name == "br")
                    {
                        notNext = false;
                    }
                    var text = Utils.NormalizationString(ch.InnerText);

                    if (text.Contains("For ") || text.Contains("for "))
                    {
                        var companyName = text.Replace("For ", "").Replace("for ", "").Trim();
                        //if (IsCompanyName(companyName))
                        {
                            currentContact.CompanyName = companyName;
                            findeNathing = false;
                        }
                    }
                    if (text.Contains(", L.P."))
                    {
                        currentContact.CompanyName = text;
                        findeNathing = false;
                        continue;
                    }

                    if (text == "or" || text == "o" || text == "oder" || text == "ou" || text == "of")
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        continue;
                    }

                    if (text == "o")
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        continue;
                    }


                    if (text == "oder")
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        continue;
                    }

                    if (ch.Name == "b" && ch.InnerText.Contains(":") && !string.IsNullOrEmpty(currentContact.Name))
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        var type2 = Utils.FindType(ch.InnerText);

                        if (type2 != string.Empty)
                            currentContact.Type = type2;
                        else
                        {
                            if (ch.InnerText == "Corporate:")
                            {

                            }
                            else
                            {
                                if (IsCompanyName(ch.InnerText))
                                {
                                    currentContact.CompanyName = Utils.NormalizationString(ch.InnerText);
                                }
                            }
                        }
                        continue;
                    }


                    if (text.Length < 3)
                        continue;

                    if (ch.InnerText.Contains("Sector:") || ch.InnerText.Contains("Sectors:"))
                    {
                        notNext = true;
                        continue;
                    }

                    if (notNext)
                    {
                        // notNext = false;
                        continue;
                    }

                    if (nextCompanyName)
                    {
                        currentContact.CompanyName = Utils.NormalizationString(ch.InnerText);
                        nextCompanyName = false;
                        continue;
                    }
                    else if (ch.InnerText.Trim().IndexOf(" for") > 0 && ch.InnerText.Trim().IndexOf(" for") == ch.InnerText.Trim().Length - " for".Length)
                    {
                        nextCompanyName = true;
                    }

                    var emails = RegExp.getEmails(text);
                    if (emails.Any())
                    {
                        AddEmail(contacts, emails);
                        //currentContact.Email = string.Join(", ", emails);
                        continue;
                    }

                    var urls = RegExp.getUrls(ch.InnerText);
                    if (urls.Any())
                    {
                        currentContact.Url = string.Join(", ", urls);
                        continue;
                    }

                    if (ch.SelectNodes(".//br") != null)
                    {
                        text = Utils.NormalizationString(ch.InnerHtml.Replace("<br>", " "));
                    }
                    text = text.Replace(", Inc.", "IiiiiiiiiiiiiiiiInc").Replace(", LLC", "Lllllllllllllllllc");

                    var parts = text.Split(Dictionaries.Separate, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < parts.Count(); i++)
                    {
                        parts[i] = parts[i].Replace("IiiiiiiiiiiiiiiiInc", ", Inc.").Replace("Lllllllllllllllllc", ", LLC");
                    }
                    //var wasfounded = false;
                    foreach (var p in parts)
                    {
                        var emails2 = RegExp.getEmails(p);
                        if (emails2.Any())
                        {
                            currentContact.Email = string.Join(", ", emails2);
                            findeNathing = false;
                            continue;
                        }

                        var urls2 = RegExp.getUrls2(p);
                        if (urls2.Any())
                        {
                            currentContact.Url = string.Join(", ", urls2);
                            findeNathing = false;
                            continue;
                        }

                        if (Utils.HasTitle(p))
                        {
                            currentContact.Title = Utils.GetTitle(p);
                            if (currentContact.Title != null && p != currentContact.Title && p.Length - currentContact.Title.Length <= 2)
                            {
                                foreach (var c in contacts)
                                {
                                    if (string.IsNullOrEmpty(c.Title))
                                    {
                                        c.Title = currentContact.Title;
                                        findeNathing = false;
                                    }
                                }
                            }
                            continue;
                        }

                        var numbers = RegExp.GetPnoneNumbers(p);

                        if (numbers.Any() || RegExp.IsNumber(p))
                        {
                            var number = Utils.NormPhone(p);
                            if (currentContact.Phone == null || !currentContact.Phone.Contains(number))
                            {
                                if (!string.IsNullOrEmpty(currentContact.Phone))
                                    currentContact.Phone += ", ";
                                currentContact.Phone += number;
                                findeNathing = false;
                            }
                            continue;
                        }

                        var type = Utils.FindType(p);
                        if (!string.IsNullOrEmpty(type))
                        {
                            findeNathing = false;
                            currentContact.Type = type;
                            var text2 = Utils.CutType(p, type);

                            if (Utils.HasTitle(text2))
                            {
                                currentContact.Title = text2;
                                findeNathing = false;
                                continue;
                            }
                            if (IsCompanyName(text2))
                                currentContact.CompanyName = text2;

                            continue;
                        }


                        if (IsCompanyName(p))
                        {
                           
                            findeNathing = false;

                            if (string.IsNullOrEmpty(currentContact.CompanyName)) 
                            {
                                if (IsCompanyName(Utils.NormalizationString(ch.InnerText)))
                                {
                                    currentContact.CompanyName = Utils.NormalizationString(ch.InnerText);
                                }
                                else
                                    currentContact.CompanyName = p;
                            }
                                

                        }
                        else
                        {
                            if (RegExp.IsName(p))
                            {
                                findeNathing = false;
                                if (string.IsNullOrEmpty(currentContact.Name))
                                    currentContact.Name = Utils.NormName(p);
                                else
                                {
                                    currentContact = new Contact();
                                    contacts.Add(currentContact);
                                    currentContact.Name = Utils.NormName(p);
                                }
                            }
                        }
                    }

                    if ( currentContact.CompanyName == null &&
                        findeNathing && !string.IsNullOrEmpty(ch.InnerText) && !ch.InnerText.Contains(":")
                        && !ValidationHelpers.IsValidStreetAddress(ch.InnerText)
                        && Utils.IsCompanyNameNotStrong(Utils.NormalizationString(ch.InnerText)))
                    {
                        currentContact.CompanyName = Utils.NormalizationString(ch.InnerText);
                    }

                }

            }
            if (contacts != null && contacts.Any())
            {
                List<Contact> noEmail = contacts.Where(c => string.IsNullOrEmpty(c.Email)).ToList();
                if (noEmail != null && noEmail.Any() && !string.IsNullOrEmpty(contacts.Last().Email) && !Utils.trueEmail(contacts.Last().Email, contacts.Last().Name))
                {
                    foreach (var c in noEmail)
                    {
                        c.Email = contacts.Last().Email;
                    }
                }

                List<Contact> noPhone = contacts.Where(c => string.IsNullOrEmpty(c.Phone)).ToList();
                if (noPhone != null && noPhone.Any() && !string.IsNullOrEmpty(contacts.Last().Phone))
                {
                    foreach (var c in noPhone)
                    {
                        c.Phone = contacts.Last().Phone;
                    }
                }

                List<Contact> noCompanyName = contacts.Where(c => string.IsNullOrEmpty(c.CompanyName)).ToList();
                if (noCompanyName != null && noCompanyName.Any() && !string.IsNullOrEmpty(contacts.First().CompanyName))
                {
                    foreach (var c in noCompanyName)
                    {
                        c.CompanyName = contacts.First().CompanyName;
                    }
                }
            }
            return contacts;
        }

        private void AddEmail(List<Contact> contacts, string[] emails)
        {
            foreach (var e in emails)
            {
                var find = false;

                foreach (var c in contacts)
                {
                    if (!string.IsNullOrEmpty(c.Name) && Utils.trueEmail(e, c.Name))
                    {
                        if (!string.IsNullOrEmpty(c.Email))
                            c.Email += ", ";
                        c.Email = c.Email + e;
                        find = true;
                    }
                }
                if (!find)
                {
                    if (contacts.Any(c => string.IsNullOrEmpty(c.Email)))
                        contacts.Last(c => string.IsNullOrEmpty(c.Email)).Email = e;
                    else
                    {
                        if (!contacts.Any(c => !c.Email.Contains(e)))
                        {
                            foreach (var c in contacts)
                            {
                                if (!string.IsNullOrEmpty(c.Email))
                                    c.Email += ", ";
                                c.Email = c.Email + e;

                            }
                        }
                    }
                }
            }
        }

        private string TryGetName(string innerText)
        {
            innerText = innerText.Replace("&#160;", " ");
            if (!innerText.Contains(" at ") || !innerText.ToLower().Contains("contact")) return null;

            var indexOfContact = innerText.ToLower().IndexOf("contact");
            var indexOffAt = innerText.ToLower().IndexOf(" at ", indexOfContact);

            if (indexOffAt <= 0) return null;
            var name = Utils.NormalizationString(innerText.Substring(indexOfContact + "contact".Length, indexOffAt - (indexOfContact + "contact".Length)));
            return RegExp.IsName(name) ? name : null;
        }

        private List<Contact> TryParceMoreCntacts(HtmlNode nodeContact)
        {
            List<Contact> contacts = new List<Contact>();
            Contact currentContact = new Contact();

            List<Contact> contactsList = new List<Contact>();

            string curentCompanyName = "";

            contacts.Add(currentContact);

            if (nodeContact.ChildNodes != null)
            {
                foreach (var ch in nodeContact.ChildNodes)
                {
                    if (ch.Name == "a" && RegExp.getEmails(ch.InnerText).Count() > 0)
                    {
                        var emails = RegExp.getEmails(ch.InnerText);
                        if (contactsList.Any())
                        {
                            if (contactsList.Count() == emails.Count())
                            {
                                for (int i = 0; i < contactsList.Count(); i++)
                                {
                                    contactsList[i].Email = emails[i];
                                }
                            }
                            else
                            {
                                foreach (var e in emails)
                                {
                                    bool finde = false;
                                    foreach (var c in contactsList)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) && Utils.trueEmail(e, c.Name))
                                        {
                                            c.Email = e;
                                            finde = true;
                                            break;
                                        }
                                    }
                                    if (!finde)
                                    {
                                        foreach (var c in contactsList)
                                        {
                                            if (string.IsNullOrEmpty(c.Email))
                                            {
                                                c.Email = e;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            currentContact.Email = string.Join(", ", emails);
                        }
                    }
                    if (ch.Name == "a" && !ch.InnerText.Contains("@") && !ch.InnerText.Contains("photos.prnewswire") && RegExp.getUrls(ch.InnerText).Count() > 0)
                    {
                        currentContact.Url = ch.InnerText;
                    }
                    if (ch.Name == "b" && !ch.InnerText.ToLower().Contains("contact"))
                    {
                        curentCompanyName = ch.InnerText;
                    }
                    if (ch.Name == "span" && (ch.Attributes["class"] != null && ch.Attributes["class"].Value == "xn-person"
                        || ch.Attributes["itemprop"] != null && ch.Attributes["itemprop"].Value == "name"))
                    {
                        if (string.IsNullOrEmpty(currentContact.Name))
                        {
                            currentContact.Name = Utils.NormalizationString(ch.InnerText);
                            currentContact.CompanyName = curentCompanyName;
                        }
                        else
                        {
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            currentContact.Name = curentCompanyName;

                            contactsList = new List<Contact>();
                        }
                    }

                    if (ch.Name == "#text")
                    {
                        var s = Utils.NormalizationString(ch.InnerText);

                        if (s == "/")
                        {
                            contactsList.Add(currentContact);

                            currentContact = new Contact();
                            contacts.Add(currentContact);

                            contactsList.Add(currentContact);

                            currentContact.CompanyName = curentCompanyName;
                            continue;

                        }
                        if (s.ToLower().Contains("contact"))
                        {
                            //next contact
                            if (!string.IsNullOrEmpty(currentContact.Name))
                            {
                                currentContact = new Contact();
                                contacts.Add(currentContact);
                                currentContact.Name = curentCompanyName;
                            }
                            continue;
                        }
                        if (s.Length < 5)
                            continue;
                        if (s.ToLower().Contains("e-mail") || s.ToLower().Contains("email"))
                            continue;
                        var numbers = RegExp.GetPnoneNumbers(s);
                        if (numbers.Any())
                        {
                            if (numbers.Count() > 1)
                            {
                                var numbersList = s.Split('/');
                                if (numbersList.Count() == contactsList.Count())
                                {
                                    for (int i = 0; i < numbersList.Count(); i++)
                                    {
                                        contactsList[i].Phone = Utils.NormPhone(numbersList[i]);
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < numbersList.Count(); i++)
                                    {

                                        if (!numbersList[i].ToLower().Contains("fax:") && !numbersList[i].ToLower().Contains("f:") && !numbersList[i].ToLower().Contains("fax.:"))
                                        {
                                            if (string.IsNullOrEmpty(currentContact.Phone) || !currentContact.Phone.Contains(Utils.NormPhone(s)))
                                            {
                                                if (!string.IsNullOrEmpty(currentContact.Phone))
                                                    currentContact.Phone = currentContact.Phone + ", ";
                                                currentContact.Phone = currentContact.Phone + Utils.NormPhone(numbersList[i]);
                                            }

                                        }
                                    }
                                }
                            }
                            else
                            {

                                if (!s.ToLower().Contains("fax:") && !s.ToLower().Contains("f:") && !s.ToLower().Contains("fax.:"))
                                {
                                    if (string.IsNullOrEmpty(currentContact.Phone) || !currentContact.Phone.Contains(Utils.NormPhone(s)))
                                    {
                                        if (!string.IsNullOrEmpty(currentContact.Phone))
                                            currentContact.Phone = currentContact.Phone + ", ";
                                        currentContact.Phone = currentContact.Phone + Utils.NormPhone(s);
                                    }
                                    if (contactsList.Any())
                                    {
                                        foreach (var c in contactsList)
                                        {
                                            if (!string.IsNullOrEmpty(c.Phone))
                                                c.Phone = c.Phone + ", ";
                                            c.Phone = c.Phone + Utils.NormPhone(s);
                                        }
                                    }
                                }
                            }

                            continue;
                        }
                        if (string.IsNullOrEmpty(currentContact.Name) && Utils.StringContensName(s))
                        {
                            currentContact.Name = s;
                            continue;
                        }
                        if (string.IsNullOrEmpty(currentContact.CompanyName) && IsCompanyName(s))
                        {
                            currentContact.CompanyName = s;
                            continue;
                        }
                        var title = Utils.GetTitle(s);
                        if (!string.IsNullOrEmpty(title))
                        {
                            currentContact.Title = title;
                            continue;
                        }
                        var type = Utils.FindType(s);
                        if (!string.IsNullOrEmpty(type))
                        {
                            currentContact.Type = type;
                            continue;
                        }
                        int position = s.IndexOf(":");

                        if (position == s.Length - 1)
                        {
                            currentContact.Type = s.Remove(position);
                            continue;
                        }

                        if (RegExp.IsName(s) && string.IsNullOrEmpty(currentContact.Name))
                            currentContact.Name = s;

                    }
                }

            }
            for (int i = 0; i < contacts.Count; i++)
            {
                var cont = contacts[i];
                if (string.IsNullOrEmpty(cont.Phone) && string.IsNullOrEmpty(cont.Email))
                {
                    var j = i + 1;
                    while (j < contacts.Count)
                    {
                        if (!string.IsNullOrEmpty(contacts[j].Phone) || !string.IsNullOrEmpty(contacts[j].Email))
                        {
                            cont.Email = contacts[j].Email;
                            cont.Phone = contacts[j].Phone;
                            break;
                        }
                        j++;
                    }
                }
            }
            return contacts;
        }

        private string TryToGetCompanyName(HtmlNode nodeContact)
        {
            var previos = nodeContact.PreviousSibling;
            while (previos != null)
            {
                var b = previos.SelectSingleNode("./b");
                if (b != null)
                {
                    if (b.InnerText.IndexOf("About") == 0 && b.InnerText.Length < maxLengContactInfo)
                    {
                        var companyName = Utils.NormalizationString(b.InnerText.Replace("About", "").Trim());
                        if (companyName.Length > 2)
                            return companyName.Replace("&#160;", " ");
                        else
                            return null;

                    }
                }
                previos = previos.PreviousSibling;
            }
            return null;
        }

        private bool TryParseLinesInformation(List<Contact> contacts, string contactText)
        {
            string[] stringData = contactText.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var st in stringData)
            {
                var parts = Utils.NormalizationString(st).Split(Dictionaries.Separate, StringSplitOptions.RemoveEmptyEntries);

                foreach (var p in parts)
                {
                    var s = Utils.NormalizationString(p);
                    if (s.Length < 5)
                        continue;
                    if (s.ToLower().Contains("e-mail") || s.ToLower().Contains("email"))
                        continue;
                    var numbers = RegExp.GetPnoneNumbers(s);
                    if (numbers.Any())
                    {
                        if (!s.ToLower().Contains("fax:") && !s.ToLower().Contains("f:") && !s.ToLower().Contains("fax.:"))
                        {
                            if (string.IsNullOrEmpty(contacts.First().Phone) || !contacts.First().Phone.Contains(Utils.NormPhone(s)))
                            {
                                if (!string.IsNullOrEmpty(contacts.First().Phone))
                                    contacts.First().Phone = contacts.First().Phone + ", ";
                                contacts.First().Phone = contacts.First().Phone + Utils.NormPhone(s);
                            }
                        }

                        continue;
                    }
                    if (string.IsNullOrEmpty(contacts.First().Name) && Utils.StringContensName(s))
                    {
                        contacts.First().Name = s;
                        continue;
                    }
                    if (string.IsNullOrEmpty(contacts.First().CompanyName) && IsCompanyName(s))
                    {
                        contacts.First().CompanyName = s;
                        continue;
                    }
                    var title = Utils.GetTitle(s);
                    if (!string.IsNullOrEmpty(title))
                    {
                        contacts.First().Title = title;
                        continue;
                    }
                    var type = Utils.FindType(s);
                    if (!string.IsNullOrEmpty(type))
                    {
                        contacts.First().Type = type;
                        continue;
                    }
                    int position = s.IndexOf(":");

                    if (position == s.Length - 1)
                    {
                        contacts.First().Type = s.Remove(position);
                        continue;
                    }

                    if (RegExp.IsName(s) && string.IsNullOrEmpty(contacts.First().Name))
                        contacts.First().Name = s;

                }
            }
            return true;
        }

        public string RemoveWordContact(string p)
        {
            foreach (var c in Dictionaries.MarkerContact)
            {
                int indexOffContact = p.ToLower().IndexOf(c);
                if (indexOffContact > 0)
                {
                    int indexOffBR = p.IndexOf("<br>", indexOffContact);
                    if (indexOffBR > 0)
                        return p.Remove(0, indexOffBR + "<br>".Length);
                    else
                        return p.Remove(0, indexOffContact + c.Length);
                }
            }

            return p;
        }



        
    }
}
