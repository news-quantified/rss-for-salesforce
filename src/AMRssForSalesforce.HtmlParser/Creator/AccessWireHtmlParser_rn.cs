﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using HtmlAgilityPack;
    using Infrastructure.Helpers;
    using Infrastructure.Constants;
    public class AccessWireHtmlParser_rn: Abstract.HtmlParser
    {
        private const int maxLengContactInfo = 50;

        private string generalEmail = null;

        string curentType = null;
        public AccessWireHtmlParser_rn(string _url) : base(_url) { }

        public AccessWireHtmlParser_rn(string _fileName, bool fromFile) : base(_fileName, fromFile) { }

        public override Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed)
        {
            try
            {
                rssFeed.English = true;
                Source = GetSource();
                rssFeed.Dateline = GetDeteLine();
                rssFeed.Redirect = responsUri;
                
                if (!string.IsNullOrEmpty(Source) && companyNames.All(cn => cn != Source))
                {
                    companyNames.Add(Source);
                }

                var contactNodes = GetContactContent();

                var contacts = new List<Contact>();

                if (contactNodes != null && contactNodes.Any())
                    foreach (var contactNode in contactNodes)
                    {
                        if (Utils.NormalizationString(contactNode.InnerText).ToLower() == "contacts:")
                            continue;
                        var contacts1 = GetContacts(contactNode);
                        foreach (var c in contacts1)
                        {
                            if (string.IsNullOrEmpty(c.Name) && string.IsNullOrEmpty(c.Email) && string.IsNullOrEmpty(c.Phone))
                                continue;
                            if (contacts.Any())
                            {

                                if (!string.IsNullOrEmpty(c.Name) && contacts.Any(con => con.Name == c.Name))
                                {
                                    var contactHas = contacts.First(con => con.Name == c.Name);
                                    if (!string.IsNullOrEmpty(c.Email))
                                        contactHas.Email = c.Email;

                                    if (!string.IsNullOrEmpty(c.Phone))
                                        contactHas.Phone = c.Phone;

                                    if (!string.IsNullOrEmpty(c.Url))
                                        contactHas.Url = c.Url;

                                    if (!string.IsNullOrEmpty(c.Title) && string.IsNullOrEmpty(contactHas.Title))
                                        contactHas.Title = c.Title;

                                    if (!string.IsNullOrEmpty(c.Type) && string.IsNullOrEmpty(contactHas.Type))
                                        contactHas.Type = c.Type;

                                    if (!string.IsNullOrEmpty(c.CompanyName) && string.IsNullOrEmpty(contactHas.CompanyName))
                                        contactHas.CompanyName = c.CompanyName;

                                    continue;
                                }

                                if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                    && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                {
                                    if (string.IsNullOrEmpty(c.Name) && !string.IsNullOrEmpty(contacts.Last().Name)
                                        && string.IsNullOrEmpty(contacts.Last().Email) && string.IsNullOrEmpty(contacts.Last().Phone))
                                    {
                                        contacts.Last().Email = c.Email;
                                        contacts.Last().Phone = c.Phone;
                                    }
                                    else if (!string.IsNullOrEmpty(c.Name) || !contacts.Any(ccc => ccc.Phone == c.Phone && ccc.Email == c.Email))
                                        contacts.Add(c);
                                }


                            }
                            else
                                contacts.Add(c);
                        }
                    }

                string url = TryToFindeUrlItText();

                generalEmail = TryGetEmail();
                foreach (var c in contacts)
                {
                    if (!string.IsNullOrEmpty(Source))
                    {
                        c.Source = Source;
                        if (string.IsNullOrEmpty(c.CompanyName))
                            c.CompanyName = Source;
                    }
                    c.NewsID = this.urlHtml;

                    if (string.IsNullOrEmpty(c.Email))
                        c.Email = generalEmail;

                    if (string.IsNullOrEmpty(c.Url))
                    {
                        c.Url = string.IsNullOrEmpty(url) ? GetRelatedLink(c) : url;
                    }

                    if (c.CompanyName != null && c.Title != null && c.CompanyName.IndexOf(c.Title) == 0)
                    {
                        c.CompanyName = c.CompanyName.Replace(c.Title, "").Trim();
                    }
                    //if(string.IsNullOrEmpty(c.Url) && !string.IsNullOrEmpty(c.Email))
                    //{
                    //    c.Url = GetRelatedLink(c);
                    //}
                }
                rssFeed.ContactInfo = Utils.SummContactData(contacts);
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                if(contacts == null || !contacts.Any())
                {
                    if (contacts == null)
                        contacts = new List<Contact>();
                    contacts.Add(new Contact() { Source = Source, NewsID = rssFeed.Link });
                }
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = this.urlHtml });
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
        }


       

        private string GetDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='articlepreview']") ??
                      doc.DocumentNode.SelectSingleNode("//div[@class='mw_release']");

            if (div == null) return ".";

            var ps = div.SelectNodes(".//strong");

            var pps = div.SelectNodes(".//p");

            if (ps == null)
                ps = pps;
            else
            {
                if( pps != null )
                {
                    foreach(var p in pps)
                    {
                        ps.Add(p);
                    }
                }
            }
            if (ps == null) 
                return ".";
            foreach (var pp in ps)
            {
                var p = Utils.NormalizationString3(pp.InnerText);

                if (!p.Contains(DateTime.Now.Year.ToString()) && !p.Contains((DateTime.Now.Year - 1).ToString()) && !p.Contains((DateTime.Now.Year - 2).ToString()))
                    continue;

                if (p.Contains(" / "))
                {
                    //TODO Why?    
                    {
                        var strings = p.Split(new string[] { " / " }, StringSplitOptions.RemoveEmptyEntries);
                        if (strings.Any())
                            return Utils.NormalizationString(strings[0]);
                        else
                        {

                        }
                    }
                }
                if (p.Contains("--"))
                {
                    //TODO Why?    
                    {
                        var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                        if (strings.Any())
                            return Utils.NormalizationString(strings[0]);
                        else
                        {

                        }
                    }
                }
            }
            return ".";
        }
        private string TryGetEmail()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectNodes("//div[@id='secondary-right']");

            if(div == null)
            {
                return null;
            }
            if(div.Count() > 1)
            {

            }

            var emails = RegExp.getEmails(div[0].InnerHtml);

            if (emails.Any())
                return string.Join(", ", emails);

            return null;
        }

        private string TryToFindeUrlItText()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("visited at") || p.InnerText.ToLower().Contains("information, visit")
                    || p.InnerText.ToLower().Contains("available at")
                     || p.InnerText.ToLower().Contains("the company website at")
                    || p.InnerText.ToLower().Contains(" website at")
                     || p.InnerText.ToLower().Contains("website:")
                    || p.InnerText.ToLower().Contains("our website")
                     || p.InnerText.ToLower().Contains("visit website"))
                {

                    var urls = RegExp.getUrls(p.InnerText);

                    if (urls.Any())
                        return string.Join(", ", urls);
                }
                else
                {
                    var urls = RegExp.getUrls(p.InnerText);
                    if(urls.Count() == 1 && urls[0] == Utils.NormalizationString(p.InnerText))
                        return urls[0];
                }
            }

            var div = doc.DocumentNode.SelectNodes("//div[@class='secondary-right']");

            if (div == null)
            {
                return null;
            }
            if (div.Count() > 1)
            {

            }

            var urls2 = RegExp.getUrls(div[0].InnerHtml);

            if (urls2.Any())
            {
                foreach(var url in urls2)
                {
                    if (!url.Contains("accesswire"))
                        return url;
                }
            }

            return null;
        }

        private bool IsEngish()
        {
            if (htmlContent.Contains("lang=\"en-US\""))
            {
                return !NoEnglish();
            }
            return false;
        }

       

        public override string GetRelatedLink()
        {
            StringBuilder sb = new StringBuilder();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//a");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.Attributes["title"] != null && p.Attributes["title"].Value.Contains("Link to") && p.Attributes["href"] != null)
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(p.Attributes["href"].Value);
                    }
                }
            }
            return sb.ToString();
        }

        public override string GetSource()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    
                        var innerText = Utils.NormalizationString(p.InnerText);
                        int SourseIndex = innerText.ToLower().Trim().IndexOf("source:");
                        if (SourseIndex == 0)
                        {
                            return innerText.Remove(0, "Source:".Length).Trim();
                        }
                    
                }
            }

            var h3 = doc.DocumentNode.SelectSingleNode("//h3[@itemprop='sourceOrganization']");
            if (h3 != null)
                return Utils.NormalizationString(h3.InnerText);

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='articlepreview']");
            if(div != null)
            {
                var strong = div.SelectNodes(".//strong");
                
                if(strong != null)
                {
                    foreach(var s in strong)
                    {
                        
                        if (s.InnerText.Contains("SOURCE:") && s.NextSibling!= null)
                        {
                            var sourse = Utils.NormalizationString(s.NextSibling.InnerText);
                            if (!string.IsNullOrEmpty(sourse))
                                return sourse;
                        }
                        
                    }
                }
            }
            return "";
        }

        public override List<HtmlNode> GetContactContent()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            bool firstBR = true;
            bool findeContact = false;
            var div = doc.DocumentNode.SelectNodes("//div[@class='content-area']");
            
            if(div == null)
            {
                div = doc.DocumentNode.SelectNodes("//div[@class='ak-container']");
            }
            if(div == null)
            {
                div = doc.DocumentNode.SelectNodes("//div[@class='mw_release']");
            }



            //var ps = doc.DocumentNode.SelectNodes(".//p");
            //if (div != null)
            //    ps = div[0].SelectNodes(".//p");

            //if (div.Count > 1)
            //{

            //}

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.InnerText.Length < 8)
                    continue;

                if (p.InnerText.ToLower().Contains("contact:") && contacts.Any() && !findeContact)
                {
                    findeContact = true;
                    contacts.Clear();
                }

                var emails = RegExp.getEmails(p.InnerHtml);
                
                var numbers = RegExp.GetPnoneNumbers(RegExp.GetInnerText(p.InnerHtml));

                if (emails.Any() || numbers.Any() || p.InnerText.Contains("E-mail:") || p.InnerText.Contains("Email:")
                    || p.InnerText.Contains("Name:") || p.InnerText.Contains("Phone:") || p.InnerText.Contains("Organization:"))
                {
                    if(contacts.Any())
                    {
                        if (p.SelectNodes(".//br") != null && emails.Any() && firstBR)
                        {
                            firstBR = false;
                            contacts.Clear();
                        }
                    }
                    else
                    {
                        if (p.SelectNodes(".//br") != null )
                            firstBR = false;
                    }

                    contacts.Add(p);
                   
                }
                else if(p.SelectNodes(".//br") != null && Utils.HasTitle(p.InnerText) && Utils.NormalizationString(p.InnerText).Length < 300)
                {
                    contacts.Add(p);
                    firstBR = false;
                }
                else if ((p.InnerText.ToLower().Contains("contacts:") || p.InnerText.ToLower().Contains("contact:")) && !string.IsNullOrEmpty(Utils.FindType(p.InnerText)))
                {
                    firstBR = false;
                    contacts.Add(p);
                }
                else if (p.InnerText.ToLower().Contains("please contact")  && RegExp.GetPnoneNumbers(p.InnerText).Any())
                {
                    
                    contacts.Add(p);
                }
            }
            return contacts;
        }
        private static string[] _separate = new string[] { " or ", "/", ";", "|", " for ", "&#8226;", "please contact", " at ", "or visit" };
        public override List<Contact> GetContacts(HtmlNode nodeContact)
        {
            List<Contact> contacts = new List<Contact>();

            Contact currentContact = new Contact {Type = curentType};


            Contact prevContact = null;
            contacts.Add(currentContact);
            bool notNext = false;
            bool nextCompanyName = false;
 
            if (nodeContact.ChildNodes != null)
            {
                bool nodeHasePhone;
               
                foreach (var ch in nodeContact.ChildNodes)
                {
                    nodeHasePhone = false;
                    if(ch.Name == "a")
                    {
                        if (ch.Attributes["href"] != null)
                        {
                            var emails2 = RegExp.getEmails(ch.OuterHtml);

                            if(emails2.Any())
                            {
                                foreach(var e in emails2)
                                {
                                    AddEmailToContactList(contacts, e);
                                }
                            }
                            else
                            {

                                var urls2 = RegExp.getUrls(Utils.NormalizationString(ch.Attributes["href"].Value));
                                if(urls2.Any())
                                   currentContact.Url = urls2[0];
                                else
                                {
                                    var phones2 = RegExp.GetPnoneNumbers(Utils.NormalizationString(ch.InnerHtml));
                                    if(phones2.Any() )
                                    {
                                        currentContact.Phone = string.Join(", ", phones2);
                                    }
                                }
                            }
                            //if(ch.Attributes["href"].Value.Contains("mailto:"))
                            //{
                            //    if (RegExp.IsEmail(ch.Attributes["href"].Value.Replace("mailto:", "")))
                            //        AddEmailToContactList(contacts, ch.Attributes["href"].Value.Replace("mailto:", ""));
                            //}
                            // else if(RegExp.IsEmail(ch.InnerText))
                            //{
                            //    AddEmailToContactList(contacts, RegExp.GetEmail(ch.InnerText));
                            //}
                            //else if(RegExp.GetPnoneNumbers(ch.InnerText).Any())
                            //{
                            //    currentContact.Phone = Utils.NormPhone(ch.InnerText);
                            //}
                            //else
                            //{
                            //    if (RegExp.getUrls(ch.Attributes["href"].Value).Any())
                            //        currentContact.Url = ch.Attributes["href"].Value;
                            //    else if(RegExp.getUrls(ch.InnerText).Any())
                            //        currentContact.Url = ch.InnerText;
                            //    else
                            //    {
                            //        var email = ch.InnerText.Replace(" (at) ", "@").Replace(" (dot) ", ".").Replace("(at)", "@").Replace("(dot)", ".");
                            //        if (RegExp.IsEmail(email))
                            //            AddEmailToContactList(contacts, email);
                            //    }
                            //}
                        }
                        continue;
                    }
                    var text = Utils.NormalizationString(ch.InnerText);

                    if(text.ToLower().Contains("name:"))
                    {
                        var names = text.Split(':');
                        if(names.Count() == 2 && RegExp.IsName(names[1]))
                        {
                            currentContact.Name = Utils.NormName(names[1]);
                            continue;
                        }
                    }
                    else if (text.ToLower().Contains("organization:") || text.ToLower().Contains("company:"))
                    {
                        var names = text.Split(':');
                        if (names.Count() == 2 )
                        {
                            currentContact.CompanyName = Utils.NormName(names[1]);
                            continue;
                        }
                        continue;
                    }

                    if(text == "or")
                    {
                        currentContact = new Contact {Type = curentType};
                        contacts.Add(currentContact);
                        continue;
                    }

                    if (text.Length < 3)
                        continue;

                    if (ch.InnerText.Contains("Sector:") || ch.InnerText.Contains("Sectors:"))
                    {
                        notNext = true;
                        continue;
                    }

                    if(notNext)
                    {
                       // notNext = false;
                        continue;
                    }

                    if(nextCompanyName)
                    {
                        currentContact.CompanyName = Utils.NormalizationString(ch.InnerText);
                        nextCompanyName = false;
                        continue;
                    }else if (ch.InnerText.Trim().IndexOf(" for") > 0 && ch.InnerText.Trim().IndexOf(" for") == ch.InnerText.Trim().Length - " for".Length)
                    {
                        nextCompanyName = true;
                    }

                    var emails = RegExp.getEmails(text);
                    if (emails.Any())
                    {
                        currentContact.Email = string.Join(", ", emails);
                        if (Utils.NormalizationString(ch.InnerText).Replace(emails[0], "").Length < 5)
                            continue;

                    }

                    var urls = RegExp.getUrls(ch.InnerText);
                    if (urls.Any())
                    {
                        currentContact.Url = string.Join(", ", urls);
                        if(Utils.NormalizationString(ch.InnerText).Replace(urls[0],"").Length < 5)
                            continue;
                    }
                    
                     if (ch.InnerText.Contains("Address:"))
                        continue;

                    if(ch.SelectNodes(".//br") != null)
                    {
                        text = Utils.NormalizationString(ch.InnerHtml.Replace("<br>", " "));
                    }
                    text = text.Replace(", Inc.", "IiiiiiiiiiiiiiiiInc").Replace(", LLC", "Lllllllllllllllllc");

                    var parts = text.Split(_separate, StringSplitOptions.RemoveEmptyEntries);

                        for (int i = 0; i < parts.Count(); i++ )
                        {
                            parts[i] = parts[i].Replace("IiiiiiiiiiiiiiiiInc", ", Inc.").Replace( "Lllllllllllllllllc",", LLC");
                        }

                            foreach (var p in parts)
                            {
                                var word = p.Trim();
                                var emails2 = RegExp.getEmails(p);
                                if (emails2.Any())
                                {
                                    currentContact.Email = string.Join(", ", emails2);
                                    continue;
                                }

                                if (Utils.HasTitle(word))
                                {
                                    if (currentContact.Title != null)
                                    {
                                        currentContact = new Contact {Type = curentType};
                                        contacts.Add(currentContact);
                                    }
                                    currentContact.Title = Utils.GetTitle(word);
                                    var type2 = Utils.FindType(word);
                                    if (type2 != null)
                                        currentContact.Type = type2;
                                    if (currentContact.Title!= null &&  word != currentContact.Title && word.Length - currentContact.Title.Length <= 2)
                                    {
                                        foreach (var c in contacts)
                                        {
                                            if (string.IsNullOrEmpty(c.Title))
                                                c.Title = currentContact.Title;
                                        }
                                    }
                            if (word.Contains(","))
                            {
                                var nameTitles = word.Split(',');
                                if (nameTitles.Count() > 1 && Utils.HasTitle(nameTitles[1]))
                                {
                                    word = nameTitles[0];
                                }

                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(word) && currentContact.Title != null)
                                {
                                    word = Utils.NormalizationString(word.Replace(currentContact.Title, " "));
                                    if (word.Length < 5)
                                        continue;
                                }
                                else
                                    continue;
                            }   
                                }

                                var numbers = RegExp.GetPnoneNumbers(word);

                                if (numbers.Any() || RegExp.IsNumber(word))
                                {
                                    var number = Utils.NormPhone(word);
                                    if (!nodeHasePhone && ( contacts.Count>=2 && contacts[contacts.Count -2].Phone != null))
                                    {
                                        nodeHasePhone = true;
                                        currentContact.Phone = number;
                                    }
                                    else
                                    {
                                        AddPhoneToContacts(contacts, number);
                                    }
                                    continue;
                                }

                                var type = Utils.FindType(word);
                                if (!string.IsNullOrEmpty(type) && !IsCompanyName(word))
                                {
                                    curentType = type;
                                    currentContact.Type = type;
                                    var text2 = Utils.CutType(word, type);

                                    if (Utils.HasTitle(text2))
                                    {
                                        currentContact.Title = text2;
                                        continue;
                                    }
                                    if (IsCompanyName(text2))
                                        currentContact.CompanyName = text2;

                                    continue;
                                }


                                if (IsCompanyName(word))
                                {
                                    if (string.IsNullOrEmpty(currentContact.CompanyName))
                                        currentContact.CompanyName = Utils.GetCompanyName(word);

                                }
                                else
                                {
                                    if (RegExp.IsName(word))
                                    {
                                        if (string.IsNullOrEmpty(currentContact.Name))
                                            currentContact.Name = Utils.NormName(word);
                                        else
                                        {
                                            currentContact = new Contact {Type = curentType};
                                            contacts.Add(currentContact);
                                            currentContact.Name = Utils.NormName(word);
                                        }
                                    }
                                    else
                                    {
                                        if (word.Contains(","))
                                        {
                                            var partWords = word.Split(',');
                                            foreach(var ww in partWords)
                                            {
                                                var titleP = Utils.GetTitle(ww);
                                                if(titleP != null)
                                                {
                                                    currentContact.Title = titleP;
                                                }
                                                else if(RegExp.IsName(ww.Trim()))
                                                {
                                                    currentContact.Name = ww.Trim();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (word.Contains(" contact ") && word.Contains(" at"))
                                            {
                                                var indexOffContact = word.IndexOf(" contact ");
                                                var indexOffAt = word.IndexOf(" at");
                                                if (indexOffContact > 0 && indexOffAt > 0 && indexOffAt > indexOffContact)
                                                {
                                                    var name = word.Substring(indexOffContact + " contact ".Length, indexOffAt - (indexOffContact + " contact ".Length)).Trim();
                                                    if (RegExp.IsName(name))
                                                        currentContact.Name = name;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    
                }
                
            }
            if (contacts != null && contacts.Any())
            {
                List<Contact> noEmail = contacts.Where(c => string.IsNullOrEmpty(c.Email)).ToList();
                if (noEmail != null && noEmail.Any() && !string.IsNullOrEmpty(contacts.Last().Email) && !Utils.trueEmail(contacts.Last().Email, contacts.Last().Name))
                {
                    foreach (var c in noEmail)
                    {
                        c.Email = contacts.Last().Email;
                    }
                }

                List<Contact> noPhone = contacts.Where(c => string.IsNullOrEmpty(c.Phone)).ToList();
                if (noPhone != null && noPhone.Any() && !string.IsNullOrEmpty(contacts.Last().Phone))
                {
                    foreach (var c in noPhone)
                    {
                        c.Phone = contacts.Last().Phone;
                    }
                }

                List<Contact> noCompanyName = contacts.Where(c => string.IsNullOrEmpty(c.CompanyName)).ToList();
                if (noCompanyName != null && noCompanyName.Any() && !string.IsNullOrEmpty(contacts.First().CompanyName))
                {
                    foreach(var c in noCompanyName)
                    {
                        c.CompanyName = contacts.First().CompanyName;
                    }
                }

                noCompanyName = contacts.Where(c => string.IsNullOrEmpty(c.CompanyName)).ToList();
                if (noCompanyName != null && noCompanyName.Any() && !string.IsNullOrEmpty(contacts.Last().CompanyName))
                {
                    foreach (var c in noCompanyName)
                    {
                        c.CompanyName = contacts.Last().CompanyName;
                    }
                }
            }
            return contacts;
        }

        private void AddPhoneToContacts(List<Contact> contacts, string number)
        {
            if (string.IsNullOrEmpty(number))
                return;
            if (contacts.Count() == 2)
            {
                if (!string.IsNullOrEmpty(contacts[0].Phone))
                    contacts[1].Phone = number;
                else
                {
                    contacts[0].Phone = number;
                    contacts[1].Phone = number; 
                }
            }
            else
            {
                foreach(var currentContact in contacts)
                if (currentContact.Phone == null || !currentContact.Phone.Contains(number))
                {
                    if (!string.IsNullOrEmpty(currentContact.Phone))
                        currentContact.Phone += ", ";
                    currentContact.Phone += number;
                }
            }
        }

        private void AddEmailToContactList(List<Contact> contacts, string email)
        {
            bool finde = false;

            foreach(var c in contacts)
            {
                if(Utils.trueEmail(email, c.Name))
                {
                    c.Email = email;
                    finde = true;
                    break;
                }
            }
            if(!finde)
            {
                foreach (var c in contacts)
                {
                    if (string.IsNullOrEmpty(c.Email))
                    {
                        c.Email = email;
                    }
                }
            }
        }

        private string TryGetName(string innerText)
        {
            innerText = innerText.Replace("&#160;"," ");
            if(innerText.Contains(" at ") && innerText.ToLower().Contains("contact") )
            {
                var indexOfContact = innerText.ToLower().IndexOf("contact");
                var indexOffAt = innerText.ToLower().IndexOf(" at ",indexOfContact);
                if(indexOffAt > 0)
                {
                    var name = Utils.NormalizationString(innerText.Substring(indexOfContact + "contact".Length, indexOffAt-(indexOfContact + "contact".Length)));
                    if (RegExp.IsName(name))
                        return name;
                }
            }

            return null;
        }

        private string TryToGetCompanyName(HtmlNode nodeContact)
        {
            var previos = nodeContact.PreviousSibling;
            while (previos != null)
            {
                var b = previos.SelectSingleNode("./b");
                if (b != null)
                {
                    if (b.InnerText.IndexOf("About") == 0 && b.InnerText.Length < maxLengContactInfo)
                    {
                        var companyName = Utils.NormalizationString(b.InnerText.Replace("About", "").Trim());
                        if (companyName.Length > 2)
                            return companyName.Replace("&#160;", " ");
                        else
                            return null;

                    }
                }
                previos = previos.PreviousSibling;
            }
            return null;
        }

        public string RemoveWordContact(string p)
        {
            foreach (var c in Dictionaries.MarkerContact)
            {
                int indexOffContact = p.ToLower().IndexOf(c);
                if (indexOffContact > 0)
                {
                    int indexOffBR = p.IndexOf("<br>", indexOffContact);
                    if (indexOffBR > 0)
                        return p.Remove(0, indexOffBR + "<br>".Length);
                    else
                        return p.Remove(0, indexOffContact + c.Length);
                }
            }

            return p;
        }

      
    }
}
