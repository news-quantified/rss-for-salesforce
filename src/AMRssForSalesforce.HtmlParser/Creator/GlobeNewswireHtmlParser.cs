﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using HtmlAgilityPack;
    using Infrastructure.Helpers;
    using Infrastructure.Constants;
    using AMRssForSalesforce.Infrastructure.Logger;
    using System.Globalization;

    public class GlobeNewswireHtmlParser: Abstract.HtmlParser
    {
        private const int maxLengContactInfo = 50;
        private string CompanyName = null;
        private string Url = null;
        private string curentType = null;

        private HtmlNode divRight = null;
        public GlobeNewswireHtmlParser(string _url) : base(_url, x => HtmlHelpers.GetResponse(x, null)) { }

        public GlobeNewswireHtmlParser(string _fileName, bool fromFile) : base(_fileName, fromFile) { }

        public override Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed)
        {
            try
            { 
                rssFeed.English = true;

                if (!FindeRightDiv())
                    return null;

                rssFeed.Redirect = responsUri;
                CompanyName = rssFeed.CompanyTitle;
                rssFeed.Dateline = GetDeteLine();
                rssFeed.DateTime = GetDateTime();

                if (!string.IsNullOrEmpty(CompanyName) && companyNames.All(cn => cn != CompanyName))
                {
                    companyNames.Add(CompanyName);
                }

                //Url = TryToGetUrl();
                //if (string.IsNullOrEmpty(Url))
                //    Url = TryToFindeUrlItText();
                //Source = GetSource();

                var contactNodes = GetContactContent();

                var contacts = new List<Contact>();

                if (contactNodes != null && contactNodes.Any())
                {
                    foreach (var contactNode in contactNodes)
                    {
                        if (Utils.NormalizationString(contactNode.InnerText).ToLower() == "contacts:")
                            continue;
                        var contacts1 = GetContacts(contactNode);
                        foreach (var c in contacts1)
                        {
                            if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                contacts.Add(c);
                        }
                    }

                }
                else
                {

                }
                    if (contacts == null)
                        contacts = new List<Contact>();

                    if (!contacts.Any())
                    {
                        contacts = TryToFindeFromText();
                      //  if (contacts == null || !contacts.Any())
                        var contactTables = TryToFindeFromTable();

                        if (!contacts.Any() && contactTables.Any() || contacts.All(c => c.Name == null) && contactTables.Any(c => c.Name != null))
                        {
                            contacts = new List<Contact>();
                            foreach (var c in contactTables)
                            {
                                if (!contacts.Any() || c.Name != null || c.Type != contacts.Last().Type || c.Title != null)
                                {
                                    contacts.Add(c);
                                }
                                else 
                                {
                                   if(c.Email != null)
                                   {
                                       var find = false;
                                       foreach(var contact in contacts)
                                       {
                                           if(Utils.trueEmail(c.Email, contact.Name))
                                           {
                                               contact.Email = c.Email;
                                               find = true;
                                           }

                                       }
                                       if (!find && contacts.Any())
                                           contacts.Last().Email = c.Email;
                                   }
                                   if (c.Phone != null)
                                   {
                                       if (contacts.Any())
                                           contacts.Last().Phone = c.Phone;
                                   }
                                }
                            
                            }
                        
                        }
                    }

                    if (contacts == null || !contacts.Any())
                        contacts = TryToFindeFromText2();

                    if (contacts == null)
                        contacts = new List<Contact>();

                    if (!contacts.Any())
                        contacts.Add(new Contact());

                    foreach (var c in contacts)
                    {

                        if (!string.IsNullOrEmpty(Source))
                            c.Source = Source;
                        else
                            c.Source = CompanyName;

                        c.NewsID = this.urlHtml;

                        if (string.IsNullOrEmpty(Url))
                            c.Url = GetRelatedLink(c);
                        else
                            c.Url = Url;

                        if (string.IsNullOrEmpty(c.CompanyName))
                        {
                            if (!string.IsNullOrEmpty(CompanyName))
                                c.CompanyName = CompanyName;
                            else
                                c.CompanyName = Source;
                        }

                        if (string.IsNullOrEmpty(c.Name) && !string.IsNullOrEmpty(c.Email))
                            {
                                c.Name = TryGetNameFromEmail(c.Email);
                            }
                    }
                rssFeed.ContactInfo = Utils.SummContactData(contacts);
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var conactsClear = new List<Contact>();

                foreach(var c in contacts)
                {
                    if(!conactsClear.Any())
                    {
                        conactsClear.Add(c);
                    }
                    if(!string.IsNullOrEmpty(c.Name) && conactsClear.Any(cc => cc.Name == c.Name))
                    {
                        var hasContact = conactsClear.First(cc => cc.Name == c.Name);

                        if(c.Email != null && hasContact.Email != c.Email)
                        {
                            if (hasContact.Email == null)
                                hasContact.Email = c.Email;
                            else
                            {
                                hasContact.Email = hasContact.Email + ", " + c.Email;
                            }
                        }

                        //phone

                        if (c.Phone != null && hasContact.Phone != c.Phone)
                        {
                            if (hasContact.Phone == null)
                                hasContact.Phone = c.Phone;
                            else
                            {
                                hasContact.Phone = hasContact.Phone + ", " + c.Phone;
                            }
                        }
                     
                        //title

                        if (c.Title != null && hasContact.Title != c.Title)
                        {
                            if (hasContact.Title == null)
                                hasContact.Title = c.Title;
                            else
                            {
                                hasContact.Title = hasContact.Title + ", " + c.Title;
                            }
                        }

                        //type


                        if (c.Type != null && hasContact.Type != c.Type)
                        {
                            if (hasContact.Type == null)
                                hasContact.Type = c.Type;
                            else
                            {
                                hasContact.Type = hasContact.Type + ", " + c.Type;
                            }
                        }

                    }
                    else
                        conactsClear.Add(c);
                }

                return new Tuple<List<Contact>, HtmlDocument>(conactsClear.Distinct().ToList(), doc);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = this.urlHtml });
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
        }

        private DateTime GetDateTime()
        {
                 var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var time = doc.DocumentNode.SelectSingleNode("//time");

            if (time != null)
            {
                string stringDateTime = time.InnerText;
                var strDateTime = Utils.ResolveTimeZoneAbbreviations(stringDateTime);

                string format = "MMMM dd, yyyy HH:mm zzz";
                if (DateTimeOffset.TryParseExact(strDateTime, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTimeOffset))
                {
                    return dateTimeOffset.ToLocalTime().DateTime;
                } 
            }

            return DateTime.Now;
        }

        private List<Contact> TryToFindeFromText2()
        {
            try
            {
                List<Contact> contacts = new List<Contact>();
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var ps = doc.DocumentNode.SelectNodes("//p");
                if (ps != null)
                {
                    int i = 0;
                    while (!ps[i].InnerText.Contains("Questions should be directed to:"))
                    {
                        if (i >= ps.Count-1)
                            return null;
                        i++;
                    }
                    i++;

                    string curentCompanyName = null;
                    string curentPhoneNumber = null;
                    string curentEmail = null;

                    //Contact currentContact = new Contact();
                    if (i >= ps.Count)
                        return null;
                    for (; i < ps.Count; i++)
                    {
                        if (Utils.NormalizationString(ps[i].InnerText).Length > 3)
                        {
                            Contact currentContact = GetContactFromP(ps[i]);
                            if (currentContact == null)
                                break;

                            if (currentContact.Name == null && currentContact.CompanyName == null && currentContact.Email == null && currentContact.Phone == null && currentContact.Title == null)
                                break;

                            if (currentContact.Name == null && currentContact.Phone != null && currentContact.Title == null)
                            {
                                if (contacts.Any())
                                {
                                    contacts.Last().Phone = currentContact.Phone;
                                }
                                else
                                    curentPhoneNumber = currentContact.Phone;
                            }

                            if (currentContact.Name == null && currentContact.Email != null && currentContact.Title == null)
                            {
                                if (contacts.Any())
                                {
                                    contacts.Last().Email = currentContact.Email;
                                }
                                else
                                    curentEmail = currentContact.Email;
                            }

                            if (currentContact.Name == null && currentContact.CompanyName != null && currentContact.Title == null)
                            {
                                if (contacts.Any())
                                {
                                    contacts.Last().CompanyName = currentContact.CompanyName;
                                }
                                else
                                    curentCompanyName = currentContact.CompanyName;
                            }
                            if (currentContact.Name != null || currentContact.Title != null)
                            {
                                if (currentContact.CompanyName == null)
                                    currentContact.CompanyName = curentCompanyName;

                                if (currentContact.Phone == null)
                                    currentContact.Phone = curentPhoneNumber;

                                if (currentContact.Email == null)
                                    currentContact.Email = curentEmail;

                                contacts.Add(currentContact);
                            }
                        }
                    }

                    if (!contacts.Any() && (curentCompanyName != null || curentEmail != null || curentPhoneNumber != null || curentType != null))
                    {
                        contacts.Add(new Contact() { CompanyName = curentCompanyName, Email = curentEmail, Phone = curentPhoneNumber });
                    }
                }

                return contacts;
            }
            catch(Exception ex)
            {
                Logger.WriteFatal(ex);
                return null;
            }
        }

        private Contact GetContactFromP(HtmlNode htmlNode)
        {
            var normStr = Utils.NormalizationString(htmlNode.InnerText);

            var numbers = RegExp.GetPnoneNumbers(normStr);

            if(numbers.Count() == 1 && numbers[0] == normStr )
            {
                return new Contact() { Phone = numbers[0] };
            }

            var parts = normStr.Split(new string[] { ": ", ", ", " - " }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Count() < 2)
                return null;

            Contact contact = new Contact();

            foreach(var p in parts)
            {
                var str = p.Trim();
                var emails = RegExp.getEmails(str);
                var phones = RegExp.GetPnoneNumbers(str);
                if(emails.Any())
                {
                    contact.Email = string.Join(", ", emails);
                }
                else if(phones.Any())
                {
                    contact.Phone = string.Join(", ", phones);
                }
                else if(IsCompanyName(str))
                {
                    contact.CompanyName = str;
                }
                else if(Utils.HasTitle(str))
                {
                    contact.Title = str;
                }
                else if(RegExp.IsName(str))
                {
                    contact.Name = str;
                }
            }

            return contact;
        }

        private string GetDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var pLocation = doc.DocumentNode.SelectSingleNode("//p[@itemprop='dateline contentLocation']");
            if(pLocation != null)
            {
                return pLocation.InnerText.Replace("\r\n", "").Trim();
            }

            var div = doc.DocumentNode.SelectSingleNode("//span[@itemprop='articleBody']");

            var ps = doc.DocumentNode.SelectNodes(".//p");

            if(div != null)
              ps = div.SelectNodes(".//p");

            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("--") && p.Contains(DateTime.Now.Year.ToString()))
                {
                    var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                    var ret = Utils.NormalizationString(strings[0]);
                    if(!string.IsNullOrEmpty(ret))
                            return ret;
                }
                else if (p.Contains("–") && p.Contains(DateTime.Now.Year.ToString()))
                {
                    var strings = p.Split(new string[] { "–" }, StringSplitOptions.RemoveEmptyEntries);
                    var ret = Utils.NormalizationString(strings[0]);
                    if (!string.IsNullOrEmpty(ret))
                    {
                        var dl = Utils.CatDataInDL(ret);
                        if (!string.IsNullOrEmpty(dl))
                            return dl;
                    }
                }
                      
            }

                foreach (var pp in ps)
                {
                    var strong = pp.SelectSingleNode(".//strong");
                    if (strong != null && (strong.InnerText.Contains(" – ") || strong.InnerText.Contains(DateTime.Now.Year.ToString())))
                    {
                        if (strong.InnerText.Contains("("))
                        {
                            var parts = Utils.NormalizationString3(strong.InnerText).Split(new string[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                            
                            foreach(var p in parts)
                            {
                                if (!p.Contains(DateTime.Now.Year.ToString()))
                                    return p.Trim();
                            }
                        }
                        else if(strong.InnerText.Contains(","))
                        {
                            var parts = Utils.NormalizationString3(strong.InnerText).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (var p in parts)
                            {
                                if (!p.Contains(DateTime.Now.Year.ToString()))
                                    return p.Trim();
                            }
                        }
                        else if (strong.InnerText.Contains(" – "))
                        {
                            var p = Utils.NormalizationString3(strong.InnerText);
                            var strings = p.Split(new string[] { " – " }, StringSplitOptions.RemoveEmptyEntries);
                            return strings[0];
                        }
                        else
                        {
                            return Utils.NormalizationString3(strong.InnerText);
                        }
                    }
                   
                }

                           
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);

                    if (p.Contains("<!--"))
                        continue;

                    if (p.Contains(DateTime.Now.Year.ToString()) )
                    {
                       if (p.Contains((DateTime.Now.Year - 1).ToString()))
                        {
                            var strings = p.Split(new string[] { (DateTime.Now.Year - 1).ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }

                        }
                        else if (p.Contains(DateTime.Now.Year.ToString()))
                        {
                            var strings = p.Split(new string[] { DateTime.Now.Year.ToString() }, StringSplitOptions.RemoveEmptyEntries);
                            if (strings.Count() == 0)
                            {

                            }
                            else
                            {
                                return Utils.NormalizationString(strings[0]);
                            }
                        }
                        // / –
                    }
                }
            }

            var pres = doc.DocumentNode.SelectNodes(".//pre");
            string curentDateLine = "";
            bool wasDate = false;
            if(pres != null)
            {
                foreach(var pre in pres)
                {
                    if(pre.ChildNodes != null)
                    {
                        var previosStr = "";
                        var parts = pre.InnerText.Split('\n');

                         foreach (var c in parts)
                        {
                            if (!string.IsNullOrEmpty(Utils.NormalizationString(c)))
                            {
                                if(c.Contains("GLOBE NEWSWIRE"))
                                {
                                    var p = c;
                                    if (p.Contains("--") && p.Contains(DateTime.Now.Year.ToString()))
                                    {
                                        var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                                        var ret = Utils.NormalizationString(strings[0]);
                                        if (!string.IsNullOrEmpty(ret))
                                            return ret;
                                    }
                                    else if (p.Contains("–") && p.Contains(DateTime.Now.Year.ToString()))
                                    {
                                        var strings = p.Split(new string[] { "–" }, StringSplitOptions.RemoveEmptyEntries);
                                        var ret = Utils.NormalizationString(strings[0]);
                                        if (!string.IsNullOrEmpty(ret))
                                        {
                                            var dl = Utils.CatDataInDL(ret);
                                            if (!string.IsNullOrEmpty(dl))
                                                return dl;
                                        }
                                    }
                                }
                            }
                        }
                       
                        foreach (var c in parts)
                        {
                            if (!string.IsNullOrEmpty(Utils.NormalizationString(c)))
                            {
                                DateTime datetime = DateTime.Now;
                                if (DateTime.TryParse(c, out datetime) || c.Contains(".2016"))
                                {
                                    wasDate = true;

                                    if (!string.IsNullOrEmpty(curentDateLine))
                                        return curentDateLine;
                                }

                                curentDateLine = Utils.CatDataInDL(Utils.NormalizationString(c));
                              

                               
                                wasDate = false;
                                
                            }
                        }
                    }
                }
            }
            return "";
        }
        private string TryGetNameFromEmail(string email)
        {
            var emailDomen = email.Split('@');
            if (emailDomen.Length < 2)
                return null;

            var names = emailDomen[0].Split('.');
            if (names.Length < 2)
                return null;

            if(names[0][0] >= 'A'  && names[0][0]<= 'Z' && names[1][0]>='A' && names[1][0]<= 'Z')
            {
                var name = string.Format("{0} {1}", names[1], names[0]);
                if(RegExp.IsName(name))
                        return name;
            }
            return null;
        }

        private List<List<HtmlNode>> GetContactsNodesFromTable(HtmlNode table)
        {
            List<List<HtmlNode>> listNodes = new List<List<HtmlNode>>();

            var trs = table.SelectNodes(".//tr");
            if (trs == null)
                return null;
            int countTD = 0;

            if (trs[0].SelectNodes("./td") == null)
                return null;
            foreach (var td in trs[0].SelectNodes("./td"))
            {
                if (td.Attributes["colspan"] != null)
                {
                    var colspan = 0;
                    if (int.TryParse(td.Attributes["colspan"].Value, out colspan))
                    {
                        countTD += colspan;
                    }
                    else
                        countTD++;
                }
                else
                    countTD++;
            }
            //trs[0].SelectNodes("./td").Count;

            if (countTD == 0)
                return null;

            for (int i = 0; i < countTD; i++)
            {
                listNodes.Add(new List<HtmlNode>());
            }

            foreach (var tr in trs)
            {

                var tds = tr.SelectNodes("./td");
                if (tds.Count > listNodes.Count)
                    //it isn't contact table
                    return null;
                for (int i = 0; i < tds.Count; i++)
                {
                    string contactString = Utils.NormalizationString(tds[i].InnerText);
                    if (!string.IsNullOrEmpty(contactString)
                        && contactString.ToLower() != "contact:"
                        && contactString.ToLower() != "contacts:"
                         && contactString.ToLower() != "contact"
                        && contactString.ToLower() != "telephone"
                        && contactString.ToLower() != "email"
                        && contactString.ToLower() != "website")
                        listNodes[i].Add(tds[i]);

                }
            }

            while (listNodes.Any(l => !l.Any()))
            {
                listNodes.Remove(listNodes.First(l => !l.Any()));
            }


            return listNodes;
        }

        private List<Contact> TryToFindeFromTable()
        {
            List<Contact> contacts = new List<Contact>();
            Contact contact = null;

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
                                                                    
            var pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse hugin']");
          
            if(pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse hugin mce-item-table']");

            if(pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table']");
            
            if(pss == null)
                 pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table']");

            if (pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table']");

            if (pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table']");

            if (pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table']");
           
            if (pss == null)
                pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table']");
                                                                  //gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse gnw_table_border_collapse hugin mce-item-table
            if (pss != null)
                foreach (var ps in pss)
                {
                    if (ps.InnerText.Contains("$") || ps.InnerText.Contains(" billion ") || ps.InnerText.Contains(" million ") || ps.InnerText.Contains(" euro "))
                        continue;

                    var emailsAll = RegExp.getEmails(RegExp.GetInnerText(ps.InnerHtml));
                    var numbersAll = RegExp.GetPnoneNumbers(RegExp.GetInnerText(ps.InnerHtml));
                    if (!emailsAll.Any() && !numbersAll.Any() && !RegExp.IsNumber(ps.InnerText))
                        continue;


                    if (ps != null)
                    {
                        var table = ps;
                        if (table != null)
                        {
                            if (!emailsAll.Any() && table.InnerText.Length > 600 || table.InnerText.ToLower().Contains("when:"))
                                continue;

                            List<List<HtmlNode>> contactsNodes = GetContactsNodesFromTable(table);

                            if (contactsNodes == null || !contactsNodes.Any())
                                return contacts;

                            foreach (var col in contactsNodes)
                            {
                              
                                foreach (var node in col)
                                {
                                    var contacts1 = GetContacts2(node);
                                    if (contacts1 == null)
                                        continue;
                                    foreach (var c in contacts1)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                            contacts.Add(c);
                                    }
                                }
                            }
                        }

                    }
                }

            if(contacts!= null && contacts.Count > 1 && contacts.Count(c => c.Name != null) == 1)
            {
                List<Contact> contactsC = new List<Contact>();
                contactsC.Add(contacts.First(c => c.Name != null));

                List<string> emails = new List<string>();
                List<string> phones = new List<string>();


                foreach(var c in contacts)
                {
                    
                    if(c.Email != null)
                    {
                        emails.Add(c.Email);
                    }
                    if (c.Phone != null)
                        phones.Add(c.Phone);

                    if (emails.Any())
                        contactsC.First().Email = string.Join(", ", emails.Distinct().ToArray());

                    if (phones.Any())
                        contactsC.First().Phone = string.Join(", ", phones.Distinct().ToArray()); 

                }

                contacts = contactsC;
            }
            return contacts;
        }

        private List<Contact> TryToFindeFromText()
        {
            List<Contact> contacts = new List<Contact>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p[@class='contacts']");
            if (ps == null)
            {
                ps = doc.DocumentNode.SelectNodes("//p");
                var pre = doc.DocumentNode.SelectNodes("//pre");

                var lis = doc.DocumentNode.SelectNodes("//li");

                var spans = doc.DocumentNode.SelectNodes("//span");
                if (ps == null)
                    return null;

                if (pre != null)
                {
                    foreach (var p in pre)
                    {
                        ps.Add(p);
                    }
                }

                if (lis != null)
                {
                    foreach (var l in lis)
                    {
                        var numbers = RegExp.GetPnoneNumbers(l.InnerText);
                        var emails = RegExp.getEmails(l.InnerText);

                        if (numbers.Any() || emails.Any())
                            ps.Add(l);
                    }
                }

                if (spans != null)
                {
                    foreach (var l in spans)
                    {
                        var numbers = RegExp.GetPnoneNumbers(l.InnerText);
                        var emails = RegExp.getEmails(l.InnerText);

                        if ((numbers.Any() || emails.Any()) && Utils.NormalizationString(l.InnerText).Length < 200)
                            ps.Add(l);
                    }
                }
            }
            var countContact = 0;
            HtmlNode p_contact_m = null;
            int countAfterWordContact = 0;

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                var text = System.Net.WebUtility.HtmlDecode(p.InnerText);
                var hasEmailsP = RegExp.getEmails(text).Any();
                var phones = RegExp.GetPnoneNumbers(text);
                if(p.ChildNodes != null && p.ChildNodes.Any() )
                {
                    if (p.ChildNodes.Max(ph => Utils.NormalizationString(ph.InnerText).Length) > 80 && !hasEmailsP && !phones.Any())
                        continue;
                }
                else
                {
                    if (text.Length > 80 && !hasEmailsP && !phones.Any() && !text.Contains("Further information"))
                        continue;
                }

                if (text.Length < 8)
                    continue;
                if (text.Contains("Logo - "))
                    continue;

                var notContactNode = false;
                if (p.ChildNodes != null)
                {
                    foreach (var node in p.ChildNodes)
                    {
                        if (p.Name == "#text" && text.Length > maxLengContactInfo)
                        {
                            notContactNode = true;
                            break;
                        }

                    }
                    if (notContactNode)
                        continue;
                }

                if (p_contact_m != null)
                {
                    if (NodeContentEmailOrPhone(p) )
                    {
                        var contacts1 = GetContacts2(p);
                        if (contacts1 != null)
                        {
                            foreach (var c in contacts1)
                            {
                                if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                    && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                    contacts.Add(c);
                            }
                            countAfterWordContact = 0;
                            continue;
                        }
                    }
                    countAfterWordContact++;

                }
                var type = Utils.FindType(text);

                if (!string.IsNullOrEmpty(type))
                {
                    p_contact_m = p;
                    countAfterWordContact = 0;
                    this.curentType = type;
                     if (NodeContentEmailOrPhone(p) )
                     {
                         var contacts1 = GetContacts2(p);
                         if(contacts1 != null )
                         foreach (var c in contacts1)
                         {
                             if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                 && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                 contacts.Add(c);
                         }
                         countAfterWordContact = 0;
                         break;
                     }
                }
                else
                {
                    if (phones.Any() || hasEmailsP && p.InnerHtml.Contains("<br>"))
                    {
                        var contacts1 = GetContacts2(p);
                        if (contacts1 != null)
                        {
                            foreach (var c in contacts1)
                            {
                                if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                    && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                    contacts.Add(c);
                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        foreach (var mc in Dictionaries.MarkerContact)
                        {
                            if (p.InnerText.ToLower().Contains(mc.ToLower()))
                            {
                                p_contact_m = p;
                                countAfterWordContact = 0;
                                if (NodeContentEmailOrPhone(p))
                                {
                                    var contacts1 = GetContacts2(p);
                                    if(contacts1 != null)
                                    foreach (var c in contacts1)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                            contacts.Add(c);
                                    }
                                    countAfterWordContact = 0;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if(!contacts.Any() && ps.Any())
            {
                foreach(var p in ps)
                {
                    if (p.SelectNodes(".//span") != null)
                    {
                        foreach (var s in p.SelectNodes(".//span"))
                        {
                            var nextContact = false;
                            foreach (var pp in s.ChildNodes)
                            {
                                var hasEmailsP = RegExp.getEmails(p.InnerText).Any();
                                var hasePhones = RegExp.GetPnoneNumbers(Utils.NormalizationString( p.InnerText)).Any();
                                if ( hasEmailsP || hasePhones)
                                {
                                    var contacts1 = GetContacts2(pp);

                                    if (contacts1 != null)
                                    foreach (var c in contacts1)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                            contacts.Add(c);
                                    }
                                    continue;
                                }
                                
                                if(pp.ChildNodes != null)
                                {
                                    foreach (var ppp in pp.ChildNodes)
                                    {
                                        var hasEmailsP1 = RegExp.getEmails(ppp.InnerText).Any();
                                        var hasePhones1 = RegExp.GetPnoneNumbers(Utils.NormalizationString(ppp.InnerText)).Any();
                                        if (hasEmailsP1 || hasePhones1)
                                        {
                                            var contacts1 = GetContacts2(ppp);
                                            if (contacts1 != null)
                                            foreach (var c in contacts1)
                                            {
                                                if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                                    && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                                    contacts.Add(c);
                                            }
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach(var c in contacts)
            {
                if(!string.IsNullOrEmpty(Source))
                    c.CompanyName = Source;
            }
            return contacts;
        }

        private List<Contact> GetContacts2(HtmlNode p)
        {
            if(!p.ChildNodes.Any())
            {
                HtmlNodeCollection col = new HtmlNodeCollection(p.ParentNode);
                col.Add(p);
                return TryParcePCntacts(col);
            }
            if (p.Name == "td")
            {
                var strong = p.SelectSingleNode(".//strong/strong");
                if (strong != null && strong.ChildNodes != null)
                    return TryParcePCntacts(strong.ChildNodes);
                else
                {
                    return TryParcePCntacts(p.ChildNodes);
                }
            }
            else
            {
                return TryParcePCntacts(p.ChildNodes);
            }
            return new List<Contact>();
        }


        private List<Contact> TryParcePCntacts(HtmlNodeCollection pppps)
        {
            List<string> names = new List<string>();
            List<string> emails = new List<string>();
            List<string> titles = new List<string>();
            List<string> companies = new List<string>();
            List<string> phones = new List<string>();
            List<string> types = new List<string>();

            bool foundContact = true;
            int countLongString = 0;

            foreach (var p in pppps)
            {
                if (Utils.NormalizationString(p.InnerText).Length < 3)
                    continue;

                var pInnerText = System.Net.WebUtility.HtmlDecode(p.InnerText);

                if(pppps.Count == 1 && pInnerText.Length > 300)
                {
                    var emailsAll = RegExp.getEmails(pInnerText);
                    var numbersAll = RegExp.GetPnoneNumbers(pInnerText);

                    if(emailsAll.Any() || numbersAll.Any())
                    {
                        int MaxIndex = 0;
                        if(emailsAll.Any())
                        {
                            var indexLastEmail = pInnerText.ToLower().IndexOf(emailsAll[emailsAll.Length - 1].ToLower());
                           
                            if ( indexLastEmail > 0 && indexLastEmail + emailsAll[emailsAll.Length - 1].Length > MaxIndex)
                                MaxIndex = indexLastEmail + emailsAll[emailsAll.Length - 1].Length;
                        }

                        if (numbersAll.Any())
                        {
                            var indexLastNumber = pInnerText.IndexOf(numbersAll[numbersAll.Length - 1]);
                            if(indexLastNumber == -1)
                                indexLastNumber = pInnerText.IndexOf(numbersAll[numbersAll.Length - 1].Replace("-","–"));
                            if (indexLastNumber >0 && indexLastNumber + numbersAll[numbersAll.Length - 1].Length > MaxIndex)
                                MaxIndex = indexLastNumber + numbersAll[numbersAll.Length - 1].Length;
                        }
                        if (MaxIndex < pInnerText.Length - 1 && MaxIndex > 0)
                           pInnerText = pInnerText.Remove(MaxIndex);
                        else
                        {
                            var text123 = pInnerText;
                        }
                    }
                }

                if (pInnerText == "Distribution")
                {
                    break;
                }

                var _names = Utils.NormalizationString(pInnerText.Replace(", Inc.", "Iiiiiinc").Replace(", LLC", "Lllllllllllllllllc")).Split(nameSplit, StringSplitOptions.RemoveEmptyEntries);
                var _names2 = pInnerText.Split('\n');

                if (_names2.Length > _names.Length)
                    _names = _names2;
                
                foreach (var nnnn in _names)
                {
                    if (nnnn.Contains("For more information") 
                        || nnnn.Contains("För ytterligare information")
                        || nnnn.Contains("For further information")
                        || nnnn.Contains("Further information")
                        || nnnn.Contains("Additional information")
                        || nnnn.Contains("Kontaktinformation")
                        )
                    {
                        names = new List<string>();
                        emails = new List<string>();
                        titles = new List<string>();
                        companies = new List<string>();
                        phones = new List<string>();
                        types = new List<string>();
                    }

                    if (nnnn.Contains("Notering:")
                        || nnnn.Contains("Reuters:"))
                        continue;

                    var _names3 = nnnn.Split(nameSplit, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var n in _names3)
                    {
                var _emails = RegExp.getEmails(n);
                if (_emails.Any())
                {
                    foreach (var e in _emails)
                        emails.Add(e);
                    continue;
                }

                // var _phones = RegExp.GetPnoneNumbers(p.InnerText);
                if (RegExp.IsNumber(n))
                {
                    phones.Add(Utils.NormPhone(n));
                    continue;
                }


                var word = Utils.NormalizationString(n.Replace("Iiiiiinc", ", Inc.").Replace( "Lllllllllllllllllc", ", LLC"));
                    if (Utils.HasTitle(word))
                   {
                       var title = Utils.GetTitle(word);
                       titles.Add(title);
                       if (!string.IsNullOrEmpty(title) && !(string.IsNullOrEmpty(word)))
                       {
                           word = Utils.NormalizationString(word.Replace(title, " "));
                           if (word.Length < 6)
                               continue;
                       }
                   }

                  var _type = Utils.FindType(word);
                   if (!string.IsNullOrEmpty(_type))
                   {
                      types.Add(_type);
                      this.curentType = _type;
                      word = word.Replace(_type, "").Trim();
                      if(word.Length < 5)
                          continue;
                   }

                   if (IsCompanyName(word))
                   {
                      companies.Add(Utils.NormalizationString(word));
                      continue;
                   }

                    if (RegExp.IsName(word))
                        names.Add(Utils.NormalizationString(word));
                }
                }
            }

            List<Contact> contacts = new List<Contact>();
            if (names.Any())
            {
                int i = 0;
                foreach (var n in names)
                {
                    contacts.Add(new Contact() { Name = n, Type = curentType });

                    if (emails.Any())
                    {
                        var emailTrue = emails.FirstOrDefault(e => Utils.trueEmail(e, n));
                        if (emailTrue != null)
                        {
                            contacts.Last().Email = emailTrue;
                            emails.Remove(emailTrue);
                        }
                        foundContact = true;
                    }

                    if (phones.Any())
                    {
                        contacts.Last().Phone = phones.Count == names.Count ? phones[i] : string.Join(", ", phones.ToArray());
                        foundContact = true;
                    }

                    if (titles.Any())
                    {
                        if (titles.Count == names.Count)
                            contacts.Last().Title = titles[i];
                        else if (titles.Count == 2 && names.Count == 1 )
                        {
                            contacts.Last().Title = string.Join(", ", titles);
                        }
                        else
                        {

                        }
                    }

                    if (types.Any())
                    {
                        contacts.Last().Type = types.Count == names.Count ? types[i] : types[0];
                    }
                    else
                    {
                        contacts.Last().Type = this.curentType;
                    }
                    if (companies.Any())
                    {
                        contacts.Last().CompanyName = companies.Count == names.Count ? companies[i] : companies[0];
                    }
                    i++;
                }

                if (emails.Any() && contacts.Any(c => string.IsNullOrEmpty(c.Email)))
                {
                    foreach (var cont in contacts.Where(c => string.IsNullOrEmpty(c.Email)))
                    {
                        cont.Email = string.Join(", ", emails.ToArray());
                    }
                }
                return contacts;
            }
            else
            {
                if (emails.Any())
                {
                    int i = 0;
                    foreach (var e in emails)
                    {
                        contacts.Add(new Contact() { Email = e, Type = curentType });
                        if (phones.Any())
                        {
                            contacts.Last().Phone = phones.Count == emails.Count ? phones[i] : string.Join(", ", phones.ToArray());
                        }

                        if (titles.Any())
                        {
                            if (titles.Count == emails.Count)
                                contacts.Last().Title = titles[i];

                        }

                        if (types.Any())
                        {
                            contacts.Last().Type = types.Count == emails.Count ? types[i] : types[0];
                        }

                        if (companies.Any())
                        {
                            if (companies.Count == emails.Count)
                                contacts.Last().CompanyName = companies[i];
                            else
                                contacts.Last().CompanyName = companies[0];
                        }
                        i++;
                    }

                    return contacts;
                }

                if (phones.Any())
                {
                    int i = 0;
                    foreach (var p in phones)
                    {
                        contacts.Add(new Contact() { Phone = p, Type = curentType });

                        if (titles.Any())
                        {
                            if (titles.Count == phones.Count)
                                contacts.Last().Title = titles[i];

                        }

                        if (types.Any())
                        {
                            contacts.Last().Type = types.Count == phones.Count ? types[i] : types[0];
                        }

                        if (companies.Any())
                        {
                            contacts.Last().CompanyName = companies.Count == phones.Count ? companies[i] : companies[0];
                        }
                        i++;
                    }

                    return contacts;
                }

                if (types.Count == 1)
                    curentType = types[0];

                
            }
            return null;
        }
       
        private string TryToGetUrl()
        {
            var imgs = this.divRight.SelectNodes(".//img[@class='valign-bott margintop-5px']");
            if (imgs == null)
                return null;
            foreach (var img in imgs)
            {
                if(img.NextSibling == null)
                {
                    continue;
                }
           
                if (img.NextSibling.Name != "a")
                {
                    if (img.NextSibling.NextSibling.Name != "a")
                      continue;
                    return img.NextSibling.NextSibling.InnerText;
        }

                return img.NextSibling.InnerText;
            }
            return null;
        }

        private bool FindeRightDiv()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var divR2s = doc.DocumentNode.SelectNodes("//div[@itemprop='articleBody']");

            if (divR2s == null)
                return false;

            if (divR2s.Count > 1)
            {

            }

            this.divRight = divR2s[0];
            return true;
        }

        private string TryToFindeUrlItText()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("web-site") || p.InnerText.ToLower().Contains("please visit")
                    || p.InnerText.ToLower().ToLower().Contains("website") || p.InnerText.ToLower().ToLower().Contains("web site")
                    || p.InnerText.ToLower().ToLower().Contains("web site")
                    || p.InnerText.ToLower().ToLower().Contains("please see")
                    || p.InnerText.ToLower().ToLower().Contains("information at")
                    || p.InnerText.ToLower().ToLower().Contains("available at")
                    )
                {

                    var urls = RegExp.getUrls(p.InnerText);

                    if (urls.Any())
                        return string.Join(", ", urls);
                }
            }

            return null;
        }

        private bool IsEngish()
        {
            //if (htmlContent.Contains("lang=\"en\""))
            //{
                return !NoEnglish();
           // }
         //   return false;
            }

       


        public override string GetRelatedLink()
        {
            StringBuilder sb = new StringBuilder();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//a");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.Attributes["title"] != null && p.Attributes["title"].Value.Contains("Link to") && p.Attributes["href"] != null)
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(p.Attributes["href"].Value);
                    }
                }
            }
            return sb.ToString();
        }

        public override string GetSource()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//span[@class='article-source']");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    
                        var innerText = Utils.NormalizationString(p.InnerText);
                        int SourseIndex = innerText.Trim().IndexOf("Source:");
                        if (SourseIndex >= 0)
                        {
                            return innerText.Remove(0, SourseIndex + "Source:".Length).Replace("multilang-release","").Trim();
                        }
                    
                }
            }
            return null;
        }

        private static string[] _separatePre = new string[] { "\n\n" };

        public override List<HtmlNode> GetContactContent()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var contact = GetNodeByfacet_header("Contact Data");

            if(contact != null)
            {
                var contactNode = contact;
                while (contactNode.Name != "pre")
                {
                    contactNode = contactNode.NextSibling;
                    if (contactNode == null)
                            return null;
                }
                if (contact != null)
                {
                    contacts.Add(contactNode);

                    if (RegExp.getEmails(contactNode.InnerHtml).Count() > 0)
                    {
                    }

                    var doc = new HtmlDocument();
                    doc.LoadHtml(htmlContent);

                    var ps = doc.DocumentNode.SelectNodes("//p");

                    var hasExternContacts = false;
                    foreach(var p in ps)
                    {
                        if (p.InnerText.Contains("Contact:"))
                            hasExternContacts = true;

                        if(hasContent)
                        {
                            if(RegExp.getEmails(Utils.NormalizationString(p.InnerHtml)).Count() > 0 )
                                contacts.Add(p);
                        }
                    }
                    return contacts;
                }
            }
            else
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var ps = doc.DocumentNode.SelectNodes("//pre");
                
                
                if(ps == null)
                    ps = doc.DocumentNode.SelectNodes("//p");

                if(ps!= null)
                {
                    foreach(var p in ps)
                    {
                        if((RegExp.getEmails(Utils.NormalizationString(p.InnerHtml)).Count() > 0 || RegExp.GetPnoneNumbers(RegExp.GetInnerText(p.InnerHtml)).Count() > 0) && Utils.NormalizationString(p.InnerText).Length < 500)
                            contacts.Add(p);
                    }
                    if(contacts.Any())
                       return contacts;

                    var nextContact = false;
                    if(ps.Count == 1)
                    {
                        var strings = ps[0].InnerHtml.Split(_separatePre, StringSplitOptions.RemoveEmptyEntries);

                        foreach (var s in strings)
                        {
                            var p = s.Replace("\n", "; ");
                            if (!p.Contains("http://"))
                            {
                                if ( nextContact || (RegExp.getEmails(p).Count() > 0 || RegExp.GetPnoneNumbers(p).Count() > 0) && Utils.NormalizationString(p).Length < 500)
                                {
                                    var newP = ps[0].Clone();

                                    p = p.Replace(", Inc.", "Iiiiiinc").Replace(", LLC", "Lllllllllllllllllc");
                                    p = p.Replace(", ", " | ");
                                    p = p.Replace("Iiiiiinc", ", Inc.").Replace( "Lllllllllllllllllc",", LLC");
                                    newP.InnerHtml = p;

                                    contacts.Add(newP);
                                    return contacts;
                                }
                                nextContact = s.ToLower().Contains("contact:");
                            }
                        }
                    }
                    return contacts;
                }

                
            }
            return null;
        }

        string[] nameSplit = new string[] { ",", " or ", "/", " | ", ":", "\t", "\n", "Tel:", "For further information please contact", "; " };

        public override List<Contact> GetContacts(HtmlNode nodeContact)
        {
            string[] _separate = new string[] { "\n\t", "\n", "/", " | ", " - ", " – ", ";", "&#8211", "   ",", and ", "Additional information:", "Distribution:"  };
            
            List<Contact> contacts = new List<Contact>();

            List<Contact> contactNames = new List<Contact>();

            Contact currentContact = new Contact();

            currentContact.Type = this.curentType;

            contacts.Add(currentContact);

            string generalType = null;
            string curenTitle = null;
            string curentCompanyName = null;

            ContactInfoType lastType = ContactInfoType.Empty;

            var parts = RegExp.GetInnerText(nodeContact.InnerHtml).Split(_separate, StringSplitOptions.None);
            int indexP = -1;
            bool ampNext = false;
            foreach (var p in parts)
            {
                indexP++;

                if(ampNext)
                {
                    ampNext = false;
                    continue;
                }
                lastType = ContactInfoType.Empty;

                var text = Utils.NormalizationString(p.Replace("Contact: ", "")).Replace("Contact Info","");

                if (p.IndexOf("&amp") == p.Length - "&amp".Length && indexP < parts.Length - 1)
                {
                    ampNext = true;
                    text = Utils.NormalizationString((p+parts[indexP + 1]).Replace("Contact: ", ""));
                }

                if (string.IsNullOrEmpty(text))
                    {
                        if ((!string.IsNullOrEmpty(currentContact.Name) || !string.IsNullOrEmpty(currentContact.Type))
                            && (!string.IsNullOrEmpty(currentContact.Email) || !string.IsNullOrEmpty(currentContact.Phone)))
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        currentContact.Type = generalType;
                        contactNames.Clear();
                        curenTitle = null;
                        curentType = null;
                        curentCompanyName = null;
                    }
                    else if (!string.IsNullOrEmpty(currentContact.Type))
                    {
                        generalType = currentContact.Type;
                    }
                        continue;
                    }
              
                        if(text == "or")
                        {
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                    currentContact.Type = generalType;
                    contactNames.Clear();
                    
                            continue;
                        }

                if (text.Length < 3)
                    continue;



                var names = text.Replace(", Inc.", "Iiiiiinc").Replace(", LLC", "Lllllllllllllllllc").Split(nameSplit, StringSplitOptions.RemoveEmptyEntries);
                var index = -1;
               foreach (var n in names)
              {
                  index++;
                  var word = n.Replace("Iiiiiinc", ", Inc.").Replace("Lllllllllllllllllc",", LLC" ).Trim();
                  if (n == "Listed" || n == "Reuters" || n == "Bloomberg")
                      break;

                  var emails = RegExp.getEmails(word);
                  if (emails.Any())
                  {
                      lastType = ContactInfoType.Email;
                      if (!contactNames.Any())
                          currentContact.Email = string.Join(", ", emails);
                      else
                      {
                          List<string> emailNobady = new List<string>();
                          foreach (var e in emails)
                          {
                              bool found = false;
                              foreach (var nnn in contactNames)
                              {
                                  if (Utils.trueEmail(e, nnn.Name))
                                  {
                                      nnn.Email = e;
                                      found = true;
                                      break;
                                  }
                              }
                              if (!found)
                                  emailNobady.Add(e);

                          }

                          if (emailNobady.Any())
                          {
                              foreach (var nnn in contactNames)
                              {
                                  if (string.IsNullOrEmpty(nnn.Email))
                                  {
                                      nnn.Email = string.Join(", ", emailNobady.ToArray());
                                  }
                              }
                          }
                      }
                      continue;
                  }

                var urls = RegExp.getUrls(word);
                if (urls.Any())
                {
                    currentContact.Url = string.Join(", ", urls);
                    lastType = ContactInfoType.Url;
                    continue;
                }



                var numbers = RegExp.GetPnoneNumbers(word);

                if (numbers.Any() || RegExp.IsNumber(word))
                {
                    var startNumber = word.IndexOf(numbers[0]);
                    string last = null;
                    if (startNumber > 0)
                    {
                        last = Utils.NormalizationString(word.Remove(startNumber));
                        if(RegExp.IsName(last))
                        {
                            if (currentContact.Name != null)
                            {
                                currentContact = new Contact();
                                contacts.Add(currentContact);
                            }
                            currentContact.Phone = Utils.NormPhone(word);
                            currentContact.Name = last;
                            continue;
                        }
                    }
                    

                    var number = Utils.NormPhone(word);
                    if (contactNames.Any())
                    {
                        if (numbers.Count() == contactNames.Count())
                        {

                            for (var i = 0; i < contactNames.Count(); i++)
                            {
                                if (contactNames[i].Phone == null || contactNames[i].Phone.Contains(numbers[i]))
                                {
                                    contactNames[i].Phone = numbers[i];
                                }
                            }
                        }
                        else
                        {
                            foreach (var nnn in contactNames)
                            {
                                if (nnn.Phone == null || !nnn.Phone.Contains(number))
                                {
                                    if (string.IsNullOrEmpty(nnn.Phone))
                                        nnn.Phone = number;
                                }
                            }

                        }
                    }
                    else
                    {
                        if (currentContact.Phone == null || !currentContact.Phone.Contains(number))
                        {
                            lastType = ContactInfoType.Phone;
                            if (!string.IsNullOrEmpty(currentContact.Phone))
                                currentContact.Phone += ", ";
                            currentContact.Phone += number;
                        }
                    }

                    
                        if(!string.IsNullOrEmpty(last) && last.Length > 3)
                        {
                            word = last;
                        }
                        else
                        {
                            continue;
                        }
                  
                }

                if(lastType == ContactInfoType.Title && index -1 >= 0)
                {
                    var complextitle = names[index - 1] + ", " + word;

                    var Complextitle1 = Utils.GetTitle(complextitle);
                    if (complextitle == Complextitle1)
                    {
                        curenTitle = Complextitle1;
                        if ((currentContact.Title != null && Complextitle1.ToLower().Contains(currentContact.Title.ToLower())))
                            currentContact.Title = Complextitle1;
                        curenTitle = Complextitle1;
                        lastType = ContactInfoType.Title;
                        continue;
                    }
                }
                var title = Utils.GetTitle(word);
                if (title != null)
                {

                    
                    curenTitle = title;
                    int indexTitle = word.IndexOf(title);
                    word = Utils.NormalizationString(word.Remove(0, indexTitle + title.Length));
                    if (RegExp.IsName(word) && currentContact.Name != null)
                    {
                        lastType = ContactInfoType.Name;
                        contactNames.Add(currentContact);
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        contactNames.Add(currentContact);
                        currentContact.Type = contacts.Last().Type;
                        currentContact.Name = Utils.NormName(word);
                        if (currentContact.Title == null)
                            currentContact.Title = title;
                        curenTitle = title;
                        lastType = ContactInfoType.Title;
                        continue;
                    }
                   
                    curenTitle = title;
                    if (string.IsNullOrEmpty(currentContact.Title))
                        currentContact.Title = title;
                    else
                    {
                        if (!currentContact.Title.Contains(title) && lastType == ContactInfoType.Title)
                            currentContact.Title += ", " + title;
                    }

                    if (string.IsNullOrEmpty(word))
                    {
                        lastType = ContactInfoType.Title;
                        continue;
                    }
                    else
                    {

                    }
                    lastType = ContactInfoType.Title;
                }

                var type = Utils.FindType(word);
                if (!string.IsNullOrEmpty(type))
                {
                    lastType = ContactInfoType.Type;
                    curentType = type;
                    if (string.IsNullOrEmpty(currentContact.Type))
                        currentContact.Type = type;

                    var catType = Utils.NormalizationString(Utils.CutType(word, type).Replace(type, "").Trim());
                    if (!string.IsNullOrEmpty(catType) && IsCompanyName(catType))
                        currentContact.CompanyName = catType;

                    if (string.IsNullOrEmpty(catType))
                        continue;
                    else
                    {
                        word = catType;
                        if (word.Contains(":"))
                        {
                            var tt = word.Split(':');
                            if (tt.Count() > 1)
                                word = tt[1].Trim();
                        }

                    }
                }

               if (IsCompanyName(word))
                    {
                        curentCompanyName = word;
                        lastType = ContactInfoType.CopanyName;
                        if (string.IsNullOrEmpty(currentContact.CompanyName))
                        {
                            currentContact.CompanyName = word;

                            if (contactNames.Any())
                            {
                                foreach (var nC in contactNames)
                                {
                                    nC.CompanyName = word;
                                }
                            }
                        }
                        continue;
                    }
                
                   if (RegExp.IsName(word))
                    {
                        lastType = ContactInfoType.Name;
                        if (string.IsNullOrEmpty(currentContact.Name))
                        {
                            currentContact.Name = Utils.NormName(word);
                            
                        }
                        else
                        {
                            contactNames.Add(currentContact);
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            contactNames.Add(currentContact);
                            currentContact.Type = curentType;
                            currentContact.Title = curenTitle;
                            currentContact.CompanyName = curentCompanyName;
                            currentContact.Name = Utils.NormName(word);
                        }
            }
        }

            }

            return contacts;
        }


        private HtmlNode GetNodeByfacet_header(string facet_header)
        {
            var divs = this.divRight.SelectNodes($".//div[@class='{facet_header}']");
            if (divs == null)
                return null;

            foreach(var div in divs)
                    {
                if(Utils.NormalizationString(div.InnerText).ToLower() == facet_header.ToLower())
                    {
                    var node = div.NextSibling;

                    while(string.IsNullOrEmpty(Utils.NormalizationString(node.InnerText)))
                    {
                        node = node.NextSibling;
                        if (node == null)
                            return null;
                    }
                    return node;
                }
            }
            return null;
        }

        private string TryToGetCompanyName()
                {
            var companyNameNode = GetNodeByfacet_header("Profile");
            if (companyNameNode != null)
                return Utils.NormalizationString(companyNameNode.InnerText);
            return null;
        }

    }
}
