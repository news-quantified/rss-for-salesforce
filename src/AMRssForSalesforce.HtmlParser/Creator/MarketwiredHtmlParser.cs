﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Linq;
    using Infrastructure.Helpers;
    using Model;
    using HtmlAgilityPack;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using Infrastructure.Constants;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    using System.IO;
    using System.Net;
    public class MarketwiredHtmlParser
    {
        public string FileName;
        public readonly string UrlAdress;
        private  DateTime _newsDateTime;

        private List<string> urlFromText= new List<string>();
        private string[] urlFromTextAll = null;
        private RssFeed rssFeed;

        private bool soursePersonName = false;

        List<Contact> savedContact = new List<Contact>();

        private string emailsCurrent = null;
        private string currentType;

        private int countFoundEmail = 0;
        public MarketwiredHtmlParser(RssFeed _rssFeed)
        {
            UrlAdress = _rssFeed.Link;//
           // "http://www.marketwired.com/press-release/former-general-motors-cio-ralph-szygenda-appointed-to-nexient-advisory-board-2101295.htm"; // ////
            _newsDateTime = _rssFeed.DateTime;
            this.rssFeed = _rssFeed;
        }

        public string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }

        public void FixHedlineContact(RssFeed rssFeed)
        {
            try
            {
                var res = HtmlHelpers.GetResponse(UrlAdress);
                if (res == null)
                    return;

                var htmlContent = res.Html;
                if (htmlContent == null)
                    return;

                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var h1 = doc.DocumentNode.SelectSingleNode("//h1");

                rssFeed.HeadLine = h1.InnerText;
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(rssFeed.Link + " " + ex);
            }
        }

        private void GetLoadDataFromNode(HtmlNode node, List<TypeRepresent> list)
        {
            var valueMain = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));
            if (!String.IsNullOrEmpty(valueMain) && valueMain != "," && valueMain != ":" && valueMain != ".")
            {
                var newRec = new TypeRepresent() {Value = valueMain};
                if (RegExp.IsNumber(valueMain))
                {
                    newRec.Type = TypeEnum.Pnone;
                }
                list.Add(newRec);
                
                
            }    
                
                

            foreach (var item in node.ChildNodes)
            {
                if (item.InnerHtml.Contains("<br />"))
                {
                    var tempList = item.InnerHtml.Split(new[] { "<br />" }, StringSplitOptions.None).ToList();

                    for (var i = 0; i < tempList.Count; i++)
                    {
                      tempList[i] =  StringHelpers.CleanAndFormatText(tempList[i]);
                    }

                    for (var i = 0; i < tempList.Count; i++)
                    {
                        if (!tempList[i].Contains("href=")) continue;
                        var regex = new Regex("<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")", RegexOptions.IgnoreCase);
                        var firstOrDefault = regex.Matches(tempList[i]).OfType<Match>().Select(m => m.Groups["href"].Value).FirstOrDefault(s => s.Contains("@"));
                        if (firstOrDefault == null) continue;
                        var tmp = firstOrDefault.Replace("mailto:","");
                        tempList[i] = tmp;
                    }

                     list.AddRange(from tempItem in tempList
                                  select StringHelpers.ScrubHtml(tempItem) into value
                                  where !String.IsNullOrEmpty(value) && value != "," && value != ":" && value != "."
                                   select new TypeRepresent() { Value = value, Type = ValidationHelpers.PhoneIsValid(value) ? TypeEnum.Pnone : TypeEnum.String});
                }
                else
                {
                    var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(item));
                    if (String.IsNullOrEmpty(value) || value == "," || value == ":" || value == ".") continue;
                    var newRec = new TypeRepresent() { Value = value };
                    if (RegExp.IsNumber(value))
                    {
                        newRec.Type = TypeEnum.Pnone;
                    }
                    list.Add(newRec);
                    if (item.HasChildNodes)
                    {
                        GetLoadDataFromNode(item, list);
                    }
                }
            }
        }


        public bool NotEnglish(HtmlDocument doc)
        {
            var contacts = new List<HtmlNode>();
            var countP = 0;
            var countPnoEnglish = 0;
            var allNoEnglish = 0;

            var ps = doc.DocumentNode.SelectNodes("//div[@class='mw_release']");
            if (ps == null) 
                return true;

            foreach (var p in ps)
            {
                countP++;
                var countNoEnglish = 0;

                foreach (var s in Dictionaries.NotEnglishCh)
                {
                    var n = p.InnerText.IndexOf(s, StringComparison.Ordinal);
                    while (n != -1)
                    {
                        countNoEnglish++;
                        n = p.InnerText.IndexOf(s, n + s.Length, StringComparison.Ordinal);
                    }
                }
                if (countNoEnglish > 0)
                {
                    countPnoEnglish++;
                    allNoEnglish += countNoEnglish;
                }

                foreach (var ch in p.InnerText)
                {
                    if (ch >= (char)12000) countNoEnglish++;

                    if (countNoEnglish > 5) return true;
                }
            }

            return countPnoEnglish * 100 / countP >= 50 || allNoEnglish > 10;
        }

        public Tuple<string, HtmlDocument> StartParse()
        {
            try
            { 
            int saved = 0;
            var web = new HtmlWeb();
            

            HtmlDocument doc = null;
            if (string.IsNullOrEmpty(FileName))
            {

                doc = new HtmlDocument();

                var res = HtmlHelpers.GetResponse(UrlAdress);
                if (res == null || res.Html == null)
                {

                    var contactsNf = new List<Contact>();
                    contactsNf.Add(new Contact() { NewsID = UrlAdress });
                    web = new HtmlWeb();
                    doc = web.Load(UrlAdress);
                    return new Tuple<string, HtmlDocument>("", doc);
                }


                var htmlContent = res.Html;

                rssFeed.Redirect = res.Redirect;

                if (htmlContent != null)
                {

                    doc.LoadHtml(htmlContent);
                }
                else
                {
                    doc = web.Load(UrlAdress);
                }

            }
            else
            {
                var htmlContent = File.ReadAllText(FileName);
                doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
            }

            ContactInfoRepository repo = new ContactInfoRepository();
            //repo.Delete(UrlAdress);

            doc.OptionWriteEmptyNodes = true;

            #region language
            //var head = doc.DocumentNode.SelectSingleNode("/html/head");
            //if (head != null)
            //{
            //    var culture = head.Attributes.FirstOrDefault(s => s.Name == "culture");
            //    if (culture != null)
            //    {
            //        var langValue = culture.Value;
            //        if (langValue != "en-US") return null;
            //        if (NotEnglish(doc)) return null;
            //    }
            //}
            //else
            //{
            //    if (NotEnglish(doc)) 
            //        return null;
            //}
            rssFeed.English = true;
            #endregion

            urlFromTextAll = RegExp.getUrls(doc.DocumentNode.InnerHtml);
           
            var typeRepresentList = new List<TypeRepresent>();

            #region Get Source Level 
            string sourceValue = null;
            var sourseNode = doc.DocumentNode.SelectNodes(String.Format(@"//table[@class='news-table']/tbody/tr[1]/td"));
            if (sourseNode != null)
            {
                sourceValue = GetPageSource(sourseNode.FirstOrDefault());
                typeRepresentList.Add(new TypeRepresent() { Type = TypeEnum.Source, Value = sourceValue.Trim() });
            }
            else
            {
                sourceValue = GetSource(doc.DocumentNode);
            }
                if(string.IsNullOrEmpty(sourceValue))
                {

                }

            
            #endregion

            var Dateline = Utils.CatDataInDL(GetDateline(doc.DocumentNode));
                if(string.IsNullOrEmpty(Dateline) && sourseNode != null && !string.IsNullOrEmpty(sourceValue))
                {
                    Dateline = GetDeteLine2(sourseNode.FirstOrDefault(), sourceValue);
                }

                if(this.rssFeed.DateTime.Year < 2000)
                {
                    _newsDateTime = GetDateTime(doc.DocumentNode);
                    this.rssFeed.DateTime = _newsDateTime;
                }

            #region GetAllNode

            var strContactInformation = StringHelpers.CleanAndFormatText(GetInformationText(doc));

            GetUrls(doc.DocumentNode,sourceValue);

            var contacts = strContactInformation.Split(new string[] { "<br /><br />", "<strong><br />", "<span style=\"text-decoration:underline\"><br />" }, StringSplitOptions.None);
            if (contacts.Length != 0)
            {

                foreach (var contact in contacts)
                {
                    var textContact = RegExp.GetInnerText(contact);
                    var emails = RegExp.getEmails(contact);
                    var numbers = RegExp.GetPnoneNumbers(textContact);
                    var titles = Utils.GetTitle(textContact);


                    if (!emails.Any() && !numbers.Any() && (string.IsNullOrEmpty(titles) || textContact.Contains("analyst should contact"))
                        && !string.IsNullOrEmpty(sourceValue) && (!textContact.Contains(sourceValue)) && !contact.Contains("<br />"))
                    {
                        var urls = RegExp.getUrls(contact);
                        if (urls.Any())
                        {
                            foreach (var u in urls)
                            {
                                if (!urlFromText.Any(uu => uu == u))
                                    urlFromText.Add(u);
                            }
                        }

                        var type = Utils.FindType(contact);
                        if (!string.IsNullOrEmpty(type))
                        {
                            currentType = type;
                        }
                        continue;
                    }

                    if (emails.Any())
                    {
                        emailsCurrent = string.Join(", ", emails);
                        countFoundEmail++;
                    }
                    else if (countFoundEmail == 0) 
                    {
                        emails = RegExp.getEmails(doc.DocumentNode.InnerHtml);
                        if (emails.Any())
                        {
                            emailsCurrent = string.Join(", ", emails);
                        }
                    }

                    var clearList = new List<TypeRepresent>();
                    var contactdata = contact.Split(new string[] { "<br />" }, StringSplitOptions.None);


                    foreach (var contactInfo in contactdata)
                    {
                        if (String.IsNullOrEmpty(contactInfo.Trim())) continue;
                        if (!clearList.Any(itemClearList => String.Equals(itemClearList.Value, StringHelpers.ScrubHtml(contactInfo).Trim(), StringComparison.CurrentCultureIgnoreCase)))
                        {
                            var value = StringHelpers.ScrubHtml(contactInfo).Trim();
                            var parts = value.Split(new string[] { " with ", ", please contact:", " or ", "/", ",", ":", " for " }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var p in parts)
                            {
                                if (!RegExp.GetPnoneNumbers(p).Any() && !p.Contains("(Bill)"))
                                {
                                    var parts2 = p.Split(new string[] { "(", ")" }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (var p2 in parts2)
                                    {
                                        var pValue = p2.Trim();
                                        if (pValue.IndexOf("or ") == 0)
                                        {
                                            pValue = pValue.Remove(0, 3);
                                        }
                                        clearList.Add(new TypeRepresent() { Type = TypeEnum.String, Value = pValue });
                                    }
                                }
                                else
                                {
                                    var pValue = p.Trim();
                                    if (pValue.IndexOf("or ") == 0)
                                    {
                                        pValue = pValue.Remove(0, 3);
                                    }
                                    clearList.Add(new TypeRepresent() { Type = TypeEnum.String, Value = pValue });
                                }
                            }
                        }
                    }

                    clearList = clearList.Where(c => !ConstValue.RemoveNode.Any(d => c.Value.IndexOf(d, StringComparison.InvariantCultureIgnoreCase) >= 0)).ToList();

                    clearList = clearList.Where(c => !ConstValue.ShortAdressList.Any(d => c.Value.IndexOf(d, StringComparison.InvariantCultureIgnoreCase) >= 0)).ToList();

                    if (!String.IsNullOrEmpty(sourceValue))
                    {
                        foreach (var item in clearList.Where(item => item.Value.ToLower().Trim() != "or" && IsCompanyName(item.Value)))
                        {
                            item.Value = Regex.Replace(item.Value, "Contact", "", RegexOptions.IgnoreCase).Trim();//продумать список
                            item.Type = TypeEnum.Company;
                        }
                        List<string> contactDomains = FindeDomansFromContact(clearList); 
                        foreach (var item in clearList)
                        {
                            if (item.Value.ToLower().Contains("@"))
                            {
                              //  item.Type = TypeEnum.Type;
                                continue;
                            }
                            if(item.Value.ToLower().Contains("media contact"))
                            {
                                item.Type = TypeEnum.Type;
                                continue;
                            }
                            if (item.Type == TypeEnum.String && !RegExp.getEmails(item.Value).Any() && !RegExp.getUrls(item.Value).Any() && IsCompanyName(item.Value))
                            {
                              
                                 item.Type = TypeEnum.Company;
                                
                            }
                        }
                    }

                    clearList = clearList.Where(c => !ConstValue.ShortAdressList.Any(d => c.Value.IndexOf(d, StringComparison.InvariantCultureIgnoreCase) >= 0)).ToList();

                    foreach (var item in clearList.Where(item => item.Type == TypeEnum.String))
                    {
                        var word = Utils.NormalizationString(item.Value);
                        item.Value = word;
                        var emails2 = RegExp.getEmails(word);
                        var urls = RegExp.getUrls(word);
                        if(emails2.Any())
                        {
                            item.Type = TypeEnum.Email;
                            item.Value = string.Join(", ", emails2);
                            foreach(var e in emails2)
                            {
                                word = word.Replace(e, " ");
                            }
                            word = word.Trim();
                            continue;
                        }
                        else if (urls.Any())
                        {
                            
                            if (urls.Any())
                            {
                                item.Value = string.Join(", ", urls);
                                item.Type = TypeEnum.Url;
                                foreach (var e in urls)
                                {
                                    word = word.Replace(e, " ");
                                }
                                word = word.Trim();
                                continue;
                            }
                        }
                        else
                        {
                            var numbers2 = RegExp.GetPnoneNumbers(word);
                            if(numbers2.Any())
                            {
                                item.Type = TypeEnum.Pnone;
                                continue;
                            }
                        }
                        if (ConstValue.TypesList.Any(c => word.IndexOf(c, c.Length <= 3 ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase) >= 0))
                        {
                            item.Type = TypeEnum.Type;
                        }

                        if (ConstValue.TitlesList.Any(c => word.IndexOf(c, c.Length <= 3 ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase) >= 0)
                            )
                        {
                            if(item.Type == TypeEnum.String)
                               item.Type = TypeEnum.Title;
                            else
                            {
                                var type = Utils.FindType(word);
                                if( !string.IsNullOrEmpty(type))
                                {
                                    if (type == word || type + " Relations" == word || type + " Contact" == word)
                                        item.Type = TypeEnum.Type;
                                    else
                                        item.Type = TypeEnum.Title;
                                }
                            }
                        }
                    }


                    foreach (var item in clearList.Where(item => item.Type == TypeEnum.String))
                    {
                        var itemLocal = item;
                        foreach (var removeWordItem in ConstValue.RemoveWords.Where(removeWordItem => itemLocal.Value.IndexOf(removeWordItem, StringComparison.InvariantCultureIgnoreCase) >= 0 && itemLocal.Value.IndexOf(removeWordItem, StringComparison.InvariantCultureIgnoreCase) < 0))
                        {
                            item.Value = Regex.Replace(item.Value, removeWordItem, "", RegexOptions.IgnoreCase).Trim();
                        }
                    }




                    if (!String.IsNullOrEmpty(sourceValue)) clearList.Add(new TypeRepresent() { Type = TypeEnum.Source, Value = sourceValue });

                    clearList = clearList.Where(c => c.Value.Length > 1).ToList();

                    TextRecognising(clearList);
                    FindMoreContactByEmail(clearList);
                    CreateandAndSaveContact(clearList);

                    saved++;

                }
            }
                else
            {

            }
            if (saved == 0) 
            {
                Contact contact = new Contact();
                contact.Email =  emailsCurrent;
                if(string.IsNullOrEmpty(contact.Email))
                {
                    var emails = RegExp.getEmails(doc.DocumentNode.InnerText);
                    if(emails.Any())
                    {
                        contact.Email = string.Join(", ", emails);
                    }
                }
                contact.Type = currentType;
                contact.Source = sourceValue;
                contact.Url = FindeUrlByCompanyName(sourceValue, sourceValue);
                contact.NewsID = UrlAdress;
                if (soursePersonName)
                    contact.Name = sourceValue;

                if (string.IsNullOrEmpty(contact.Url) && !string.IsNullOrEmpty(contact.Email))
                {
                    contact.Url = GetRelatedLink(contact);
                }
                contact.NewsReleaseDate = _newsDateTime.ToString(CultureInfo.InvariantCulture);
                contact.LastUpdated = DateTime.Now.ToString(CultureInfo.InvariantCulture); 
                repo.Add(contact);
                savedContact.Add(contact);
            }
            #endregion
            rssFeed.ContactInfo = Utils.SummContactData(savedContact);
            return new Tuple<string, HtmlDocument>(Dateline, doc);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = UrlAdress });
                var web = new HtmlWeb();
                var doc = web.Load(UrlAdress);
                return new Tuple<string, HtmlDocument>("", doc);
            }
        }

        private DateTime GetDateTime(HtmlNode htmlNode)
        {
            //news-date
            var time = htmlNode.SelectSingleNode("//p[@id='news-date']");

            if (time != null)
            {
                string stringDateTime = Utils.NormalizationString(time.InnerText);
                //string cont = "";
                //if (time.NextSibling.InnerText.Length == 2)
                //{
                //    cont = " " + time.NextSibling.InnerText;
                //}
                //else if (time.NextSibling.InnerText.Length == 3)
                //{
                //    cont = time.NextSibling.InnerText;
                //}

                var strDateTime = stringDateTime;
                DateTime dataTime = DateTime.Now;
                if (DateTimeHelpers.TryParse(strDateTime, out dataTime))
                {
                    if (dataTime.Year > 2000)
                        return dataTime;
                }
                strDateTime = stringDateTime.Replace(" ET", "");
                if (DateTime.TryParse(strDateTime, out dataTime))
                {
                    return dataTime;
                }
            }
            else
            {

            }

            return DateTime.Now;
      
        }

        private string GetSource(HtmlNode htmlNode)
        {
          
            var ps = htmlNode.SelectNodes("//p");

            if (ps != null)
            {
                foreach (var p in ps)
                {

                    int SourseIndex = p.InnerText.Trim().IndexOf("SOURCE");
                    if (SourseIndex >= 0)
                    {
                        return Utils.NormalizationString(p.InnerText.Replace("SOURCE", "").Trim());
                    }

                }
            }
            return "";
        }

       
        private string GetDateline(HtmlNode htmlNode)
        {
            //mw_release
            var metaDate = htmlNode.SelectSingleNode("//meta[@name='description']");

            if (metaDate != null && metaDate.Attributes["content"] != null)
            {
                var strMetaDate = metaDate.Attributes["content"].Value;

                if (!string.IsNullOrEmpty(strMetaDate))
                {
                    if (strMetaDate.Contains("--"))
                    {
                        var strings = strMetaDate.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                        return Utils.NormalizationString(strings[0]);
                    }
                    else if (strMetaDate.Contains("/ - "))
                    {
                        var strings = strMetaDate.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                        return Utils.NormalizationString(strings[0]);

                    }
                    else
                    {
                        var longDL = strMetaDate;
                    }
                }

            }


            var spanRegion = htmlNode.SelectSingleNode("//span[@class='mw_region']");
            if (spanRegion != null)
                return spanRegion.InnerText;

            var ps = htmlNode.SelectNodes("//p");


            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("--"))
                    {
                        var strings = p.Split(new string[] { "--" }, StringSplitOptions.RemoveEmptyEntries);
                        return Utils.NormalizationString(strings[0]);
                    }
                    else if (p.Contains("/ - "))
                    {
                        var strings = p.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                        return Utils.NormalizationString(strings[0]);

                    }
                }
            }

          
            var div = htmlNode.SelectSingleNode("//div[@class='dateline']");
            if (div != null)
                return Utils.NormalizationString(div.InnerText);

            div = htmlNode.SelectSingleNode("//span[@class='dateline']");
            if (div != null)
                return Utils.NormalizationString(div.InnerText);

            return ".";
        }

        private List<string> FindeDomansFromContact(List<TypeRepresent> clearList)
        {
            List<string> domains = new List<string>();

            foreach(var item in clearList)
            {
                var emails = RegExp.getEmails(item.Value);
                if (emails.Any())
                {
                    foreach(var e in emails)
                    {
                        var parts = e.Split('@');
                        if( parts.Count() >=2 )
                        {
                            domains.Add(parts[1]);
                        }
                    }
                }
                else
                {
                    var urls = RegExp.getUrls(item.Value);
                    if (urls.Any())
                    {
                        foreach (var u in urls)
                        {
                            domains.Add(u);
                        }
                    }
                }
            }
            return domains.Distinct().ToList();
        }

        public virtual string GetRelatedLink(Contact contact)
        {
            List<string> urls = new List<string>();
            if (!string.IsNullOrEmpty(contact.Email))
            {
                string[] partsEmail = contact.Email.Split(',');
                foreach (var email in partsEmail)
                {
                    string[] parts = email.Split('@');
                    var url = parts[1];
                    if (!urls.Any(u => u == url))
                        urls.Add(url);


                }
                if (urls.Any())
                    return string.Join(", ", urls.ToArray());
            }
           
            return null;
        }


        private void GetUrls(HtmlNode htmlNode, string source)
        {
            var ps = htmlNode.SelectNodes("//p");
            string url = null;

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.InnerText.ToLower().Contains("information visit") || p.InnerText.ToLower().Contains("replay at") || p.InnerText.ToLower().Contains("web address")
                        || p.InnerText.ToLower().Contains("visited at") || p.InnerText.ToLower().Contains("information, visit")
                        || p.InnerText.ToLower().Contains("available at")
                         || p.InnerText.ToLower().Contains("the company website at")
                        || p.InnerText.ToLower().Contains(" website at")
                         || p.InnerText.ToLower().Contains("website:")
                        || p.InnerText.ToLower().Contains("our website")
                         || p.InnerText.ToLower().Contains("visit website")
                         || p.InnerText.ToLower().Contains("please visit")
                        || p.InnerText.ToLower().Contains(" information ")
                        || p.InnerText.ToLower().Contains(" information,")
                        || p.InnerText.ToLower().Contains("more at")
                        || p.InnerText.ToLower().Contains("visit")
                         || p.InnerText.ToLower().Contains("inc. (")
                        || p.InnerText.ToLower().Contains("available on")
                        || !string.IsNullOrEmpty(source) && p.InnerText.ToLower().Contains(source.ToLower())
                        || !string.IsNullOrEmpty(source) && p.InnerText.ToLower().Contains(source.ToLower().Replace(" ", "")))
                    {

                        var urls = RegExp.getUrls(p.InnerHtml);

                        if (urls.Any())
                        {
                            foreach(var u in urls)
                            {
                                if (!urlFromText.Any(uu => uu == u) && !u.Contains("2fwww.")  && !u.Contains("marketwire.com") && !u.Contains("howheasked.com"))
                                    urlFromText.Add(u);
                            }
                        }
                        else
                        { 
                           
                        }
                    }
                }
            }
            //if (urlFromText.Any())
            //    return;
            var div = htmlNode.SelectSingleNode("//div[@class='newsroom-right-content']");

            if (div != null)
            {


                var a = div.SelectSingleNode(".//a");

                if (a != null && a.Attributes["href"] != null)
                {
                    var urls = RegExp.getUrls(a.Attributes["href"].Value);
                    foreach (var u in urls)
                    {
                        if (!urlFromText.Any(uu => uu == u) && !u.Contains("2fwww.") && !u.Contains("marketwire.com") && !u.Contains("howheasked.com"))
                            urlFromText.Add(u);
                    }
                }
            }

            var divCompany = htmlNode.SelectSingleNode("//div[@id='newsroom-copy']");

            if (divCompany != null)
            {

                var a2 = divCompany.SelectSingleNode(".//a");

                if (a2 != null && a2.Attributes["href"] != null)
                {
                    var urls = RegExp.getUrls(a2.Attributes["href"].Value);
                    foreach (var u in urls)
                    {
                        if (!urlFromText.Any(uu => uu == u) && !u.Contains("2fwww.")  && !u.Contains("marketwire.com") && !u.Contains("howheasked.com"))
                            urlFromText.Add(u);
                    }
                }
            }
        }

        private string GetInformationText(HtmlDocument doc)
        {
            const string classMainToFind = "newsroom-contact-middle";
            const string classBlockToFind = "mw-contact";

            string result;
            if (doc.DocumentNode.SelectNodes(String.Format(@"//div[@class='{0}']", classBlockToFind)) != null)
            {
                var allElementsWithClassFloat = doc.DocumentNode.SelectNodes(String.Format(@"//div[@class='{0}']", classBlockToFind));
                if (allElementsWithClassFloat == null || allElementsWithClassFloat.Count == 0) return String.Empty;
                result = allElementsWithClassFloat[0].InnerHtml;
            }
            else
            {
                var allElementsWithClassFloat = doc.DocumentNode.SelectNodes(String.Format(@"//div[@id='{0}']/ul/li", classMainToFind));
                if (allElementsWithClassFloat == null || allElementsWithClassFloat.Count == 0)
                {
                    var ps = doc.DocumentNode.SelectNodes("//p");
                    var div_mw_release = doc.DocumentNode.SelectSingleNode("//div[@class='mw_release']");
                    if (div_mw_release != null)
                        ps = div_mw_release.SelectNodes(".//p");
                    foreach(var p in ps)
                    {
                        var emails = RegExp.getEmails(p.InnerHtml);
                        var numbers = RegExp.GetPnoneNumbers(RegExp.GetInnerText(p.InnerHtml));

                        if(emails.Any() || numbers.Any())
                        {
                            return p.InnerHtml;
                        }
                    }
                    return String.Empty;
                }
                result = allElementsWithClassFloat[0].InnerHtml;
            }
            return result;
        }

        private IEnumerable<TypeRepresent> GetAllContactNode(HtmlDocument doc)
        {
            const string classMainToFind = "newsroom-contact-middle";
            const string classBlockToFind = "mw-contact";

            var representList = new List<TypeRepresent>();
            if (doc.DocumentNode.SelectNodes(String.Format(@"//div[@class='mw-contact']")) != null)
            {
                var allElementsWithClassFloat = doc.DocumentNode.SelectNodes(String.Format(@"//div[@class='{0}']", classBlockToFind));
                if (allElementsWithClassFloat == null || allElementsWithClassFloat.Count == 0) return representList;

                foreach (var item in allElementsWithClassFloat.Nodes())
                {
                    GetLoadDataFromNode(item, representList);
                }
            }
            else
            {
                var allElementsWithClassFloat = doc.DocumentNode.SelectNodes(String.Format(@"//div[@id='{0}']/ul/li", classMainToFind));
                if (allElementsWithClassFloat == null || allElementsWithClassFloat.Count == 0) return representList;
                foreach (var item in allElementsWithClassFloat[0].ChildNodes)
                {
                    GetLoadDataFromNode(item, representList);
                }
            }
            return representList.AsEnumerable();
        }

        private string GetPageSource(HtmlNode sourceNode)
        {
            var result = String.Empty;
            foreach (var node in sourceNode.ChildNodes)
            {
                var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));
                if (String.IsNullOrEmpty(value) || value == "," || value == ":" || value == ".") continue;
                value = ConstValue.RemoveWords.Aggregate(value, (current, item) => Regex.Replace(current, item, "", RegexOptions.IgnoreCase));
                result = value;
                break;
            }
            if(string.IsNullOrEmpty(result))
            {
                var value2 = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(sourceNode));
                if(!string.IsNullOrEmpty(value2) && RegExp.IsName(value2))
                {
                    result = value2;
                    soursePersonName = true;
                }
            }
            return result.Trim();
        }

        private string GetDeteLine2(HtmlNode sourceNode, string sourse)
        {
            var result = String.Empty;
            foreach (var node in sourceNode.ChildNodes)
            {
                var value = StringHelpers.CleanAndFormatText(HtmlHelpers.GetNodeText(node));
                if (String.IsNullOrEmpty(value) || value == sourse) continue;
               
                result = value;
                break;
            }
           
            return result.Trim();
        }

        private void FindMoreContactByTelephone(List<TypeRepresent> clearList)
        {
            var phoneList = clearList.Where(c => c.Type == TypeEnum.Pnone).ToList();
            var listPosition = new List<int>{0};

            foreach (var item in phoneList)
            {
                var startPosition = listPosition.Last();
                var index = clearList.FindIndex(startPosition, c => c.Type == item.Type && c.Value == item.Value);

                var testList = clearList.Skip(startPosition).Take(index - startPosition).ToList();
                if (testList.Any(s => s.Type == TypeEnum.Contact)) listPosition.Add(index + 1);
                else 
                {
                    var tempitem = testList.FirstOrDefault(c => c.Type == TypeEnum.String);
                    var tempindex = clearList.FindIndex(startPosition, c => tempitem != null && (c.Type == tempitem.Type && c.Value == tempitem.Value));
                    if (tempindex >= 0)
                    {
                        if ( ConstValue.NoNameList.Any(c => String.Equals(clearList[tempindex].Value, c, StringComparison.CurrentCultureIgnoreCase)))
                        {
                            clearList[tempindex].Type = TypeEnum.NotContact;
                            if (clearList.Count >= tempindex &&  clearList[tempindex + 1].Type == TypeEnum.String)
                            {
                                if (!ConstValue.NoNameList.Any(c =>String.Equals(clearList[tempindex + 1].Value, c,StringComparison.CurrentCultureIgnoreCase)))
                                {
                                    clearList[tempindex + 1].Type = TypeEnum.Contact;
                                }
                                else
                                {
                                    if (clearList.Count >= tempindex + 1 && clearList[tempindex + 2].Type == TypeEnum.String)
                                    {
                                        if (!ConstValue.NoNameList.Any(c => String.Equals(clearList[tempindex + 2].Value, c, StringComparison.CurrentCultureIgnoreCase)))
                                        {
                                            clearList[tempindex + 2].Type = TypeEnum.Contact;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (clearList.Count >= tempindex + 1 && clearList[tempindex + 2].Type == TypeEnum.String)
                                {
                                    if (!ConstValue.NoNameList.Any(c => String.Equals(clearList[tempindex + 2].Value, c, StringComparison.CurrentCultureIgnoreCase)))
                                    {
                                        clearList[tempindex + 2].Type = TypeEnum.Contact;
                                    }
                                }
                            }
                        }
                        else
                        {
                            clearList[tempindex].Type = TypeEnum.Contact;
                        }
                            
                            
                            
                            
                            
                            
                            
                    }




                    listPosition.Add(index + 1);
                }


               
               
            }

        }

        private void FindMoreContactByEmail(List<TypeRepresent> clearList)
        {
            var emailList = clearList.Where(c => c.Type == TypeEnum.Email).ToList();
            if (emailList.Any())
            {
                foreach (var item in emailList)
                {
                    var emails = RegExp.getEmails(item.Value);
                    foreach(var e in emails)
                    { 
                        var addr = new MailAddress(e);
                    
                        var username = addr.User;
                        var splitArray = username.Split('.');
                        if (splitArray.Length == 0) splitArray = username.Split('_');
                        foreach (string t in splitArray)
                        {
                            foreach (var clearListItem in clearList)
                            {
                                if (clearListItem.Type != TypeEnum.String) continue;

                                var tempValue = clearListItem.Value.Replace(" ", "").Trim();
                                if ((clearListItem.Value.IndexOf(t, StringComparison.OrdinalIgnoreCase) >= 0) || (tempValue.IndexOf(t, StringComparison.OrdinalIgnoreCase) >= 0))
                                {
                                    clearListItem.Value = ConstValue.RemoveWords.Aggregate(clearListItem.Value, (current, itemreg) => Regex.Replace(current, itemreg, "", RegexOptions.IgnoreCase));
                                    clearListItem.Type = TypeEnum.Contact;

                                }
                            }
                        }
                    }
                }
            }
        }


        protected void CreateandAndSaveContact(List<TypeRepresent> clearList)
        {
            var contact = new ContactValidate
            {
                NewsID = UrlAdress,
                LastUpdated = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                NewsReleaseDate = _newsDateTime.ToString(CultureInfo.InvariantCulture),

            };




            var nameValue = clearList.FirstOrDefault(c => c.Type == TypeEnum.Contact && RegExp.IsName(c.Value));
            if (nameValue != null)
            {
                contact.Name = nameValue.Value;
            }
            else
            {
                nameValue = clearList.FirstOrDefault(c => c.Type == TypeEnum.String && Utils.GetName(c.Value) != null);

                if (nameValue != null)
                {
                    contact.Name = Utils.GetName( nameValue.Value);
                }
                else
                {
                    var saveContact = false;
                    foreach (var s in clearList.Where(c => c.Type == TypeEnum.Pnone))
                    {
                        var words = s.Value.Split(new string[] { ",", ":" }, StringSplitOptions.RemoveEmptyEntries); 
                        if(words.Count() > 1)
                        {
                            if(RegExp.IsName(words[0]) && RegExp.IsNumber(words[1]))
                            {
                                var repo1 = new ContactInfoRepository();
                                ContactValidate contactAdd = contact.Clone() as ContactValidate;
                                var companyName = clearList.FirstOrDefault(c => c.Type == TypeEnum.Company);
                                if (companyName != null)
                                    contactAdd.CompanyName = companyName.Value;

                                var type2 = clearList.FirstOrDefault(c => c.Type == TypeEnum.Type);
                                if (type2 != null)
                                    contactAdd.Type = type2.Value;

                                var title2 = clearList.FirstOrDefault(c => c.Type == TypeEnum.Title);
                                if (title2 != null)
                                    contactAdd.Title = title2.Value;
                                contactAdd.Name = words[0].Trim();
                                if (RegExp.IsNumber(words[1].Trim()))
                                   contactAdd.Phone = words[1].Trim();

                                var source2 = clearList.FirstOrDefault(c => c.Type == TypeEnum.Source);
                                if (source2 != null) contactAdd.Source = source2.Value;

                                var url2 = clearList.FirstOrDefault(c => c.Type == TypeEnum.Url);
                                if (url2 != null) contactAdd.Url = url2.Value;
                                else
                                {
                                    contactAdd.Url = FindeUrlByCompanyName(contactAdd.CompanyName, contactAdd.Source);
                                }

                                var emails = clearList.Where(c => c.Type == TypeEnum.Email);
                                if(emails.Any())
                                {
                                    var found = false;

                                    foreach(var e in emails)
                                    {
                                        if(Utils.trueEmail(e.Value, contactAdd.Name))
                                        {
                                            contactAdd.Email = e.Value;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(!found)
                                    {
                                        contactAdd.Email = string.Join(", ", emails.Select(e => e.Value).ToArray());
                                    }
                                }
                                else if(emailsCurrent != null)
                                {
                                    contactAdd.Email = emailsCurrent;
                                }
                                repo1.Add(contactAdd);
                                saveContact = true;
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                    if (saveContact)
                        return;
                }
            }




            var type = clearList.FirstOrDefault(c => c.Type == TypeEnum.Type);
            if (type != null) contact.Type = type.Value;


            var titleValue = clearList.Where(c => c.Type == TypeEnum.Title).ToList().Aggregate(String.Empty, (current, item) => current + String.Format("{0},", item.Value));
            if (!String.IsNullOrEmpty(titleValue))
            {
                titleValue = titleValue.Remove(titleValue.Length - 1, 1);

                var title2 = Utils.GetTitle(titleValue);
                contact.Title = title2;
            }
           
            var email = clearList.FirstOrDefault(c => c.Type == TypeEnum.Email);
            if (email != null) contact.Email = email.Value;

            foreach (var item in clearList.Where(c => c.Type == TypeEnum.Pnone).ToList())
            {
                contact.Phone += String.Format("{0}, ", item.Value);
            }
            if (!String.IsNullOrEmpty(contact.Phone)) contact.Phone = contact.Phone.Remove(contact.Phone.Length - 1, 1);

            var company = clearList.FirstOrDefault(c => c.Type == TypeEnum.Company);
            if (company != null) 
                contact.CompanyName = company.Value;

            var source = clearList.FirstOrDefault(c => c.Type == TypeEnum.Source);
            if (source != null) contact.Source = source.Value;

            var url = clearList.FirstOrDefault(c => c.Type == TypeEnum.Url);

            contact.Url = url != null ? url.Value : GetUrl(contact.Email);


            if (String.IsNullOrEmpty(contact.CompanyName))
            {
                contact.CompanyName = contact.Source;
            }

            if(contact.Name == null )
            {
                //if(contact.CompanyName != null)
                //{
                //    if (RegExp.IsName(contact.CompanyName))
                //        contact.Name = contact.CompanyName;
                //}
            }

            var repo = new ContactInfoRepository();
            var isParse = false;
           
            //if (contact.Name != null && contact.Name.Length > 21) contact.Name = null;

            //if (contact.Name != null && ConstValue.NoNameList.Any(c => String.Equals(c, contact.Name, StringComparison.CurrentCultureIgnoreCase))) contact.Name = null;

            //if (contact.Name != null)
            //{
            //    foreach (var itemSeparator in ConstValue.SeparatorList)
            //    {
            //        if (contact.Name.IndexOf(itemSeparator, StringComparison.InvariantCultureIgnoreCase) < 0)
            //            continue;
            //        var nameList = Regex.Split(contact.Name, itemSeparator, RegexOptions.IgnoreCase);
            //        foreach (var t in nameList)
            //        {
            //            isParse = true;
            //            var newContact = (contact.Clone() as ContactValidate);
            //            if (newContact == null) continue;
            //            newContact.Name = t;
            //            newContact = GetValidContact(newContact);
            //            repo.Add(newContact);
            //        }
            //    }
            //}
            if (!isParse)
            {
                contact = GetValidContact(contact);
                if (string.IsNullOrEmpty(contact.Url))
                    contact.Url = FindeUrlByCompanyName(contact.CompanyName, contact.Source);
                else
                {
                    var urls = RegExp.getUrls(contact.Url);
                    contact.Url = string.Join(", ", urls);
                }

                if(string.IsNullOrEmpty(contact.Email) && !string.IsNullOrEmpty(emailsCurrent))
                {
                    contact.Email = emailsCurrent;
                    if (!string.IsNullOrEmpty(contact.Name) && Utils.trueEmail(contact.Email, contact.Name))
                        emailsCurrent = null;
                }

                if(string.IsNullOrEmpty(contact.Type) && !string.IsNullOrEmpty(currentType))
                {
                    contact.Type = currentType;
                }

                if(string.IsNullOrEmpty(contact.Url) || string.IsNullOrEmpty(contact.Name) || string.IsNullOrEmpty(contact.Email) || string.IsNullOrEmpty(contact.Phone))
                {

                }

                if(contact.Type != null)
                {
                    contact.Type = Utils.FindType(contact.Type);
                }
                else
                {
                    
                }

                if(contact.Title != null)
                {
                    contact.Title = Utils.GetTitle(contact.Title);
                }

                //temp
                if (contact.Name != null && contact.Phone != null || !savedContact.Any())
                {
                    repo.Add(contact);
                    if (contact.Email != null && emailsCurrent != null)
                    {
                        emailsCurrent = emailsCurrent.Replace(", " + contact.Email, "").Replace(contact.Email, "");
                    }
                    savedContact.Add(contact);
                }
                else
                {

                }
                if(clearList.Count(c => c.Type == TypeEnum.Contact) > 1)
                {
                    var names = clearList.Where(c => c.Type == TypeEnum.Contact).Skip(1).ToList();

                    int i =1;
                    foreach(var n in names)
                    {
                        ContactValidate contactAdd = contact.Clone() as ContactValidate;
                        contactAdd.Name = n.Value;

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Email))
                        {
                            contactAdd.Email = clearList.Where(c => c.Type == TypeEnum.Email).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Type))
                        {
                            contactAdd.Type = clearList.Where(c => c.Type == TypeEnum.Type).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Title))
                        {
                            contactAdd.Title = clearList.Where(c => c.Type == TypeEnum.Title).ToArray()[i].Value;
                        }

                        if (clearList.Count(c => c.Type == TypeEnum.Contact) == clearList.Count(c => c.Type == TypeEnum.Pnone))
                        {
                            contactAdd.Phone = clearList.Where(c => c.Type == TypeEnum.Pnone).ToArray()[i].Value;
                        }
                        repo.Add(contactAdd);
                        savedContact.Add(contact);
                        i++;
                    }
                }
            }
        }

        private string FindeUrlByCompanyName(string companyName, string Sourse)
        {
            if (!urlFromText.Any())
            {
                if (!string.IsNullOrEmpty(emailsCurrent))
                    return  "www." +RegExp.getUrls2(emailsCurrent)[0];

                if (!string.IsNullOrEmpty(companyName))
                {
                    var partOfNameCompany = Regex.Replace(companyName.ToLower(), "[^a-z0-9,]", "").Trim();
                    if (partOfNameCompany.Length > 5)
                        partOfNameCompany = partOfNameCompany.Remove(5);


                    foreach (var domen in urlFromTextAll)
                    {
                        if (domen.Contains(partOfNameCompany))
                            return domen;
                    }
                }
                else if (!string.IsNullOrEmpty(Sourse))
                {
                    var partOfNameCompany = Regex.Replace(Sourse.ToLower(), "[^a-z0-9,]", "").Trim();
                    if (partOfNameCompany.Length > 5)
                        partOfNameCompany = partOfNameCompany.Remove(5);


                    foreach (var domen in urlFromTextAll)
                    {
                        if (domen.Contains(partOfNameCompany))
                            return domen;
                    }
                }
                return null;
            }
            if (!string.IsNullOrEmpty(companyName))
            {
                var partcompanyNames = companyName.ToLower().Split(new string[] { " ", "(", ")", ":", ".", ",", "@" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var url in urlFromText)
                {
                    foreach (var word in partcompanyNames)
                    {
                        var partWord = word.Length > 5 ? word.Remove(5) : word;
                        if (url.ToLower().Contains(partWord))
                            return url;
                    }
                }

            }

            if (!string.IsNullOrEmpty(Sourse))
            {
                var partcompanyNames = Sourse.ToLower().Split(new string[] { " ", "(", ")", ":", ".", ",", "@" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var url in urlFromText)
                {
                    foreach (var word in partcompanyNames)
                    {
                        var partWord = word.Length > 5 ? word.Remove(5) : word;
                        if (url.ToLower().Contains(partWord))
                            return url;
                    }
                }

            }
            return string.Join(", ", urlFromText.ToArray());
        }

        public ContactValidate GetValidContact(ContactValidate contact)
        {
            var newContact = (contact.Clone() as ContactValidate);
            if (newContact == null) return null;
            var isValidateContact = true;


            #region CheckName
            if (!String.IsNullOrEmpty(newContact.Name))
            {
                var isTitle = ValidationHelpers.TitleIsValid(newContact.Name);
                if (isTitle.Item1)
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Title)) newContact.Title = newContact.Name;
                    newContact.Name = null;
                } else 
                if (ValidationHelpers.UrlIsValid(newContact.Name))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Url)) newContact.Url = newContact.Name;
                    newContact.Name = null;
                } else 
                if (ValidationHelpers.EmailIsValid(newContact.Name))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Email)) newContact.Email = newContact.Name;
                    newContact.Name = null;
                }else 
                if (ValidationHelpers.PhoneIsValid(newContact.Name))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Phone)) newContact.Phone = newContact.Name;
                    newContact.Name = null;
                }else 
                if (ValidationHelpers.TypeIsValid(newContact.Name))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Type)) newContact.Type = newContact.Name;
                    newContact.Name = null;
                }
                else
                    if (IsCompanyName(newContact.Name))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.CompanyName)) 
                        newContact.CompanyName = newContact.Name;
                    newContact.Name = null;
                }
                if (!String.IsNullOrEmpty(newContact.Name)) newContact.Name = newContact.Name.Trim();


                foreach (var item in ConstValue.NoNameList)
                {
                    if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                    newContact.Name = null;
                    break;
                }

                foreach (var item in ConstValue.RemoveWords)
                {
                    if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                    newContact.Name = null;
                    break;
                }

                if (newContact.Name != null)
                {
                    foreach (var item in ConstValue.RemoveNode)
                    {
                        if (!String.Equals(item, newContact.Name, StringComparison.CurrentCultureIgnoreCase)) continue;
                        newContact.Name = null;
                        break;
                    }
                }
                if (newContact.Name != null)
                {


                 
                    newContact.Name = newContact.Name.Replace(", CPA", " ");
                    newContact.Name = newContact.Name.Replace(" CPA", " ");
                    newContact.Name = newContact.Name.Replace(", CA", " ");
                    newContact.Name = newContact.Name.Replace(",CA", " ");
                    newContact.Name = newContact.Name.Replace(" CA", " ");
                    newContact.Name = newContact.Name.Replace("(USA)", " ");
                    newContact.Name = newContact.Name.Replace("(U.S.", " ");
                    newContact.Name = newContact.Name.Replace("USA", " ");

                    if (newContact.Name.Length < 5) newContact.Name = null;
                }


                
            }
            #endregion

           /* #region CompanyName
            if (!String.IsNullOrEmpty(newContact.CompanyName))
            {
                var isTitle = ValidationHelpers.TitleIsValid(newContact.CompanyName);
                if (isTitle.Item1)
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Title)) newContact.Title = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.UrlIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Url)) newContact.Url = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.EmailIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Email)) newContact.Email = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.PhoneIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Phone)) newContact.Phone = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.TypeIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Type)) newContact.Type = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                else if (ValidationHelpers.ContactIsValid(newContact.CompanyName))
                {
                    isValidateContact = false;
                    if (String.IsNullOrEmpty(newContact.Name)) newContact.Name = newContact.CompanyName;
                    newContact.CompanyName = null;
                }
                if (!String.IsNullOrEmpty(newContact.CompanyName)) newContact.CompanyName = newContact.CompanyName.Trim();
            }
            #endregion*/
            #region CheckPhone

            if (!String.IsNullOrEmpty(newContact.Phone))
            {
                newContact.Phone = newContact.Phone.TrimStart('.', '-','/',' ');
                newContact.Phone = newContact.Phone.Replace("()", " ");
                newContact.Phone = newContact.Phone.Replace("(..  )", " ");
              
            }
            #endregion




            if (!isValidateContact) newContact = GetValidContact(newContact);
            return newContact;
        }

        private string GetUrl(string value)
        {
            try
            {
                var addr = new MailAddress(value);
                return String.Format("www.{0}", addr.Host);
            }
            catch
            {
                return String.Empty;
            }
        }

        private void TextRecognising(IList<TypeRepresent> typeRepresentList)
        {
            if (typeRepresentList == null)
            {
                Logger.WriteError(new ArgumentNullException("typeRepresentList"));
                return;
            }

            var typeRepresentTempList = new List<Tuple<int, TypeRepresent>>();
            foreach (var item in typeRepresentList.Where(item => !String.IsNullOrEmpty(item.Value)))
            {
                if (item.Type == TypeEnum.Company || item.Type == TypeEnum.Source) continue;

                var isTitle = ValidationHelpers.TitleIsValid(item.Value);
                if (isTitle.Item1)
                {
                    if (String.Equals(item.Value, isTitle.Item2, StringComparison.CurrentCultureIgnoreCase))
                    {
                        item.Type = TypeEnum.Title;
                    }
                    else
                    {

                        var remainValue = item.Value.Replace(isTitle.Item2, "");
                        if (remainValue.Length <= 3)
                        {
                            item.Value = isTitle.Item2;
                        }
                        else
                        {
                            typeRepresentTempList.Add(new Tuple<int, TypeRepresent>(typeRepresentList.IndexOf(item), new TypeRepresent() { Type = TypeEnum.Title, Value = isTitle.Item2 }));
                            item.Value = remainValue;
                        }
                    }
                }
                else if (ValidationHelpers.EmailIsValid(item.Value))
                {
                    item.Type = TypeEnum.Email;
                }
                else if (RegExp.IsNumber(item.Value))
                {
                    item.Type = TypeEnum.Pnone;

                }
                else if (ValidationHelpers.UrlIsValid(item.Value.ToLower()))
                {
                    item.Type = TypeEnum.Url;
                }
                else if (ValidationHelpers.TypeIsValid(item.Value))
                {
                    var type = Utils.FindType(item.Value);
                    if (!string.IsNullOrEmpty(type))
                    {
                        item.Type = TypeEnum.Type;
                        item.Value = type;
                    }
                    else
                    {
                        var name = item.Value.Replace(":", "").Trim();
                        if(RegExp.IsName(name))
                        {
                            item.Type = TypeEnum.Contact;
                            item.Value = name;
                        }
                    }
                }
                else if (IsCompanyName(item.Value))
                {
                    item.Type = TypeEnum.Company;
                }

                else if (ValidationHelpers.ContactIsValid(item.Value))
                {
                    item.Type = TypeEnum.Contact;

                }
                else if (ValidationHelpers.IsValidStreetAddress(item.Value))
                {
                    item.Type = TypeEnum.Adress;
                }
                else if(RegExp.IsName(item.Value))
                {
                    item.Type = TypeEnum.Contact;
                }
                
            }

            foreach (var item in typeRepresentTempList)
            {
                typeRepresentList.Insert(item.Item1 + 1, item.Item2);
            }
          //  typeRepresentList.AddRange(typeRepresentTempList);
        }


        protected bool IsCompanyName(string val)
        {
            return Utils.IsCompanyName(val, this.urlFromText, null, null, null);


        }
    }
}
