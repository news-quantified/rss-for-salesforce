﻿namespace AMRssForSalesforce.HtmlParser.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Model;
    using HtmlAgilityPack;
    using Infrastructure.Helpers;
    using Infrastructure.Constants;

    public class PRWebHtmlParser : Abstract.HtmlParser
    {
        private const int maxLengContactInfo = 50;
        private string CompanyName = null;
        private string Url = null;
        private string curentType = null;
        private string[] EmailFromText;

        private HtmlNode divRight = null;
        public PRWebHtmlParser(string _url) : base(_url) { }

        public PRWebHtmlParser(string _fileName, bool fromFile) : base(_fileName, fromFile) { }

        public override Tuple<List<Contact>, HtmlDocument> ParserContact(RssFeed rssFeed)
        {
            try
            { 
            rssFeed.English = true;
            rssFeed.Redirect = responsUri;

            if (!FindeRightDiv())
                return null;

            companyNames = TryToGetCompanyName();

            if (companyNames.Any())
                CompanyName = companyNames.First();

            rssFeed.Dateline = GetDeteLine();

            rssFeed.DateTime = GetDateTime();
          

            if (!string.IsNullOrEmpty(CompanyName) && companyNames.All(cn => cn != CompanyName))
            {
                companyNames.Add(CompanyName);
            }

            Url = TryToGetUrl();
            if (string.IsNullOrEmpty(Url))
                Url = TryToFindeUrlItText();
            // Source = GetSource();

            var contactsRight = GetRightPartContact();

            
            var  contacts = new List<Contact>();

            contacts = TryToFindeFromText();
            foreach(var c in contacts)
            {
                Contact rContact = FindeSimularNameInRight(c, contactsRight);
                if (rContact != null)
                    MergContact(rContact, c);
            }   

            if (contacts == null || !contacts.Any())
                contacts = contactsRight;
            else if(contactsRight!= null && contactsRight.Any())
            {
                foreach (var cr in contactsRight)
                {
                    if (FindeSimularNameInRight(cr, contacts) == null)
                    {
                        contacts.Add(cr);
                    }
                }
            }

            

            if (!contacts.Any())
                contacts.Add(new Contact());

            if(string.IsNullOrEmpty(CompanyName))
            {
                var withCopany = contacts.Where(c => c.CompanyName != null);
                if(withCopany != null && withCopany.Any())
                {
                    CompanyName = withCopany.First().CompanyName;
                }
               
            }
            int i = 0;   
            foreach (var c in contacts)
            {
                // if (!string.IsNullOrEmpty(Source))
                c.Source = CompanyName;
                c.NewsID = this.urlHtml;

                if (string.IsNullOrEmpty(Url))
                    c.Url = GetRelatedLink(c);
                else
                    c.Url = Url;

                if (string.IsNullOrEmpty(c.CompanyName))
                {
                    if (companyNames.Any() && companyNames.Count == contacts.Count)
                        c.CompanyName = companyNames[i];
                    else
                        c.CompanyName = CompanyName;

                }

                if (string.IsNullOrEmpty(c.Name) && !string.IsNullOrEmpty(c.Email))
                {
                    c.Name = TryGetNameFromEmail(c.Email);
                }

                if (string.IsNullOrEmpty(c.Email))
                {
                    c.Email = TryFindeEmailByNameInAllEmail(c.Name, contacts);
                }

                if (c.Url == null)
                    c.Url = GetRelatedLink(c);

                if(string.IsNullOrEmpty( c.Url)  && ! string.IsNullOrEmpty(c.CompanyName))
                {
                    var urls = RegExp.getUrls(c.CompanyName);
                    if(urls.Any())
                    {
                        c.Url = string.Join(", ", urls);
                    }
                }
                i++;
            }
            rssFeed.ContactInfo = Utils.SummContactData(contacts);
            var doc2 = new HtmlDocument();
            doc2.LoadHtml(htmlContent);
            return new Tuple<List<Contact>, HtmlDocument>(contacts, doc2);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var contacts = new List<Contact>();
                contacts.Add(new Contact() { NewsID = this.urlHtml });
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                return new Tuple<List<Contact>, HtmlDocument>(contacts, doc);
            }
        }

        private DateTime GetDateTime()
        {

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var metaDate = doc.DocumentNode.SelectSingleNode("//meta[@itemprop='datePublished']");

            if(metaDate!= null && metaDate.Attributes["content"] != null)
            {
                var strMetaDate = metaDate.Attributes["content"].Value;

                DateTime dataTime = DateTime.Now;

                if (DateTime.TryParse(strMetaDate, out dataTime))
                {
                    return dataTime;
                }

               
            }

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='article-text']");
            if (div == null)
            {
                return DateTime.Now;
            }

            var ps = div.SelectNodes(".//p");

            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("(PRWEB"))
                    {
                        int PRWEB_Index = p.IndexOf(")");

                        var strDateTime = p.Remove(0, PRWEB_Index + 1).Replace("\r\n","").Trim();

                        var parts = strDateTime.Split(new string[] { " -- "}, StringSplitOptions.RemoveEmptyEntries);

                        strDateTime = parts[0];

                        DateTime dataTime = DateTime.Now;

                        if (!DateTime.TryParse(strDateTime, out dataTime))
                        {

                        }

                        return dataTime;
                    }
                }
            }

    

            return DateTime.Now;
        }

        private string TryFindeEmailByNameInAllEmail(string name, List<Contact> contacts)
        {
            if (EmailFromText != null && EmailFromText.Any())
            {
                foreach (var email in EmailFromText)
                {
                    if (Utils.trueEmail(email, name))
                        return email;
                }

                return EmailFromText.FirstOrDefault(e => e.ToLower().Contains("info"));
            }
            return null;
        }

        private void MergContact(Contact rContact, Contact c)
        {
            if (string.IsNullOrEmpty(c.CompanyName))
                c.CompanyName = rContact.CompanyName;


            if (string.IsNullOrEmpty(c.Email))
                c.Email = rContact.Email;

            if (string.IsNullOrEmpty(c.Phone))
                c.Phone = rContact.Phone;

            if (string.IsNullOrEmpty(c.RelateLinks))
                c.RelateLinks = rContact.RelateLinks;

            if (string.IsNullOrEmpty(c.Source))
                c.Source = rContact.Source;

            if (string.IsNullOrEmpty(c.Title))
                c.Title = rContact.Title;

            if (string.IsNullOrEmpty(c.Type))
                c.Type = rContact.Type;

            if (string.IsNullOrEmpty(c.Url))
                c.Url = rContact.Url;
        }

        private Contact FindeSimularNameInRight(Contact c, List<Contact> contactsRight)
        {
            if (string.IsNullOrEmpty(c.Name))
                return null;

            var c1 = c.Name.ToLower();
            foreach(var contact in contactsRight)
            {
                if (string.IsNullOrEmpty(contact.Name)) continue;
                var c2 = contact.Name.ToLower();
                if (c2.Contains(c1) || c1.Contains(c2))
                    return contact;
            }
            return null;
        }

        private string TryFindeEmailByName(string name, List<Contact> contacts)
        {
            throw new NotImplementedException();
        }

        private string GetDeteLine()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var metaDate = doc.DocumentNode.SelectSingleNode("//meta[@itemprop='contentLocation']");

            if (metaDate != null && metaDate.Attributes["content"] != null)
            {
                var strMetaDate = metaDate.Attributes["content"].Value;

                if (!string.IsNullOrEmpty(strMetaDate))
                    return strMetaDate;

            }

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='article-text']");
            if(div == null)
            {
                return null;
            }

            EmailFromText = RegExp.getEmails(div.InnerHtml.Replace("[at]", "@").Replace(" (at) ", "@").Replace("(at)", "@").Replace(" (dot) ", ".").Replace("(dot)", "."));

            var ps = div.SelectNodes(".//p");

            if (ps != null)
            {
                foreach (var pp in ps)
                {
                    var p = Utils.NormalizationString3(pp.InnerText);
                    if (p.Contains("(PRWEB"))
                    {
                        int PRWEB_Index = p.IndexOf(")");
                        return p.Remove(PRWEB_Index+1).Replace("\r\n                                ", " ").Replace("\r\n", " ").Trim();
                    }
                }
            }

            return ".";
        }
        private string TryGetNameFromEmail(string email)
        {
            var emailDomen = email.Split('@');
            if (emailDomen.Length < 2)
                return null;

            var names = emailDomen[0].Split('.');
            if (names.Length < 2)
                return null;

            if (names[0][0] >= 'A' && names[0][0] <= 'Z' && names[1][0] >= 'A' && names[1][0] <= 'Z')
            {
                var name = string.Format("{0} {1}", names[1], names[0]);
                if (RegExp.IsName(name))
                    return name;
            }
            return null;
        }

        private List<List<HtmlNode>> GetContactsNodesFromTable(HtmlNode table)
        {
            List<List<HtmlNode>> listNodes = new List<List<HtmlNode>>();

            var trs = table.SelectNodes("./tr");
            if (trs == null)
                return null;
            int countTD = 0;

            if (trs[0].SelectNodes("./td") == null)
                return null;
            foreach (var td in trs[0].SelectNodes("./td"))
            {
                if (td.Attributes["colspan"] != null)
                {
                    var colspan = 0;
                    if (int.TryParse(td.Attributes["colspan"].Value, out colspan))
                    {
                        countTD += colspan;
                    }
                    else
                        countTD++;
                }
                else
                    countTD++;
            }
            //trs[0].SelectNodes("./td").Count;

            if (countTD == 0)
                return null;

            for (int i = 0; i < countTD; i++)
            {
                listNodes.Add(new List<HtmlNode>());
            }

            foreach (var tr in trs)
            {

                var tds = tr.SelectNodes("./td");
                if (tds.Count > listNodes.Count)
                    //it isn't contact table
                    return null;
                for (int i = 0; i < tds.Count; i++)
                {
                    string contactString = Utils.NormalizationString(tds[i].InnerText);
                    if (!string.IsNullOrEmpty(contactString)
                        && contactString.ToLower() != "contact:"
                        && contactString.ToLower() != "contacts:"
                         && contactString.ToLower() != "contact"
                        && contactString.ToLower() != "telephone"
                        && contactString.ToLower() != "email"
                        && contactString.ToLower() != "website")
                        listNodes[i].Add(tds[i]);

                }
            }

            while (listNodes.Any(l => !l.Any()))
            {
                listNodes.Remove(listNodes.First(l => !l.Any()));
            }


            return listNodes;
        }

        private List<Contact> TryToFindeFromTable()
        {
            List<Contact> contacts = new List<Contact>();
            Contact contact = null;

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var pss = doc.DocumentNode.SelectNodes("//table[@class='gnw_table_border_collapse hugin']");
            if (pss != null)
                foreach (var ps in pss)
                {
                    if (ps.InnerText.Contains("$") || ps.InnerText.Contains(" billion ") || ps.InnerText.Contains(" million ") || ps.InnerText.Contains(" euro "))
                        continue;

                    var emailsAll = RegExp.getEmails(ps.InnerText);
                    var numbersAll = RegExp.GetPnoneNumbers(ps.InnerText);
                    if (!emailsAll.Any() && !numbersAll.Any() && !RegExp.IsNumber(ps.InnerText))
                        continue;


                    if (ps != null)
                    {
                        var table = ps;
                        if (table != null)
                        {
                            if (!emailsAll.Any() && table.InnerText.Length > 600 || table.InnerText.ToLower().Contains("when:"))
                                continue;

                            List<List<HtmlNode>> contactsNodes = GetContactsNodesFromTable(table);

                            if (contactsNodes == null || !contactsNodes.Any())
                                return contacts;

                            foreach (var col in contactsNodes)
                            {

                                for (int i = 0; i < col.Count; i++)
                                {
                                    var contacts1 = GetContacts2(col[i]);
                                    if (contacts1 == null)
                                        continue;
                                    foreach (var c in contacts1)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                            contacts.Add(c);
                                    }

                                }
                            }
                        }

                    }
                }
            return contacts;
        }

        private List<Contact> TryToFindeFromText()
        {
            List<Contact> contacts = new List<Contact>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p[@class='responsiveNews']");
            if (ps == null)
            {
                ps = doc.DocumentNode.SelectNodes("//p");
                var pre = doc.DocumentNode.SelectNodes("//pre");

                if (ps == null)
                    return null;

                if (pre != null)
                {
                    foreach (var p in pre)
                    {
                        ps.Add(p);
                    }
                }
            }
            var countContact = 0;
            HtmlNode p_contact_m = null;
            int countAfterWordContact = 0;

            if (ps == null)
                return null;
            foreach (var p in ps)
            {
                var hasEmailsP = RegExp.getEmails(p.InnerText).Any();
                if (p.ChildNodes != null && p.ChildNodes.Any())
                {
                    if (p.ChildNodes.Max(ph => Utils.NormalizationString(ph.InnerText).Length) > 80 && !hasEmailsP)
                        continue;
                }
                else
                {
                    if (p.InnerText.Length > 80 && !hasEmailsP)
                        continue;
                }

                if (p.InnerText.Length < 8)
                    continue;
                if (p.InnerText.Contains("Logo - "))
                    continue;

                var notContactNode = false;
                if (p.ChildNodes != null)
                {
                    foreach (var node in p.ChildNodes)
                    {
                        if (p.Name == "#text" && p.InnerText.Length > maxLengContactInfo)
                        {
                            notContactNode = true;
                            break;
                        }

                    }
                    if (notContactNode)
                        continue;
                }

                if (p_contact_m != null)
                {
                    if (NodeContentEmailOrPhone(p))
                    {
                        var contacts1 = GetContacts2(p);
                        if (contacts1 != null)
                        {
                            foreach (var c in contacts1)
                            {
                                if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                    && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                    contacts.Add(c);
                            }
                            countAfterWordContact = 0;
                            continue;
                        }
                    }
                    countAfterWordContact++;

                }
                var type = Utils.FindType(p.InnerText);

                if (!string.IsNullOrEmpty(type))
                {
                    p_contact_m = p;
                    countAfterWordContact = 0;
                    this.curentType = type;
                }
                else
                {
                    foreach (var mc in Dictionaries.MarkerContact)
                    {
                        if (p.InnerText.ToLower().Contains(mc))
                        {
                            p_contact_m = p;
                            countAfterWordContact = 0;
                            if (NodeContentEmailOrPhone(p))
                            {
                                var contacts1 = GetContacts2(p);
                                if (contacts1 != null && contacts1.Any())
                                {
                                    foreach (var c in contacts1)
                                    {
                                        if (!string.IsNullOrEmpty(c.Name) || !string.IsNullOrEmpty(c.Phone) || !string.IsNullOrEmpty(c.Email)
                                            && !contacts.Any(cont => cont.Name == c.Name && cont.Phone == c.Phone && cont.Email == c.Email))
                                            contacts.Add(c);
                                    }
                                }
                                countAfterWordContact = 0;
                                break;
                            }
                        }
                    }
                }
            }

            return contacts;
        }

        private List<Contact> GetContacts2(HtmlNode p)
        {
            if (p.Name == "td")
            {
                var strong = p.SelectSingleNode(".//strong/strong");
                if (strong != null && strong.ChildNodes != null)
                    return TryParcePCntacts(strong.ChildNodes);
                else
                {
                    return TryParcePCntacts(p.ChildNodes);
                }
            }
            else
            {
                return TryParcePCntacts(p.ChildNodes);
            }
            return new List<Contact>();
        }


        private List<Contact> TryParcePCntacts(HtmlNodeCollection pppps)
        {
            List<string> names = new List<string>();
            List<string> urls = new List<string>();
            List<string> emails = new List<string>();
            List<string> titles = new List<string>();
            List<string> companies = new List<string>();
            List<string> phones = new List<string>();
            List<string> types = new List<string>();
           

            foreach (var p in pppps)
            {
                if (p.Name == "a")
                {
                    var _urls2 = RegExp.getUrls(p.InnerText);
                    if (_urls2.Any())
                    {
                        urls.Add(_urls2[0]);
                        continue;
                    }
                }
                if (ValidationHelpers.IsValidStreetAddress(p.InnerText))
                {
                   
                    continue;
                }
                if (Utils.NormalizationString(p.InnerText).Length < 3)
                    continue;

                var _names = Utils.NormalizationString(p.InnerText.Replace(", Inc.", "Iiiiiinc").Replace(", LLC", "Lllllllllllllllllc")).Split(nameSplit, StringSplitOptions.RemoveEmptyEntries);

                foreach (var n in _names)
                {

                    var _emails = RegExp.getEmails(n);
                    if (_emails.Any())
                    {
                        foreach (var e in _emails)
                            emails.Add(e);
                        continue;
                    }

                    var _urls = RegExp.getUrls(n);
                    if (_urls.Any())
                    {
                        foreach (var e in _urls)
                            urls.Add(e);
                        continue;
                    }

                    // var _phones = RegExp.GetPnoneNumbers(p.InnerText);
                    if (RegExp.IsNumber(n))
                    {
                        phones.Add(Utils.NormPhone(n));
                        continue;
                    }


                    var word = Utils.NormalizationString(n.Replace("Iiiiiinc", ", Inc.").Replace("Lllllllllllllllllc", ", LLC"));
                    if (Utils.HasTitle(word))
                    {
                        var title = Utils.GetTitle(word);
                        if (title != null)
                        {
                            titles.Add(title);
                            word = Utils.NormalizationString(word.Replace(title, " "));
                            if (word.Length < 6)
                                continue;
                        }
                    }

                    var _type = Utils.FindType(word);
                    if (!string.IsNullOrEmpty(_type))
                    {
                        types.Add(_type);
                        this.curentType = _type;
                        word = word.Replace(_type, "").Trim();
                        if (word.Length < 5)
                            continue;
                    }

                    if (IsCompanyName(word))
                    {
                        companies.Add(Utils.NormalizationString(word));
                        continue;
                    }

                    if (RegExp.IsName(word))
                        names.Add(Utils.NormalizationString(word));
                }
            }

            List<Contact> contacts = new List<Contact>();
            if (names.Any())
            {
                int i = 0;
                foreach (var n in names)
                {
                    contacts.Add(new Contact() { Name = n, Type = curentType });

                    if (emails.Any())
                    {
                        var emailTrue = emails.FirstOrDefault(e => Utils.trueEmail(e, n));
                        if (emailTrue != null)
                        {
                            contacts.Last().Email = emailTrue;
                            emails.Remove(emailTrue);
                        }
                    }

                    if (phones.Any())
                    {
                        if (phones.Count == names.Count)
                            contacts.Last().Phone = phones[i];
                        else
                            contacts.Last().Phone = string.Join(", ", phones.ToArray());
                    }

                    if (titles.Any())
                    {
                        if (titles.Count == names.Count)
                            contacts.Last().Title = titles[i];
                        else if (titles.Count == 2 && names.Count == 1)
                        {
                            contacts.Last().Title = string.Join(", ", titles);
                        }
                        else
                        {

                        }
                    }

                    if (types.Any())
                    {
                        if (types.Count == names.Count)
                            contacts.Last().Type = types[i];
                        else
                            contacts.Last().Type = types[0];
                    }
                    else
                    {
                        contacts.Last().Type = this.curentType;
                    }
                    if (companies.Any())
                    {
                        if (companies.Count == names.Count)
                            contacts.Last().CompanyName = companies[i];
                        else
                            contacts.Last().CompanyName = companies[0];
                    }

                    if (urls.Any())
                    {
                        if (urls.Count == names.Count)
                            contacts.Last().Url = urls[i];
                        else
                            contacts.Last().Url = urls[0];
                    }
                   
                    i++;
                }

                if (emails.Any() && contacts.Any(c => string.IsNullOrEmpty(c.Email)))
                {
                    foreach (var cont in contacts.Where(c => string.IsNullOrEmpty(c.Email)))
                    {
                        cont.Email = string.Join(", ", emails.ToArray());
                    }
                }


                return contacts;
            }
            else
            {
                if (emails.Any())
                {
                    int i = 0;
                    foreach (var e in emails)
                    {
                        contacts.Add(new Contact() { Email = e, Type = curentType });
                        if (phones.Any())
                        {
                            if (phones.Count == emails.Count)
                                contacts.Last().Phone = phones[i];
                            else
                                contacts.Last().Phone = string.Join(", ", phones.ToArray());
                        }

                        if (titles.Any())
                        {
                            if (titles.Count == emails.Count)
                                contacts.Last().Title = titles[i];

                        }

                        if (types.Any())
                        {
                            if (types.Count == emails.Count)
                                contacts.Last().Type = types[i];
                            else
                                contacts.Last().Type = types[0];
                        }

                        if (companies.Any())
                        {
                            if (companies.Count == emails.Count)
                                contacts.Last().CompanyName = companies[i];
                            else
                                contacts.Last().CompanyName = companies[0];
                        }

                        if (urls.Any())
                        {
                            if (urls.Count == names.Count)
                                contacts.Last().Url = urls[i];
                            else
                                contacts.Last().Url = urls[0];
                        }

                     
                        i++;
                    }

                    return contacts;
                }

                if (phones.Any())
                {
                    int i = 0;
                    foreach (var p in phones)
                    {
                        contacts.Add(new Contact() { Phone = p, Type = curentType });

                        if (titles.Any())
                        {
                            if (titles.Count == phones.Count)
                                contacts.Last().Title = titles[i];

                        }

                        if (types.Any())
                        {
                            if (types.Count == phones.Count)
                                contacts.Last().Type = types[i];
                            else
                                contacts.Last().Type = types[0];
                        }

                        if (companies.Any())
                        {
                            if (companies.Count == phones.Count)
                                contacts.Last().CompanyName = companies[i];
                            else
                                contacts.Last().CompanyName = companies[0];
                        }
                        if (urls.Any())
                        {
                            if (urls.Count == names.Count)
                                contacts.Last().Url = urls[i];
                            else
                                contacts.Last().Url = urls[0];
                        }


                        i++;
                    }

                    return contacts;
                }
            }
            return null;
        }


        private string TryToGetUrl()
        {
            var imgs = this.divRight.SelectNodes(".//img[@class='valign-bott margintop-5px']");
            if (imgs == null)
                return null;
            foreach (var img in imgs)
            {
                if (img.NextSibling == null)
                {
                    continue;
                }

                if (img.NextSibling.Name != "a")
                {
                    if (img.NextSibling.NextSibling.Name != "a")
                        continue;
                    return img.NextSibling.NextSibling.InnerText;
                }

                return img.NextSibling.InnerText;
            }
            return null;
        }

        private bool FindeRightDiv()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var divR2s = doc.DocumentNode.SelectNodes("//div[@class='article-col-right table-col-desk box-el-list box-half-lap clearfix']");

            if (divR2s == null)
                return false;

            if (divR2s.Count > 1)
            {

            }

            this.divRight = divR2s[0];
            return true;
        }

        private string TryToFindeUrlItText()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var div = doc.DocumentNode.SelectSingleNode("//div[@class='article-text']");
            if (div == null)
            {
                return null;
            }

           

           var urls = RegExp.getUrls(div.InnerText);

           if (urls.Any())
                        return string.Join(", ", urls);
                

            return null;
        }

        private bool IsEngish()
        {
            //if (htmlContent.Contains("lang=\"en\""))
            //{
            return !NoEnglish();
            // }
            //   return false;
        }




        public override string GetRelatedLink()
        {
            StringBuilder sb = new StringBuilder();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//a");

            if (ps != null)
            {
                foreach (var p in ps)
                {
                    if (p.Attributes["title"] != null && p.Attributes["title"].Value.Contains("Link to") && p.Attributes["href"] != null)
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(p.Attributes["href"].Value);
                    }
                }
            }
            return sb.ToString();
        }

        public override string GetSource()
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var ps = doc.DocumentNode.SelectNodes("//p");

            if (ps != null)
            {
                foreach (var p in ps)
                {

                    var innerText = Utils.NormalizationString(p.InnerText);
                    int SourseIndex = innerText.Trim().IndexOf("Source:");
                    if (SourseIndex == 0)
                    {
                        return innerText.Replace("Source:", "").Trim();
                    }

                }
            }
            return "";
        }

        private static string[] _separatePre = new string[] { "\n\n" };

        private List<Contact> GetRightPartContact()
        {
            List<Contact> contacts = new List<Contact>();
            Contact curentConact = null;


            var contactNode = GetNodeByfacet_header("Contact Author");

            if (contactNode == null)
                return null;

            if (contactNode.ChildNodes == null || !contactNode.ChildNodes.Any())
                return null;

            var i = 0;
            foreach (var ch in contactNode.ChildNodes)
            {
                var text = Utils.NormalizationString(RegExp.GetInnerText(ch.InnerHtml));

                if (ch.Attributes["class"] != null && ch.Attributes["class"].Value == "box-contact-name")
                {
                    curentConact = new Contact();
                    contacts.Add(curentConact);

                    var type = Utils.FindType(text);
                    if (type != null)
                    {
                        curentConact.Type = type;
                        var catText = text.Replace(type, "").Replace("Release", "").Trim();
                        if (!string.IsNullOrEmpty(catText))
                        {
                            text = catText;
                        }
                    }

                    var nameTitle = text.Split(',');
                    if (nameTitle.Count() > 1)
                    {
                        var title = Utils.GetTitle(string.Join(", ", nameTitle.Skip(1)));
                        if (title != null)
                            curentConact.Title = title;
                    }
                    if (RegExp.IsName(nameTitle[0].Trim()))
                    {
                        curentConact.Name = nameTitle[0].Trim();
                    }
                    else if (IsCompanyName(text))
                    {
                        curentConact.CompanyName = text;
                    }
                }
                else if (ch.Attributes["class"] != null && ch.Attributes["class"].Value == "box-contact-el")
                {
                    if (curentConact == null)
                    {
                        curentConact = new Contact();
                        contacts.Add(curentConact);
                        
                    }
                    if (ch.ChildNodes == null || !ch.ChildNodes.Any())
                        continue;

                    int countElement = 0;
                    foreach (var cch in ch.ChildNodes)
                    {
                        var textCCh = cch.InnerText;
                        if (textCCh == "Xulon Press")
                        {
                            curentConact.CompanyName = textCCh;
                            continue;
                        }
                        if (string.IsNullOrEmpty(textCCh))
                            continue;
                        var type = Utils.FindType(textCCh);
                        if (type != null)
                        {
                            curentConact.Type = type;
                            textCCh = textCCh.Replace(type, "").Trim();
                            if (IsCompanyName(textCCh))
                            {
                                curentConact.CompanyName = textCCh;
                                continue;
                            }
                            else if (curentConact.Name != null)
                            {

                            }
                        }
                        if (countElement == 0 && IsCompanyName(textCCh))
                        {
                            curentConact.CompanyName = Utils.NormalizationString(textCCh);
                        }
                        else
                        {
                            var phones = RegExp.GetPnoneNumbers(cch.InnerText);

                            if (phones.Any())
                            {
                                curentConact.Phone = String.Join(", ", phones);
                                continue;
                            }

                            var emails = RegExp.getEmails(cch.InnerText);

                            if (emails.Any())
                            {
                                curentConact.Email = string.Join(", ", emails);
                                continue;
                            }

                            if (Utils.NormalizationString(cch.InnerText).Contains("Email >") || Utils.NormalizationString(cch.InnerText).Contains("Follow us"))
                                break;

                            if (countElement == 0 )
                            {
                                curentConact.CompanyName = Utils.NormalizationString(cch.InnerText);
                            }
                        }
                        countElement++;
                    }
                }
                else if (ch.Attributes["class"] != null && ch.Attributes["class"].Value == "box-contact-social box-contact-el")
                {
                    break;
                }
                i++;
            }
            return contacts;
        }


        public override List<HtmlNode> GetContactContent()
        {
            List<HtmlNode> contacts = new List<HtmlNode>();
            var contact = GetNodeByfacet_header("Contact Author");

            if (contact != null)
            {
                var contactNode = contact;
                while (contactNode.Name != "pre")
                {
                    contactNode = contactNode.NextSibling;
                    if (contactNode == null)
                        return null;
                }
                if (contact != null)
                {
                    contacts.Add(contactNode);

                    if (RegExp.getEmails(contactNode.InnerHtml).Count() > 0)
                    {
                    }

                    var doc = new HtmlDocument();
                    doc.LoadHtml(htmlContent);

                    var ps = doc.DocumentNode.SelectNodes("//p");

                    var hasExternContacts = false;
                    foreach (var p in ps)
                    {
                        if (p.InnerText.Contains("Contact:"))
                            hasExternContacts = true;

                        if (hasContent)
                        {
                            if (RegExp.getEmails(Utils.NormalizationString(p.InnerHtml)).Count() > 0)
                                contacts.Add(p);
                        }
                    }
                    return contacts;
                }
            }
            else
            {

  

            }
            return null;
        }

        string[] nameSplit = new string[] { ",", " or ", "/", " | ", ":", "\t", "\n", "Tel:", " with ", " call " };

        public override List<Contact> GetContacts(HtmlNode nodeContact)
        {
            string[] _separate = new string[] { "\n\t", "\n", "/", " | ", " - ", " – ", ";", "&#8211" };

            List<Contact> contacts = new List<Contact>();

            List<Contact> contactNames = new List<Contact>();

            Contact currentContact = new Contact();

            currentContact.Type = this.curentType;

            contacts.Add(currentContact);

            string generalType = null;
            string curenTitle = null;
            string curentCompanyName = null;

            ContactInfoType lastType = ContactInfoType.Empty;

            var parts = nodeContact.InnerText.Split(_separate, StringSplitOptions.None);
            int indexP = -1;
            bool ampNext = false;
            foreach (var p in parts)
            {
                indexP++;

                if (ampNext)
                {
                    ampNext = false;
                    continue;
                }
                lastType = ContactInfoType.Empty;

                var text = Utils.NormalizationString(p.Replace("Contact: ", ""));

                if (p.IndexOf("&amp") == p.Length - "&amp".Length && indexP < parts.Length - 1)
                {
                    ampNext = true;
                    text = Utils.NormalizationString((p + parts[indexP + 1]).Replace("Contact: ", ""));
                }

                if (string.IsNullOrEmpty(text))
                {
                    if ((!string.IsNullOrEmpty(currentContact.Name) || !string.IsNullOrEmpty(currentContact.Type))
                        && (!string.IsNullOrEmpty(currentContact.Email) || !string.IsNullOrEmpty(currentContact.Phone)))
                    {
                        currentContact = new Contact();
                        contacts.Add(currentContact);
                        currentContact.Type = generalType;
                        contactNames.Clear();
                        curenTitle = null;
                        curentType = null;
                        curentCompanyName = null;
                    }
                    else if (!string.IsNullOrEmpty(currentContact.Type))
                    {
                        generalType = currentContact.Type;
                    }
                    continue;
                }

                if (text == "or")
                {
                    currentContact = new Contact();
                    contacts.Add(currentContact);
                    currentContact.Type = generalType;
                    contactNames.Clear();

                    continue;
                }

                if (text.Length < 3)
                    continue;



                var names = text.Replace(", Inc.", "Iiiiiinc").Replace(", LLC", "Lllllllllllllllllc").Split(nameSplit, StringSplitOptions.RemoveEmptyEntries);
                var index = -1;
                foreach (var n in names)
                {
                    index++;
                    var word = n.Replace("Iiiiiinc", ", Inc.").Replace("Lllllllllllllllllc", ", LLC").Trim();


                    var urls = RegExp.getUrls(word);
                    if (urls.Any())
                    {
                        currentContact.Url = string.Join(", ", urls);
                        lastType = ContactInfoType.Url;
                        continue;
                    }

                    var emails = RegExp.getEmails(word);
                    if (emails.Any())
                    {
                        lastType = ContactInfoType.Email;
                        if (!contactNames.Any())
                            currentContact.Email = string.Join(", ", emails);
                        else
                        {
                            List<string> emailNobady = new List<string>();
                            foreach (var e in emails)
                            {
                                bool found = false;
                                foreach (var nnn in contactNames)
                                {
                                    if (Utils.trueEmail(e, nnn.Name))
                                    {
                                        nnn.Email = e;
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                    emailNobady.Add(e);

                            }

                            if (emailNobady.Any())
                            {
                                foreach (var nnn in contactNames)
                                {
                                    if (string.IsNullOrEmpty(nnn.Email))
                                    {
                                        nnn.Email = string.Join(", ", emailNobady.ToArray());
                                    }
                                }
                            }
                        }
                        continue;
                    }

                    var numbers = RegExp.GetPnoneNumbers(word);

                    if (numbers.Any() || RegExp.IsNumber(word))
                    {
                        var startNumber = word.IndexOf(numbers[0]);
                        string last = null;
                        if (startNumber > 0)
                        {
                            last = Utils.NormalizationString(word.Remove(startNumber));
                            if (RegExp.IsName(last))
                            {
                                if (currentContact.Name != null)
                                {
                                    currentContact = new Contact();
                                    contacts.Add(currentContact);
                                }
                                currentContact.Phone = Utils.NormPhone(word);
                                currentContact.Name = last;
                                continue;
                            }
                        }


                        var number = Utils.NormPhone(word);
                        if (contactNames.Any())
                        {
                            if (numbers.Count() == contactNames.Count())
                            {

                                for (var i = 0; i < contactNames.Count(); i++)
                                {
                                    if (contactNames[i].Phone == null || contactNames[i].Phone.Contains(numbers[i]))
                                    {
                                        contactNames[i].Phone = numbers[i];
                                    }
                                }
                            }
                            else
                            {
                                foreach (var nnn in contactNames)
                                {
                                    if (nnn.Phone == null || !nnn.Phone.Contains(number))
                                    {
                                        if (string.IsNullOrEmpty(nnn.Phone))
                                            nnn.Phone = number;
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (currentContact.Phone == null || !currentContact.Phone.Contains(number))
                            {
                                lastType = ContactInfoType.Phone;
                                if (!string.IsNullOrEmpty(currentContact.Phone))
                                    currentContact.Phone += ", ";
                                currentContact.Phone += number;
                            }
                        }


                        if (!string.IsNullOrEmpty(last) && last.Length > 3)
                        {
                            word = last;
                        }
                        else
                        {
                            continue;
                        }

                    }

                    if (lastType == ContactInfoType.Title && index - 1 >= 0)
                    {
                        var complextitle = names[index - 1] + ", " + word;

                        var Complextitle1 = Utils.GetTitle(complextitle);
                        if (complextitle == Complextitle1)
                        {
                            curenTitle = Complextitle1;
                            if ((currentContact.Title != null && Complextitle1.ToLower().Contains(currentContact.Title.ToLower())))
                                currentContact.Title = Complextitle1;
                            curenTitle = Complextitle1;
                            lastType = ContactInfoType.Title;
                            continue;
                        }
                    }
                    var title = Utils.GetTitle(word);
                    if (title != null)
                    {


                        curenTitle = title;
                        word = Utils.NormalizationString(word.Replace(title, ""));
                        if (RegExp.IsName(word) && currentContact.Name != null)
                        {
                            lastType = ContactInfoType.Name;
                            contactNames.Add(currentContact);
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            contactNames.Add(currentContact);
                            currentContact.Type = contacts.Last().Type;
                            currentContact.Name = Utils.NormName(word);
                            if (currentContact.Title == null)
                                currentContact.Title = title;
                            curenTitle = title;
                            lastType = ContactInfoType.Title;
                            continue;
                        }
                        curenTitle = title;
                        if (string.IsNullOrEmpty(currentContact.Title))
                            currentContact.Title = title;
                        else
                        {
                            if (!currentContact.Title.Contains(title) && lastType == ContactInfoType.Title)
                                currentContact.Title += ", " + title;
                        }

                        if (string.IsNullOrEmpty(word))
                        {
                            lastType = ContactInfoType.Title;
                            continue;
                        }
                        else
                        {

                        }
                        lastType = ContactInfoType.Title;
                    }

                    var type = Utils.FindType(word);
                    if (!string.IsNullOrEmpty(type))
                    {
                        lastType = ContactInfoType.Type;
                        curentType = type;
                        if (string.IsNullOrEmpty(currentContact.Type))
                            currentContact.Type = type;

                        var catType = Utils.NormalizationString(Utils.CutType(word, type).Replace(type, "").Trim());
                        if (!string.IsNullOrEmpty(catType) && IsCompanyName(catType))
                            currentContact.CompanyName = catType;

                        if (string.IsNullOrEmpty(catType))
                            continue;
                        else
                        {
                            word = catType;
                            if (word.Contains(":"))
                            {
                                var tt = word.Split(':');
                                if (tt.Count() > 1)
                                    word = tt[1].Trim();
                            }

                        }
                    }

                    if (IsCompanyName(word))
                    {
                        curentCompanyName = word;
                        lastType = ContactInfoType.CopanyName;
                        if (string.IsNullOrEmpty(currentContact.CompanyName))
                        {
                            currentContact.CompanyName = word;

                            if (contactNames.Any())
                            {
                                foreach (var nC in contactNames)
                                {
                                    nC.CompanyName = word;
                                }
                            }
                        }
                        continue;
                    }

                    if (RegExp.IsName(word))
                    {
                        lastType = ContactInfoType.Name;
                        if (string.IsNullOrEmpty(currentContact.Name))
                        {
                            currentContact.Name = Utils.NormName(word);

                        }
                        else
                        {
                            contactNames.Add(currentContact);
                            currentContact = new Contact();
                            contacts.Add(currentContact);
                            contactNames.Add(currentContact);
                            currentContact.Type = curentType;
                            currentContact.Title = curenTitle;
                            currentContact.CompanyName = curentCompanyName;
                            currentContact.Name = Utils.NormName(word);
                        }
                    }
                }

            }

            return contacts;
        }


        private HtmlNode GetNodeByfacet_header(string facet_header)
        {
            var divs = this.divRight.SelectNodes(".//div[@class='box box-contact']");
            foreach (var div in divs)
            {
                var h3 = div.SelectSingleNode(".//h3");
                if (Utils.NormalizationString(h3.InnerText).ToLower() == facet_header.ToLower())
                {
                    
                    return div.SelectSingleNode(".//div[@class='box-cont']");
                }
            }
            return null;
        }

        private List<string> TryToGetCompanyName()
        {
            List<string> companyNames = new List<string>();
            var companyNameNode = GetNodeByfacet_header("Profile");

            if (companyNameNode == null)
            {
                companyNameNode = GetNodeByfacet_header("Contact Author");
                if (companyNameNode != null)
                {
                    var aas = companyNameNode.SelectNodes(".//a");
                    foreach (var a in aas)
                    {
                        if (!string.IsNullOrEmpty(Utils.NormalizationString(a.InnerText))
                            && !a.InnerText.Contains("Like")
                            && !a.InnerText.Contains("Follow")
                            && !a.InnerText.Contains("Email")
                            && !RegExp.GetPnoneNumbers(a.InnerText).Any() && !RegExp.getEmails(a.InnerText).Any() && !string.IsNullOrEmpty(a.InnerText)
                            )
                        {
                            companyNames.Add(a.InnerText);
                        }
                    }

                    if (companyNames.Any())
                        return companyNames;
                }

                //responsiveNews
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);
                var responsiveNews = doc.DocumentNode.SelectSingleNode("//p[@class='responsiveNews']");
                if (responsiveNews != null)
                {
                    var a = responsiveNews.SelectSingleNode(".//a");
                    if (a != null)
                    {
                        var companyName = Utils.NormalizationString(a.InnerText);

                        if (IsCompanyName(companyName))
                        {
                            companyNames.Add(companyName);
                        }
                        else
                        {

                        }
                    }
                }
            }
            else
            {

                companyNames.Add(Utils.NormalizationString(companyNameNode.InnerText));
            }

            if (!companyNames.Any())
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(htmlContent);

                var responsiveNews = doc.DocumentNode.SelectNodes("//p[@class='responsiveNews']");
                if (responsiveNews != null)
                {
                    foreach(var p in responsiveNews)
                    {
                        if(p.ChildNodes != null && p.ChildNodes.Count>0)
                        {
                            var text = Utils.NormalizationString(p.ChildNodes[0].InnerText);
                            if(!string.IsNullOrEmpty(text))
                            {
                                if (text.Contains("About ") || text.Contains("ABOUT "))
                                {
                                    companyNames.Add(text.Replace("About ", "").Replace("ABOUT ","").Trim());
                                }
                            }
                        }
                    }
                }
            }
            return companyNames;
        }


    }
}
