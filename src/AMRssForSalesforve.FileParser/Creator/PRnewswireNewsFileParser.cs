﻿using AMRssForSalesforce.HtmlParser.Creator;
using AMRssForSalesforce.Infrastructure.Repository;
using AMRssForSalesforce.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AMRssForSalesforve.FileParser.Creator
{
    public sealed class PRnewswireNewsFileParser : AMRssForSalesforve.FileParser.Abstract.FileParser
    {
        public PRnewswireNewsFileParser()
        {
            FileNameFiltre = "PR_NEWS_USPR";
            FeedName = "PR Newswire";
        }

        public override bool TrySaveFileList(IEnumerable<string> list)
        {
            try
            {
                var contactRep = new ContactInfoRepository();
                var rssRep = new RssFeedRepository();
                foreach (var fileName in list)
                {
                    var parcerContent = new ParcerPRNewswireHtmlParser(fileName, true);

                    RssFeed rssFeed = rssRep.CreateRssByFileName(System.IO.Path.GetFileNameWithoutExtension(fileName));
                    var contentData = parcerContent.ParserContact(rssFeed);
                    List<Contact> contacts = contentData.Item1;
                    var content = contentData.Item2;
                    if (contacts != null && contacts.Any())
                    {
                        
                        if (rssFeed != null)
                        {
                            rssRep.Add(rssFeed);
                            contactRep.Delete(System.IO.Path.GetFileName(fileName));
                            contactRep.Delete(rssFeed.Link);
                            foreach (Contact c in contacts)
                            {
                                c.NewsID = rssFeed.Link;
                                c.NewsReleaseDate = GetNewsReleaseDateFromFileName(System.IO.Path.GetFileNameWithoutExtension(fileName));
                                contactRep.Add(c);
                            }
                        }
                    }

                    System.IO.File.Move(fileName, System.IO.Path.Combine(_pathDelete, System.IO.Path.GetFileName(fileName)));

                }
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        }
             
    }
}
