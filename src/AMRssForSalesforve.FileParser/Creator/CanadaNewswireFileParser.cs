﻿using AMRssForSalesforce.HtmlParser.Creator;
using AMRssForSalesforce.Infrastructure.Repository;
using AMRssForSalesforce.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRssForSalesforve.FileParser.Creator
{
    
    public sealed class CanadaNewswireFileParser : AMRssForSalesforve.FileParser.Abstract.FileParser
    {
        public CanadaNewswireFileParser()
        {
            FileNameFiltre = "CANADANWCANADAPR";
            FeedName = @"Canada Newswire";
        }

        public override bool TrySaveFileList(IEnumerable<string> list)
        {
            try
            {
                var contactRep = new ContactInfoRepository();
                var rssRep = new RssFeedRepository();
                //var parcerContent = new ParcerPRNewswireHtmlParser(
                //    @"C:\newsquantified\All_news\201501010100PR_NEWS_USPR_____DE99368.XML", true);
                //List<Contact> contacts = parcerContent.ParserContact();
                //foreach (Contact c in contacts)
                //{
                //    contactRep.Add(c);
                //}


                foreach (var fileName in list)
                {
                    RssFeed rssFeed = rssRep.CreateRssByFileName(System.IO.Path.GetFileNameWithoutExtension(fileName));
                    if (rssFeed != null)
                    {
                        var marketwire = new CanadaNewswireHtmlParser(rssFeed);

                        //marketwire.FileName = fileName;
                        //if (marketwire.StartParse())
                        //    rssRep.Add(rssFeed);
                    }

                    System.IO.File.Move(fileName, System.IO.Path.Combine(_pathDelete, System.IO.Path.GetFileName(fileName)));
                    
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

    }
}
