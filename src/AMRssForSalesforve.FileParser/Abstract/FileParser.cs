﻿using AMRssForSalesforce.Infrastructure.Logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace AMRssForSalesforve.FileParser.Abstract
{
    public abstract class FileParser
    {
        public virtual string FileNameFiltre { get; protected set; }
        public virtual string FeedName { get; protected set; }

        protected string _pathDelete { get; set; }
        
        public virtual IEnumerable<string> FindeFiles(string path)
        {
            if (!Directory.Exists(path))
            {
                Logger.WriteError("Error: Path source is not exist!");
                return null;
            }

            var files = Directory.EnumerateFiles(path, string.Format("*{0}*.xml", FileNameFiltre));
            return files;
        }

        public abstract bool TrySaveFileList(IEnumerable<string> list);

        public virtual bool Start(string path, string pathDelete)
        {
            _pathDelete = pathDelete;
            if(!System.IO.Directory.Exists(_pathDelete))
            {
                System.IO.Directory.CreateDirectory(_pathDelete);
            }
            try
            {
                var fileList = FindeFiles(path).ToList();
                if (!TrySaveFileList(fileList))
                    return false;

            }
            catch (Exception exception)
            {
                return false;
            }
            finally
            {

            }
            return true;
        }

        public string GetNewsReleaseDateFromFileName(string p)
        {
            //2015-12-09 15:00:00.000
            int year = 0;
            int MM = 0;
            int dd = 0;
            int hh = 0;
            int mm = 0;

            int.TryParse(p.Substring(0, 4), out year);
            int.TryParse(p.Substring(4, 2), out MM);
            int.TryParse(p.Substring(6, 2), out dd);
            int.TryParse(p.Substring(8, 2), out hh);
            int.TryParse(p.Substring(10, 2), out mm);

            var datetime = new DateTime(year, MM,dd,hh,mm,00).ToString();

            return datetime;
        }
    }
}
