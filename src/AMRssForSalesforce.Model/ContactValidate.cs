﻿using System;
using System.Text.RegularExpressions;

namespace AMRssForSalesforce.Model
{
    public class ContactValidate : Contact
    {
        private string _phone;
        private string _company;
        private string _type;
        private string _title;
        public override string Phone
        {
            get { return _phone; }
            set
            {
                if (!String.IsNullOrEmpty(value)) _phone = Regex.Replace(value, "[a-zA-Z:@&]", "").Trim();
                if (!String.IsNullOrEmpty(_phone) && _phone.Length <= 5) _phone = String.Empty;
            }
        }


        public override string Title
        {
            get { return _title; }
            set
            {
                if (String.IsNullOrEmpty(value)) return;
                value = value.TrimStart('.', '-', '/', ' ');
                value = value.TrimEnd('.', '-', '/', ' ');
                value = value.Replace("()", " ");
                value = value.Replace("(..  )", " ");
                _title = value.Replace(":", "");
            }

        }

        public override string Type
        {
            get { return _type; }
            set { if (!String.IsNullOrEmpty(value)) _type = value.Replace(":", ""); }
        }
        public override string CompanyName
        {
            get { return _company; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    _company =
                        Regex.Replace(value, "[0-9]", "")
                            .Replace(".com", "")
                            .Replace("(", "")
                            .TrimStart()
                            .Replace(")", "");

            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        
    }
}
