﻿namespace AMRssForSalesforce.Model
{
    public class Contact
    {
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string Url { get; set; }
        public virtual string NewsID { get; set; }
        public virtual string NewsReleaseDate { get; set; }
        public virtual string Source { get; set; }
        public virtual string Type { get; set; }
        public virtual string UncnounPart { get; set; }
        public virtual string Error { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string Title { get; set; }
        public virtual string LastUpdated { get; set; }
        public virtual string RelateLinks { get; set; }
  
    }
}
