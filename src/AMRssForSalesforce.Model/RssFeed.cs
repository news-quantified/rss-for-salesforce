﻿using System;

namespace AMRssForSalesforce.Model
{
    public class RssFeed
    {
        public string Source { get; set; }
        public string Id { get; set; }
        public string Provider { get; set; }
        public string Link { get; set; }
        public string HeadLine { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public DateTime DateTime { get; set; }
        public int? StoryCount { get; set; }
        public string Dateline { get; set; }
        public string ContactInfo { get; set; }
        public bool English { get; set; }
        public string Language { get; set; }

        public string Redirect { get; set; }
        public string CompanyTitle { get; set; }

        public int? MultiMediaCount { get; set; }

        public int? StaticMediaCount { get; set; }
    }
}
