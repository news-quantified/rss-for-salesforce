﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Model
{
    public class RssProvidersAndContactCount
    {
        public string Provider { get; set; }
        public int CountRecords { get; set; }
        public int PhoneCount { get; set; }

        public int EmailCount { get; set; }
        public int NameCount { get; set; }
    }
}
