﻿namespace AMRssForSalesforce.Model
{
    public enum TypeEnum
    {

        String,
        Adress,
        Contact,
        NotContact,
        Source,
        Email,
        RelationLink,
        Url,
        Company,
        Pnone,
        ContactAndPhone,
        ContactsAndPhone,
        Contacts,
        ContactAndTitle,
        Multiple,
        Press,
        Type,
        Title,
        StringSeveralValues,
        Separator,
        LabelInformation,
    };
}
