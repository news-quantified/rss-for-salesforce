﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Model
{
    public class TypeRepresent
    {
        public TypeEnum Type { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return (string.IsNullOrEmpty(Value)) ? String.Empty : Value;
        }
    }
}
