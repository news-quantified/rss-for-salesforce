﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMRssForSalesforce.Model
{
    public class News
    {
        public string NewsKey { get; set; }
        public string ResourceID { get; set; }
        public string Provider { get; set; }
        public string Id { get; set; }
        public string HeadLine { get; set; }
        public DateTime PubTime { get; set; }
        public string URL { get; set; }
        public string Story { get; set; }
       
    }
}
