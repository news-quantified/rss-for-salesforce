﻿using System.Globalization;

namespace AMRssForSalesforce.RssReader.Reloader
{
    using System;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Helpers;
    using Infrastructure.Repository;
    using Model;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    using System.Collections.Generic;
    public class CheckAccessWirePages
    {
        private int   _startIndex ;
        private readonly int _maxErrorPages ;
        private readonly RankedLanguageIdentifier _rankedLanguage;

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;

            var article = doc.DocumentNode.SelectNodes("//div[@class='articlepreview']");
            if (article != null)
            {
                var container = article.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);


                    //---------------------------------GetData----------------------------------------
                    try
                    {
                        var strDate = String.Empty;
                        var articlepreViewFirstParagraph = container
                            .SelectNodes("//p")
                            .FirstOrDefault();

                        if (articlepreViewFirstParagraph == null) return;

                        var dateContainer = articlepreViewFirstParagraph
                            .SelectNodes("//strong")
                            .FirstOrDefault();

                        if (dateContainer == null) return;

                        IEnumerable<HtmlNode> dateNodes = dateContainer.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                        strDate = dateNodes.Aggregate(strDate, (current, node) => current + node.InnerText);
                        strDate = strDate.Split('/')[2].Trim();



                        DateTime newsDateTime;
                        if (strDate.IndexOf("mars", StringComparison.Ordinal) >= 0)
                        {
                            strDate = strDate.Replace("mars", "March");
                        }

                        if (DateTime.TryParse(strDate, out newsDateTime))
                        {
                            rssFeed.DateTime = newsDateTime;
                        }
                        else if (DateTimeHelpers.TryParse(strDate, out newsDateTime))
                        {
                            rssFeed.DateTime = newsDateTime;
                        }
                        else rssFeed.DateTime = newsDateTime;
                    }
                    catch (Exception)
                    {
                        var strDate = String.Empty;
                        var articlepreViewFirstParagraph = container
                            .SelectNodes("//p")
                            .FirstOrDefault();

                        if (articlepreViewFirstParagraph == null) return;

                        var dateContainer = articlepreViewFirstParagraph
                            .SelectNodes("//strong")[1];
                            

                        if (dateContainer == null) return;

                        IEnumerable<HtmlNode> dateNodes = dateContainer.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                        strDate = dateNodes.Aggregate(strDate, (current, node) => current + node.InnerText);
                        strDate = strDate.Split('/')[2].Trim();



                        DateTime newsDateTime;
                        if (strDate.IndexOf("mars", StringComparison.Ordinal) >= 0)
                        {
                            strDate = strDate.Replace("mars", "March");
                        }

                        if (DateTime.TryParse(strDate, out newsDateTime))
                        {
                            rssFeed.DateTime = newsDateTime;
                        }
                        else if (DateTimeHelpers.TryParse(strDate, out newsDateTime))
                        {
                            rssFeed.DateTime = newsDateTime;
                        }
                        else rssFeed.DateTime = newsDateTime;
                    }
                    

                    //---------------------------------GetData----------------------------------------

                }
            }


            //--------------------------------------HeadLine-------------------------------------------
            var title = doc.DocumentNode.SelectNodes("//h1[@class='entry-title ak-container']");
            if (title != null)
            {
                var titleText = String.Empty;
                IEnumerable<HtmlNode> nodes = title.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                rssFeed.HeadLine = nodes.Aggregate(titleText, (current, node) => current + node.InnerText).Trim();
                
            }
            //--------------------------------------HeadLine-------------------------------------------

            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(_rankedLanguage).GetReturnValues(strHtml);

            rssFeed.StoryCount = storyCount.StoryCount;
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
        }
        

        public CheckAccessWirePages(RankedLanguageIdentifier rankedLanguageIdentifier)
            : this(rankedLanguageIdentifier,  1)
        {
            
        }
        public CheckAccessWirePages(RankedLanguageIdentifier rankedLanguageIdentifier,  int maxErrorPages) 
        {
            

             _maxErrorPages = maxErrorPages;
             _rankedLanguage = rankedLanguageIdentifier;
        }
        public void ChekCurrentPages()
        {
            var contactRep = new ContactInfoRepository();
            var rssFeedRepository = new RssFeedRepository();
            _startIndex = rssFeedRepository.GetMaxNewsNumberById(47, 46, "Access Wire") - 500;

            var errorPagesCount = 0;
            while (errorPagesCount < _maxErrorPages)
            {
                try   
                {
                    var url = string.Format(@"http://proxy9747.my-addr.org/myaddrproxy.php/https/www.accesswire.com/viewarticle.aspx?id={0}", _startIndex);
                    if (rssFeedRepository.AnyFeedItemById(url))
                    {
                        _startIndex++;
                        continue;
                    }

                    var rssFeed = new RssFeed
                    {
                        Provider = "Access Wire",
                        Source = "https://www.accesswire.com/rss/articles.ashx?reset=1",
                        Link = url,
                        Id = "1",
                        Language = "eng",
                        English = true
                    };

                    var web = new HtmlWeb();
                    var doc = web.Load(url);

                    var container = doc.DocumentNode.SelectNodes("//div[@class='articlepreview']");
                    if (container == null)
                    {
                        errorPagesCount++;
                        _startIndex++;
                        continue;
                    }

                    var checkContainer = doc.DocumentNode.SelectNodes("//div[@class='ak-container']");
                    if (checkContainer != null)
                    {
                        var strHtml = String.Empty;
                        IEnumerable<HtmlNode> nodes = checkContainer.Descendants()
                                            .Where(n =>
                                                n.NodeType == HtmlNodeType.Text &&
                                                n.ParentNode.Name != "script" &&
                                                n.ParentNode.Name != "style");

                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        if (strHtml.Contains("Member & Client Login"))
                        {
                            errorPagesCount++;
                            _startIndex++;
                            continue;
                        }
                    }


                    var accessWireHtmlParser = new AccessWireHtmlParser_rn(url);
                    var contentData = accessWireHtmlParser.ParserContact(rssFeed);
                    if (contentData == null)
                    {
                        errorPagesCount++;
                        _startIndex++;
                        continue;
                    }


                    var contacts = contentData.Item1;
                    CalculateStoryCount(doc, rssFeed);

                    rssFeedRepository.AddNotUpdate(rssFeed);

                    foreach (var c in contacts)
                    {
                        try
                        {
                            c.NewsReleaseDate = rssFeed.DateTime.ToString(CultureInfo.InvariantCulture);
                            contactRep.Add(c);
                        }
                        catch (Exception)
                        {

                        }

                    }
                    _startIndex++;
                }
                catch (Exception)
                {
                    errorPagesCount++;
                    _startIndex++;
                }
            }
        }
    }
}
