﻿namespace AMRssForSalesforce.RssReader.Reloader
{
    using System.Collections.Generic;
    using System;
    using System.Linq;
    using Infrastructure.Helpers;
    using Infrastructure.Repository;
    using HtmlAgilityPack;
    public class CheckMarketWiredDateTime
    {
        private int   _startIndex ;
        private int  _errorPagesCount;
        private int _checkCount;

        private DateTime ReCalculateDateTime(HtmlDocument doc)
        {
            var strDateHtml = String.Empty;
            var resultDatetime = new DateTime(2002, 2, 2);
            var datecontainer = doc.DocumentNode.SelectNodes("//p[@id='news-date']");
            if (datecontainer != null)
            {
                IEnumerable<HtmlNode> nodes = datecontainer.Descendants().Where(n =>
                    n.NodeType == HtmlNodeType.Text &&
                    n.ParentNode.Name != "script" &&
                    n.ParentNode.Name != "style");
                strDateHtml = nodes.Aggregate(strDateHtml, (current, node) => current + node.InnerText);
                strDateHtml = strDateHtml.Trim().Remove(strDateHtml.Length - 2, 2).Trim();
                if (strDateHtml.IndexOf("mars", StringComparison.Ordinal) >= 0)
                {
                    strDateHtml = strDateHtml.Replace("h", ":");
                    strDateHtml = strDateHtml.Replace("mars", "March");
                      
                }
                

                DateTime feedDateTime;


                if (DateTime.TryParse(strDateHtml, out feedDateTime))
                {
                    resultDatetime = feedDateTime;
                }
                else if (DateTimeHelpers.TryParse(strDateHtml, out feedDateTime))
                {
                    resultDatetime = feedDateTime;
                }

            }
            return resultDatetime;
        }

        //http://www.marketwired.com/mw/release.do?id=2107486&sourceType=3
        public CheckMarketWiredDateTime()
            : this(2107486, 0, 1000)
        {
            
        }
        public CheckMarketWiredDateTime(int startIndex, int errorPagesCount, int checkCount) 
        {
             _startIndex = startIndex;
             _errorPagesCount = errorPagesCount;
             _checkCount = checkCount;
        }
        public void ChekCurrentPages()
        {
            var rssFeedRepository = new RssFeedRepository();
            while (_checkCount > 0)
            {
                try
                {
                    var url = string.Format(@"http://www.marketwired.com/mw/release.do?id={0}&sourceType=3", _startIndex);
                    var web = new HtmlWeb();
                    var doc = web.Load(url);

                    if (!rssFeedRepository.AnyFeedItemById(url))
                    {
                        _checkCount--;
                        _startIndex--;
                        continue;
                    }

                    var container = doc.DocumentNode.SelectNodes("//div[@id='main_content']");
                    if (container == null)
                    {
                        _checkCount--;
                        _startIndex--;
                        continue;

                    }
                    var strHtml = String.Empty;
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");

                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    if (strHtml.Contains("We're sorry but you have tried to access a page that no longer exists, a page whose web address has changed, or a page you might not have permission to view."))
                    {
                        _checkCount--;
                        _startIndex--;
                        continue;
                    }

                    var newDatetime = ReCalculateDateTime(doc);
                    rssFeedRepository.UpdateDateTime(newDatetime, url);
                    _checkCount--;
                    _startIndex--;
                }
                catch (Exception)
                {
                    _checkCount--;
                    _startIndex--;
                }
            }
        }
    }
}
