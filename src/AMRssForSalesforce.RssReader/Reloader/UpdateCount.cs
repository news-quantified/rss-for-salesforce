﻿namespace AMRssForSalesforce.RssReader.Reloader
{
    using System;
    using Infrastructure.Repository;
    using NTextCat;


    public class UpdateCount
    {
        public static void UpdateStoryCount(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            
            var repo = new RssFeedRepository();
            foreach (var item in repo.FindAll())
            {
                try
                {
                    OldDataUpdate.UpdateStoryCount(item.Link, item.Provider, rankedLanguageIdentifier);
                }
                catch (Exception ex)
                {


                }
            }
        }
    }
}
