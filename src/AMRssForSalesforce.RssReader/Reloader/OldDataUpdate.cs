﻿namespace AMRssForSalesforce.RssReader.Reloader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Infrastructure.Repository;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    public class OldDataUpdate
    {
        public static bool UpdateStoryCount(string url, string provider, RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(url);
                doc.OptionWriteEmptyNodes = true;

                if (provider == "Business Wire")
                {

                    var strHtml = String.Empty;

                    var article = doc.DocumentNode.SelectNodes("//div[@itemprop='articleBody']")??doc.DocumentNode.SelectNodes("//div[@class='bw-release-story']");
                    if (article != null)
                    {
                        var container = article.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes = container.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                    }



                    var title = doc.DocumentNode.SelectNodes("//h1[@class='epi-fontLg']") ?? doc.DocumentNode.SelectNodes("//h1[@class='epi-fontLg bwalignc']");
                    if (title != null)
                    {
                        IEnumerable<HtmlNode> nodes = title.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }

                    var contact = doc.DocumentNode.SelectNodes("//div[@class='bw-release-contact']");
                    if (contact != null)
                    {
                        IEnumerable<HtmlNode> nodes = contact.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }

                    if (String.IsNullOrEmpty(strHtml))
                    {
                        article = doc.DocumentNode.SelectNodes("//div[bw-release-body  ]");
                        if (article != null)
                        {
                            var container = article.FirstOrDefault();
                            if (container != null)
                            {
                                IEnumerable<HtmlNode> nodes = container.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");


                                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                            }
                        }
                        title = doc.DocumentNode.SelectNodes("//h1[@class='epi-fontLg']");
                        if (title != null)
                        {
                            IEnumerable<HtmlNode> nodes = title.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }

                         contact = doc.DocumentNode.SelectNodes("//div[@class='bw-release-contact']");
                        if (contact != null)
                        {
                            IEnumerable<HtmlNode> nodes = contact.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                    }


                    if (String.IsNullOrEmpty(strHtml)) return false;
                    var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                    string language;
                    bool isEnglish;
                    if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                    {
                        language = "eng";
                        isEnglish = true;
                    }
                    else
                    {
                        language = count.Language == "en" ? "eng" : count.Language;
                        isEnglish = count.Language == "en" || count.Language == "eng";
                    }
                    var repo = new RssFeedRepository();
                    repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                    return true;


                }
                else if (provider == "PR Web")
                {

                    var strHtml = String.Empty;

                    var article = doc.DocumentNode.SelectNodes("//div[@class='article-text']");
                    if (article != null)
                    {
                        var container = article.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes = container.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                    }
                    var title = doc.DocumentNode.SelectNodes("//h1[@class='article-title']");
                    if (title != null)
                    {
                        IEnumerable<HtmlNode> nodes = title.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }
                    if (String.IsNullOrEmpty(strHtml)) return false;
                    var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                    string language;
                    bool isEnglish;
                    if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                    {
                        language = "eng";
                        isEnglish = true;
                    }
                    else
                    {
                        language = count.Language == "en" ? "eng" : count.Language;
                        isEnglish = count.Language == "en" || count.Language == "eng";
                    }


                    var repo = new RssFeedRepository();
                    repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                    return true;


                }
                else if (provider == "Access Wire")
                {
                    
                    var strHtml = String.Empty;

                    var article = doc.DocumentNode.SelectNodes("//div[@class='articlepreview']");
                    if (article != null)
                    {
                        var container = article.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes =container.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                    }
                    var title = doc.DocumentNode.SelectNodes("//span[@class='artitles']");
                    if (title != null)
                    {
                        IEnumerable<HtmlNode> nodes = title.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }
                    if (String.IsNullOrEmpty(strHtml)) return false;
                    var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);

                    string language;
                    bool isEnglish;
                    if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                    {
                        language = "eng";
                        isEnglish = true;
                    }
                    else
                    {
                        language = count.Language == "en" ? "eng" : count.Language;
                        isEnglish = count.Language == "en" || count.Language == "eng";
                    }


                    var repo = new RssFeedRepository();
                    repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                    return true;


                }else if (provider == "Globe Newswire")
                {
                    var strHtml = String.Empty;

                    var article = doc.DocumentNode.SelectNodes("//span[@itemprop='articleBody']");
                    if (article != null)
                    {
                        var container = article.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes =container.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                    }

                    var title = doc.DocumentNode.SelectNodes("//h1[@class='article-headline']");
                    if (title != null)
                    {
                        IEnumerable<HtmlNode> nodes = title.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }

                    var title2 = doc.DocumentNode.SelectNodes("//h2[@class='subheadline']");
                    if (title2 != null)
                    {
                        IEnumerable<HtmlNode> nodes = title2.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml =  nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }

                    if (String.IsNullOrEmpty(strHtml)) return false;

                    var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                    string language;
                    bool isEnglish;
                    if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                    {
                        language = "eng";
                        isEnglish = true;
                    }
                    else
                    {
                        language = count.Language == "en" ? "eng" : count.Language;
                        isEnglish = count.Language == "en" || count.Language == "eng";
                    }


                    var repo = new RssFeedRepository();
                    repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                    return true;
                }


                else if (provider == "Canada Newswire")
                {
                    var strHtml = String.Empty;
                    var first =
                        doc.DocumentNode.SelectNodes("//div[@class='col-sm-9 large-bottom-margin']").FirstOrDefault();
                    if (first != null)
                        strHtml = strHtml + first.OuterHtml;

                    var container = doc.DocumentNode.SelectNodes("//div[@class='container']").FirstOrDefault();
                    if (container != null)
                    {
                        var header = container.SelectNodes("//h1").FirstOrDefault();
                        if (header != null)
                            strHtml = strHtml + header.OuterHtml;
                    }
                    if (String.IsNullOrEmpty(strHtml)) return false;
                    var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                    string language;
                    bool isEnglish;
                    if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                    {
                        language = "eng";
                        isEnglish = true;
                    }
                    else
                    {
                        language = count.Language == "en" ? "eng" : count.Language;
                        isEnglish = count.Language == "en" || count.Language == "eng";
                    }


                    var repo = new RssFeedRepository();
                    repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                    return true;
                }
                else if (provider == "PR Newswire")
                {
                    var strHtml = String.Empty;
                    var allcontainer = doc.DocumentNode.SelectNodes("//div[@class='news-release-detail']");
                    if (allcontainer != null)
                    {
                        var container = allcontainer.FirstOrDefault();
                        if (container != null)
                        {
                            /*foreach (var comment in container.ChildNodes.Remove()Where(comment => !comment.InnerText.StartsWith("DOCTYPE")))
                            {
                                comment.Remove();
                            }*/

                            var header = container.SelectNodes("//h1").FirstOrDefault();
                            if (header != null)
                                strHtml = strHtml + header.OuterHtml;

                            IEnumerable<HtmlNode> nodes =
                                container.SelectNodes("//div[@class='col-sm-9']")
                                    .LastOrDefault()
                                    .Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);

                            if (String.IsNullOrEmpty(strHtml)) return false;
                            var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                            string language;
                            bool isEnglish;
                            if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                            {
                                language = "eng";
                                isEnglish = true;
                            }
                            else
                            {
                                language = count.Language == "en" ? "eng" : count.Language;
                                isEnglish = count.Language == "en" || count.Language == "eng";
                            }


                            var repo = new RssFeedRepository();
                            repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                            return true;
                        }
                    }

                    allcontainer = doc.DocumentNode.SelectNodes("//article[@class='news-release inline-gallery-template']");
                    if (allcontainer != null)
                    {
                        var container = allcontainer.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                            if (String.IsNullOrEmpty(strHtml)) return false;

                            var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                            string language;
                            bool isEnglish;
                            if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                            {
                                language = "eng";
                                isEnglish = true;
                            }
                            else
                            {
                                language = count.Language == "en" ? "eng" : count.Language;
                                isEnglish = count.Language == "en" || count.Language == "eng";
                            }


                            var repo = new RssFeedRepository();
                            repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                            return true;
                        }
                    }

                    allcontainer = doc.DocumentNode.SelectNodes("//section[@class='release-body container ']");
                    if (allcontainer != null)
                    {
                        var container = allcontainer.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }

                        var title = doc.DocumentNode.SelectNodes("//header[@class='container release-header']");
                        if (title != null)
                        {
                            IEnumerable<HtmlNode> nodes = title.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                        }
                        if (String.IsNullOrEmpty(strHtml)) return false;

                        var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                        string language;
                        bool isEnglish;
                        if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                        {
                            language = "eng";
                            isEnglish = true;
                        }
                        else
                        {
                            language = count.Language == "en" ? "eng" : count.Language;
                            isEnglish = count.Language == "en" || count.Language == "eng";
                        }


                        var repo = new RssFeedRepository();
                        repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                        return true;

                    }

                    allcontainer = doc.DocumentNode.SelectNodes("//div[@id='lede']");
                    if (allcontainer != null)
                    {
                        var container = allcontainer.FirstOrDefault();
                        if (container != null)
                        {
                            IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");
                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                            if (String.IsNullOrEmpty(strHtml)) return false;

                            var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                            string language;
                            bool isEnglish;
                            if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                            {
                                language = "eng";
                                isEnglish = true;
                            }
                            else
                            {
                                language = count.Language == "en" ? "eng" : count.Language;
                                isEnglish = count.Language == "en" || count.Language == "eng";
                            }


                            var repo = new RssFeedRepository();
                            repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                            return true;
                        }
                    }

                }
                else if (provider == "Marketwired")
                {
                    var strHtml = String.Empty;
                    var allcontainer = doc.DocumentNode.SelectNodes("//div[@id='newsroom-copy']");
                    if (allcontainer != null)
                    {
                        var container = allcontainer.FirstOrDefault();
                        if (container != null)
                        {

                            IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                            strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                            if (String.IsNullOrEmpty(strHtml)) return false;

                            var count = new BWClippUtilParseLib(rankedLanguageIdentifier).GetReturnValues(strHtml);
                            string language;
                            bool isEnglish;
                            if (count.Language.ToUpper() == "SIMPLE" || count.IsEnglish == true)
                            {
                                language = "eng";
                                isEnglish = true;
                            }
                            else
                            {
                                language = count.Language == "en" ? "eng" : count.Language;
                                isEnglish = count.Language == "en" || count.Language == "eng";
                            }


                            var repo = new RssFeedRepository();
                            repo.UpdateStoryCount(count.StoryCount, isEnglish, language, url);
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;

        }
    }
}
