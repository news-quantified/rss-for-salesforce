﻿namespace AMRssForSalesforce.RssReader.Reloader
{
    using System.Collections.Generic;
    using System;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Helpers;
    using Infrastructure.Repository;
    using Model;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    public class CheckMarketWiredPages
    {
        private int   _startIndex ;
        private int  _errorPagesCount;
        private readonly int _maxErrorPages ;
        private readonly RankedLanguageIdentifier _rankedLanguage;

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;
            var strHtmlHeadline = String.Empty;
            var strDateHtml = String.Empty;
            var allcontainer = doc.DocumentNode.SelectNodes("//div[@id='newsroom-copy']");
            if (allcontainer != null)
            {
                var container = allcontainer.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                        n.NodeType == HtmlNodeType.Text &&
                        n.ParentNode.Name != "script" &&
                        n.ParentNode.Name != "style");
                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);

                    var hedline = container.SelectNodes("//h1").FirstOrDefault();
                    if (hedline != null)
                    {

                        IEnumerable<HtmlNode> nodesheadline = hedline.Descendants().Where(n =>
                            n.NodeType == HtmlNodeType.Text &&
                            n.ParentNode.Name != "script" &&
                            n.ParentNode.Name != "style");
                        strHtmlHeadline = nodesheadline.Aggregate(strHtmlHeadline, (current, node) => current + node.InnerText);
                        rssFeed.HeadLine = strHtmlHeadline;
                    }
                }
            }
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(_rankedLanguage).GetReturnValues(strHtml);
            rssFeed.StoryCount = storyCount.StoryCount;

            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }

            
            
            var datecontainer = doc.DocumentNode.SelectNodes("//p[@id='news-date']");
            if (datecontainer != null)
            {
                IEnumerable<HtmlNode> nodes = datecontainer.Descendants().Where(n =>
                    n.NodeType == HtmlNodeType.Text &&
                    n.ParentNode.Name != "script" &&
                    n.ParentNode.Name != "style");
                strDateHtml = nodes.Aggregate(strDateHtml, (current, node) => current + node.InnerText);
                strDateHtml = strDateHtml.Trim().Remove(strDateHtml.Length - 2, 2).Trim();
                if (strDateHtml.IndexOf("mars", StringComparison.Ordinal) >= 0)
                {
                    strDateHtml = strDateHtml.Replace("h", ":");
                    strDateHtml = strDateHtml.Replace("mars", "March");
                }
                DateTime feedDateTime;
                if (DateTime.TryParse(strDateHtml, out feedDateTime))
                {
                    rssFeed.DateTime = feedDateTime;
                }
                else if (DateTimeHelpers.TryParse(strDateHtml, out feedDateTime))
                {
                    rssFeed.DateTime = feedDateTime;
                }
            }
        }
        

        public CheckMarketWiredPages(RankedLanguageIdentifier rankedLanguageIdentifier)
            : this(rankedLanguageIdentifier, 0, 1)
        {
            
        }
        public CheckMarketWiredPages(RankedLanguageIdentifier rankedLanguageIdentifier, int errorPagesCount,int maxErrorPages ) 
        {
             _errorPagesCount = errorPagesCount;
             _maxErrorPages = maxErrorPages;
             _rankedLanguage = rankedLanguageIdentifier;
        }
        public void ChekCurrentPages()
        {
            var rssFeedRepository = new RssFeedRepository();
            _startIndex = rssFeedRepository.GetMaxNewsNumberById(45, 57, "Marketwired") - 1000;

            while (_errorPagesCount < _maxErrorPages)
            {
                try
                {
                    var url = string.Format(@"http://www.marketwired.com/mw/release.do?id={0}&sourceType=3", _startIndex);
                    if (rssFeedRepository.AnyFeedItemById(url))
                    {
                        _startIndex++;
                        continue;
                    }
                    RssFeed rssFeed = new RssFeed
                    {
                        Provider = "Marketwired",
                        Source = "http://www.marketwire.com/rss/recentheadlines.xml",
                        Link = url,
                        Id = "1"
                        
                    };
                    var marketwire = new MarketwiredHtmlParser(rssFeed);
                    var contentData = marketwire.StartParse();
                    if (contentData == null) continue;
                    var dateLine = contentData.Item1;
                    var content = contentData.Item2;

                    var container = content.DocumentNode.SelectNodes("//div[@id='main_content']");
                    if (container == null)
                    {
                        _errorPagesCount++;
                        _startIndex++;
                        continue;

                    }
                    var strHtml = String.Empty;
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                                        .Where(n =>
                                            n.NodeType == HtmlNodeType.Text &&
                                            n.ParentNode.Name != "script" &&
                                            n.ParentNode.Name != "style");

                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    if (strHtml.Contains("We're sorry but you have tried to access a page that no longer exists, a page whose web address has changed, or a page you might not have permission to view."))
                    {
                        _errorPagesCount++;
                        _startIndex++;
                        continue;
                    }
                    if (dateLine == null)
                    {
                        _startIndex++;
                        continue;
                    }
                    rssFeed.Dateline = dateLine;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    rssFeedRepository.AddNotUpdate(rssFeed);
                    _startIndex++;
                }
                catch (Exception)
                {
                    _errorPagesCount++;
                    _startIndex++;
                }
            }
        }
    }
}
