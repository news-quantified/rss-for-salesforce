﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using AMRssForSalesforce.Infrastructure.Helpers;
using AMRssForSalesforce.Infrastructure.Logger;
using AMRssForSalesforce.Model;
using AMRssForSalesforce.Infrastructure.Repository;
using NTextCat;
using System.Net;
using System.IO;
using System.Xml;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SimpleBrowser.WebDriver;

namespace AMRssForSalesforce.RssReader.Abstract
{
    public abstract class RssParser
    {
        public virtual string RssSource { get; protected set; }
        public virtual  string RssFeedName { get; protected set; }
        public virtual RankedLanguageIdentifier RankedLanguage { get; protected set; }

        public abstract bool TrySaveRssList(IEnumerable<RssFeed> list);

        public virtual IEnumerable<RssFeed> Parse(string url)
        {
            //url = "https://feeds.businesswire.com/BW/Automotive_News-rss";
            Console.WriteLine("Parse Start - " + url);
            var doc = new XDocument();
            AccesswireJsonRSS list = null;
            try
            {

                if (RssFeedName == "Business Wire")
                {
                    using (IWebDriver driver = new SimpleBrowserDriver())
                    {
                        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        driver.Navigate().GoToUrl(url);

                        var content = driver.PageSource;
                        doc = XDocument.Parse(content);
                    }
                }
                else if (RssFeedName == "Marketwired")
                {
                    doc = XDocument.Load(url);
                }
                else if (RssFeedName == "Access Wire")
                {

                    try
                    {
                        //WebProxy wp = new WebProxy("136.233.215.136:80");
                        WebClient wb = new WebClient();
                        //wb.Proxy = wp;
                        wb.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
                        var jsontext = wb.DownloadString(url);
                        if (!string.IsNullOrEmpty(jsontext))
                        {
                            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(AccesswireJsonRSS));
                            list = (AccesswireJsonRSS)json.ReadObject(new System.IO.MemoryStream(Encoding.Unicode.GetBytes(jsontext)));
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }
                else
                {
                    doc = XDocument.Load(url);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(ex);
            }

            if(RssFeedName != "Access Wire")
            {
                var items = (from x in doc.Descendants("item")
                             select new
                             {
                                 title = x.TryGetElementValue("title", ""),
                                 link = x.TryGetElementValue("link", ""),
                                 pubDate = x.TryGetElementValue("pubDate", ""),
                                 description = x.TryGetElementValue("description", "")
                             });

                foreach (var item in items)
                {
                    DateTime dateTime = new DateTime();
                    string date = String.Empty;
                    string time = String.Empty; ;
                    try
                    {
                        if (DateTimeHelpers.TryParse(item.pubDate, out dateTime, true))
                        {
                            date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                            time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                        }
                        else if (DateTime.TryParse(item.pubDate, out dateTime))
                        {
                            date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                            time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                        }
                        else
                        {
                            dateTime = DateTime.Now;
                            date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                            time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteError(ex);
                    }
                    yield return
                        new RssFeed
                        {
                            DateTime = dateTime,
                            Date = date,
                            Time = time,
                            Link = item.link,
                            HeadLine = item.title,
                            Source = url,
                            Id = "1",
                            Provider = RssFeedName
                        };
                }
            }
            else
            {
                if (list != null && list.data != null && list.data.articles != null)
                {
                    var items = (from x in list.data.articles
                                 select new
                                 {
                                     title = x.title,
                                     link = x.releaseurl,
                                     pubDate = x.adate,
                                     description = x.title
                                 });

                    foreach (var item in items)
                    {
                        DateTime dateTime = new DateTime();
                        string date = String.Empty;
                        string time = String.Empty; ;
                        try
                        {
                            if (DateTimeHelpers.TryParse(item.pubDate, out dateTime, true))
                            {
                                date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                                time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                            }
                            else if (DateTime.TryParse(item.pubDate, out dateTime))
                            {
                                date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                                time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                            }
                            else
                            {
                                dateTime = DateTime.Now;
                                date = String.Format("{0}/{1}", dateTime.Month, dateTime.Day);
                                time = dateTime.ToString(@"hh:mm:ss tt", new CultureInfo("en-US"));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteError(ex);
                        }
                        yield return
                            new RssFeed
                            {
                                DateTime = dateTime,
                                Date = date,
                                Time = time,
                                Link = item.link,
                                HeadLine = item.title,
                                Source = url,
                                Id = "1",
                                Provider = RssFeedName
                            };
                    }
                }
            }
            
            Console.WriteLine("Parse End - " + url);
        }
        public virtual IEnumerable<RssFeed> Parse()
        {
            return Parse(RssSource);
       }

        public virtual IEnumerable<RssFeed> GetNotContacts()
        {
            var repo = new RssFeedRepository();
            var rssList = repo.FindNotContact(this.RssFeedName);
            return rssList;
        }

        public abstract bool Start();
    }

    [DataContract]
    public class AccesswireJsonRSS
    {
        [DataMember(Name = "data")]
        public AccesswireJsonRSSData data { get; set; }
    }
    [DataContract]
    public class AccesswireJsonRSSData
    {
        [DataMember(Name = "articles")]
        public List<AccesswireJsonRSSArticle> articles { get; set; }
    }
    [DataContract]
    public class AccesswireJsonRSSArticle
    {
        [DataMember(Name = "adate")]
        public string adate { get; set; }
        [DataMember(Name = "releaseurl")]
        public string releaseurl { get; set; }
        [DataMember(Name = "title")]
        public string title { get; set; }
    }
}
