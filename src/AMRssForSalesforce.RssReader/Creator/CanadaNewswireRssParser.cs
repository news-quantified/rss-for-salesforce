﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using Infrastructure.Logger;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    using System.Net;
    using System.IO;
    using System.Threading;
    using AMRssForSalesforce.Infrastructure.Helpers;
    public sealed class CanadaNewswireRssParser : RssParser
    {

        private static string[] _rssSources = new string[] {
            "http://www.newswire.ca/rss/auto-transportation/aerospace-defense-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/air-freight-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/airlines-aviation-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/automotive-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/maritime-shipbuilding-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/railroads-intermodal-transportation-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/transportation-trucking-railroad-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/travel-news.rss",
            "http://www.newswire.ca/rss/auto-transportation/trucking-road-transportation-news.rss",

"http://www.newswire.ca/rss/business-technology/broadcast-tech-news.rss" ,
"http://www.newswire.ca/rss/business-technology/computer-electronics-news.rss " ,
"http://www.newswire.ca/rss/business-technology/computer-hardware-news.rss" ,
"http://www.newswire.ca/rss/business-technology/computer-software-news.rss " ,
"http://www.newswire.ca/rss/business-technology/electronic-commerce-news.rss" ,
"http://www.newswire.ca/rss/business-technology/electronic-components-news.rss" ,
"http://www.newswire.ca/rss/business-technology/electronic-design-automation-news.rss " ,
"http://www.newswire.ca/rss/business-technology/electronics-performance-measurement.rss" ,
"http://www.newswire.ca/rss/business-technology/high-tech-security.rss" ,
"http://www.newswire.ca/rss/business-technology/internet-technology-news.rss" ,
"http://www.newswire.ca/rss/business-technology/nanotechnology-news.rss" ,
"http://www.newswire.ca/rss/business-technology/networks-news.rss" ,
"http://www.newswire.ca/rss/business-technology/peripherals-news.rss" ,
"http://www.newswire.ca/rss/business-technology/rfid-news.rss" ,
"http://www.newswire.ca/rss/business-technology/semantic-web-news.rss" ,
"http://www.newswire.ca/rss/business-technology/semiconductors-news.rss",

"http://www.newswire.ca/rss/consumer-products-retail/all-consumer-products-retail-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/beers-wines-spirits-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/beverages-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/bridal-services.rss",
"http://www.newswire.ca/rss/consumer-products-retail/cosmetics-personal-care-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/fashion-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/food-beverages-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/furniture-furnishings-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/home-improvements-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/household-consumer-cosmetics-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/jewelry-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/non-alcoholic-beverages-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/office-products-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/organic-food-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/product-recalls-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/restaurants-news.rss",
"http://www.newswire.ca/rss/consumer-products/retail-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/supermarkets-news.rss",
"http://www.newswire.ca/rss/consumer-products-retail/toys-news.rss",

"http://www.newswire.ca/rss/consumer-technology/all-consumer-technology-news.rss",
"http://www.newswire.ca/rss/consumer-technology/computer-electronics-news.rss",
"http://www.newswire.ca/rss/consumer-technology/computer-hardware-news.rss",
"http://www.newswire.ca/rss/consumer-technology/computer-software-news.rss",
"http://www.newswire.ca/rss/consumer-technology/consumer-electronics-news.rss",
"http://www.newswire.ca/rss/consumer-technology/electronic-commerce-news.rss",
"http://www.newswire.ca/rss/consumer-technology/electronic-gaming-news.rss",
"http://www.newswire.ca/rss/consumer-technology/mobile-entertainment-news.rss",
"http://www.newswire.ca/rss/consumer-technology/multimedia-internet-news.rss",
"http://www.newswire.ca/rss/consumer-technology/peripherals-news.rss",
"http://www.newswire.ca/rss/consumer-technology/social-media-news.rss",
"http://www.newswire.ca/rss/consumer-technology/website-news.rss",
"http://www.newswire.ca/rss/consumer-technology/wireless-communications-news.rss",


"http://www.newswire.ca/rss/energy/all-energy-news.rss",
"http://www.newswire.ca/rss/energy/alternative-energies-news.rss",
"http://www.newswire.ca/rss/energy/chemical-news.rss",
"http://www.newswire.ca/rss/energy/electrical-utilities-news.rss",
"http://www.newswire.ca/rss/energy/gas-news.rss",
"http://www.newswire.ca/rss/energy/mining-metals-news.rss",
"http://www.newswire.ca/rss/energy/mining-news.rss",
"http://www.newswire.ca/rss/energy/oil-energy-news.rss",
"http://www.newswire.ca/rss/energy/oil-gas-discoveries-news.rss",
"http://www.newswire.ca/rss/energy/utilities-news.rss",
"http://www.newswire.ca/rss/energy/water-utilities-news.rss",

"http://www.newswire.ca/rss/entertainment-media/all-entertainment-media-news.rss",
"http://www.newswire.ca/rss/entertainment-media/advertising-news.rss",
"http://www.newswire.ca/rss/entertainment-media/books-news.rss",
"http://www.newswire.ca/rss/entertainment-media/books-news.rss",
"http://www.newswire.ca/rss/entertainment-media/entertainment-news.rss",
"http://www.newswire.ca/rss/entertainment-media/film-motion-picture-news.rss",
"http://www.newswire.ca/rss/entertainment-media/music-news.rss",
"http://www.newswire.ca/rss/entertainment-media/publishing-information-services-news.rss",
"http://www.newswire.ca/rss/entertainment-media/radio-news.rss",
"http://www.newswire.ca/rss/entertainment-media/television-news.rss",

"http://www.newswire.ca/rss/environment/all-environment-news.rss",
"http://www.newswire.ca/rss/environment/conservation-recycling-news.rss",
"http://www.newswire.ca/rss/environment/environmental-issues-news.rss",
"http://www.newswire.ca/rss/environment/environmental-policy-news.rss",
"http://www.newswire.ca/rss/environment/environmental-products-services-news.rss",
"http://www.newswire.ca/rss/environment/green-technology-news.rss",

"http://www.newswire.ca/rss/financial-services/all-financial-services-news.rss",
"http://www.newswire.ca/rss/financial-services/accounting-news-issues-news.rss",
"http://www.newswire.ca/rss/financial-services/acquisitions-mergers-takeovers-news.rss",
"http://www.newswire.ca/rss/financial-services/banking-financial-services-news.rss",
"http://www.newswire.ca/rss/financial-services/bankruptcy-news.rss",
"http://www.newswire.ca/rss/financial-services/bond-stock-ratings-news.rss",
"http://www.newswire.ca/rss/financial-services/contracts-news.rss",
"http://www.newswire.ca/rss/financial-services/dividends-news.rss",
"http://www.newswire.ca/rss/financial-services/earnings-forecasts-projections-news.rss",
"http://www.newswire.ca/rss/financial-services/earnings-news.rss",
"http://www.newswire.ca/rss/financial-services/financing-agreements-news.rss",
"http://www.newswire.ca/rss/financial-services/insurance-news.rss",
"http://www.newswire.ca/rss/financial-services/investment-opinions-news.rss",
"http://www.newswire.ca/rss/financial-services/joint-ventures-news.rss",
"http://www.newswire.ca/rss/financial-services/mutual-funds-news.rss",
"http://www.newswire.ca/rss/financial-services/otc-small-cap-news.rss",
"http://www.newswire.ca/rss/financial-services/real-estate-news.rss",
"http://www.newswire.ca/rss/financial-services/restructuring-recapitalization-news.rss",
"http://www.newswire.ca/rss/financial-services/sales-reports-news.rss",
"http://www.newswire.ca/rss/financial-services/shareholders-rights-plans-news.rss",
"http://www.newswire.ca/rss/financial-services/stock-offering-news.rss",
"http://www.newswire.ca/rss/financial-services/stock-split-news.rss",
"http://www.newswire.ca/rss/financial-services/venture-capital-news.rss",


"http://www.newswire.ca/rss/general-business/all-general-business-news.rss",
"http://www.newswire.ca/rss/general-business/awards-news.rss",
"http://www.newswire.ca/rss/general-business/commercial-real-estate-news.rss",
"http://www.newswire.ca/rss/general-business/conference-call-announcements-news.rss",
"http://www.newswire.ca/rss/general-business/corporate-expansion-news.rss",
"http://www.newswire.ca/rss/general-business/earnings-news.rss",
"http://www.newswire.ca/rss/general-business/human-resource-workforce-management-news.rss",
"http://www.newswire.ca/rss/general-business/licensing-news.rss",
"http://www.newswire.ca/rss/general-business/new-products-services-news.rss",
"http://www.newswire.ca/rss/general-business/obituaries.rss",
"http://www.newswire.ca/rss/general-business/outsourcing-businesses-news.rss",
"http://www.newswire.ca/rss/general-business/overseas-real-estate-news.rss",
"http://www.newswire.ca/rss/general-business/personnel-announcements-news.rss",
"http://www.newswire.ca/rss/general-business/real-estate-transactions-news.rss",
"http://www.newswire.ca/rss/general-business/residential-real-estate-news.rss",
"http://www.newswire.ca/rss/general-business/small-business-services-news.rss",
"http://www.newswire.ca/rss/general-business/socially-responsible-investing-news.rss",
"http://www.newswire.ca/rss/general-business/surveys-polls-research-news.rss",
"http://www.newswire.ca/rss/general-business/trade-show-news.rss",

"http://www.newswire.ca/rss/health/all-health-news.rss",
"http://www.newswire.ca/rss/health/biometrics-news.rss",
"http://www.newswire.ca/rss/health/biotechnology-news.rss",
"http://www.newswire.ca/rss/health/clinical-trials-medial-discoveries-news.rss/",
"http://www.newswire.ca/rss/health/dentistry-news.rss",
"http://www.newswire.ca/rss/health/health-care-hospitals-news.rss",
"http://www.newswire.ca/rss/health/health-insurance-news.rss",
"http://www.newswire.ca/rss/health/medical-equipment-news.rss",
"http://www.newswire.ca/rss/health/medical-pharmaceuticals-news.rss",
"http://www.newswire.ca/rss/health/mental-health-news.rss",
"http://www.newswire.ca/rss/health/supplemental-medicine-news.rss",

"http://www.newswire.ca/rss/heavy-industry-manufacturing/all-heavy-industry-manufacturing-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/aerospace-defense-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/agriculture-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/chemical-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/construction-building-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/hvac-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/machine-tools-metalworking-metallury-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/machinery-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/mining-metals-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/mining-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/paper-forest-products-containers-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/precious-metals-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/textiles-news.rss",
"http://www.newswire.ca/rss/heavy-industry-manufacturing/tobacco-news.rss",

"http://www.newswire.ca/rss/multicultural/all-multicultural-news.rss",
"http://www.newswire.ca/rss/multicultural/african-american-related-news.rss",
"http://www.newswire.ca/rss/multicultural/asian-related-news.rss",
"http://www.newswire.ca/rss/multicultural/children-related-news.rss",
"http://www.newswire.ca/rss/multicultural/handicapped-disabled.rss",
"http://www.newswire.ca/rss/multicultural/hispanic-oriented-news.rss",
"http://www.newswire.ca/rss/multicultural/lesbian-gay-bisexual.rss",
"http://www.newswire.ca/rss/multicultural/native-american.rss",
"http://www.newswire.ca/rss/multicultural/religion.rss",
"http://www.newswire.ca/rss/multicultural/senior-citizens.rss",
"http://www.newswire.ca/rss/multicultural/veterans.rss",
"http://www.newswire.ca/rss/multicultural/women-related-news.rss",

"http://www.newswire.ca/rss/policy-public-interest/all-policy-public-interest-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/animal-welfare-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/corporate-social-responsibility-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/domestic-policy-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/economic-news-trends-analysis-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/education-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/environmental-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/european-government-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/fda-approval-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/federal-state-legislation-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/federal-executive-branch-agency-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/foreign-policy-international-affairs-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/homeland-security-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/labor-union-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/legal-issues-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/not-for-profit-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/political-campaigns-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/public-safety-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/trade-policy-news.rss",
"http://www.newswire.ca/rss/policy-public-interest/us-state-policy-news.rss",


"http://www.newswire.ca/rss/sports/all-sports-news.rss",
"http://www.newswire.ca/rss/sports/sporting-news.rss",
"http://www.newswire.ca/rss/sports/sporting-events-news.rss",
"http://www.newswire.ca/rss/sports/sports-equipment-accessories-news.rss",

"http://www.newswire.ca/rss/telecommunications/all-telecommunications-news.rss",
"http://www.newswire.ca/rss/telecommunications/carriers-services-news.rss",
"http://www.newswire.ca/rss/telecommunications/mobile-entertainment-news.rss",
"http://www.newswire.ca/rss/telecommunications/networks-news.rss",
"http://www.newswire.ca/rss/telecommunications/peripherals-news.rss",
"http://www.newswire.ca/rss/telecommunications/telecommunications-equipment-news.rss",
"http://www.newswire.ca/rss/telecommunications/telecommunications-news.rss",
"http://www.newswire.ca/rss/telecommunications/voip-news.rss",
"http://www.newswire.ca/rss/telecommunications/wireless-communications-news.rss",

"http://www.newswire.ca/rss/travel/all-travel-news.rss",
"http://www.newswire.ca/rss/travel/amusement-parks-tourist-attractions-news.rss",
"http://www.newswire.ca/rss/travel/gambling-casinos-news.rss",
"http://www.newswire.ca/rss/travel/hotels-resorts-news.rss",
"http://www.newswire.ca/rss/travel/leisure-tourism-hotels-news.rss",
"http://www.newswire.ca/rss/travel/passenger-aviation-news.rss",
"http://www.newswire.ca/rss/travel/travel-news.rss"

 };
        public CanadaNewswireRssParser(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            //http://cnw.en.mediaroom.com/rss
            RssSource = @"http://www.newswire.ca/rss/financial-services/all-financial-services-news.rss";
            RssFeedName = @"Canada Newswire";
            RankedLanguage = rankedLanguageIdentifier;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            try
            {
                var strHtml = String.Empty;
                var first =
                        doc.DocumentNode.SelectSingleNode("//div[@class='col-sm-9 large-bottom-margin']");
                if (first != null)
                    strHtml = strHtml + first.OuterHtml;

                var container = doc.DocumentNode.SelectSingleNode("//div[@class='container']");

                if (container == null)
                {
                    container = doc.DocumentNode.SelectSingleNode("//div[@class='main']");
                }
                if (container != null)
                {
                    var header = container.SelectNodes("//h1").FirstOrDefault();
                    if (header != null)
                        strHtml = strHtml + header.OuterHtml;
                }
                if (String.IsNullOrEmpty(strHtml)) return;
                var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
                rssFeed.StoryCount = storyCount.StoryCount;
                if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
                {
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                }
                else
                {
                    rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                    rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
            }
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
            List<RssFeed> newRss = new List<RssFeed>();
            int countAdd = 0;

            var repo = new RssFeedRepository();
            foreach (var rssFeed in list)
            {
                try
                {
                    if (repo.AnyById(rssFeed.Link)) continue;
                    
                    countAdd++;

                    //if (!rssFeed.Link.Contains("all-"))
                    //    newRss.Add(rssFeed);

                    var canadatnews = new CanadaNewswireHtmlParser(rssFeed);
                    var contentData = canadatnews.StartParse();

                    if (contentData == null) continue;

                    var dateLine = contentData.Item1;
                    var content = contentData.Item2;

                    if (dateLine == null) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    rssFeed.Dateline = dateLine;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }
            if (countAdd == 0)
            {

            }
            return true;
        }


        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
            var repo = new RssFeedRepository();

            if (list == null || !list.Any()) return false;

            foreach (var rssFeed in list)
            {
                try
                {
                    var canadatnews = new CanadaNewswireHtmlParser(rssFeed);
                   
                    //canadatnews.FixHedlineContact(rssFeed);
                    //repo.Add(rssFeed);
                    //continue;

                    ContactInfoRepository repoC = new ContactInfoRepository();
                    repoC.Delete(rssFeed.Link);

                    var contentData = canadatnews.StartParse();

                    if (contentData == null) continue;

                    var dateLine = contentData.Item1;
                    var content = contentData.Item2;

                    if (dateLine == null) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    rssFeed.Dateline = dateLine;
                    repo.Add(rssFeed);
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }
            return true;
        }


        private List<RssFeed> GetNwesFromHtml(string urlpage)
        {
            List<RssFeed> rssFeeds = new List<RssFeed>();

            var res = HtmlHelpers.GetResponse(urlpage);
            if(res == null)
                return rssFeeds;

            var htmlContent = res.Html;
            if (htmlContent == null)
                return rssFeeds;


            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var countP = 0;
            var countPnoEnglish = 0;
            var allNoEnglish = 0;

            var rssDivs = doc.DocumentNode.SelectNodes("//div[@class='row']");

            if (rssDivs != null)
            {
                foreach (var div in rssDivs)
                {
                    var rssFeed = new RssFeed();
                    var pdatetime = div.SelectSingleNode(".//div[@class='col-sm-3 col-md-2']");

                    if (pdatetime == null)
                        continue;

                    if (pdatetime != null)
                    {
                        var st = Utils.NormalizationString(pdatetime.InnerText).Replace(";", "").Replace("\r", "").Replace("ET","").Trim();
                        DateTime deteRss = DateTime.Now;

                        if(DateTime.TryParse(st, out deteRss))
                        {
                            rssFeed.DateTime = deteRss;

                        }
                        else
                        {
                            rssFeed.DateTime = DateTime.Now;
                        }
                    }
                    else
                    {
                        Logger.WriteError(string.Format("Didn't finde companyTitle in {0}", urlpage));
                    }

                    var pHeadLine = div.SelectSingleNode(".//h4");
                    if (pHeadLine == null)
                    {
                        Logger.WriteError(string.Format("Didn't finde hedline in {0}", urlpage));
                        continue;
                    }

                    var aHeadline = pHeadLine.SelectSingleNode(".//a");
                    if (aHeadline == null || aHeadline.Attributes["href"] == null)
                    {
                        Logger.WriteError(string.Format("Didn't finde hedline in {0}", urlpage));
                        continue;
                    }

                    var url = aHeadline.Attributes["href"].Value;
                   

                    var headline = aHeadline.InnerText;

                    rssFeed.HeadLine = headline;
                    rssFeed.Link = url;
                    rssFeed.Provider = this.RssFeedName;
                    rssFeed.Source = urlpage;
                   

                    rssFeeds.Add(rssFeed);
                }
            }

            return rssFeeds;
        }
        
        private static string[] _errorContactDataList = new string[] {
            "'http://www.newswire.ca/news-releases/dynex-power-announces-first-quarter-results-581093981.html'",
            "'http://www.newswire.ca/news-releases/higher-income-earners-claim-considerably-more-on-medical-expense-tax-credit-580954111.html'",
            "'http://www.newswire.ca/news-releases/gogold-announces-revenue-growth-over-previous-quarter-579438681.html'",
            "'http://www.newswire.ca/news-releases/absolute-enhances-education-solution-with-chromebook-mtm-extension-578386671.html'",
        };

        public override bool Start()
        {
            Logger.WriteInformation(this.GetType().Name + " was started");
            try
            {
                var repo = new RssFeedRepository();
               // var errContact = repo.FindNotContact(RssFeedName);
                //var errContact = repo.FindErrContact(_errorContactDataList);
                //if (!TryReSaveRssList(errContact))
                //{

                //}
                //return true;
                //Logger.WriteInformation(this.GetType().Name + " was fixed");

                //foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                //{
                //}

                 

                    List<RssFeed> rssFeeds = GetNwesFromHtml("http://www.newswire.ca/news-releases/news-releases-list/?c=n?page=1&pagesize=200");
                    TrySaveRssList(rssFeeds);
                    int countNew = 0;

                 

                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            Logger.WriteInformation(this.GetType().Name + " was ended.");
            return true;
        }

     
    }
}
