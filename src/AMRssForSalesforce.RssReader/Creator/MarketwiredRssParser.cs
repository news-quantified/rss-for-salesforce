﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using Infrastructure.Logger;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    public sealed class MarketwiredRssParser : RssParser
    {

        //to do add
        //http://www.marketwired.com/News_Room/rss_newsfeeds
        //http://www.globenewswire.com/Rss/List - 2021-02-07
        private static string[] _rssSources = new string[] {
            //"http://www.marketwire.com/rss/mwAUPA.xml",

            //"http://www.marketwire.com/rss/recentheadlines.xml",

            //"http://www.marketwire.com/rss/mwAerospaceDefense.xml",
            //"http://www.marketwire.com/rss/mwADAI.xml",
            //"http://www.marketwire.com/rss/mwADEC.xml",
            //"http://www.marketwire.com/rss/mwADLV.xml",
            //"http://www.marketwire.com/rss/mwADMA.xml",
            //"http://www.marketwire.com/rss/mwADSP.xml",
            //"http://www.marketwire.com/rss/mwADWE.xml",

            //"http://www.marketwire.com/rss/mwAgriculture.xml",
            //"http://www.marketwire.com/rss/mwAGES.xml",
            //"http://www.marketwire.com/rss/mwAGFA.xml",
            //"http://www.marketwire.com/rss/mwAGFO.xml",
            //"http://www.marketwire.com/rss/mwAGLI.xml",

            //"http://www.marketwire.com/rss/mwAutomotive.xml",
            //"http://www.marketwire.com/rss/mwAUCA.xml",
            //"http://www.marketwire.com/rss/mwAUMO.xml",
            //"http://www.marketwire.com/rss/mwAUOP.xml",
            //"http://www.marketwire.com/rss/mwAUPA.xml",
            //"http://www.marketwire.com/rss/mwAURR.xml",
            //"http://www.marketwire.com/rss/mwAUTR.xml",

            //"http://www.marketwire.com/rss/mwChemicals.xml",
            //"http://www.marketwire.com/rss/mwCHCC.xml",
            //"http://www.marketwire.com/rss/mwCHPE.xml",
            //"http://www.marketwire.com/rss/mwCHPF.xml",
            //"http://www.marketwire.com/rss/mwCHSC.xml",
            //"http://www.marketwire.com/rss/mwCHWD.xml",

            //"http://www.marketwire.com/rss/mwComputersandSoftware.xml",
            //"http://www.marketwire.com/rss/mwCSHA.xml",
            //"http://www.marketwire.com/rss/mwCSIN.xml",
            //"http://www.marketwire.com/rss/mwCSNE.xml",
            //"http://www.marketwire.com/rss/mwCSPE.xml",
            //"http://www.marketwire.com/rss/mwCSSO.xml",

            //"http://www.marketwire.com/rss/mwEnvironment.xml",
            //"http://www.marketwire.com/rss/mwENAP.xml",
            //"http://www.marketwire.com/rss/mwENHM.xml",
            //"http://www.marketwire.com/rss/mwENNR.xml",
            //"http://www.marketwire.com/rss/mwENRL.xml",
            //"http://www.marketwire.com/rss/mwENWM.xml",

            //"http://www.marketwire.com/rss/mwElectronicsandSemiconductors.xml",
            //"http://www.marketwire.com/rss/mwESEC.xml",
            //"http://www.marketwire.com/rss/mwESED.xml",
            //"http://www.marketwire.com/rss/mwESMN.xml",
            //"http://www.marketwire.com/rss/mwESOC.xml",
            //"http://www.marketwire.com/rss/mwESRC.xml",
            //"http://www.marketwire.com/rss/mwESSE.xml",
            //"http://www.marketwire.com/rss/mwESTE.xml",

            //"http://www.marketwire.com/rss/mwEducationTraining.xml",
            //"http://www.marketwire.com/rss/mwETEA.xml",
            //"http://www.marketwire.com/rss/mwETSC.xml",
            //"http://www.marketwire.com/rss/mwETTR.xml",
            
            //"http://www.marketwire.com/rss/mwEnergyandUtilities.xml",
            //"http://www.marketwire.com/rss/mwEUAE.xml",
            //"http://www.marketwire.com/rss/mwEUCO.xml",
            //"http://www.marketwire.com/rss/mwEUEQ.xml",
            //"http://www.marketwire.com/rss/mwEUNU.xml",
            //"http://www.marketwire.com/rss/mwEUOG.xml",
            //"http://www.marketwire.com/rss/mwEUPI.xml",
            //"http://www.marketwire.com/rss/mwEUUT.xml",

            //"http://www.marketwire.com/rss/mwFoodandBeverage.xml",
            //"http://www.marketwire.com/rss/mwFBBE.xml",
            //"http://www.marketwire.com/rss/mwFBDA.xml",
            //"http://www.marketwire.com/rss/mwFBDA.xml",
            //"http://www.marketwire.com/rss/mwFBIN.xml",
            //"http://www.marketwire.com/rss/mwFBPA.xml",
            //"http://www.marketwire.com/rss/mwFBRE.xml",
            //"http://www.marketwire.com/rss/mwFBVE.xml",

            //"http://www.marketwire.com/rss/mwFinancialServices.xml",
            //"http://www.marketwire.com/rss/mwFSCI.xml",
            //"http://www.marketwire.com/rss/mwFSIN.xml",
            //"http://www.marketwire.com/rss/mwFSIO.xml",
            //"http://www.marketwire.com/rss/mwFSIS.xml",
            //"http://www.marketwire.com/rss/mwFSPF.xml",
            //"http://www.marketwire.com/rss/mwFSRB.xml",
            //"http://www.marketwire.com/rss/mwFSVC.xml",

            //"http://www.marketwire.com/rss/mwGovernment.xml",
            //"http://www.marketwire.com/rss/mwGOIN.xml",
            //"http://www.marketwire.com/rss/mwGOLO.xml",
            //"http://www.marketwire.com/rss/mwGONA.xml",
            //"http://www.marketwire.com/rss/mwGOSE.xml",
            //"http://www.marketwire.com/rss/mwGOST.xml",
           
            //"http://www.marketwire.com/rss/mwLifestyleandLeisure.xml",
            //"http://www.marketwire.com/rss/mwLLFA.xml",
            //"http://www.marketwire.com/rss/mwLLFS.xml",
            //"http://www.marketwire.com/rss/mwLLHG.xml",
            //"http://www.marketwire.com/rss/mwLLPC.xml",
            //"http://www.marketwire.com/rss/mwLLRE.xml",
            //"http://www.marketwire.com/rss/mwLLRG.xml",
            //"http://www.marketwire.com/rss/mwLLTH.xml",
            //"http://www.marketwire.com/rss/mwLLWI.xml",

            //"http://www.marketwire.com/rss/mwMediaandEntertainment.xml",
            //"http://www.marketwire.com/rss/mwMEAR.xml",
            //"http://www.marketwire.com/rss/mwMEBP.xml",
            //"http://www.marketwire.com/rss/mwMEIS.xml",
            //"http://www.marketwire.com/rss/mwMEMO.xml",
            //"http://www.marketwire.com/rss/mwMEMR.xml",
            //"http://www.marketwire.com/rss/mwMETE.xml",
            //"http://www.marketwire.com/rss/mwMEVG.xml",

            //"http://www.marketwire.com/rss/mwMedicalandHealthcare.xml",
            //"http://www.marketwire.com/rss/mwMHAL.xml",
            //"http://www.marketwire.com/rss/mwMHDE.xml",
            //"http://www.marketwire.com/rss/mwMHFP.xml",
            //"http://www.marketwire.com/rss/mwMHHN.xml",
            //"http://www.marketwire.com/rss/mwMHHE.xml",
            //"http://www.marketwire.com/rss/mwMHHN.xml",
            //"http://www.marketwire.com/rss/mwMHMD.xml",
            //"http://www.marketwire.com/rss/mwMHMH.xml",
            //"http://www.marketwire.com/rss/mwMHMH.xml",
            //"http://www.marketwire.com/rss/mwMHSU.xml",

            //"http://www.marketwire.com/rss/mwManufacturingandProduction.xml",
            //"http://www.marketwire.com/rss/mwMPFP.xml",
            //"http://www.marketwire.com/rss/mwMPMT.xml",
            //"http://www.marketwire.com/rss/mwMPMM.xml",
            //"http://www.marketwire.com/rss/mwMPPC.xml",
            //"http://www.marketwire.com/rss/mwMPTE.xml",

            //"http://www.marketwire.com/rss/mwPharmaceuticalsandBiotech.xml",
            //"http://www.marketwire.com/rss/mwPBBI.xml",
            //"http://www.marketwire.com/rss/mwPBDR.xml",
            //"http://www.marketwire.com/rss/mwPBES.xml",
            //"http://www.marketwire.com/rss/mwPBTR.xml",

            //"http://www.marketwire.com/rss/mwProfessionalServices.xml",
            //"http://www.marketwire.com/rss/mwPSAA.xml",
            //"http://www.marketwire.com/rss/mwPSAS.xml",
            //"http://www.marketwire.com/rss/mwPSAS.xml",
            //"http://www.marketwire.com/rss/mwPSCO.xml",
            //"http://www.marketwire.com/rss/mwPSEN.xml",
            //"http://www.marketwire.com/rss/mwPSHR.xml",
            //"http://www.marketwire.com/rss/mwPSIR.xml",
            //"http://www.marketwire.com/rss/mwPSLE.xml",
            //"http://www.marketwire.com/rss/mwPSNP.xml",
            //"http://www.marketwire.com/rss/mwPSOP.xml",
            //"http://www.marketwire.com/rss/mwPSPU.xml",

            //"http://www.marketwire.com/rss/mwRealEstateandConstruction.xml",
            //"http://www.marketwire.com/rss/mwRECR.xml",
            //"http://www.marketwire.com/rss/mwRECO.xml",
            //"http://www.marketwire.com/rss/mwRERR.xml",

            //"http://www.marketwire.com/rss/mwRetail.xml",
            //"http://www.marketwire.com/rss/mwRTAL.xml",
            //"http://www.marketwire.com/rss/mwRTAP.xml",
            //"http://www.marketwire.com/rss/mwRTCA.xml",
            //"http://www.marketwire.com/rss/mwRTCE.xml",
            //"http://www.marketwire.com/rss/mwRTCI.xml",
            //"http://www.marketwire.com/rss/mwRTEC.xml",
            //"http://www.marketwire.com/rss/mwRTFF.xml",
            //"http://www.marketwire.com/rss/mwRTSU.xml",

            //"http://www.marketwire.com/rss/mwSports.xml",
            //"http://www.marketwire.com/rss/mwSPCV.xml",
            //"http://www.marketwire.com/rss/mwSPEA.xml",
            //"http://www.marketwire.com/rss/mwSPIT.xml",

            //"http://www.marketwire.com/rss/mwTelecom.xml",
            //"http://www.marketwire.com/rss/mwTECS.xml",
            //"http://www.marketwire.com/rss/mwTENE.xml",
            //"http://www.marketwire.com/rss/mwTETE.xml",
            //"http://www.marketwire.com/rss/mwTETS.xml",
            //"http://www.marketwire.com/rss/mwTEWI.xml",

            //"http://www.marketwire.com/rss/mwTravelandHospitality.xml",
            //"http://www.marketwire.com/rss/mwTHAI.xml",
            //"http://www.marketwire.com/rss/mwTHCG.xml",
            //"http://www.marketwire.com/rss/mwTHCR.xml",
            //"http://www.marketwire.com/rss/mwTHHO.xml",
            //"http://www.marketwire.com/rss/mwTHRE.xml",
            //"http://www.marketwire.com/rss/mwTHTO.xml",

            //"http://www.marketwire.com/rss/mwTransportationandLogistics.xml",
            //"http://www.marketwire.com/rss/mwTLAF.xml",
            //"http://www.marketwire.com/rss/mwTLMA.xml",
            //"http://www.marketwire.com/rss/mwTLPT.xml",
            //"http://www.marketwire.com/rss/mwTLRI.xml",
            //"http://www.marketwire.com/rss/mwTLTR.xml"


            "http://www.globenewswire.com/RssFeed/orgclass/1/feedTitle/GlobeNewswire%20-%20News%20about%20Public%20Companies",
            "http://www.globenewswire.com/RssFeed/subjectcode/12-Dividend%20Reports%20And%20Estimates/feedTitle/GlobeNewswire%20-%20Dividend%20Reports%20And%20Estimates",
            "http://www.globenewswire.com/RssFeed/subjectcode/13-Earnings%20Releases%20And%20Operating%20Results/feedTitle/GlobeNewswire%20-%20Earnings%20Releases%20And%20Operating%20Results",
            "http://www.globenewswire.com/RssFeed/subjectcode/27-Mergers%20And%20Acquisitions/feedTitle/GlobeNewswire%20-%20Mergers%20And%20Acquisitions",
            "http://www.globenewswire.com/RssFeed/subjectcode/1-Advisory/feedTitle/GlobeNewswire%20-%20Advisory",
            "http://www.globenewswire.com/RssFeed/subjectcode/3-Analyst%20Recommendations/feedTitle/GlobeNewswire%20-%20Analyst%20Recommendations",
            "http://www.globenewswire.com/RssFeed/subjectcode/2-Annual%20Meetings%2026%20Shareholder%20Rights/feedTitle/GlobeNewswire%20-%20Annual%20Meetings%20and%20Shareholder%20Rights",
            "http://www.globenewswire.com/RssFeed/subjectcode/65-Annual%20Report/feedTitle/GlobeNewswire%20-%20Annual%20Report",
            "http://www.globenewswire.com/RssFeed/subjectcode/4-Arts%2026%20Entertainment/feedTitle/GlobeNewswire%20-%20Arts%20and%20Entertainment",
            "http://www.globenewswire.com/RssFeed/subjectcode/5-Bankruptcy/feedTitle/GlobeNewswire%20-%20Bankruptcy",
            "http://www.globenewswire.com/RssFeed/subjectcode/6-Bond%20Market%20News/feedTitle/GlobeNewswire%20-%20Bond%20Market%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/74-Bonds%20Market%20Information/feedTitle/GlobeNewswire%20-%20Bonds%20Market%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/7-Business%20Contracts/feedTitle/GlobeNewswire%20-%20Business%20Contracts",
            "http://www.globenewswire.com/RssFeed/subjectcode/8-Calendar%20Of%20Events/feedTitle/GlobeNewswire%20-%20Calendar%20Of%20Events",
            "http://www.globenewswire.com/RssFeed/subjectcode/58-Changes%20In%20Company%2027s%20Own%20Shares/feedTitle/GlobeNewswire%20-%20Changes%20In%20Company%20s%20Own%20Shares",
            "http://www.globenewswire.com/RssFeed/subjectcode/57-Changes%20In%20Share%20Capital%20And%20Votes/feedTitle/GlobeNewswire%20-%20Changes%20In%20Share%20Capital%20And%20Votes",
            "http://www.globenewswire.com/RssFeed/subjectcode/71-Changes%20To%20Observation%20Segment/feedTitle/GlobeNewswire%20-%20Changes%20To%20Observation%20Segment",
            "http://www.globenewswire.com/RssFeed/subjectcode/84-Class%20Action/feedTitle/GlobeNewswire%20-%20Class%20Action",
            "http://www.globenewswire.com/RssFeed/subjectcode/9-Company%20Announcement/feedTitle/GlobeNewswire%20-%20Company%20Announcement",
            "http://www.globenewswire.com/RssFeed/subjectcode/10-Company%20Regulatory%20Filings/feedTitle/GlobeNewswire%20-%20Company%20Regulatory%20Filings",
            "http://www.globenewswire.com/RssFeed/subjectcode/61-Corporate%20Action/feedTitle/GlobeNewswire%20-%20Corporate%20Action",
            "http://www.globenewswire.com/RssFeed/subjectcode/75-Derivative%20Market%20Information/feedTitle/GlobeNewswire%20-%20Derivative%20Market%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/11-Directors%20And%20Officers/feedTitle/GlobeNewswire%20-%20Directors%20And%20Officers",
            "http://www.globenewswire.com/RssFeed/subjectcode/12-Dividend%20Reports%20And%20Estimates/feedTitle/GlobeNewswire%20-%20Dividend%20Reports%20And%20Estimates",
            "http://www.globenewswire.com/RssFeed/subjectcode/13-Earnings%20Releases%20And%20Operating%20Results/feedTitle/GlobeNewswire%20-%20Earnings%20Releases%20And%20Operating%20Results",
            "http://www.globenewswire.com/RssFeed/subjectcode/14-Economic%20Research%20And%20Reports/feedTitle/GlobeNewswire%20-%20Economic%20Research%20And%20Reports",
            "http://www.globenewswire.com/RssFeed/subjectcode/76-Equity%20Market%20Information/feedTitle/GlobeNewswire%20-%20Equity%20Market%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/85-European%20Regulatory%20News/feedTitle/GlobeNewswire%20-%20European%20Regulatory%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/70-Exchange%20Announcement/feedTitle/GlobeNewswire%20-%20Exchange%20Announcement",
            "http://www.globenewswire.com/RssFeed/subjectcode/77-Exchange%20Members/feedTitle/GlobeNewswire%20-%20Exchange%20Members",
            "http://www.globenewswire.com/RssFeed/subjectcode/78-Exchange%20News/feedTitle/GlobeNewswire%20-%20Exchange%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/15-Fashion/feedTitle/GlobeNewswire%20-%20Fashion",
            "http://www.globenewswire.com/RssFeed/subjectcode/16-Feature%20Article/feedTitle/GlobeNewswire%20-%20Feature%20Article",
            "http://www.globenewswire.com/RssFeed/subjectcode/17-Financing%20Agreements/feedTitle/GlobeNewswire%20-%20Financing%20Agreements",
            "http://www.globenewswire.com/RssFeed/subjectcode/69-First%20North%20Announcement/feedTitle/GlobeNewswire%20-%20First%20North%20Announcement",
            "http://www.globenewswire.com/RssFeed/subjectcode/79-First%20North%20Information/feedTitle/GlobeNewswire%20-%20First%20North%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/18-Food/feedTitle/GlobeNewswire%20-%20Food",
            "http://www.globenewswire.com/RssFeed/subjectcode/19-Government%20News/feedTitle/GlobeNewswire%20-%20Government%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/20-Health/feedTitle/GlobeNewswire%20-%20Health",
            "http://www.globenewswire.com/RssFeed/subjectcode/21-Initial%20Public%20Offerings/feedTitle/GlobeNewswire%20-%20Initial%20Public%20Offerings",
            "http://www.globenewswire.com/RssFeed/subjectcode/22-Insider%2027s%20Buy%202fSell/feedTitle/GlobeNewswire%20-%20Insider%20s%20Buy,%20Sell",
            "http://www.globenewswire.com/RssFeed/subjectcode/66-Interim%20Information/feedTitle/GlobeNewswire%20-%20Interim%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/80-Investment%20Fund%20Information/feedTitle/GlobeNewswire%20-%20Investment%20Fund%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/83-Investment%20Opinion/feedTitle/GlobeNewswire%20-%20Investment%20Opinion",
            "http://www.globenewswire.com/RssFeed/subjectcode/23-Joint%20Venture/feedTitle/GlobeNewswire%20-%20Joint%20Venture",
            "http://www.globenewswire.com/RssFeed/subjectcode/24-Law%2026%20Legal%20Issues/feedTitle/GlobeNewswire%20-%20Law%20and%20Legal%20Issues",
            "http://www.globenewswire.com/RssFeed/subjectcode/25-Licensing%20Agreements/feedTitle/GlobeNewswire%20-%20Licensing%20Agreements",
            "http://www.globenewswire.com/RssFeed/subjectcode/26-Lifestyle/feedTitle/GlobeNewswire%20-%20Lifestyle",
            "http://www.globenewswire.com/RssFeed/subjectcode/59-Major%20Shareholder%20Announcements/feedTitle/GlobeNewswire%20-%20Major%20Shareholder%20Announcements",
            "http://www.globenewswire.com/RssFeed/subjectcode/67-Management%20Statements/feedTitle/GlobeNewswire%20-%20Management%20Statements",
            "http://www.globenewswire.com/RssFeed/subjectcode/81-Market%20Research%20Reports/feedTitle/GlobeNewswire%20-%20Market%20Research%20Reports",
            "http://www.globenewswire.com/RssFeed/subjectcode/27-Mergers%20And%20Acquisitions/feedTitle/GlobeNewswire%20-%20Mergers%20And%20Acquisitions",
            "http://www.globenewswire.com/RssFeed/subjectcode/64-Mutual%20Fund%20Information/feedTitle/GlobeNewswire%20-%20Mutual%20Fund%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/62-Net%20Asset%20Value/feedTitle/GlobeNewswire%20-%20Net%20Asset%20Value",
            "http://www.globenewswire.com/RssFeed/subjectcode/28-Other%20News/feedTitle/GlobeNewswire%20-%20Other%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/29-Partnerships/feedTitle/GlobeNewswire%20-%20Partnerships",
            "http://www.globenewswire.com/RssFeed/subjectcode/30-Patents/feedTitle/GlobeNewswire%20-%20Patents",
            "http://www.globenewswire.com/RssFeed/subjectcode/34-Politics/feedTitle/GlobeNewswire%20-%20Politics",
            "http://www.globenewswire.com/RssFeed/subjectcode/31-Pre%20Release%20Comments/feedTitle/GlobeNewswire%20-%20Pre%20Release%20Comments",
            "http://www.globenewswire.com/RssFeed/subjectcode/72-Press%20Releases/feedTitle/GlobeNewswire%20-%20Press%20Releases",
            "http://www.globenewswire.com/RssFeed/subjectcode/32-Product%202f%20Services%20Announcement/feedTitle/GlobeNewswire%20-%20Product%20,%20Services%20Announcement",
            "http://www.globenewswire.com/RssFeed/subjectcode/63-Prospectus%202fAnnouncement%20Of%20Prospectus/feedTitle/GlobeNewswire%20-%20Prospectus,%20Announcement%20Of%20Prospectus",
            "http://www.globenewswire.com/RssFeed/subjectcode/33-Proxy%20Statements%20And%20Analysis/feedTitle/GlobeNewswire%20-%20Proxy%20Statements%20And%20Analysis",
            "http://www.globenewswire.com/RssFeed/subjectcode/73-Regulatory%20Information/feedTitle/GlobeNewswire%20-%20Regulatory%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/35-Religion/feedTitle/GlobeNewswire%20-%20Religion",
            "http://www.globenewswire.com/RssFeed/subjectcode/36-Research%20Analysis%20And%20Reports/feedTitle/GlobeNewswire%20-%20Research%20Analysis%20And%20Reports",
            "http://www.globenewswire.com/RssFeed/subjectcode/37-Restructuring%202f%20Recapitalization/feedTitle/GlobeNewswire%20-%20Restructuring%20,%20Recapitalization",
            "http://www.globenewswire.com/RssFeed/subjectcode/38-Sports/feedTitle/GlobeNewswire%20-%20Sports",
            "http://www.globenewswire.com/RssFeed/subjectcode/39-Stock%20Market%20News/feedTitle/GlobeNewswire%20-%20Stock%20Market%20News",
            "http://www.globenewswire.com/RssFeed/subjectcode/40-Tax%20Issues%202fAccounting/feedTitle/GlobeNewswire%20-%20Tax%20Issues,%20Accounting",
            "http://www.globenewswire.com/RssFeed/subjectcode/43-Technical%20Analysis/feedTitle/GlobeNewswire%20-%20Technical%20Analysis",
            "http://www.globenewswire.com/RssFeed/subjectcode/41-Trade%20Show/feedTitle/GlobeNewswire%20-%20Trade%20Show",
            "http://www.globenewswire.com/RssFeed/subjectcode/68-Trading%20Information/feedTitle/GlobeNewswire%20-%20Trading%20Information",
            "http://www.globenewswire.com/RssFeed/subjectcode/42-Travel/feedTitle/GlobeNewswire%20-%20Travel",
            "http://www.globenewswire.com/RssFeed/subjectcode/82-Warrants%20And%20Certificates/feedTitle/GlobeNewswire%20-%20Warrants%20And%20Certificates",
            

        };

        private static string[] _errorContactDataList = new string[] {
            "'http://www.marketwired.com/mw/release.do?id=2130455&sourceType=3'",

"'http://www.marketwired.com/mw/release.do?id=2130927&sourceType=3'",
"'http://www.marketwired.com/mw/release.do?id=2131204&sourceType=3'",
"'http://www.marketwired.com/mw/release.do?id=2131175&sourceType=3'"
           };

        public MarketwiredRssParser(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            RssSource =  @"http://www.marketwire.com/rss/recentheadlines.xml";
            RssFeedName = @"Marketwired";
            RankedLanguage = rankedLanguageIdentifier;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;
            var allcontainer = doc.DocumentNode.SelectNodes("//div[@id='newsroom-copy']");
            if (allcontainer != null)
            {
                var container = allcontainer.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                        n.NodeType == HtmlNodeType.Text &&
                        n.ParentNode.Name != "script" &&
                        n.ParentNode.Name != "style");
                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            rssFeed.StoryCount = storyCount.StoryCount;
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
            var repo = new RssFeedRepository();
            foreach (var rssFeed in list)
            {
                try 
                {
                    if (repo.AnyById(rssFeed.Link)) continue;
                    var marketwire = new MarketwiredHtmlParser(rssFeed);

                    var contentData = marketwire.StartParse();
                    if (contentData == null) continue;

                    var dateLine = contentData.Item1;
                    var content = contentData.Item2;
                    
                    if (dateLine == null) continue;

                    rssFeed.Dateline = dateLine;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);
                }
            }
            return true;
        }


        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
            
            var repo = new RssFeedRepository();
            if (list == null || !list.Any())
               return false;
            int i = 0;
            foreach (var rssFeed in list)
            {
                i++;
                try
                {
                    var marketwire = new MarketwiredHtmlParser(rssFeed);

                    ContactInfoRepository repoC = new ContactInfoRepository();
                    repoC.Delete(rssFeed.Link);

                    //marketwire.FixHedlineContact(rssFeed);
                    //repo.Add(rssFeed);
                    //continue;


                    var contentData = marketwire.StartParse();
                    if (contentData == null) continue;

                    var dateLine = contentData.Item1;
                    var content = contentData.Item2;
                    if (dateLine == null) continue;
                    rssFeed.Dateline = dateLine;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);
                  
                }
            }
            return true;
        }


        public override bool Start()
        {
            Logger.WriteInformation(this.GetType().Name + " was started");
            try
            {
                //fix data
                var repo = new RssFeedRepository();
               // var errContact = repo.FindNotContact(RssFeedName);
                //var errContact = repo.FindErrContact(_errorContactDataList);
                //if (!TryReSaveRssList(errContact))
                //{

                //}
                //end fix data
                //return true;
                //Logger.WriteInformation(this.GetType().Name + " was fixed");

                foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                {
                }

                //var rssList2 = Parse().ToList();
                //if (!TrySaveRssList(rssList2))
                //{

                //}
                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            Logger.WriteInformation(this.GetType().Name + " was ended.");
            return true;
        }
    }
}
