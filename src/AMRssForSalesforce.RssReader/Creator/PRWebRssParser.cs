﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using Infrastructure.Logger;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    using System.Net;
    using System.IO;
    using System.Threading;
    using AMRssForSalesforce.Infrastructure.Helpers;
    
    public sealed class PRWebRssParser : RssParser
    {
        private static string[] _rssSources = new string[] {
            "http://www.prweb.com/rss2/entertainment.xml",
"http://www.prweb.com/rss2/artbooks.xml",
"http://www.prweb.com/rss2/9910celebrities.xml",
"http://www.prweb.com/rss2/aecountrymusic.xml",
"http://www.prweb.com/rss2/9912dance.xml",
"http://www.prweb.com/rss2/fineart.xml",
"http://www.prweb.com/rss2/aemagazines.xml",
"http://www.prweb.com/rss2/aemovies.xml",
"http://www.prweb.com/rss2/aemuseums.xml",
"http://www.prweb.com/rss2/music.xml",
"http://www.prweb.com/rss2/ipodmp3.xml",
"http://www.prweb.com/rss2/aenewstalk.xml",
"http://www.prweb.com/rss2/aeperforming.xml",
"http://www.prweb.com/rss2/9914photography.xml",
"http://www.prweb.com/rss2/television.xml",
"http://www.prweb.com/rss2/aevideogames.xml",
"http://www.prweb.com/rss2/aewebsites.xml",
"http://www.prweb.com/rss2/automotive.xml",
"http://www.prweb.com/rss2/autoaftermkt.xml",
"http://www.prweb.com/rss2/autoclassic.xml",
"http://www.prweb.com/rss2/autoconsumerpubs.xml",
"http://www.prweb.com/rss2/autobike.xml",
"http://www.prweb.com/rss2/autoracing.xml",
"http://www.prweb.com/rss2/autorv.xml",
"http://www.prweb.com/rss2/autorepair.xml",
"http://www.prweb.com/rss2/autotrades.xml",
"http://www.prweb.com/rss2/business.xml",
"http://www.prweb.com/rss2/advertising.xml",
"http://www.prweb.com/rss2/bizbooks.xml",
"http://www.prweb.com/rss2/9916consumerresearch.xml",
"http://www.prweb.com/rss2/bizcorp.xml",
"http://www.prweb.com/rss2/9918directmarketing.xml",
"http://www.prweb.com/rss2/bizecomm.xml",
"http://www.prweb.com/rss2/economy.xml",
"http://www.prweb.com/rss2/employmentcareers.xml",
"http://www.prweb.com/rss2/9920entrepreneurs.xml",
"http://www.prweb.com/rss2/nbizexecs.xml",
"http://www.prweb.com/rss2/finance.xml",
"http://www.prweb.com/rss2/franchise.xml",
"http://www.prweb.com/rss2/bizhr.xml",
"http://www.prweb.com/rss2/bizinsurance.xml",
"http://www.prweb.com/rss2/investment.xml",
"http://www.prweb.com/rss2/9920bizmanagement.xml",
"http://www.prweb.com/rss2/markets.xml",
"http://www.prweb.com/rss2/nbizacquire.xml",
"http://www.prweb.com/rss2/mlm.xml",
"http://www.prweb.com/rss2/nbizequity.xml",
"http://www.prweb.com/rss2/biz_pubcom.xml",
"http://www.prweb.com/rss2/bizpubs.xml",
"http://www.prweb.com/rss2/realestate.xml",
"http://www.prweb.com/rss2/retail.xml",
"http://www.prweb.com/rss2/smallbiz.xml",
"http://www.prweb.com/rss2/bizstartup.xml",
"http://www.prweb.com/rss2/stocks.xml",
"http://www.prweb.com/rss2/supermarkets.xml",
"http://www.prweb.com/rss2/trade.xml",
"http://www.prweb.com/rss2/businesstravel.xml",
"http://www.prweb.com/rss2/consumerwww.xml",
"http://www.prweb.com/rss2/9924bizwomen.xml",
"http://www.prweb.com/rss2/computers.xml",
"http://www.prweb.com/rss2/computermacintosh.xml",
"http://www.prweb.com/rss2/computerdatabases.xml",
"http://www.prweb.com/rss2/computergames.xml",
"http://www.prweb.com/rss2/computerinstruction.xml",
"http://www.prweb.com/rss2/opensource.xml",
"http://www.prweb.com/rss2/computermicrosotwindowspc.xml",
"http://www.prweb.com/rss2/computeros.xml",
"http://www.prweb.com/rss2/computerprogram.xml",
"http://www.prweb.com/rss2/computersecurity.xml",
"http://www.prweb.com/rss2/software.xml",
"http://www.prweb.com/rss2/computerutilities.xml",
"http://www.prweb.com/rss2/education.xml",
"http://www.prweb.com/rss2/educollege.xml",
"http://www.prweb.com/rss2/homeschooling.xml",
"http://www.prweb.com/rss2/eduk12.xml",
"http://www.prweb.com/rss2/edupostgrad.xml",
"http://www.prweb.com/rss2/edutech.xml",
"http://www.prweb.com/rss2/environment.xml",
"http://www.prweb.com/rss2/eventstradeshows.xml",
"http://www.prweb.com/rss2/government.xml",
"http://www.prweb.com/rss2/govedu.xml",
"http://www.prweb.com/rss2/elections.xml",
"http://www.prweb.com/rss2/enviroregs.xml",
"http://www.prweb.com/rss2/govbudget.xml",
"http://www.prweb.com/rss2/foreignconflict.xml",
"http://www.prweb.com/rss2/foriengpolicy.xml",
"http://www.prweb.com/rss2/9940governmentjudicial.xml",
"http://www.prweb.com/rss2/govlaw.xml",
"http://www.prweb.com/rss2/govlegislative.xml",
"http://www.prweb.com/rss2/govlocal.xml",
"http://www.prweb.com/rss2/9962military.xml",
"http://www.prweb.com/rss2/govnational.xml",
"http://www.prweb.com/rss2/politics.xml",
"http://www.prweb.com/rss2/govpublicsvc.xml",
"http://www.prweb.com/rss2/9942govsecurity.xml",
"http://www.prweb.com/rss2/govstate.xml",
"http://www.prweb.com/rss2/govtransport.xml",
"http://www.prweb.com/rss2/medical.xml",
"http://www.prweb.com/rss2/medicalabortion.xml",
"http://www.prweb.com/rss2/9956allergies.xml",
"http://www.prweb.com/rss2/medaltmed.xml",
"http://www.prweb.com/rss2/medasthma.xml",
"http://www.prweb.com/rss2/medcancer.xml",
"http://www.prweb.com/rss2/medcardiology.xml",
"http://www.prweb.com/rss2/medchiropractic.xml",
"http://www.prweb.com/rss2/meddental.xml",
"http://www.prweb.com/rss2/meddermatology.xml",
"http://www.prweb.com/rss2/meddiabetes.xml",
"http://www.prweb.com/rss2/med911.xml",
"http://www.prweb.com/rss2/medfamily.xml",
"http://www.prweb.com/rss2/medgeneral.xml",
"http://www.prweb.com/rss2/medgeriatrics.xml",
"http://www.prweb.com/rss2/medhospitals.xml",
"http://www.prweb.com/rss2/meddisease.xml",
"http://www.prweb.com/rss2/medim.xml",
"http://www.prweb.com/rss2/medhmo.xml",
"http://www.prweb.com/rss2/medproducts.xml",
"http://www.prweb.com/rss2/medmentalhealth.xml",
"http://www.prweb.com/rss2/medneurology.xml",
"http://www.prweb.com/rss2/mednursing.xml",
"http://www.prweb.com/rss2/mednutrition.xml",
"http://www.prweb.com/rss2/medobgyn.xml",
"http://www.prweb.com/rss2/9966occupationalsafety.xml",
"http://www.prweb.com/rss2/medpediatrics.xml",
"http://www.prweb.com/rss2/pharmaceuticals.xml",
"http://www.prweb.com/rss2/medpt.xml",
"http://www.prweb.com/rss2/9958plasticsurgery.xml",
"http://www.prweb.com/rss2/9970psychology.xml",
"http://www.prweb.com/rss2/medresearch.xml",
"http://www.prweb.com/rss2/9960sportsmedicine.xml",
"http://www.prweb.com/rss2/medsurgery.xml",
"http://www.prweb.com/rss2/veterinary.xml",
"http://www.prweb.com/rss2/medvision.xml",
"http://www.prweb.com/rss2/home.xml",
"http://www.prweb.com/rss2/homefinance.xml",
"http://www.prweb.com/rss2/9978loss.xml",
"http://www.prweb.com/rss2/homeinteriors.xml",
"http://www.prweb.com/rss2/landscapinggardening.xml",
"http://www.prweb.com/rss2/9944marriage.xml",
"http://www.prweb.com/rss2/9964money.xml",
"http://www.prweb.com/rss2/homeparenting.xml",
"http://www.prweb.com/rss2/9968pets.xml",
"http://www.prweb.com/rss2/9946taxes.xml",
"http://www.prweb.com/rss2/weddingbridal.xml",
"http://www.prweb.com/rss2/aerospacedefense.xml",
"http://www.prweb.com/rss2/agriculture.xml",
"http://www.prweb.com/rss2/enviroalt.xml",
"http://www.prweb.com/rss2/architectural.xml",
"http://www.prweb.com/rss2/construction.xml",
"http://www.prweb.com/rss2/indelectrical.xml",
"http://www.prweb.com/rss2/enviroenergy.xml",
"http://www.prweb.com/rss2/9936engineering.xml",
"http://www.prweb.com/rss2/indfood.xml",
"http://www.prweb.com/rss2/foodsafety.xml",
"http://www.prweb.com/rss2/9938fraud.xml",
"http://www.prweb.com/rss2/indfuneral.xml",
"http://www.prweb.com/rss2/gaming.xml",
"http://www.prweb.com/rss2/healthcare.xml",
"http://www.prweb.com/rss2/insurance.xml",
"http://www.prweb.com/rss2/leisurehospitality.xml",
"http://www.prweb.com/rss2/indlogistics.xml",
"http://www.prweb.com/rss2/machinery.xml",
"http://www.prweb.com/rss2/manucacturing.xml",
"http://www.prweb.com/rss2/maritime.xml",
"http://www.prweb.com/rss2/miningmetals.xml",
"http://www.prweb.com/rss2/nonprofit.xml",
"http://www.prweb.com/rss2/oilenergy.xml",
"http://www.prweb.com/rss2/paperforestproducts.xml",
"http://www.prweb.com/rss2/indphvac.xml",
"http://www.prweb.com/rss2/utilities.xml",
"http://www.prweb.com/rss2/restaurants.xml",
"http://www.prweb.com/rss2/telecom.xml",
"http://www.prweb.com/rss2/tobacco.xml",
"http://www.prweb.com/rss2/indtoy.xml",
"http://www.prweb.com/rss2/transportation.xml",
"http://www.prweb.com/rss2/legal.xml",
"http://www.prweb.com/rss2/law_attorneys.xml",
"http://www.prweb.com/rss2/law_cr.xml",
"http://www.prweb.com/rss2/law_gl.xml",
"http://www.prweb.com/rss2/law_ip.xml",
"http://www.prweb.com/rss2/law_firms.xml",
"http://www.prweb.com/rss2/law_re.xml",
"http://www.prweb.com/rss2/lifestyle.xml",
"http://www.prweb.com/rss2/lsbeauty.xml",
"http://www.prweb.com/rss2/9926coaching.xml",
"http://www.prweb.com/rss2/consumer.xml",
"http://www.prweb.com/rss2/9950datingsingles.xml",
"http://www.prweb.com/rss2/9952diet.xml",
"http://www.prweb.com/rss2/lsfashion.xml",
"http://www.prweb.com/rss2/foodbeverage.xml",
"http://www.prweb.com/rss2/consumergifts.xml",
"http://www.prweb.com/rss2/lshealthfitness.xml",
"http://www.prweb.com/rss2/consumerhobbies.xml",
"http://www.prweb.com/rss2/hotelresorts.xml",
"http://www.prweb.com/rss2/lspastimes.xml",
"http://www.prweb.com/rss2/lsrestaurants.xml",
"http://www.prweb.com/rss2/lspastimes.xml",
"http://www.prweb.com/rss2/lsrestaurants.xml",
"http://www.prweb.com/rss2/lsretire.xml",
"http://www.prweb.com/rss2/9972personalgrowth.xml",
"http://www.prweb.com/rss2/travel.xml",
"http://www.prweb.com/rss2/media.xml",
"http://www.prweb.com/rss2/blogging.xml",
"http://www.prweb.com/rss2/indbroadcast.xml",
"http://www.prweb.com/rss2/9928design.xml",
"http://www.prweb.com/rss2/9930graphicdesign.xml",
"http://www.prweb.com/rss2/9932industrialdesign.xml",
"http://www.prweb.com/rss2/9976seo.xml",
"http://www.prweb.com/rss2/podcasting.xml",
"http://www.prweb.com/rss2/9948printingindustry.xml",
"http://www.prweb.com/rss2/printmedia.xml",
"http://www.prweb.com/rss2/bizpr.xml",
"http://www.prweb.com/rss2/publishing.xml",
"http://www.prweb.com/rss2/radio.xml",
"http://www.prweb.com/rss2/rsscontentsyndication.xml",
"http://www.prweb.com/rss2/9934webdesign.xml",
"http://www.prweb.com/rss2/miscellaneous.xml",
"http://www.prweb.com/rss2/opinion.xml",
"http://www.prweb.com/rss2/podcastingannounce.xml",
"http://www.prweb.com/rss2/podcastingtools.xml",
"http://www.prweb.com/rss2/religionother.xml",
"http://www.prweb.com/rss2/scienceresearch.xml",
"http://www.prweb.com/rss2/sciastr.xml",
"http://www.prweb.com/rss2/scibi.xml",
"http://www.prweb.com/rss2/biotechnology.xml",
"http://www.prweb.com/rss2/chemical.xml",
"http://www.prweb.com/rss2/nanotechnology.xml",
"http://www.prweb.com/rss2/sciphys.xml",
"http://www.prweb.com/rss2/weather.xml",
"http://www.prweb.com/rss2/society.xml",
"http://www.prweb.com/rss2/societyaffirm.xml",
"http://www.prweb.com/rss2/socafricanamerican.xml",
"http://www.prweb.com/rss2/americapost911.xml",
"http://www.prweb.com/rss2/enviroanimal.xml",
"http://www.prweb.com/rss2/socasian.xml",
"http://www.prweb.com/rss2/societychildren.xml",
"http://www.prweb.com/rss2/religionchristian.xml",
"http://www.prweb.com/rss2/civilrights.xml",
"http://www.prweb.com/rss2/societycrime.xml",
"http://www.prweb.com/rss2/deathpenalty.xml",
"http://www.prweb.com/rss2/9980disabled.xml",
"http://www.prweb.com/rss2/socgay.xml",
"http://www.prweb.com/rss2/envirowarm.xml",
"http://www.prweb.com/rss2/societygun.xml",
"http://www.prweb.com/rss2/sochispanic.xml",
"http://www.prweb.com/rss2/humanrights.xml",
"http://www.prweb.com/rss2/religionislam.xml",
"http://www.prweb.com/rss2/socmen.xml",
"http://www.prweb.com/rss2/societynativeamerican.xml",
"http://www.prweb.com/rss2/enivroresource.xml",
"http://www.prweb.com/rss2/10254socpeople.xml",
"http://www.prweb.com/rss2/religion.xml",
"http://www.prweb.com/rss2/socseniors.xml",
"http://www.prweb.com/rss2/societyss.xml",
"http://www.prweb.com/rss2/religion_spirituality.xml",
"http://www.prweb.com/rss2/religion_spirituality.xml",
"http://www.prweb.com/rss2/socteen.xml",
"http://www.prweb.com/rss2/volunteer.xml",
"http://www.prweb.com/rss2/societywomen.xml",
"http://www.prweb.com/rss2/sports.xml",
"http://www.prweb.com/rss2/sportsbaseball.xml",
"http://www.prweb.com/rss2/sportsbike.xml",
"http://www.prweb.com/rss2/sportsboat.xml",
"http://www.prweb.com/rss2/sportsbowling.xml",
"http://www.prweb.com/rss2/sportsboxing.xml",
"http://www.prweb.com/rss2/sportfishing.xml",
"http://www.prweb.com/rss2/sportfootball.xml",
"http://www.prweb.com/rss2/sportsgolf.xml",
"http://www.prweb.com/rss2/sportshockey.xml",
"http://www.prweb.com/rss2/sportshunting.xml",
"http://www.prweb.com/rss2/sportsmartialarts.xml",
"http://www.prweb.com/rss2/sportsolympics.xml",
"http://www.prweb.com/rss2/sportsoutdoors.xml",
"http://www.prweb.com/rss2/sportsrugby.xml",
"http://www.prweb.com/rss2/sportssoccer.xml",
"http://www.prweb.com/rss2/sportswater.xml",
"http://www.prweb.com/rss2/sportswinter.xml",
"http://www.prweb.com/rss2/technology.xml",
"http://www.prweb.com/rss2/techcomputer.xml",
"http://www.prweb.com/rss2/electronics.xml",
"http://www.prweb.com/rss2/techentsoftware.xml",
"http://www.prweb.com/rss2/techgames.xml",
"http://www.prweb.com/rss2/techprinting.xml",
"http://www.prweb.com/rss2/techhardware.xml",
"http://www.prweb.com/rss2/techindustrial.xml",
"http://www.prweb.com/rss2/9974techinfo.xml",
"http://www.prweb.com/rss2/internet.xml",
"http://www.prweb.com/rss2/techmobile.xml",
"http://www.prweb.com/rss2/techmultimedia.xml",
"http://www.prweb.com/rss2/technano.xml",
"http://www.prweb.com/rss2/technetworking.xml",
"http://www.prweb.com/rss2/techgaming.xml",
"http://www.prweb.com/rss2/techgov.xml",
"http://www.prweb.com/rss2/techrobotics.xml",
"http://www.prweb.com/rss2/techsoftware.xml",
"http://www.prweb.com/rss2/techtelecom.xml",
"http://www.prweb.com/rss2/webmasters.xml"
         };
        public PRWebRssParser(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            //http://www.prweb.com/rss.htm
            RssSource = @"http://www.prweb.com/rss2/daily.xml";
            RssFeedName = @"PR Web";
            RankedLanguage = rankedLanguageIdentifier;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;

            var article = doc.DocumentNode.SelectNodes("//div[@class='article-text']");
            if (article != null)
            {
                var container = article.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }
            var title = doc.DocumentNode.SelectNodes("//h1[@class='article-title']");
            if (title != null)
            {
                IEnumerable<HtmlNode> nodes = title.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            rssFeed.StoryCount = storyCount.StoryCount;
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
            //StaticMediaCount
            var boxsMedia = doc.DocumentNode.SelectNodes("//div[@class='box-el-cont']");
            if(boxsMedia!= null)
            {
                foreach(var divM in boxsMedia)
                {
                    var h3 = divM.SelectSingleNode(".//h3");
                    if(h3!= null && Utils.NormalizationString(h3.InnerText) == "Media")
                    {
                        var boxcont = divM.SelectSingleNode(".//div[@class='box-cont']");
                        if(boxcont != null)
                        {
                            var media_a = boxcont.SelectNodes(".//a");
                            if(media_a != null)
                            {
                                rssFeed.StaticMediaCount = media_a.Count;
                            }
                        }
                    }
                }
            }
            if (!rssFeed.StaticMediaCount.HasValue)
            {
                rssFeed.StaticMediaCount = 0;
            }
            //MultiMediaCount
            var boxsMediaVideo = doc.DocumentNode.SelectNodes("//div[@itemprop='video']");
            if (boxsMediaVideo != null)
                rssFeed.MultiMediaCount = boxsMediaVideo.Count;
            else
                rssFeed.MultiMediaCount = 0;
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {

            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();

            foreach (var rssFeed in list)
            {
                try
                {
                    if (repo.AnyById(rssFeed.Link)) continue;

                    var parcerContent = new PRWebHtmlParser(rssFeed.Link);

                    if (!parcerContent.hasContent) continue;

                    var contentData = parcerContent.ParserContact(rssFeed);
                    if (contentData == null) continue;

                    var contacts = contentData.Item1;
                    var content = contentData.Item2;

                    if (contacts == null || !contacts.Any()) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                    foreach (var c in contacts)
                    {
                        c.NewsReleaseDate = rssFeed.DateTime.ToString();
                        contactRep.Add(c);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }


            return true;
        }

        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {

            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();

            if (list == null || !list.Any())
                return false;
            int i = 0;
            foreach (var rssFeed in list)
            {
                try
                {
                    i++;
                    var parcerContent = new PRWebHtmlParser(rssFeed.Link);

                    if (!parcerContent.hasContent) continue;

                    //parcerContent.FixHedlineContact(rssFeed);
                    //repo.Add(rssFeed);
                    //continue;

                    var contentData = parcerContent.ParserContact(rssFeed);
                    if (contentData == null) continue;

                    var contacts = contentData.Item1;
                    var content = contentData.Item2;

                    if (contacts != null && contacts.Any())
                    {
                        rssFeed.Language = "eng";
                        rssFeed.English = true;
                        CalculateStoryCount(content, rssFeed);
                        repo.Add(rssFeed);
                        contactRep.Delete(rssFeed.Link);
                        foreach (Contact c in contacts)
                        {
                            c.NewsReleaseDate = rssFeed.DateTime.ToString();
                            contactRep.Add(c);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }


            return true;
        }

        private static string[] _errorContactDataList = new string[] { 
            
"'http://www.prweb.com/releases/2016/06/prweb13454145.htm'",
 "'http://www.prweb.com/releases/2016/06/prweb13458013.htm'",
"'http://www.prweb.com/releases/2016/06/prweb13458013.htm'",
"'http://www.prweb.com/releases/2016/05/prweb13448729.htm'",
"'http://www.prweb.com/releases/2016/05/prweb13436094.htm'"};

        private static int maxPage = 100;

        private static string htmlNews = "http://www.prweb.com/recentnews/{0}.htm";
        public override bool Start()
        {
            Logger.WriteInformation(this.GetType().Name + " was started");
            try
            {
              
                var repo = new RssFeedRepository();
             //   var errContact = repo.FindNotContact(RssFeedName);
                var contactRep = new ContactInfoRepository();
               
                 // fix data
               // var errContact = repo.FindNotContact(RssFeedName);
                 var errContact = repo.FindErrContact(_errorContactDataList);
                if (!TryReSaveRssList(errContact))
                {

                }
               // end fix data
                //Logger.WriteInformation(this.GetType().Name + " was fixed");
                //var rssList = Parse().ToList();
                //if (!TrySaveRssList(rssList))
                //{

                //}
               // return true;
                //foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                //{
                //}
                
                //get from html

                //Logger.WriteInformation(this.GetType().Name + " start html read");

                int countNew = 0;
                for (int i = 1; i < maxPage; i++)
                {
                    var urlpage = string.Format(htmlNews, i);

                    List<RssFeed> rssFeeds = GetNwesFromHtml(urlpage);

                    if (rssFeeds == null)
                        break;
                    

                    foreach (var rssFeed in rssFeeds)
                    {
                        try
                        {
                            if (repo.AnyById(rssFeed.Link))
                            {
                                continue;
                            }

                            countNew++;

                            var parcerContent = new PRWebHtmlParser(rssFeed.Link);

                            if (!parcerContent.hasContent) continue;

                            var contentData = parcerContent.ParserContact(rssFeed);
                            if (contentData == null) continue;

                            var contacts = contentData.Item1;
                            var content = contentData.Item2;

                            if (contacts == null || !contacts.Any()) continue;
                            rssFeed.Language = "eng";
                            rssFeed.English = true;
                            CalculateStoryCount(content, rssFeed);
                            repo.Add(rssFeed);
                            contactRep.Delete(rssFeed.Link);
                            foreach (Contact c in contacts)
                            {
                                c.NewsReleaseDate = rssFeed.DateTime.ToString();
                                contactRep.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteFatal(ex);

                        }
                    }

                    //if (countNew == 0)
                    //{
                    //    notNewCountPage++;
                    //    if (notNewCountPage > 10)
                    //        return true;
                    //}
                }

               // Logger.WriteDebug("New from html " + countNew.ToString());
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            finally
            {
                Logger.WriteInformation(this.GetType().Name + " was ended.");
            }
            return true;
        }

        private List<RssFeed> GetNwesFromHtml(string urlpage)
        {
            List<RssFeed> rssFeeds = new List<RssFeed>();

            var res = HtmlHelpers.GetResponse(urlpage);
            if (res == null)
                return rssFeeds;

            var htmlContent = res.Html;
            if (htmlContent == null)
                return rssFeeds;


            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var countP = 0;
            var countPnoEnglish = 0;
            var allNoEnglish = 0;

            var rssDivs = doc.DocumentNode.SelectNodes("//div[@class='article-box-cont']");

            if (rssDivs != null)
            {
                foreach (var div in rssDivs)
                {
                    var rssFeed = new RssFeed();
                    
                    var pHeadLine = div.SelectSingleNode(".//h1");
                    if (pHeadLine == null)
                    {
                        Logger.WriteError(string.Format("Didn't finde hedline in {0}", urlpage));
                        continue;
                    }

                    var headline = pHeadLine.InnerText;

                    var aHeadline = div.SelectSingleNode(".//a");
                    if (aHeadline == null || aHeadline.Attributes["href"] == null)
                    {
                        Logger.WriteError(string.Format("Didn't finde hedline in {0}", urlpage));
                        continue;
                    }

                    var url = aHeadline.Attributes["href"].Value;
                    if (!url.Contains("http://www.prweb.com"))
                        url = "http://www.prweb.com" + url;

                  
                    rssFeed.HeadLine = headline;
                    rssFeed.Link = url;
                    rssFeed.Provider = this.RssFeedName;
                    rssFeed.Source = urlpage;
                    rssFeed.DateTime = DateTime.Now;

                    rssFeeds.Add(rssFeed);
                }
            }

            return rssFeeds;
        }
       
    }
}
