﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using HtmlParser.Creator;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;

    public sealed class BusinessWireRssParsercs: RssParser
    {
        private static string[] RssSourceS = new string[] {
            "http://feeds.businesswire.com/BW/Automotive_News-rss",
            "http://feeds.businesswire.com/BW/Communications_News-rss",
            "http://feeds.businesswire.com/BW/Construction_and_Property_News-rss",
            "http://feeds.businesswire.com/BW/Consumer_News-rss",
            "http://feeds.businesswire.com/BW/Corporate_Social_Responsibility_News-rss",
            "http://feeds.businesswire.com/BW/Defense_News-rss",
            "http://feeds.businesswire.com/BW/Education_News-rss",
            "http://feeds.businesswire.com/BW/Energy_News-rss",
            "http://feeds.businesswire.com/BW/Entertainment_News-rss",
            "http://feeds.businesswire.com/BW/Environment_News-rss",
            "http://feeds.businesswire.com/BW/Feature_News-rss",
            "http://feeds.businesswire.com/BW/Health_News-rss",
            "http://feeds.businesswire.com/BW/Manufacturing_News-rss",
            "http://feeds.businesswire.com/BW/News_with_Multimedia-rss",
            "http://feeds.businesswire.com/BW/Natural_Resources_News-rss",
            "http://feeds.businesswire.com/BW/Philanthropy_News-rss",
            "http://feeds.businesswire.com/BW/Professional_Services_News-rss",
            "http://feeds.businesswire.com/BW/Public_Policy/Government_News-rss",
            "http://feeds.businesswire.com/BW/Retail_News-rss",
            "http://feeds.businesswire.com/BW/Science_News-rss",
            "http://feeds.businesswire.com/BW/Sports_News-rss",
            "http://feeds.businesswire.com/BW/Technology_News-rss",
            "http://feeds.businesswire.com/BW/Trade_Shows_News-rss",
            "http://feeds.businesswire.com/BW/Transport_News-rss",
            "http://feeds.businesswire.com/BW/Travel_News-rss",
            "http://feeds.businesswire.com/BW/Webcasts-rss"
        };
        private static string[] _errorContactDataList = new string[] {
            "'http://feeds.businesswire.com/click.phdo?i=d24a495e870bcf3938146df7f66fe13a'",
"'http://feeds.businesswire.com/click.phdo?i=ed838ee5646435b2eabc787701cb4f98'",
"'http://feeds.businesswire.com/click.phdo?i=da747773a32791e834385ed081febc35'",

        };
        public BusinessWireRssParsercs(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            RssSource = @"http://feeds.businesswire.com/BW/Health_News-rss";
            RssFeedName = @"Business Wire";
            RankedLanguage = rankedLanguageIdentifier;
        }


        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;

            var article = ( doc.DocumentNode.SelectNodes("//div[@itemprop='articleBody']") ??  
                            doc.DocumentNode.SelectNodes("//div[bw-release-body  ]")) ??
                            doc.DocumentNode.SelectNodes("//div[@class='bw-release-story']");
            if (article != null)
            {
                var container = article.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                        .Where(n =>
                            n.NodeType == HtmlNodeType.Text &&
                            n.ParentNode.Name != "script" &&
                            n.ParentNode.Name != "style");
                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }

            var title = doc.DocumentNode.SelectNodes("//h1[@class='epi-fontLg']") ?? doc.DocumentNode.SelectNodes("//h1[@class='epi-fontLg bwalignc']");
            if (title != null)
            {
                IEnumerable<HtmlNode> nodes = title.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }

            var contact = doc.DocumentNode.SelectNodes("//div[@class='bw-release-contact']");
            if (contact != null)
            {
                IEnumerable<HtmlNode> nodes = contact.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }

            
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
            rssFeed.StoryCount = storyCount.StoryCount;
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
           
            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();

            foreach (var rssFeed in list)
            {
                try
                {
                    if (repo.AnyById(rssFeed.Link)) continue;

                    var parcerContent = new BusinessWireHtmlParser(rssFeed.Link);

                    if (!parcerContent.hasContent) continue;

                    var contentData = parcerContent.ParserContact(rssFeed);
                    
                    if (contentData == null) continue;

                    List<Contact> contacts = contentData.Item1;
                    var content = contentData.Item2;

                    if (contacts == null || !contacts.Any()) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);

                    foreach (Contact c in contacts)
                    {
                        c.NewsReleaseDate = rssFeed.DateTime.ToString();
                        contactRep.Add(c);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }


            return true;
        }

        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
           
                var repo = new RssFeedRepository();
                var contactRep = new ContactInfoRepository();

            if (list == null || !list.Any())
                return false;

                foreach (var rssFeed in list)
                {
                    try 
                    { 
                        var parcerContent = new BusinessWireHtmlParser(rssFeed.Link);
                        if (!parcerContent.hasContent) continue;


                        //parcerContent.FixHedlineContact(rssFeed);
                        //repo.Add(rssFeed);
                        //continue;

                        var contentData = parcerContent.ParserContact(rssFeed);
                        if (contentData == null) continue;

                        var contacts = contentData.Item1;
                        var content = contentData.Item2;

                        if (contacts == null || !contacts.Any()) continue;
                        rssFeed.Language = "eng";
                        rssFeed.English = true;
                        CalculateStoryCount(content, rssFeed);
                        repo.Add(rssFeed);
                        contactRep.Delete(rssFeed.Link);

                        foreach (var c in contacts)
                        {
                            c.NewsReleaseDate = rssFeed.DateTime.ToString();
                            contactRep.Add(c);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteFatal(ex);

                    }
            }
     
            
            return true;
        }

        public override bool Start()
        {
            //////////////////////
            //var parcerContent = new AMRssForSalesforce.HtmlParser.Creator.BusinessWireHtmlParser("http://feeds.businesswire.com/click.phdo?i=04215823d5cee3ee9f0ac16ef6cf5829"); 
            var parcerContent = new AMRssForSalesforce.HtmlParser.Creator.BusinessWireHtmlParser("https://www.businesswire.com/portal/site/home/news/");
            var contentData = parcerContent.ParserContact(new AMRssForSalesforce.Model.RssFeed());
            if (contentData != null)
            {
                var contacts = contentData.Item1;
                var content = contentData.Item2;
            }
            ///////////////////////////////////

            Logger.WriteInformation(this.GetType().Name + " was started");
            try
            {
               var repo = new RssFeedRepository();
             //  var errContact = repo.FindNotContact(RssFeedName);
               //var errContact = repo.FindErrContact(_errorContactDataList);
               //if (!TryReSaveRssList(errContact))
               //{

               //}

              //  return true;

                foreach (var url in RssSourceS)
                {
                    var rssList = Parse(url).ToList();
                    if (!TrySaveRssList(rssList))
                    {

                    }
                }

                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            finally
            {
                Logger.WriteInformation(this.GetType().Name + " was ended.");
            }
            return true;
        }
    }
}
