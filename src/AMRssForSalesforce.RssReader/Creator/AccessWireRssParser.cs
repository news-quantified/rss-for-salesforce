﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HtmlParser.Creator;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using Infrastructure.Logger;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    using AMRssForSalesforce.Infrastructure.Helpers;
    using System.Globalization;

    public sealed class AccessWireRssParser : RssParser
    {
        private static string[] _rssSources = new string[] {

            //"https://www.accesswire.com/api/newsroom.ashx?industryid=16",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=17",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=18",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=19",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=20",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=22",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=1",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=2",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=3",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=14",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=15",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=23",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=24",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=8",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=7",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=12",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=10",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=26",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=27",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=28",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=13",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=25",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=5",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=4",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=6",
            //"https://www.accesswire.com/api/newsroom.ashx?industryid=9"
            //"https://www.accesswire.com/newsroom/"
            //"http://proxy9747.my-addr.org/myaddrproxy.php/https/www.accesswire.com/rss/index.xml",
            //"http://proxy9747.my-addr.org/myaddrproxy.php/https/www.accesswire.com/rss/articles.ashx?reset=1"
            //"https://www.accesswire.com/rss/index.xml",
            //"https://www.accesswire.com/rss",
            //"https://www.accesswire.com/rss/articles.ashx?reset=1"
            //"https://www.accesswire.com/rssfeed.aspx"
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Aerospace%20&%20Defense",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Agriculture",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Automotive",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Banking%20&%20Financial%20Services",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Biotechnology",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Business%20&%20Professional%20Services",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Chemicals",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Clean%20Technology",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Closed%20End%20Funds%20&%20Trusts",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Computers,%20Technology%20&%20Internet",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Consumer%20&%20Retail%20Products",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Electronics%20&%20Engineering",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Exchange%20Traded%20Funds",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Food%20&%20Beverage%20Products",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Healthcare%20&%20Pharmaceutical",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Industrial%20&%20Manufacturing",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Metals%20&%20Mining",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Oil,%20Gas%20&%20Energy",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Paper%20&%20Packaging",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Publishing%20&%20Media",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Real%20Estate",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Sports,%20Leisure%20&%20Entertainment",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Telecommunications",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Transportation",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Travel",
            "https://www.accesswire.com/users/api/newsroom?articleindustry=Utilities",
        };

        //to do err contact 
        private static string[] _errorContactDataList = new string[] {

"'http://www.accesswire.com/viewarticle.aspx?id=440271'",
"'http://www.accesswire.com/viewarticle.aspx?id=438394'"
         };

        public AccessWireRssParser(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            //add https://www.accesswire.com/rss/index.xml
            //RssSource = @"https://www.accesswire.com/rss/articles.ashx?reset=1";
            RssSource = @"https://www.accesswire.com/newsroom/";
            RssFeedName = @"Access Wire";
            RankedLanguage = rankedLanguageIdentifier;
        }


        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
            
            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();

            foreach (var rssFeed in list)
            {
                Console.WriteLine("TrySaveRssList Start - " + rssFeed.Link);
                try
                {
                    if (repo.AnyById_New(rssFeed.Link)) continue;

                    var parcerContent = new AccessWireHtmlParser_rn(rssFeed.Link);
                    if (!parcerContent.hasContent) continue;
                    var contentData = parcerContent.ParserContact(rssFeed);
                    if (contentData == null) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    var contacts = contentData.Item1;
                    var content = contentData.Item2;
                    if (contacts == null || !contacts.Any()) continue;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add_New(rssFeed);
                        
                    foreach (var c in contacts)
                    {
                        c.NewsReleaseDate = rssFeed.DateTime.ToString();
                        contactRep.Add_New(c);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TrySaveRssList Error - " + rssFeed.Link);
                    Console.WriteLine(ex.Message);
                    Logger.WriteFatal(ex);
                }
                Console.WriteLine("TrySaveRssList End - " + rssFeed.Link);
            }
            return true;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;

            var article = doc.DocumentNode.SelectNodes("//div[@class='articlepreview']");
            if (article != null)
            {
                var container = article.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }
            var title = doc.DocumentNode.SelectNodes("//span[@class='artitles']");
            if (title != null)
            {
                IEnumerable<HtmlNode> nodes = title.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }

            rssFeed.StoryCount = storyCount.StoryCount;
        }


        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
           
                var repo = new RssFeedRepository();
                var contactRep = new ContactInfoRepository();

            if (list == null || !list.Any())
                return false;

            foreach (var rssFeed in list)
            {
                try
                {

                    var parcerContent = new AccessWireHtmlParser_rn(rssFeed.Link);
                    if (parcerContent.hasContent)
                    {
                        //parcerContent.FixHedlineContact(rssFeed);
                        //repo.Add(rssFeed);
                        //continue;

                        var contentData = parcerContent.ParserContact(rssFeed);
                        
                        if (contentData == null) continue;
                        
                        List<Contact> contacts = contentData.Item1;
                        var content = contentData.Item2;

                        
                        if (contacts != null && contacts.Any())
                        {
                            rssFeed.Language = "eng";
                            rssFeed.English = true;
                            CalculateStoryCount(content, rssFeed);
                            repo.Add(rssFeed);
                            contactRep.Delete(rssFeed.Link);
                            foreach (Contact c in contacts)
                            {
                                c.NewsReleaseDate = rssFeed.DateTime.ToString();
                                contactRep.Add(c);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }

            
            return true;
        }

        public override bool Start()
        {
            Logger.WriteInformation(this.GetType().Name + " was started");
            int i = 0;
            try
            {
                //i++;
                ////fix data
                var repo = new RssFeedRepository();
                //  var errContact = repo.FindNotContact(RssFeedName);
                //   var errContact = repo.FindErrContact(_errorContactDataList);
                //if (!TryReSaveRssList(errContact))
                //{

                //}

                // return true;
                //end fix data



                //var rssList = Parse().ToList();
                //if (!TrySaveRssList(rssList))
                //{

                //}

                foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                {
                }
                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
                //return false;

                //List<RssFeed> list = new List<RssFeed>();
                //foreach (var rssList in _rssSources)
                //{
                //    list.AddRange(GetNwesFromHtml(rssList));
                //    Console.WriteLine("Added - " + rssList);
                //}

                //TrySaveRssList(list);

            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            finally
            {
                Logger.WriteInformation(this.GetType().Name + " was ended.");
            }
            return true;
        }

        private List<RssFeed> GetNwesFromHtml(string urlpage)
        {
            List<RssFeed> list = new List<RssFeed>();

            var res = HtmlHelpers.GetResponse(urlpage);
            if (res == null)
            {
                return list;
            }

            var htmlContent = res.Html;
            if (htmlContent == null)
            {
                return list;
            }

            var articlegroups = htmlContent.Split(new string[] { "<div class=\"articlegroup\">" }, StringSplitOptions.None);
            if (articlegroups != null && articlegroups.Count() > 0)
            {
                foreach(string article in articlegroups)
                {
                    RssFeed rss = new RssFeed();
                    rss.Provider = this.RssFeedName;
                    rss.Source = urlpage;

                    if (article.Contains("<div class=\"headlinelink\"><a href=\""))
                    {
                        rss.Link = article.Split(new string[] { "<div class=\"headlinelink\"><a href=\"" }, StringSplitOptions.None).Last()
                                          .Split(new string[] { "\"" }, StringSplitOptions.None).First();
                    }
                    if (article.Contains("\" class=\"headlinelink\">"))
                    {
                        rss.HeadLine = article.Split(new string[] { "\" class=\"headlinelink\">" }, StringSplitOptions.None).Last()
                                          .Split(new string[] { "<" }, StringSplitOptions.None).First();
                    }
                    if (article.Contains("\" class=\"datelink\">"))
                    {
                        string datestr = article.Split(new string[] { "\" class=\"datelink\">" }, StringSplitOptions.None).Last()
                                                .Split(new string[] { "<" }, StringSplitOptions.None).First()
                                                .Replace(" EST", string.Empty)
                                                .Replace(" EDT", string.Empty)
                                                .Replace(" ET", string.Empty);

                        try
                        {
                            rss.DateTime = DateTime.ParseExact(datestr, "dddd, MMMM d, yyyy h:mm tt", CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        catch
                        {
                            rss.DateTime = DateTime.Now;
                        }
                        
                    }

                    if (!string.IsNullOrEmpty(rss.Link) && !string.IsNullOrEmpty(rss.HeadLine))
                    {
                        list.Add(rss);
                    }
                }
            }

            return list;
        }
    }
}
