﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using HtmlParser.Creator;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    public sealed class PRnewswireNewsRssParser : RssParser
    {
        //to do add
        //http://www.prnewswire.com/rss/
        private static string[] _rssSources = new string[] {
            //"http://www.prnewswire.com/rss/all-news-releases-from-PR-newswire-news.rss",
            //"http://www.prnewswire.com/rss/english-releases-news.rss",
            //"http://prnewswire.mediaroom.com/index.php?s=29517&pagetemplate=rss",
            //"http://prnewswire.mediaroom.com/index.php?s=29508&pagetemplate=rss",
            //"http://prnewswire.mediaroom.com/index.php?s=29533&pagetemplate=rss",
            //"http://prnewswire.com/rss/webinars/all-on-demand-webinars.rss",
            //"http://www.prnewswire.com/rss/news-for-investors-from-PR-Newswire-news.rss"

            //"http://www.prnewswire.com/rss/all-news-releases-from-PR-newswire-news.rss",
            "http://www.prnewswire.com/rss/automotive-transportation/all-automotive-transportation-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/aerospace-defense-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/air-freight-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/airlines-aviation-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/automotive-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/maritime-shipbuilding-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/railroads-intermodal-transportation-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/transportation-trucking-railroad-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/travel-news.rss",
"http://www.prnewswire.com/rss/auto-transportation/trucking-road-transportation-news.rss",
"http://www.prnewswire.com/rss/business-technology/all-business-technology-news.rss",
"http://www.prnewswire.com/rss/business-technology/broadcast-tech-news.rss",
"http://www.prnewswire.com/rss/business-technology/computer-electronics-news.rss",
"http://www.prnewswire.com/rss/business-technology/computer-hardware-news.rss",
"http://www.prnewswire.com/rss/business-technology/computer-software-news.rss",
"http://www.prnewswire.com/rss/business-technology/electronic-commerce-news.rss",
"http://www.prnewswire.com/rss/business-technology/electronic-components-news.rss",
"http://www.prnewswire.com/rss/business-technology/electronic-design-automation-news.rss",
"http://www.prnewswire.com/rss/business-technology/electronics-performance-measurement.rss",
"http://www.prnewswire.com/rss/business-technology/high-tech-security.rss",
"http://www.prnewswire.com/rss/business-technology/internet-technology-news.rss",
"http://www.prnewswire.com/rss/business-technology/nanotechnology-news.rss",
"http://www.prnewswire.com/rss/business-technology/networks-news.rss",
"http://www.prnewswire.com/rss/business-technology/peripherals-news.rss",
"http://www.prnewswire.com/rss/business-technology/rfid-news.rss",
"http://www.prnewswire.com/rss/business-technology/semantic-web-news.rss",
"http://www.prnewswire.com/rss/business-technology/semiconductors-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/all-consumer-products-retail-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/animals-pet-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/beers-wines-spirits-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/beverages-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/bridal-services.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/cosmetics-personal-care-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/fashion-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/food-beverages-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/furniture-furnishings-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/home-improvements-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/household-products-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/household-consumer-cosmetics-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/jewelry-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/non-alcoholic-beverages-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/office-products-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/organic-food-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/product-recalls-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/restaurants-news.rss",
"http://www.prnewswire.com/rss/consumer-products/retail-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/supermarkets-news.rss/rss/consumer-products-retail/supermarkets-news.rss",
"http://www.prnewswire.com/rss/consumer-products-retail/toys-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/all-consumer-technology-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/computer-electronics-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/computer-hardware-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/computer-software-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/consumer-electronics-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/electronic-commerce-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/electronic-gaming-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/mobile-entertainment-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/multimedia-internet-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/peripherals-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/website-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/social-media-news.rss",
"http://www.prnewswire.com/rss/consumer-technology/wireless-communications-news.rss",
"http://www.prnewswire.com/rss/energy/all-energy-news.rss",
"http://www.prnewswire.com/rss/energy/alternative-energies-news.rss",
"http://www.prnewswire.com/rss/energy/chemical-news.rss",
"http://www.prnewswire.com/rss/energy/electrical-utilities-news.rss",
"http://www.prnewswire.com/rss/energy/gas-news.rss",
"http://www.prnewswire.com/rss/energy/mining-news.rss",
"http://www.prnewswire.com/rss/energy/mining-metals-news.rss",
"http://www.prnewswire.com/rss/energy/oil-energy-news.rss",
"http://www.prnewswire.com/rss/energy/oil-gas-discoveries-news.rss",
"http://www.prnewswire.com/rss/energy/utilities-news.rss",
"http://www.prnewswire.com/rss/energy/water-utilities-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/all-entertainment-media-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/advertising-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/art-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/books-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/entertainment-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/film-motion-picture-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/magazines-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/music-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/publishing-information-services-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/radio-news.rss",
"http://www.prnewswire.com/rss/entertainment-media/television-news.rss",
"http://www.prnewswire.com/rss/environment/all-environment-news.rss",
"http://www.prnewswire.com/rss/environment/conservation-recycling-news.rss",
"http://www.prnewswire.com/rss/environment/environmental-issues-news.rss",
"http://www.prnewswire.com/rss/environment/environmental-policy-news.rss",
"http://www.prnewswire.com/rss/environment/environmental-products-services-news.rss",
"http://www.prnewswire.com/rss/environment/green-technology-news.rss",
"http://www.prnewswire.com/rss/financial-services/all-financial-services-news.rss",
"http://www.prnewswire.com/rss/financial-services/accounting-news-issues-news.rss",
"http://www.prnewswire.com/rss/financial-services/acquisitions-mergers-takeovers-news.rss",
"http://www.prnewswire.com/rss/financial-services/banking-financial-services-news.rss",
"http://www.prnewswire.com/rss/financial-services/bankruptcy-news.rss",
"http://www.prnewswire.com/rss/financial-services/bond-stock-ratings-news.rss",
"http://www.prnewswire.com/rss/financial-services/dividends-news.rss",
"http://www.prnewswire.com/rss/financial-services/earnings-news.rss",
"http://www.prnewswire.com/rss/financial-services/earnings-forecasts-projections-news.rss",
"http://www.prnewswire.com/rss/financial-services/financing-agreements-news.rss",
"http://www.prnewswire.com/rss/financial-services/insurance-news.rss",
"http://www.prnewswire.com/rss/financial-services/investments-opinions-news.rss",
"http://www.prnewswire.com/rss/financial-services/joint-ventures-news.rss",
"http://www.prnewswire.com/rss/financial-services/mutual-funds-news.rss",
"http://www.prnewswire.com/rss/financial-services/otc-small-cap-news.rss",
"http://www.prnewswire.com/rss/financial-services/real-estate-news.rss",
"http://www.prnewswire.com/rss/financial-services/restructuring-recapitalization-news.rss",
"http://www.prnewswire.com/rss/financial-services/sales-reports-news.rss",
"http://www.prnewswire.com/rss/financial-services/shareholders-rights-plans-news.rss",
"http://www.prnewswire.com/rss/financial-services/stock-offering-news.rss",
"http://www.prnewswire.com/rss/financial-services/stock-split-news.rss",
"http://www.prnewswire.com/rss/financial-services/venture-capital-news.rss",
"http://www.prnewswire.com/rss/general-business/all-general-business-news.rss",
"http://www.prnewswire.com/rss/general-business/agency-roster-news.rss",
"http://www.prnewswire.com/rss/general-business/awards-news.rss",
"http://www.prnewswire.com/rss/general-business/commercial-real-estate-news.rss",
"http://www.prnewswire.com/rss/general-business/conference-call-announcements-news.rss",
"http://www.prnewswire.com/rss/general-business/corporate-expansion-news.rss",
"http://www.prnewswire.com/rss/general-business/earnings-news.rss",
"http://www.prnewswire.com/rss/general-business/human-resource-workforce-management-news.rss",
"http://www.prnewswire.com/rss/general-business/licensing-news.rss",
"http://www.prnewswire.com/rss/general-business/new-products-services-news.rss",
"http://www.prnewswire.com/rss/general-business/obituaries.rss",
"http://www.prnewswire.com/rss/general-business/outsourcing-businesses-news.rss",
"http://www.prnewswire.com/rss/general-business/overseas-real-estate-news.rss",
"http://www.prnewswire.com/rss/general-business/personnel-announcements-news.rss",
"http://www.prnewswire.com/rss/general-business/real-estate-transactions-news.rss",
"http://www.prnewswire.com/rss/general-business/residential-real-estate-news.rss",
"http://www.prnewswire.com/rss/general-business/small-business-services-news.rss",
"http://www.prnewswire.com/rss/general-business/socially-responsible-investing-news.rss",
"http://www.prnewswire.com/rss/general-business/surveys-polls-research-news.rss",
"http://www.prnewswire.com/rss/general-business/trade-show-news.rss",
"http://www.prnewswire.com/rss/health/all-health-news.rss",
"http://www.prnewswire.com/rss/health/biometrics-news.rss",
"http://www.prnewswire.com/rss/health/biotechnology-news.rss",
"http://www.prnewswire.com/rss/health/clinical-trials-medial-discoveries-news.rss",
"http://www.prnewswire.com/rss/health/dentistry-news.rss",
"http://www.prnewswire.com/rss/health/health-care-hospitals-news.rss",
"http://www.prnewswire.com/rss/health/health-insurance-news.rss",
"http://www.prnewswire.com/rss/health/infection-control-news.rss",
"http://www.prnewswire.com/rss/health/medical-equipment-news.rss",
"http://www.prnewswire.com/rss/health/medical-pharmaceuticals-news.rss",
"http://www.prnewswire.com/rss/health/mental-health-news.rss",
"http://www.prnewswire.com/rss/health/pharmaceuticals-news.rss",
"http://www.prnewswire.com/rss/health/supplemental-medicine-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/all-heavy-industry-manufacturing-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/aerospace-defense-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/agriculture-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/chemical-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/construction-building-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/hvac-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/machine-tools-metalworking-metallury-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/machinery-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/mining-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/mining-metals-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/paper-forest-products-containers-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/precious-metals-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/textiles-news.rss",
"http://www.prnewswire.com/rss/heavy-industry-manufacturing/tobacco-news.rss",
"http://www.prnewswire.com/rss/multicultural/all-multicultural-news.rss",
"http://www.prnewswire.com/rss/multicultural/african-american-related-news.rss",
"http://www.prnewswire.com/rss/multicultural/asian-related-news.rss",
"http://www.prnewswire.com/rss/multicultural/children-related-news.rss",
"http://www.prnewswire.com/rss/multicultural/handicapped-disabled.rss",
"http://www.prnewswire.com/rss/multicultural/hispanic-oriented-news.rss",
"http://www.prnewswire.com/rss/multicultural/native-american.rss",
"http://www.prnewswire.com/rss/multicultural/religion.rss",
"http://www.prnewswire.com/rss/multicultural/senior-citizens.rss",
"http://www.prnewswire.com/rss/multicultural/veterans.rss",
"http://www.prnewswire.com/rss/multicultural/women-related-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/all-policy-public-interest-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/animal-welfare-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/corporate-social-responsibility-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/domestic-policy-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/economic-news-trends-analysis-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/education-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/environmental-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/european-government-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/federal-state-legislation-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/federal-executive-branch-agency-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/foreign-policy-international-affairs-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/homeland-security-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/labor-union--news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/legal-issues-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/not-for-profit-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/political-campaigns-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/public-safety-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/trade-policy-news.rss",
"http://www.prnewswire.com/rss/policy-public-interest/us-state-policy-news.rss",
"http://www.prnewswire.com/rss/sports/all-sports-news.rss",
"http://www.prnewswire.com/rss/sports/sporting-news.rss",
"http://www.prnewswire.com/rss/sports/sporting-events-news.rss",
"http://www.prnewswire.com/rss/sports/sports-equipment-accessories-news.rss",
"http://www.prnewswire.com/rss/telecommunications/all-telecommunications-news.rss",
"http://www.prnewswire.com/rss/telecommunications/carriers-services-news.rss",
"http://www.prnewswire.com/rss/telecommunications/mobile-entertainment-news.rss",
"http://www.prnewswire.com/rss/telecommunications/networks-news.rss",
"http://www.prnewswire.com/rss/telecommunications/peripherals-news.rss",
"http://www.prnewswire.com/rss/telecommunications/telecommunications-equipment-news.rss",
"http://www.prnewswire.com/rss/telecommunications/telecommunications-news.rss",
"http://www.prnewswire.com/rss/telecommunications/voip-news.rss",
"http://www.prnewswire.com/rss/telecommunications/wireless-communications-news.rss",
"http://www.prnewswire.com/rss/travel/all-travel-news.rss",
"http://www.prnewswire.com/rss/travel/amusement-parks-tourist-attractions-news.rss",
"http://www.prnewswire.com/rss/travel/gambling-casinos-news.rss",
"http://www.prnewswire.com/rss/travel/hotels-resorts-news.rss",
"http://www.prnewswire.com/rss/travel/leisure-tourism-hotels-news.rss",
"http://www.prnewswire.com/rss/travel/passenger-aviation-news.rss",
"http://www.prnewswire.com/rss/travel/travel-news.rss"

         };


        private static string[] _errorContactDataList = new string[] {

"'http://www.prnewswire.com/news-releases/severe-weather-possible-across-pa-this-weekend-300279426.html'",
"'http://www.prnewswire.com/news-releases/study-shows-why-immune-boosting-therapy-doesnt-work-for-everyone-with-widespread-melanoma-300278004.html'",
"'http://www.prnewswire.com/news-releases/tso3-responds-to-us-regulators-on-extended-claims-and-provides-operations-update-581566671.html'",
"'http://www.prnewswire.com/news-releases/study-shows-why-immune-boosting-therapy-doesnt-work-for-everyone-with-widespread-melanoma-300278004.html'"

           };
        public PRnewswireNewsRssParser(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            RssSource = @"http://www.prnewswire.com/rss/all-news-releases-from-PR-newswire-news.rss";
            RssFeedName = @"PR Newswire";
            RankedLanguage = rankedLanguageIdentifier;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;
            var allcontainer = doc.DocumentNode.SelectNodes("//div[@class='news-release-detail']");
            //if (allcontainer == null)
            //    allcontainer = doc.DocumentNode.SelectNodes("//article");

            if (allcontainer != null)
            {
                var container = allcontainer.FirstOrDefault();
                if (container != null)
                {
                    var header = container.SelectNodes("//h1").FirstOrDefault();
                    if (header != null)
                        strHtml = strHtml + header.OuterHtml;

                    IEnumerable<HtmlNode> nodes =
                        container.SelectNodes("//div[@class='col-sm-9']")
                            .LastOrDefault()
                            .Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }
            else
            {

            }

            if (allcontainer == null)
            {
                allcontainer = doc.DocumentNode.SelectNodes("//article[@class='news-release inline-gallery-template']");
                if (allcontainer == null)
                    allcontainer = doc.DocumentNode.SelectNodes("//article");

                if (allcontainer != null)
                {
                    var container = allcontainer.FirstOrDefault();
                    if (container != null)
                    {
                        IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                            n.NodeType == HtmlNodeType.Text &&
                            n.ParentNode.Name != "script" &&
                            n.ParentNode.Name != "style");
                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }
                }
            }

            if (allcontainer == null)
            {
                allcontainer = doc.DocumentNode.SelectNodes("//section[@class='release-body container ']");
                if (allcontainer != null)
                {
                    var container = allcontainer.FirstOrDefault();
                    if (container != null)
                    {
                        IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                            n.NodeType == HtmlNodeType.Text &&
                            n.ParentNode.Name != "script" &&
                            n.ParentNode.Name != "style");
                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }

                    var title = doc.DocumentNode.SelectNodes("//header[@class='container release-header']");
                    if (title != null)
                    {
                        IEnumerable<HtmlNode> nodes = title.Descendants()
                                    .Where(n =>
                                        n.NodeType == HtmlNodeType.Text &&
                                        n.ParentNode.Name != "script" &&
                                        n.ParentNode.Name != "style");


                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }
                }
            }

            if (allcontainer == null)
            {
                allcontainer = doc.DocumentNode.SelectNodes("//div[@id='lede']");
                if (allcontainer != null)
                {
                    var container = allcontainer.FirstOrDefault();
                    if (container != null)
                    {
                        IEnumerable<HtmlNode> nodes = container.Descendants().Where(n =>
                            n.NodeType == HtmlNodeType.Text &&
                            n.ParentNode.Name != "script" &&
                            n.ParentNode.Name != "style");
                        strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                    }
                }
            }


            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            rssFeed.StoryCount = storyCount.StoryCount;
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();

            foreach (var rssFeed in list)
            {
                try
                {
                    if (repo.AnyById(rssFeed.Link)) continue;

                    var parcerContent = new ParcerPRNewswireHtmlParser(rssFeed.Link);

                    if (!parcerContent.hasContent) continue;

                    var contentData = parcerContent.ParserContact(rssFeed);
                    if (contentData == null) continue;

                    var contacts = contentData.Item1;
                    var content = contentData.Item2;

                    if (contacts == null || !contacts.Any()) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                    foreach (var c in contacts)
                    {
                        c.NewsReleaseDate = rssFeed.DateTime.ToString();
                        contactRep.Add(c);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }
            }
            return true;
        }


        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
          
                var repo = new RssFeedRepository();
                var contactRep = new ContactInfoRepository();

            if(list == null || !list.Any())
            {
                return false;
            }
            foreach (var rssFeed in list)
            {
                if(rssFeed.Link.Contains("subscriber.newsquantified.com"))
                {
                    rssFeed.Dateline = " ";
                    repo.Add(rssFeed);
                    continue;
                }
                try
                {
                    var parcerContent = new ParcerPRNewswireHtmlParser(rssFeed.Link);
                    if (parcerContent.hasContent)
                    {
                        //parcerContent.FixHedlineContact(rssFeed);
                        //repo.Add(rssFeed);
                        //continue;


                        var contentData = parcerContent.ParserContact(rssFeed);
                        if (contentData == null) continue;

                        var contacts = contentData.Item1;
                        var content = contentData.Item2;

                        if (contacts != null && contacts.Any())
                        {
                            rssFeed.Language = "eng";
                            rssFeed.English = true;
                            CalculateStoryCount(content, rssFeed);
                            repo.Add(rssFeed);
                            contactRep.Delete(rssFeed.Link);
                            foreach (Contact c in contacts)
                            {
                                c.NewsReleaseDate = rssFeed.DateTime.ToString();
                                contactRep.Add(c);
                            }
                        }
                    }


                }
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);

                }

            }
            return true;
        }
        public override bool Start()
        {
            try
            {
                Logger.WriteInformation(this.GetType().Name + " was started");
                var repo = new RssFeedRepository();
              // var errContact = repo.FindNotContact(RssFeedName);
                //var errContact = repo.FindErrContact(_errorContactDataList);
                //if (!TryReSaveRssList(errContact))
                //{

                //}
                //Logger.WriteInformation(this.GetType().Name + " was fixed");
               // return true;

                //Logger.WriteInformation(this.GetType().Name + " was fixed");

                foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                {
                }


                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }
            Logger.WriteInformation(this.GetType().Name + " was ended.");
            return true;
        }
    }
}
