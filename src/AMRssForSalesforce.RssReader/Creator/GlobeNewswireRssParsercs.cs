﻿namespace AMRssForSalesforce.RssReader.Creator
{
    using HtmlParser.Creator;
    using Infrastructure.Logger;
    using Infrastructure.Repository;
    using Model;
    using Abstract;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ClippingUtilityLib;
    using HtmlAgilityPack;
    using NTextCat;
    using System.Net;
    using System.IO;
    using System.Threading;
    using AMRssForSalesforce.Infrastructure.Helpers;
    using System.Net.Http;

    public sealed class GlobeNewswireRssParsercs: RssParser
    {
        private static string htmlNews = "http://www.globenewswire.com/NewsRoom?page={0}#pagerPos";
        private static int maxPage = 100;
        private static int notfindePage = 10;
        //to do add
        //http://globenewswire.com/Rss/List
        private static string[] _rssSources = new string[] {
            //to do ask Sergey
            //http://globenewswire.com/Rss/List
            //"http://globenewswire.com/OrganizationRss/jPdoA8VH3NmkM8qUT57s-g%3d%3d",
            
            "https://globenewswire.com/Rss/orgclass/1",
            "https://globenewswire.com/Rss/subjectcode/12/Dividend%20Reports%20And%20Estimates",
            "https://globenewswire.com/Rss/subjectcode/13/Earnings%20Releases%20And%20Operating%20Results",
            "https://globenewswire.com/Rss/subjectcode/27/Mergers%20And%20Acquisitions",

            //by industry
            "https://globenewswire.com/Rss/subjectcode/12/Dividend%20Reports%20And%20Estimates",
            "https://globenewswire.com/Rss/subjectcode/13/Earnings%20Releases%20And%20Operating%20Results",
            "https://globenewswire.com/Rss/subjectcode/27/Mergers%20And%20Acquisitions",
            "http://globenewswire.com/OrganizationRss/jPdoA8VH3NmkM8qUT57s-g%3d%3d",
            "http://globenewswire.com/OrganizationRss/WvgZRNFONHu1LxqCUTIxIg%3d%3d",
            "http://www.globenewswire.com/OrganizationRss/xQkMvfh0hVpaHaJo4nNIPw%3d%3d",
            "https://globenewswire.com/OrganizationRss/zAOikeIZ5ZszFvkGoW8BxQ%3d%3d",
            "https://globenewswire.com/OrganizationRss/hTpafSiv1IrmrXkNtXAAow%3d%3d",
            "http://www.globenewswire.com/OrganizationRss/lYNiKUOG4ORLMufYE2yWLQ%3d%3d",
            "http://www.globenewswire.com/OrganizationRss/s6wSxHxWNo_ushX6aE8nwA%3d%3d",

            "http://globenewswire.com/OrganizationRss/jPdoA8VH3NmkM8qUT57s-g%3d%3d",
            "http://www.globenewswire.com/OrganizationRss/lYNiKUOG4ORLMufYE2yWLQ%3d%3d",
            "https://globenewswire.com/OrganizationRss/l3tJO3889FVYAnRq2ISkZg%3d%3d",
            "http://globenewswire.com/OrganizationRss/wpoH5OBqNnpxeYWfZwnDDQ%3d%3d",
            "http://globenewswire.com/OrganizationRss/UtjqRuqXqahsaWRsvDWcvQ%3d%3d",


            "http://globenewswire.com/Rss/industry/1000/Basic%20Materials",
"http://globenewswire.com/Rss/industry/1753/Aluminum",
"http://globenewswire.com/Rss/industry/1771/Coal",
"http://globenewswire.com/Rss/industry/1353/Commodity%20Chemicals",
"http://globenewswire.com/Rss/industry/1773/Diamonds%2026%20Gemstones",
"http://globenewswire.com/Rss/industry/1733/Forestry",
"http://globenewswire.com/Rss/industry/1775/General%20Mining",
"http://globenewswire.com/Rss/industry/1777/Gold%20Mining",
"http://globenewswire.com/Rss/industry/1757/Iron%2026%20Steel",
"http://globenewswire.com/Rss/industry/1755/Nonferrous%20Metals",
"http://globenewswire.com/Rss/industry/1737/Paper",
"http://globenewswire.com/Rss/industry/1779/Platinum%2026%20Precious%20Metals",
"http://globenewswire.com/Rss/industry/1357/Specialty%20Chemicals",
"http://globenewswire.com/Rss/industry/3000/Consumer%20Goods",
"http://globenewswire.com/Rss/industry/3355/Auto%20Parts",
"http://globenewswire.com/Rss/industry/3353/Automobiles",
"http://globenewswire.com/Rss/industry/3533/Brewers",
"http://globenewswire.com/Rss/industry/3763/Clothing%2026%20Accessories",
"http://globenewswire.com/Rss/industry/3743/Consumer%20Electronics",
"http://globenewswire.com/Rss/industry/3535/Distillers%2026%20Vintners",
"http://globenewswire.com/Rss/industry/3722/Durable%20Household%20Products",
"http://globenewswire.com/Rss/industry/3573/Farming%2026%20Fishing",
"http://globenewswire.com/Rss/industry/3577/Food%20Products",
"http://globenewswire.com/Rss/industry/3765/Footwear",
"http://globenewswire.com/Rss/industry/3726/Furnishings",
"http://globenewswire.com/Rss/industry/3728/Home%20Construction",
"http://globenewswire.com/Rss/industry/3724/Nondurable%20Household%20Products",
"http://globenewswire.com/Rss/industry/3767/Personal%20Products",
"http://globenewswire.com/Rss/industry/3745/Recreational%20Products",
"http://globenewswire.com/Rss/industry/3537/Soft%20Drinks",
"http://globenewswire.com/Rss/industry/3357/Tires",
"http://globenewswire.com/Rss/industry/3785/Tobacco",
"http://globenewswire.com/Rss/industry/3747/Toys",
"http://globenewswire.com/Rss/industry/5000/Consumer%20Services",
"http://globenewswire.com/Rss/industry/5751/Airlines",
"http://globenewswire.com/Rss/industry/5371/Apparel%20Retailers",
"http://globenewswire.com/Rss/industry/5553/Broadcasting%2026%20Entertainment",
"http://globenewswire.com/Rss/industry/5373/Broadline%20Retailers",
"http://globenewswire.com/Rss/industry/5333/Drug%20Retailers",
"http://globenewswire.com/Rss/industry/5337/Food%20Retailers%2026%20Wholesalers",
"http://globenewswire.com/Rss/industry/5752/Gambling",
"http://globenewswire.com/Rss/industry/5375/Home%20Improvement%20Retailers",
"http://globenewswire.com/Rss/industry/5753/Hotels",
"http://globenewswire.com/Rss/industry/5555/Media%20Agencies",
"http://globenewswire.com/Rss/industry/5557/Publishing",
"http://globenewswire.com/Rss/industry/5755/Recreational%20Services",
"http://globenewswire.com/Rss/industry/5757/Restaurants%2026%20Bars",
"http://globenewswire.com/Rss/industry/5377/Specialized%20Consumer%20Services",
"http://globenewswire.com/Rss/industry/5379/Specialty%20Retailers",
"http://globenewswire.com/Rss/industry/5759/Travel%2026%20Tourism",
"http://globenewswire.com/Rss/industry/8000/Financials",
"http://globenewswire.com/Rss/industry/8771/Asset%20Managers",
"http://globenewswire.com/Rss/industry/8355/Banks",
"http://globenewswire.com/Rss/industry/8773/Consumer%20Finance",
"http://globenewswire.com/Rss/industry/8674/Diversified%20REITs",
"http://globenewswire.com/Rss/industry/8985/Equity%20Investment%20Instruments",
"http://globenewswire.com/Rss/industry/8532/Full%20Line%20Insurance",
"http://globenewswire.com/Rss/industry/8677/Hotel%2026%20Lodging%20REITs",
"http://globenewswire.com/Rss/industry/8671/Industrial%2026%20Office%20REITs",
"http://globenewswire.com/Rss/industry/8534/Insurance%20Brokers",
"http://globenewswire.com/Rss/industry/8777/Investment%20Services",
"http://globenewswire.com/Rss/industry/8575/Life%20Insurance",
"http://globenewswire.com/Rss/industry/8779/Mortgage%20Finance",
"http://globenewswire.com/Rss/industry/8676/Mortgage%20REITs",
"http://globenewswire.com/Rss/industry/8995/Nonequity%20Investment%20Instruments",
"http://globenewswire.com/Rss/industry/8536/Property%2026%20Casualty%20Insurance",
"http://globenewswire.com/Rss/industry/8633/Real%20Estate%20Holding%2026%20Development",
"http://globenewswire.com/Rss/industry/8637/Real%20Estate%20Services",
"http://globenewswire.com/Rss/industry/8538/Reinsurance",
"http://globenewswire.com/Rss/industry/8673/Residential%20REITs",
"http://globenewswire.com/Rss/industry/8672/Retail%20REITs",
"http://globenewswire.com/Rss/industry/8775/Specialty%20Finance",
"http://globenewswire.com/Rss/industry/8675/Specialty%20REITs",
"http://globenewswire.com/Rss/industry/4000/Health%20Care",
"http://globenewswire.com/Rss/industry/4573/Biotechnology",
"http://globenewswire.com/Rss/industry/4533/Health%20Care%20Providers",
"http://globenewswire.com/Rss/industry/4535/Medical%20Equipment",
"http://globenewswire.com/Rss/industry/4537/Medical%20Supplies",
"http://globenewswire.com/Rss/industry/4577/Pharmaceuticals",
"http://globenewswire.com/Rss/industry/2000/Industrials",
"http://globenewswire.com/Rss/industry/2713/Aerospace",
"http://globenewswire.com/Rss/industry/2353/Building%20Materials%2026%20Fixtures",
"http://globenewswire.com/Rss/industry/2791/Business%20Support%20Services",
"http://globenewswire.com/Rss/industry/2793/Business%20Training%2026%20Employment%20Agencies",
"http://globenewswire.com/Rss/industry/2753/Commercial%20Vehicles%2026%20Trucks",
"http://globenewswire.com/Rss/industry/2723/Containers%2026%20Packaging",
"http://globenewswire.com/Rss/industry/2717/Defense",
"http://globenewswire.com/Rss/industry/2771/Delivery%20Services",
"http://globenewswire.com/Rss/industry/2727/Diversified%20Industrials",
"http://globenewswire.com/Rss/industry/2733/Electrical%20Components%2026%20Equipment",
"http://globenewswire.com/Rss/industry/2737/Electronic%20Equipment",
"http://globenewswire.com/Rss/industry/2795/Financial%20Administration",
"http://globenewswire.com/Rss/industry/2357/Heavy%20Construction",
"http://globenewswire.com/Rss/industry/2757/Industrial%20Machinery",
"http://globenewswire.com/Rss/industry/2797/Industrial%20Suppliers",
"http://globenewswire.com/Rss/industry/2773/Marine%20Transportation",
"http://globenewswire.com/Rss/industry/2775/Railroads",
"http://globenewswire.com/Rss/industry/2777/Transportation%20Services",
"http://globenewswire.com/Rss/industry/2779/Trucking",
"http://globenewswire.com/Rss/industry/2799/Waste%2026%20Disposal%20Services",
"http://globenewswire.com/Rss/industry/1/Oil%2026%20Gas",
"http://globenewswire.com/Rss/industry/587/Alternative%20Fuels",
"http://globenewswire.com/Rss/industry/533/Exploration%2026%20Production",
"http://globenewswire.com/Rss/industry/537/Integrated%20Oil%2026%20Gas",
"http://globenewswire.com/Rss/industry/573/Oil%20Equipment%2026%20Services",
"http://globenewswire.com/Rss/industry/577/Pipelines",
"http://globenewswire.com/Rss/industry/583/Renewable%20Energy%20Equipment",
"http://globenewswire.com/Rss/industry/9000/Technology",
"http://globenewswire.com/Rss/industry/9572/Computer%20Hardware",
"http://globenewswire.com/Rss/industry/9533/Computer%20Services",
"http://globenewswire.com/Rss/industry/9574/Electronic%20Office%20Equipment",
"http://globenewswire.com/Rss/industry/9535/Internet",
"http://globenewswire.com/Rss/industry/9576/Semiconductors",
"http://globenewswire.com/Rss/industry/9537/Software",
"http://globenewswire.com/Rss/industry/9578/Telecommunications%20Equipment",
"http://globenewswire.com/Rss/industry/6535/Fixed%20Line%20Telecommunications",
"http://globenewswire.com/Rss/industry/6000/Telecommunications",
"http://globenewswire.com/Rss/industry/6575/Mobile%20Telecommunications",
"http://globenewswire.com/Rss/industry/7000/Utilities",
"http://globenewswire.com/Rss/industry/7537/Alternative%20Electricity",
"http://globenewswire.com/Rss/industry/7535/Conventional%20Electricity",
"http://globenewswire.com/Rss/industry/7573/Gas%20Distribution",
"http://globenewswire.com/Rss/industry/7575/Multiutilities",
"http://globenewswire.com/Rss/industry/7577/Water"


         };

        private static string[] _errorContactDataList = new string[] {
             "'http://www.globenewswire.com/news-release/2016/03/31/824453/0/en/Global-Oncology-Ablation-Market-Outlook-2015-2020-Market-to-Reach-820-9-Million-by-2020.html'"
             ,"'http://www.globenewswire.com/news-release/2016/03/31/824456/0/en/Industrial-Robotics-Market-Poised-To-Reach-USD-40-Billion-By-2020-Radiant-Insights-Inc.html'"
             ,"'http://www.globenewswire.com/news-release/2016/03/30/823931/0/en/Zealand-increases-its-share-capital-as-a-consequence-of-exercise-of-employee-warrants.html'"
             ,"'http://www.globenewswire.com/news-release/2016/03/30/824159/0/en/Resolutions-of-the-Ordinary-General-Meeting-of-Shareholders.html'"
             ,"'http://www.globenewswire.com/news-release/2016/03/31/824483/0/en/Web-Real-Time-Communication-Market-to-2020-IT-Telecom-Media-Entertainment-BFSI-Retail-and-Consumer-Goods-Public-Sector-Education-Healthcare-Transportation-Logistics-Breakdown.html'"
         };


        public GlobeNewswireRssParsercs(RankedLanguageIdentifier rankedLanguageIdentifier)
        {
            RssSource = @"http://inpublic.globenewswire.com/feed?params=category:PR,REG;language:en&feedType=rss20";
            RssFeedName = @"Globe Newswire";
            RankedLanguage = rankedLanguageIdentifier;
        }

        private void CalculateStoryCount(HtmlDocument doc, RssFeed rssFeed)
        {
            var strHtml = String.Empty;

            var article = doc.DocumentNode.SelectNodes("//span[@itemprop='articleBody']");
            if (article != null)
            {
                var container = article.FirstOrDefault();
                if (container != null)
                {
                    IEnumerable<HtmlNode> nodes = container.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                    strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
                }
            }

            var title = doc.DocumentNode.SelectNodes("//h1[@class='article-headline']");
            if (title != null)
            {
                IEnumerable<HtmlNode> nodes = title.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }

            var title2 = doc.DocumentNode.SelectNodes("//h2[@class='subheadline']");
            if (title2 != null)
            {
                IEnumerable<HtmlNode> nodes = title2.Descendants()
                            .Where(n =>
                                n.NodeType == HtmlNodeType.Text &&
                                n.ParentNode.Name != "script" &&
                                n.ParentNode.Name != "style");


                strHtml = nodes.Aggregate(strHtml, (current, node) => current + node.InnerText);
            }
            if (String.IsNullOrEmpty(strHtml)) return;
            var storyCount = new BWClippUtilParseLib(RankedLanguage).GetReturnValues(strHtml);
            rssFeed.StoryCount = storyCount.StoryCount;
            if (storyCount.Language.ToUpper() == "SIMPLE" || storyCount.IsEnglish == true)
            {
                rssFeed.Language = "eng";
                rssFeed.English = true;
            }
            else
            {
                rssFeed.Language = storyCount.Language == "en" ? "eng" : storyCount.Language;
                rssFeed.English = storyCount.Language == "en" || storyCount.Language == "eng";
            }
        }

        public override bool TrySaveRssList(IEnumerable<RssFeed> list)
        {
            
                var repo = new RssFeedRepository();
                var contactRep = new ContactInfoRepository();
                
                foreach(var rssFeed in list)
                { 
                    try
                    {
                        if (rssFeed.HeadLine.Contains("Genmab Provides Update on Ofatumumab Development in Autoimmune Indications"))
                        {

                        }
                        if (repo.AnyById(rssFeed.Link)) 
                            continue;
                        var parcerContent = new GlobeNewswireHtmlParser(rssFeed.Link);
                        if (!parcerContent.hasContent) continue;
                        var contentData =  parcerContent.ParserContact(rssFeed);
                        if (contentData == null) continue;

                        var contacts = contentData.Item1;
                        var content = contentData.Item2;

                        if (contacts == null || !contacts.Any()) continue;
                        rssFeed.Language = "eng";
                        rssFeed.English = true;
                        CalculateStoryCount(content, rssFeed);
                        repo.Add(rssFeed);

                        foreach (var c in contacts)
                        {
                            c.NewsReleaseDate = rssFeed.DateTime.ToString();
                            contactRep.Add(c);
                        }
                    }

                    catch (Exception ex)
                    {
                        Logger.WriteFatal(ex);
                       
                    }
                }
           
            return true;
        }

        public bool TryReSaveRssList(IEnumerable<RssFeed> list)
        {
              var repo = new RssFeedRepository();
                var contactRep = new ContactInfoRepository();

            if (list == null || !list.Any()) return false;

            foreach (var rssFeed in list)
            {
                try
                {
             
                    var parcerContent = new GlobeNewswireHtmlParser(rssFeed.Link);

                    if (!parcerContent.hasContent) continue;

                    //parcerContent.FixHedlineContact(rssFeed);
                    //repo.Add(rssFeed);
                    //continue;


                    var contentData = parcerContent.ParserContact(rssFeed);
                    if (contentData == null) continue;

                    var contacts = contentData.Item1;
                    var content = contentData.Item2;

                    if (contacts == null || !contacts.Any()) continue;
                    rssFeed.Language = "eng";
                    rssFeed.English = true;
                    CalculateStoryCount(content, rssFeed);
                    repo.Add(rssFeed);
                    contactRep.Delete(rssFeed.Link);
                    foreach (Contact c in contacts)
                    {
                        c.NewsReleaseDate = rssFeed.DateTime.ToString();
                        contactRep.Add(c);
                    }
                }   
                catch (Exception ex)
                {
                    Logger.WriteFatal(ex);
               
                }
            }
            return true;
        }

        public override bool Start()
        {
            var repo = new RssFeedRepository();
            var contactRep = new ContactInfoRepository();
            var NewCountAfter10 = 0;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            Logger.WriteInformation(this.GetType().Name + " was started");

           
           // var errContact = repo.FindNotContact(RssFeedName);
           //var errContact = repo.FindErrContact(_errorContactDataList);
           //if (!TryReSaveRssList(errContact))
           //{

           //}

           // return true;
           // Logger.WriteInformation(this.GetType().Name + " was fixed");

            try {

                //var repo = new RssFeedRepository();
                //var errContact = repo.FindNotContact(RssFeedName);
                ////  var errContact = repo.FindErrContact(_errorContactDataList);
                //if (!TryReSaveRssList(errContact))
                //{

                //}
                //end fix data

                //var rssList = Parse().ToList();
                //if (!TrySaveRssList(rssList))
                //{

                //}
                //Logger.WriteDebug("Glob end");
                int not_found = 0;
                HttpClient client = new HttpClient();
                
                for(int i = 1; i< maxPage; i++)
                {
                    var urlpage = string.Format(htmlNews, i);

                    List<RssFeed> rssFeeds = GetNwesFromHtml(client, urlpage);
                    int countNew = 0;

                    foreach(var rssFeed in rssFeeds)
                    {
                        try
                        {
                            if (repo.AnyById(rssFeed.Link))
                            {
                                continue;
                            }
                            
                            countNew++;

                            if (not_found > notfindePage)
                                  NewCountAfter10++;

                            var parcerContent = new GlobeNewswireHtmlParser(rssFeed.Link);

                            if (!parcerContent.hasContent) continue;

                            var contentData = parcerContent.ParserContact(rssFeed);
                            if (contentData == null) continue;

                            var contacts = contentData.Item1;
                            var content = contentData.Item2;

                            if (contacts == null || !contacts.Any()) continue;
                            rssFeed.Language = "eng";
                            rssFeed.English = true;
                            CalculateStoryCount(content, rssFeed);
                            repo.Add(rssFeed);
                            contactRep.Delete(rssFeed.Link);
                            foreach (Contact c in contacts)
                            {
                                c.NewsReleaseDate = rssFeed.DateTime.ToString();
                                contactRep.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteFatal(ex);

                        }
                    }

                    if (countNew == 0)
                    {
                        not_found++;
                        //if (not_found > notfindePage)
                        //    return true;
                            
                    }
                }

                Logger.WriteInformation(this.GetType().Name + " was ended");
                //foreach (var rssList in _rssSources.Select(url => Parse(url).ToList()).Where(rssList => !TrySaveRssList(rssList)))
                //{
                //}

                //var dosentHaveContacts = GetNotContacts();
                //if (!TryReSaveRssList(dosentHaveContacts))
                //{

                //}
            }
            catch (Exception ex)
            {
                Logger.WriteFatal(ex);
                return false;
            }

            Logger.WriteInformation(this.GetType().Name + " was ended.");
            return true;
        }

        private List<RssFeed> GetNwesFromHtml(HttpClient client, string urlpage)
        {
            List<RssFeed> rssFeeds = new List<RssFeed>();

            var res = client.GetAsync(urlpage).Result;
            if (res == null)
                return rssFeeds;

            var htmlContent = res.Content.ReadAsStringAsync().Result;
            if (htmlContent == null)
                return rssFeeds;

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var rssDivs = doc.DocumentNode.SelectNodes("//div[@data-autid='article-details']");
            if(rssDivs != null)
            {
                foreach(var div in rssDivs)
                {
                    var rssFeed = new RssFeed();

                    var aHeadline = div.SelectSingleNode(".//a[@data-autid='article-url']");
                    if (aHeadline == null || aHeadline.Attributes["href"] == null)
                    {
                        Logger.WriteError(string.Format("Didn't find headline in {0}", urlpage));
                        continue;
                    }

                    var url = aHeadline.Attributes["href"].Value;
                    if (!url.Contains("http://www.globenewswire.com"))
                        url = "http://www.globenewswire.com" + url;

                    var headline = aHeadline.InnerText;

                    rssFeed.HeadLine = headline;
                    rssFeed.Link = url;
                    rssFeed.Provider = this.RssFeedName;
                    rssFeed.Source = urlpage;
                    rssFeed.DateTime = DateTime.Now;
                    
                    rssFeeds.Add(rssFeed);
                }
            }
            else
            {
                Logger.WriteError(string.Format("Didn't find articles in {0}", urlpage));
            }

            return rssFeeds;
        }
        
    }
}
