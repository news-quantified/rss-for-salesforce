﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMRssForSalesforce.Infrastructure.Logger;
using AMRssForSalesforve.FileParser.Creator;
using System.Configuration;

namespace AMParserFiles
{
    class Program
    {
        static void Main(string[] args)
        {

            AMRssForSalesforce.Infrastructure.Constants.Dictionaries.Init();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            Logger.WriteInformation("AMParserFiles started");

            PRnewswireNewsFileParser pr = new PRnewswireNewsFileParser();
            TimerCallback tm = new TimerCallback(PRProcess);
            Timer timer = new Timer(tm, pr, 0, 30000000);

            MarketwiredFileParser mw = new MarketwiredFileParser();
            TimerCallback mm = new TimerCallback(MarketwiredProcess);
            Timer timerMW = new Timer(mm, mw, 0, 30000000);

            BusinessWireFileParser bw = new BusinessWireFileParser();
            TimerCallback bm = new TimerCallback(BusinessWireProcess);
            Timer timerBW = new Timer(bm, bw, 0, 30000000);
            
            Console.ReadKey();
        }

        public static void PRProcess(object obj)
        {
            PRnewswireNewsFileParser pr = obj as PRnewswireNewsFileParser;

            if (pr != null)
            {
                pr.Start(System.Configuration.ConfigurationSettings.AppSettings["fullPath"], System.Configuration.ConfigurationSettings.AppSettings["fullPathMove"]);
            }
        }

        public static void MarketwiredProcess(object obj)
        {
            MarketwiredFileParser pr = obj as MarketwiredFileParser;

            if (pr != null)
            {
                pr.Start(System.Configuration.ConfigurationSettings.AppSettings["fullPath"], System.Configuration.ConfigurationSettings.AppSettings["fullPathMove"]);
            }
        }

        public static void BusinessWireProcess(object obj)
        {
            BusinessWireFileParser pr = obj as BusinessWireFileParser;

            if (pr != null)
            {
                pr.Start(System.Configuration.ConfigurationSettings.AppSettings["fullPath"], System.Configuration.ConfigurationSettings.AppSettings["fullPathMove"]);
            }
        }
    }
}
